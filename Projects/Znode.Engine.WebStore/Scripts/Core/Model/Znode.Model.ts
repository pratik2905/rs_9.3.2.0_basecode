﻿declare module Znode.Core {
    interface BaseModel {
        uri: string;
        properties: { [key: string]: string };
    }
    interface MultiSelectDDlModel extends BaseModel {
        Value: any;
        IsAjax: boolean;
        IsMultiple: boolean;
        Controller: string;
        Action: string;
        ItemIds: string[];
        SuccessCallBack: any;
        flag: boolean;
    }

    interface ServerConfigurationDDlModel extends BaseModel {
        ServerId: number;
        PartialViewName: string;
        SuccessCallBack: any;
        flag: boolean;
    }

    type ProductDetailModel = {
        ProductId: number;
        MainProductId: number;
        SKU: string;
        MainProductSKU: string;
        Quantity: string;
        MaxQuantity: number;
        MinQuantity: number;
        DecimalValue: number;
        DecimalPoint: number;
        InventoryRoundOff: number;
        QuantityError: string;
    }

    type AddOnDetails = {
        SKU: string;
        ParentSKU: string;
        Quantity: string;
        ParentProductId: number;
    }

    type AddressModel = {
        AddressId: number;
        FirstName: string;
        Address1: string;
        Address2: string;
        LastName: string;
        CityName: string;
        StateName: string;
        PostalCode: string;
        CountryName: string;
        AddressType: string;
    }
}

declare module System.Collections.Generic {
    interface KeyValuePair<TKey, TValue> {
        key: TKey;
        value: TValue;
    }
}
declare module System {
    interface Guid {
    }
    interface Tuple<T1, T2> {
        item1: T1;
        item2: T2;
    }
}

module Constant {
    export const GET = "GET";
    export const json = "json";
    export const Function = "function";
    export const string = "string";
    export const object = "object";
    export const Post = "post"
    export const ZnodeCustomerShipping = "ZnodeCustomerShipping";
    export const AmericanExpressCardCode = "AMEX";
    export const gocoderGoogleAPI = $("#gocoderGoogleAPI").val();//To be fetched from config file
    export const gocoderGoogleAPIKey = $("#gocoderGoogleAPIKey").val();//To be fetched from config file 
}

module ErrorMsg {
    export const CallbackFunction = "Callback is not defined. No request made.";
    export const APIEndpoint = "API Endpoint not available: ";
    export const InvalidFunction = "invalid function name : ";

}
