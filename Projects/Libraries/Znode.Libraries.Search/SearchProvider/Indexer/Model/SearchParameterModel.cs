﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Libraries.Search
{
    public class SearchParameterModel
    {
        public int CatalogId { get; set; }
        public int SearchIndexMonitorId { get; set; }
        public int SearchIndexServerStatusId { get; set; }
        public long IndexStartTime { get; set; }
        public int VersionId { get; set; }
        public string revisionType { get; set; }
        public List<LocaleModel> ActiveLocales { get; set; }
    }
}
