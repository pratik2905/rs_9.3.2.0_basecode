﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Libraries.Search
{
    public class SearchCategoryService : ISearchCategoryService
    {
        #region Private properties.
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        #endregion

        #region Constructor.
        public SearchCategoryService()
        {
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
        }
        #endregion

        #region Public methods.
        //Gets product Category.
        public List<SearchCategory> GetProductCategory(int categoryIds, int localeId, int catalogId, List<SEODetailsModel> categorySeoDetails)
        {
            FilterCollection categoryFilter = new FilterCollection() { new FilterTuple(WebStoreEnum.ZnodeCategoryId.ToString(), FilterOperators.Equals,  categoryIds.ToString()),
                                                                               new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId.ToString()),
                                                                               new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, catalogId.ToString())};

            List<CategoryEntity> categoryList = _categoryMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(categoryFilter.ToFilterMongoCollection())).ToList();

            return categoryList?.Select(x => GetElasticCategory(categorySeoDetails, x)).ToList();
        }


        //Get published category list
        public virtual List<CategoryEntity> GetPublishCategoryList(int catalogId, int versionId)
        {
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query.And(Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                Query<CategoryEntity>.EQ(pr => pr.VersionId, versionId),
                                Query<CategoryEntity>.EQ(pr => pr.IsActive, true)));


            //Get publish categories from mongo
            List<CategoryEntity> categories = _categoryMongoRepository.GetEntityList(Query.And(query));

            //Filter list by expiration date and activation date.
            categories = GetFilterDateReult(categories);

            return categories;
        }

        #endregion

        #region Private methods.
        //Gets parent category hierarchy.
        private List<SearchCategory> GetParentCategories(int[] parentCategoryIds, List<SEODetailsModel> categorySeoDetails)
        {
            List<SearchCategory> parentCategories = new List<SearchCategory>();
            foreach (int categoryId in parentCategoryIds)
            {
                FilterCollection categoryFilter = new FilterCollection() { new FilterTuple(WebStoreEnum.ZnodeCategoryId.ToString(), FilterOperators.In, string.Join(",", categoryId)) };

                CategoryEntity parentCategory = _categoryMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(categoryFilter.ToFilterMongoCollection()));
                if (HelperUtility.IsNotNull(parentCategory))
                {
                    SearchCategory elasticParentCategory = GetElasticCategory(categorySeoDetails, parentCategory);

                    parentCategories.Add(elasticParentCategory);
                }
            }
            return parentCategories;
        }

        //Maps category entity to Elatic category.
        private SearchCategory GetElasticCategory(List<SEODetailsModel> categorySeoDetails, CategoryEntity parentCategory)
        {
            SearchCategory category = new SearchCategory();

            category.categoryname = parentCategory.Name;
            category.categoryid = parentCategory.ZnodeCategoryId;
            category.seourl = categorySeoDetails.Where(seoDetail => seoDetail.SEOId == parentCategory.ZnodeCategoryId && seoDetail.LocaleId == parentCategory.LocaleId)?.FirstOrDefault()?.SEOUrl;
            category.profileids = parentCategory.ProfileIds;
            category.parentcategories = (parentCategory.ZnodeParentCategoryIds?.Count() > 0) ? GetParentCategories(parentCategory.ZnodeParentCategoryIds, categorySeoDetails) : null;
            category.isactive = parentCategory.IsActive;
            category.ActivationDate = parentCategory.ActivationDate;
            category.ExpirationDate = parentCategory.ExpirationDate;
            return category;
        }

        //Filter list by expiration date and activation date.
        private List<CategoryEntity> GetFilterDateReult(List<CategoryEntity> list) =>
         list.Where(x => (x.ActivationDate == null || x.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (x.ExpirationDate == null || x.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate())).ToList();
        #endregion
    }
}
