﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Libraries.Search
{
    public class SearchProductService : ISearchProductService
    {
        #region Private properties.
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IMongoRepository<VersionEntity> _versionEntity;

        #endregion

        #region Constructor.
        public SearchProductService()
        {
            _productMongoRepository = new MongoRepository<ProductEntity>();
            _versionEntity = new MongoRepository<VersionEntity>();
        }
        #endregion

        #region Public Methods.
        public virtual List<SearchProduct> GetAllProducts(int catalogId, int versionId, IEnumerable<int> publishCategoryIds, int start, int pageLength, long indexStartTime, out decimal totalPages)
        {
            int totalCount = 0;

            FilterCollection filters = new FilterCollection();

            if (catalogId > 0)
                filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, catalogId.ToString());

            filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, versionId.ToString());
            filters.Add("ZnodeCategoryIds", FilterOperators.NotEquals, "0");

            IMongoQuery mongoWhereClause = MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection());

            //Get all products.
            List<ProductEntity> mongoProducts = _productMongoRepository.GetPagedList(mongoWhereClause, null, start + 1, pageLength, out totalCount);

            ProductAttributeMapper.MergeSimpleProductAttributes(mongoProducts);

            if (totalCount < pageLength)
                totalPages = 1;
            else
                totalPages = Math.Ceiling((decimal)totalCount / pageLength);

            List<SearchProduct> elasticProductList = RemoveAtrributes(mongoProducts);

            if (elasticProductList.Count > 0)
            {
                List<PublishProductModel> productsWithReviewCount = GetProductReviewCount(0, catalogId, 0);
                SetElasticProduct(publishCategoryIds, indexStartTime, elasticProductList, productsWithReviewCount);
            }

            return elasticProductList;
        }

        public virtual void SetElasticProduct(IEnumerable<int> publishCategoyIds, long indexStartTime, List<SearchProduct> elasticProductList, List<PublishProductModel> productsWithReviewCount)
        {

            elasticProductList.ForEach(product =>
            {
                PublishProductModel productReviews = productsWithReviewCount?.FirstOrDefault(x => x.PublishProductId == product.znodeproductid);
                if (HelperUtility.IsNotNull(productReviews))
                {
                    product.rating = productReviews.Rating.GetValueOrDefault();
                    product.totalreviewcount = productReviews.TotalReviews.GetValueOrDefault();
                }


                //Sort by price to be work on sales price or retail price if sales price is null.
                decimal productPrice;
                if (!string.IsNullOrEmpty(product.salesprice))
                    decimal.TryParse(product.salesprice, out productPrice);
                else
                    decimal.TryParse(product.retailprice, out productPrice);

                product.productprice = productPrice;

                product.timestamp = indexStartTime;
            });

            if (publishCategoyIds?.Count() > 0)
                elasticProductList.RemoveAll(product => !publishCategoyIds.Contains(product.categoryid));
        }

        private List<SearchProduct> RemoveAtrributes(List<ProductEntity> mongoProducts)
        {
            Dictionary<string, string> removableProductAttribute = JsonConvert.DeserializeObject<Dictionary<string, string>>(ZnodeApiSettings.RemovableProductAttributes);
            mongoProducts.RemoveAll(mongoProduct => mongoProduct.Attributes.Any(attr => removableProductAttribute.ContainsKey(attr.AttributeCode)
                                                                                  && (attr.AttributeValues == removableProductAttribute.Where(x => x.Key == attr.AttributeCode).Select(x => x.Value).FirstOrDefault())));
            //Attribute code to be exclude from removing.
            List<string> attributeCode = new List<string>();
            attributeCode.Add(ZnodeConstant.CallForPricing);
            attributeCode.Add(ZnodeConstant.OutOfStockOptions);
            attributeCode.Add(ZnodeConstant.ProductType);

            List<SearchProduct> productList = mongoProducts.ToModel<SearchProduct>().ToList();

            productList.ForEach(x => x.searchableattributes.AddRange(x.attributes.Where(y => attributeCode.Contains(y.attributecode) || y.isuseinsearch || y.isfacets).ToList()));

            return productList;
        }

        public List<SearchProduct> GetElasticProducts(List<ProductEntity> mongoProducts, long indexStartTime)
        {
            List<SearchProduct> elasticProductList = null;
            if (mongoProducts?.Count > 0)
            {
                elasticProductList = RemoveAtrributes(mongoProducts);

                if (elasticProductList.Any())
                    SetElasticProduct(null, indexStartTime, elasticProductList, null);

                return elasticProductList;
            }
            else
                return elasticProductList;
        }

        public SearchProduct GetProductByProductId(int productId)
        {
            var mongoProduct = _productMongoRepository.GetById(productId.ToString());
            if (HelperUtility.IsNotNull(mongoProduct))
                return MongoDBToElasticMapper.ElasticProduct(mongoProduct);
            return null;
        }

        public int? GetCatalogVersionId(int catalogId)
           => _versionEntity.GetEntity(Query<VersionEntity>.EQ(d => d.ZnodeCatalogId, catalogId))?.VersionId;

        public int GetVersionId(int publishCatalogId, string revisionType, int localeId = 0)
        {
            List<VersionEntity> versionEntity = new MongoRepository<VersionEntity>().GetEntityList(Query.And(
            Query<VersionEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
            Query<VersionEntity>.EQ(x => x.RevisionType, revisionType),
            Query<VersionEntity>.EQ(x => x.IsPublishSuccess, true),
            Query<VersionEntity>.EQ(x => x.LocaleId, localeId))).OrderByDescending(c => c.VersionId)?.ToList();
            return versionEntity.Count > 0 ? versionEntity.FirstOrDefault().VersionId : 0;
        }

        public int GetLatestVersionId(int publishCatalogId, string revisionType, int localeId = 0)
        {
            VersionEntity versionEntity = new MongoRepository<VersionEntity>().GetEntityList(Query.And(
            Query<VersionEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
            Query<VersionEntity>.EQ(x => x.RevisionType, revisionType),
            Query<VersionEntity>.EQ(x => x.LocaleId, localeId))).OrderByDescending(x => x.VersionId).FirstOrDefault();
            return versionEntity != null ? versionEntity.VersionId : 0;
        }
        #endregion

        #region Private methods.
        private List<PublishProductModel> GetProductReviewCount(int portalId, int catalogId, int localeId)
        {
            IZnodeViewRepository<PublishProductModel> objStoredProc = new ZnodeViewRepository<PublishProductModel>();
            objStoredProc.SetParameter("@SKU", "", ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@PublishCatalogId", catalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PublishProductId", "", ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("Znode_GetProductDataForWebStore @SKU,@PublishCatalogId,@PublishProductId,@PortalId,@LocaleId").ToList();
        }

        //Get synonyms data for catalog.
        public List<ZnodeSearchSynonym> GetSynonymsData(int catalogId)
        {
            IZnodeRepository<ZnodeSearchSynonym> _searchSynonymRepository = new ZnodeRepository<ZnodeSearchSynonym>();
            string whereClause = $"PublishCatalogId={catalogId}";
            List<ZnodeSearchSynonym> searchSynonymList = _searchSynonymRepository.GetEntityList(whereClause)?.ToList();
            return searchSynonymList;
        }

        public List<ZnodeSearchKeywordsRedirect> GetKeywordsData(int catalogId)
        {
            IZnodeRepository<ZnodeSearchKeywordsRedirect> _searchKeywordsRepository = new ZnodeRepository<ZnodeSearchKeywordsRedirect>();
            string whereClause = $"PublishCatalogId={catalogId}";
            List<ZnodeSearchKeywordsRedirect> searchKeywordsList = _searchKeywordsRepository.GetEntityList(whereClause)?.ToList();
            return searchKeywordsList;
        }

        #endregion
    }
}
