﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Libraries.Search
{
    public static class ProductAttributeMapper
    {
        #region public methods

        //find associate simple products of complex product and merge its facets attribute with complex product attribute.
        public static void MergeSimpleProductAttributes(List<ProductEntity> productEntities)
        {
            //TODO the bellow code taking a long time to execute with a large volume of data required code optimization 
            return;
            List<ProductEntity> _productComplexEntities = productEntities?.Where(p => p.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Code != ZnodeConstant.SimpleProduct)?.ToList();
            _productComplexEntities?.ForEach(product =>
            {
                string _productType = product.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Code;

                if (_productType == ZnodeConstant.ConfigurableProduct)
                {
                    List<ConfigurableProductEntity> _variantProducts = GetConfigurableProductEntity(product.ZnodeProductId, product.VersionId);
                    _variantProducts?.ForEach(variantProduct => MergeFacetAttribute(variantProduct.AssociatedZnodeProductId, product));
                }
                else if (_productType == ZnodeConstant.BundleProduct)
                {
                    List<BundleProductEntity> _variantProducts = GetBundleProductEntity(product.ZnodeProductId, product.VersionId);
                    _variantProducts?.ForEach(variantProduct => MergeFacetAttribute(variantProduct.AssociatedZnodeProductId, product));
                }
                else if (_productType == ZnodeConstant.GroupedProduct)
                {
                    List<GroupProductEntity> _variantProducts = GetGroupProductEntity(product.ZnodeProductId, product.VersionId);
                    _variantProducts?.ForEach(variantProduct => MergeFacetAttribute(variantProduct.AssociatedZnodeProductId, product));
                }
            });
        }
        #endregion
        #region private methods

        //Merge facet attribute of simple products  with complex product attribute.
        private static void MergeFacetAttribute(int znodeProductId, ProductEntity product)
        {
            ProductEntity _variantProductEntity = GetProductEntity(znodeProductId, product.VersionId);
            List<AttributeEntity> _variantFacetAttributes = _variantProductEntity?.Attributes?.Where(m => m.IsFacets)?.ToList();
            _variantFacetAttributes?.ForEach(variantFacetAttribute =>
            {
                if (Equals(true, product.Attributes?.Exists(x => x.AttributeName == variantFacetAttribute.AttributeName)))
                {
                    List<string> productAttributeSelectValues = product.Attributes?.Where(x => x.AttributeName == variantFacetAttribute.AttributeName)?.FirstOrDefault()?.SelectValues?.Select(x => x.Code)?.ToList();
                    variantFacetAttribute.SelectValues?.Where(t => Equals(false, productAttributeSelectValues?.Contains(t.Code)))?.ToList()?.ForEach(x => product.Attributes?.Find(m => m.AttributeName == variantFacetAttribute.AttributeName)?.SelectValues?.Add(x));
                }
                else
                    product.Attributes?.Add(variantFacetAttribute);
            });
        }

        //Get promotional price of product if any promotion associated to it.
        private static List<ConfigurableProductEntity> GetConfigurableProductEntity(int productId, int? catalogVersionId)
        {
            IMongoRepository<ConfigurableProductEntity> _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ConfigurableProductEntity>.EQ(d => d.ZnodeProductId, productId));
            if (catalogVersionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

            //Get Configurable Product
            List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(Query.And(query), true);

            return configEntity;
        }

        //Get promotional price of product if any promotion associated to it.
        private static List<BundleProductEntity> GetBundleProductEntity(int productId, int? catalogVersionId)
        {
            IMongoRepository<BundleProductEntity> _bundleProductEntityproductRepository = new MongoRepository<BundleProductEntity>();

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ConfigurableProductEntity>.EQ(d => d.ZnodeProductId, productId));
            if (catalogVersionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

            //Get Configurable Product
            List<BundleProductEntity> bundleEntity = _bundleProductEntityproductRepository.GetEntityList(Query.And(query), true);

            return bundleEntity;
        }

        //Get promotional price of product if any promotion associated to it.
        private static List<GroupProductEntity> GetGroupProductEntity(int productId, int? catalogVersionId)
        {
            IMongoRepository<GroupProductEntity> _groupProductEntityproductRepository = new MongoRepository<GroupProductEntity>();

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ConfigurableProductEntity>.EQ(d => d.ZnodeProductId, productId));
            if (catalogVersionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

            //Get Configurable Product
            List<GroupProductEntity> groupEntity = _groupProductEntityproductRepository.GetEntityList(Query.And(query), true);

            return groupEntity;
        }

        //Get promotional price of product if any promotion associated to it.
        private static ProductEntity GetProductEntity(int productId, int? catalogVersionId)
        {
            IMongoRepository<ProductEntity> _productRepository = new MongoRepository<ProductEntity>();

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ProductEntity>.EQ(d => d.ZnodeProductId, productId));
            if (catalogVersionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

            //Get Configurable Product
            ProductEntity configEntity = _productRepository.GetEntityList(Query.And(query), true).FirstOrDefault();

            return configEntity;
        }
        #endregion
    }
}
