﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Configuration;
using System.Diagnostics;
using Znode.Libraries.Framework.Business;
using ZNode.Libraries.MongoDB.Data.Constants;

namespace Znode.Libraries.MongoDB.Data
{
    public static class MongoIndexHelper
    {
        #region Private Variables
        //Get log Component Name.
        private static readonly string logComponentName = "MongoDB";

        #endregion Private Variables

        #region Public Method
        //Create the mongo indexes on Collection if index not exists.
        public static void EnsureIndex(string collectionName, string indexField, bool isAscending, bool isCompoundIndex)
        {
            MongoCollection<BsonDocument> collection = GetMongoCollection(collectionName);
            string[] columnCollection = EnumerateCommaSeparatedString(indexField);
            bool isIndexExists = false;
            try
            {
                if (columnCollection == null || collection == null)
                {
                    ZnodeLogging.LogMessage($"Mongo Collection/Column Collection not found in Database :{collectionName} ", logComponentName, TraceLevel.Error, new { CollectionName = collectionName, ColumnCollection = columnCollection });
                    return;
                }

                if (isCompoundIndex)
                {
                    isIndexExists = collection.IndexExists(columnCollection);
                    //Create compound index. 
                    if (!isIndexExists)
                    {
                        collection.CreateIndex(columnCollection);
                    }
                    LogMessageBasedOnMongoIndexType(collectionName, isCompoundIndex, isIndexExists, indexField);
                }
                else
                {
                    //Create the simple/single index if compound index is false. 
                    foreach (string column in columnCollection)
                    {
                        IndexOptionsBuilder options = IndexOptions.SetUnique(false).SetSparse(false);
                        IndexKeysBuilder keys = (isAscending ? IndexKeys.Ascending(column) : IndexKeys.Descending(column));
                        isIndexExists = collection.IndexExists(keys);
                        if (!isIndexExists)
                        {
                            collection.CreateIndex(keys, options);
                        }
                        LogMessageBasedOnMongoIndexType(collectionName, isCompoundIndex, isIndexExists, column);
                    }
                }
            }
            catch (MongoCommandException ex)
            {
                ZnodeLogging.LogMessage($"Unable to create Mongo Indexes on {collectionName} collection." + ex.ErrorMessage, logComponentName, TraceLevel.Error, new { Collection = collectionName, ColumnCollection = columnCollection });
            }
        }
        #endregion Public Method

        #region Private Method
        //Get Mongo collection.
        private static MongoCollection<BsonDocument> GetMongoCollection(string collectionName)
        {
            bool isLogEntity = collectionName == MongoSettings.DBLogEntityKey.ToLower();
            string connectionString = GetConnectionString(isLogEntity);
            MongoDatabase mongoDb = MongoContextHelper.GetMongoContext(connectionString);
            try
            {
                if (mongoDb.GetCollection(collectionName).Exists())
                {
                    return mongoDb.GetCollection(collectionName);
                }
                else
                {
                    ZnodeLogging.LogMessage($"Collection not found :{collectionName} ", logComponentName, TraceLevel.Error, new { Collection = collectionName });
                    return null;
                }
            }
            catch (MongoCommandException ex)
            {
                ZnodeLogging.LogMessage($"Unable to get mongo collection from MongoDB Instance : {collectionName} " + ex.ErrorMessage, logComponentName, TraceLevel.Error, new { Collection = collectionName });
                return null;
            }
        }

        //Comma Separated String to string type Array.
        private static string[] EnumerateCommaSeparatedString(string commaSeparatedString)
        {
            return !string.IsNullOrEmpty(commaSeparatedString) ? commaSeparatedString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries) : null;
        }

        //Log Messages for index creation.
        private static void LogMessageBasedOnMongoIndexType(string collectionName, bool isCompoundIndex, bool indexExists, string indexField)
        {
            string indexType = isCompoundIndex ? "Compound" : "Simple";
            string logMessage = indexExists ? $"Compound Index is already exists for {indexField} Column on mongo collection {collectionName}." : $"{indexType} Index is created on {collectionName} for {indexField} Column.";
            ZnodeLogging.LogMessage(logMessage, logComponentName, TraceLevel.Verbose, new { CollectionName = collectionName, ColumnCollection = indexField });
        }

        //Get Connection String on the basis of entity DB type.
        private static string GetConnectionString(bool isLogEntity)
        {
            string connectionString = null;

            connectionString = isLogEntity ? ConfigurationManager.ConnectionStrings[MongoSettings.DBLogKey]?.ConnectionString : ConfigurationManager.ConnectionStrings[MongoSettings.DBKey]?.ConnectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = ConfigurationManager.ConnectionStrings[MongoSettings.DBKey]?.ConnectionString;
            }

            return connectionString;
        }
        #endregion Private Method
    }

}
