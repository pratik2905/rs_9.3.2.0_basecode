﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogWidgetTitleEntity : WidgetTitleEntity, IDisposable
    {
        ~_LogWidgetTitleEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
