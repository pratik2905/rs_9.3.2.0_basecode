﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class __ProgressNotifierEntity : MongoEntity, IDisposable
    {
        public Guid JobId { get; set; }
        public string JobName { get; set; }
        public int ProgressMark { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsFailed { get; set; }
        public string ExceptionMessage { get; set; }
        public int StartedBy { get; set; }
        public string StartedByFriendlyName { get; set; }

        [BsonIgnore]
        public new int VersionId { get; set; }

        ~__ProgressNotifierEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
