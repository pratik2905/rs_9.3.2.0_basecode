﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogGroupProductEntity:GroupProductEntity, IDisposable
    {
        ~_LogGroupProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
