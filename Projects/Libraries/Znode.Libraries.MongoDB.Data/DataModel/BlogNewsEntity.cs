﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class BlogNewsEntity : MongoEntity, IDisposable
    {
        public int BlogNewsId { get; set; }
        public int PortalId { get; set; }
        public int? MediaId { get; set; }
        public int? CMSContentPagesId { get; set; }
        public int LocaleId { get; set; }

        public string BlogNewsType { get; set; }
        public string BlogNewsTitle { get; set; }
        public string BodyOverview { get; set; }
        public string Tags { get; set; }
        public string BlogNewsContent { get; set; }
        public string MediaPath { get; set; }

        public bool IsBlogNewsActive { get; set; }
        public bool IsAllowGuestComment { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ActivationDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ExpirationDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? CreatedDate { get; set; }
        public string BlogNewsCode { get; set; }

        ~BlogNewsEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
