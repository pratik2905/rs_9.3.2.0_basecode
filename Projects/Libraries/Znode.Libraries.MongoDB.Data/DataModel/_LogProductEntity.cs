﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogProductEntity : ProductEntity, IDisposable
    {
        ~_LogProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
