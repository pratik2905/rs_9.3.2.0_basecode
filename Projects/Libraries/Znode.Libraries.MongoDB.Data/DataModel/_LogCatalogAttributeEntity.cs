﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogCatalogAttributeEntity : CatalogAttributeEntity, IDisposable
    {
        ~_LogCatalogAttributeEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
