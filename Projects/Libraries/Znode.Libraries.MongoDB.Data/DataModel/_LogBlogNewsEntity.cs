﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogBlogNewsEntity : BlogNewsEntity, IDisposable
    {
        ~_LogBlogNewsEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
