﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogSearchWidgetEntity : SearchWidgetEntity, IDisposable
    {
        ~_LogSearchWidgetEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
