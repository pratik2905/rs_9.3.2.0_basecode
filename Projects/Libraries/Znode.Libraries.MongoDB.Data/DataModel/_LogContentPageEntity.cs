﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogContentPageConfigEntity : ContentPageConfigEntity,IDisposable
    {
        ~_LogContentPageConfigEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
