﻿using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    public class _LogPortalGlobalAttributeEntity : PortalGlobalAttributeEntity, IDisposable
    {
        ~_LogPortalGlobalAttributeEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
