﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class TextWidgetEntity : MongoEntity, IDisposable
    {
        public int TextWidgetConfigurationId { get; set; }
        public int MappingId { get; set; }
        public int PortalId { get; set; }
        public string TypeOFMapping { get; set; }
        public int LocaleId { get; set; }
        public string WidgetsKey { get; set; }
        public string Text { get; set; }

        ~TextWidgetEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
