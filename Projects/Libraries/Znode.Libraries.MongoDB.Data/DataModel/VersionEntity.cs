﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class VersionEntity : MongoEntity,IDisposable
    {
        public int ZnodeCatalogId { get; set; }
        public string RevisionType { get; set; }
        public int LocaleId { get; set; }
        public bool IsPublishSuccess { get; set; }

        ~VersionEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
