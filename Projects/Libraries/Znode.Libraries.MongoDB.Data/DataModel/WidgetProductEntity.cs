﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class WidgetProductEntity : MongoEntity, IDisposable
    {
        public int WidgetProductId { get; set; }
        public int ZnodeProductId { get; set; }
        public int PortalId { get; set; }
        public int MappingId { get; set; }
        public string WidgetsKey { get; set; }
        public string TypeOFMapping { get; set; }
        public int? DisplayOrder { get; set; }
        public string SKU { get; set; }


        ~WidgetProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
