﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class ProductPageEntity : MongoEntity, IDisposable
    {
        public int ProductPageId { get; set; }
        public int PortalId { get; set; }
        public string ProductType { get; set; }
        public string TemplateName { get; set; }

        ~ProductPageEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
