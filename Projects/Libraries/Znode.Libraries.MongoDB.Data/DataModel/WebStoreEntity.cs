﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class WebStoreEntity : MongoEntity, IDisposable
    {
        public int PortalThemeId { get; set; }
        public int PortalId { get; set; }
        public int ThemeId { get; set; }
        public string ThemeName { get; set; }
        public int CSSId { get; set; }
        public string CSSName { get; set; }
        public string WebsiteLogo { get; set; }
        public string WebsiteTitle { get; set; }
        public string FaviconImage { get; set; }
        public string WebsiteDescription { get; set; }      
        public string PublishState { get; set; }
        public int LocaleId { get; set; }

        ~WebStoreEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
