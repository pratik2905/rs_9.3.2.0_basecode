﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogSeoEntity : SeoEntity, IDisposable
    {
        ~_LogSeoEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
