﻿using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    public class WidgetSliderBannerEntity : MongoEntity,IDisposable
    {
        public int WidgetSliderBannerId { get; set; }
        public int MappingId { get; set; }
        public int PortalId { get; set; }
        public int LocaleId { get; set; }
        
        public string Type { get; set; }
        public string Navigation { get; set; }
        public bool AutoPlay { get; set; }
        public int? AutoplayTimeOut { get; set; }
        public bool AutoplayHoverPause { get; set; }
        public string TransactionStyle { get; set; }
        public string WidgetsKey { get; set; }
        public string TypeOFMapping { get; set; }
        public int SliderId { get; set; }

        public List<SliderBannerEntity> SliderBanners { get; set; }

        public WidgetSliderBannerEntity()
        {
            SliderBanners = new List<SliderBannerEntity>();
        }

        ~WidgetSliderBannerEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
