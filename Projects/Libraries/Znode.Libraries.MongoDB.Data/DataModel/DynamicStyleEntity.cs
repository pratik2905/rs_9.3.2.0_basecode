﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class DynamicStyleEntity : MongoEntity, IDisposable
    {
        public int PortalId { get; set; }
        public string DynamicStyle { get; set; }
        public string PublishState { get; set; }
        public int LocaleId { get; set; }

        ~DynamicStyleEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
