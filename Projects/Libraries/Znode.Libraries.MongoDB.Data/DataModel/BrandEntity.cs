﻿namespace Znode.Libraries.MongoDB.Data
{
    public class BrandEntity : MongoEntity
    {
        public int BrandId { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }

    }
}
