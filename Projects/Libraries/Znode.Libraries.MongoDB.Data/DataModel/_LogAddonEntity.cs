﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogAddonEntity : AddonEntity, IDisposable
    {
        ~_LogAddonEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
