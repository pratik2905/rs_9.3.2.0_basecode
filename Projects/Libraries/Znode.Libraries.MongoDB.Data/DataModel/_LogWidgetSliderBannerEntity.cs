﻿using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogWidgetSliderBannerEntity : WidgetSliderBannerEntity, IDisposable
    {
        ~_LogWidgetSliderBannerEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
