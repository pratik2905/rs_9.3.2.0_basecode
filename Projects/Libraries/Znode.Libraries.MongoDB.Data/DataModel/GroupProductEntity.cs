﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class GroupProductEntity : AssociatedProductEntity,IDisposable
    {
        ~GroupProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
