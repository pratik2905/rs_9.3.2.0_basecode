﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogConfigurableProductEntity : ConfigurableProductEntity, IDisposable
    {
        ~_LogConfigurableProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
