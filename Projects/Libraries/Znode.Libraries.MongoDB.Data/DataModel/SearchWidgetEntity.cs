﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.MongoDB.Data
{
    public class SearchWidgetEntity : MongoEntity, IDisposable
    {
        public int CMSSearchWidgetId { get; set; }
        public int MappingId { get; set; }
        public int WidgetsId { get; set; }
        public int PortalId { get; set; }
        public string TypeOFMapping { get; set; }
        public int LocaleId { get; set; }
        public string WidgetsKey { get; set; }
        public string AttributeCode { get; set; }

        public string SearchKeyword { get; set; }

        ~SearchWidgetEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
