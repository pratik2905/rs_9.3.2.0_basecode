﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    [BsonIgnoreExtraElements]
    public class CategoryListEntity : MongoEntity
    {
        public List<CategoryEntity> CategoryList { get; set; }
        public string MongoCatalogId { get; set; }

        public CategoryListEntity()
        {
            CategoryList = new List<CategoryEntity>();
        }
    }
}
