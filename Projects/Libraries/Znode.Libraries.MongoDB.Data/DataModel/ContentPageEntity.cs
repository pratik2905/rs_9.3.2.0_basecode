﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class ContentPageConfigEntity : MongoEntity,IDisposable
    {
        public int ContentPageId { get; set; }
        public string FileName { get; set; }
        public int?[] ProfileId { get; set; }
        public int PortalId { get; set; }
        public string PageTitle { get; set; }
        public string PageName { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ActivationDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ExpirationDate { get; set; }
        public int LocaleId { get; set; }
        public bool IsActive { get; set; }

        ~ContentPageConfigEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
