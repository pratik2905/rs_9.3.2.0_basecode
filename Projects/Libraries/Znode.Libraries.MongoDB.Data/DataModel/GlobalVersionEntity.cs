﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class GlobalVersionEntity : MongoEntity, IDisposable
    {        
        public string PublishState { get; set; }
        public int LocaleId { get; set; }

        ~GlobalVersionEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
