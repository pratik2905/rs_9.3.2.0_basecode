﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
   public class _LogVersionEntity : VersionEntity, IDisposable
    {
        ~_LogVersionEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
