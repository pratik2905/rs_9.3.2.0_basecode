﻿using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    public class PortalGlobalAttributeEntity : MongoEntity, IDisposable
    {
        public int PortalId { get; set; }
        public string PortalName { get; set; }
        public int LocaleId { get; set; }
        public List<GlobalAttributeGroupEntity> GlobalAttributeGroups { get; set; }
        ~PortalGlobalAttributeEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
