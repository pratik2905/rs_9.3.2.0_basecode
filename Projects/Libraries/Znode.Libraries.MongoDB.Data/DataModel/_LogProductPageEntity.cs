﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogProductPageEntity : ProductPageEntity, IDisposable
    {
        ~_LogProductPageEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
