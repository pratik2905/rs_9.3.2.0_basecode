﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogDynamicStyleEntity : DynamicStyleEntity, IDisposable
    {
        ~_LogDynamicStyleEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
