﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class PublishLogEntity : MongoEntity, IDisposable
    {
        public int ZnodeCatalogId { get; set; }
        public string RevisionType { get; set; }
        public string LogMessage { get; set; }

        ~PublishLogEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
