﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.MongoDB.Data
{
    public class MediaWidgetEntity : MongoEntity, IDisposable
    {
        public int MediaWidgetConfigurationId { get; set; }
        public int MappingId { get; set; }
        public int PortalId { get; set; }
        public string TypeOFMapping { get; set; }
        public string MediaPath { get; set; }
        public string WidgetsKey { get; set; }

        ~MediaWidgetEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
