﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class BundleProductEntity : AssociatedProductEntity,IDisposable
    {
        ~BundleProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
