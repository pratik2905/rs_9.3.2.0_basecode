﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogWebStoreEntity : WebStoreEntity, IDisposable
    {
        ~_LogWebStoreEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
