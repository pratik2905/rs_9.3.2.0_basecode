﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogBundleProductEntity:BundleProductEntity,IDisposable
    {
        ~_LogBundleProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
