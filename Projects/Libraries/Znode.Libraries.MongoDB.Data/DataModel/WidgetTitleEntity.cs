﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class WidgetTitleEntity : MongoEntity,IDisposable
    {
        public int WidgetTitleConfigurationId { get; set; }
        public int PortalId { get; set; }
        public int MappingId { get; set; }
        public string MediaPath { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string WidgetsKey { get; set; }
        public string TypeOFMapping { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsActive { get; set; }
        public int LocaleId { get; set; }
        public string TitleCode { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsNewTab { get; set; }

        ~WidgetTitleEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
