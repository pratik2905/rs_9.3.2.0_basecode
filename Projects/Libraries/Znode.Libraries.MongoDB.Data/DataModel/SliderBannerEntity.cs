﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class SliderBannerEntity : MongoEntity, IDisposable
    {
        public int SliderBannerId { get; set; }
        public int SliderId { get; set; }
        public string MediaPath { get; set; }
        public string Title { get; set; }
        public string ImageAlternateText { get; set; }
        public string ButtonLabelName { get; set; }
        public string ButtonLink { get; set; }
        public string TextAlignment { get; set; }
        public int? BannerSequence { get; set; }
        public string Description { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ActivationDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ExpirationDate { get; set; }

        ~SliderBannerEntity()
        {
            if (!isDisposed)
                Dispose();
        }
        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
