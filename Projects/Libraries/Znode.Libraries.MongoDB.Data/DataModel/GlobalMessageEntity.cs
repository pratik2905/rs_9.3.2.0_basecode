﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class GlobalMessageEntity : MongoEntity, IDisposable
    {
        public int LocaleId { get; set; }
        public string MessageKey { get; set; }
        public string Message { get; set; }
        public string Area { get; set; }

        [BsonIgnore]
        public int CMSMessageId { get; set; }

        ~GlobalMessageEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
