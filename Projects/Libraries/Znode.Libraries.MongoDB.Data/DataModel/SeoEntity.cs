﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class SeoEntity : MongoEntity, IDisposable
    {
        public string ItemName { get; set; }
        public int CMSSEODetailId { get; set; }
        public int CMSSEODetailLocaleId { get; set; }
        public int CMSSEOTypeId { get; set; }
        public int? SEOId { get; set; }
        public string SEOTypeName { get; set; }
        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }
        public string SEOKeywords { get; set; }
        public string SEOUrl { get; set; }
        public bool? IsRedirect { get; set; }
        public string MetaInformation { get; set; }
        public string OldSEOURL { get; set; }
        public int? LocaleId { get; set; }
        public int CMSContentPagesId { get; set; }
        public int? PortalId { get; set; }
        public string SEOCode { get; set; }
        public string CanonicalURL { get; set; }
        public string RobotTag { get; set; }

        ~SeoEntity()
        {
            if (!isDisposed)
                Dispose();
        }
        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
