﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    [BsonIgnoreExtraElements]
    public class ProductListEntity : MongoEntity
    {
        public List<ProductEntity> Products { get; set; }
        public ProductListEntity()
        {
            Products = new List<ProductEntity>();
        }
    }
}
