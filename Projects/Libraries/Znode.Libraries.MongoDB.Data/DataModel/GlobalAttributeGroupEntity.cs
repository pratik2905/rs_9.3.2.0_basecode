﻿using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    public class GlobalAttributeGroupEntity : MongoEntity
    {
        public int GlobalAttributeGroupId { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public List<GlobalAttributeEntity> GlobalAttributes { get; set; }
    }
}
