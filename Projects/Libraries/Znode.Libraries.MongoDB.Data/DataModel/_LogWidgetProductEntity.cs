﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogWidgetProductEntity : WidgetProductEntity, IDisposable
    {
        ~_LogWidgetProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
