﻿using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    public class CatalogAttributeEntity : MongoEntity, IDisposable
    {
        public int ZnodeCatalogId { get; set; }
        public string AttributeCode { get; set; }
        public string AttributeTypeName { get; set; }
        public bool IsPromoRuleCondition { get; set; }
        public bool IsComparable { get; set; }
        public bool IsHtmlTags { get; set; }
        public bool IsFacets { get; set; }
        public bool IsUseInSearch { get; set; }
        public bool IsPersonalizable { get; set; }
        public bool IsConfigurable { get; set; }
        public string AttributeName { get; set; }
        public int LocaleId { get; set; }
        public int DisplayOrder { get; set; }
        public List<SelectAttributeValuesEntity> SelectValues { get; set; }

        ~CatalogAttributeEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}