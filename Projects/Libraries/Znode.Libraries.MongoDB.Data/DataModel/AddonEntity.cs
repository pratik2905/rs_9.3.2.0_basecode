﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class AddonEntity : AssociatedProductEntity,IDisposable
    {
        public int LocaleId { get; set; }
        public string GroupName { get; set; }
        public string DisplayType { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsRequired { get; set; }
        public string RequiredType { get; set; }
        public bool IsDefault { get; set; }

        ~AddonEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
