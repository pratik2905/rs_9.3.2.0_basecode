﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Znode.Libraries.MongoDB.Data
{
    public class ConfigurableProductEntity : AssociatedProductEntity, IDisposable
    {
        [XmlArray]
        [XmlArrayItem(ElementName = "ConfigurableAttributeCode")]
        public List<string> ConfigurableAttributeCodes { get; set; }

        public List<SelectValuesEntity> SelectValues { get; set; }
        ~ConfigurableProductEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }

    }
}
