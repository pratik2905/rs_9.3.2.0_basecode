﻿using System.Web;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: PreApplicationStartMethod(typeof(Znode.Infrastructure.PluginManager.PreApplicationInit), "InitializePlugins")]

[assembly: AssemblyTitle("Znode.Infrastructure.PluginManager")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Znode")]
[assembly: AssemblyProduct("Znode.Infrastructure.PluginManager")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("9.3.2.0")]
[assembly: AssemblyFileVersion("9.3.2.0")]
