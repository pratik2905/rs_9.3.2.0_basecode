﻿using System;
using System.Collections.Generic;

using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using System.Linq;
using System.Diagnostics;

namespace Znode.Libraries.ECommerce.Fulfillment
{
    // Provides methods to manage Orders and OrderLine Items during Checkout
    [Serializable()]
    public class ZnodeOrderFulfillment : ZnodeOrder
    {
        #region Private Variables
        private ZnodeShoppingCart _cart;
        private readonly IZnodeOrderHelper orderHelper = ZnodeDependencyResolver.GetService<IZnodeOrderHelper>();
        private readonly IZnodeRepository<ZnodeOmsCustomerShipping> _omsCustomerShippingRepository;
        private readonly IZnodeRepository<Data.DataModel.ZnodeShipping> _znodeShippingRepository;
        private readonly IZnodeRepository<ZnodeShippingType> _znodeShippingTypeRepository;
        #endregion

        #region Public Properties
        public ZnodeShoppingCart Cart { get { return _cart; } }
        #endregion

        #region Constructor

        // Initializes a new instance of the ZNodeOrderFulfillment class       
        public ZnodeOrderFulfillment() : base(new ZnodeShoppingCart())
        {
            _omsCustomerShippingRepository = new ZnodeRepository<ZnodeOmsCustomerShipping>();
            _znodeShippingRepository = new ZnodeRepository<Data.DataModel.ZnodeShipping>();
            _znodeShippingTypeRepository = new ZnodeRepository<ZnodeShippingType>();

        }

        public ZnodeOrderFulfillment(ZnodeShoppingCart shoppingcart) : base(shoppingcart)
        {
            if (IsNull(_cart))
                _cart = shoppingcart;

            if (IsNull(shoppingcart))
                this.shoppingCart = shoppingcart;

            _omsCustomerShippingRepository = new ZnodeRepository<ZnodeOmsCustomerShipping>();
            _znodeShippingRepository = new ZnodeRepository<Data.DataModel.ZnodeShipping>();
            _znodeShippingTypeRepository = new ZnodeRepository<ZnodeShippingType>();
        }

        #endregion

        #region Public Method

        // Submits order to database       
        public int AddOrderToDatabase(ZnodeOrderFulfillment order, ShoppingCartModel shoppingCartModel)
        {
            SetOrderFulfillment(order);
            this.Order.FirstName = shoppingCartModel?.UserDetails?.FirstName ?? shoppingCartModel?.BillingAddress?.FirstName;
            this.Order.LastName = shoppingCartModel?.UserDetails?.LastName ?? shoppingCartModel?.BillingAddress?.LastName;
            int orderId = orderHelper.SaveOrderDetails(this.Order);
            order.PaymentDisplayName = Order.PaymentDisplayName;
            order.PaymentExternalId = Order.PaymentExternalId;
            // Loop through the order line items
            List<OrderDiscountModel> orderDiscounts = GetOrderDiscount(order.Order);
            //Set order level discount to existing OrderDiscountModel list.
            UpdateOrderLevelDiscountDetails(order.shoppingCart.OrderLevelDiscountDetails, orderDiscounts);

            foreach (OrderLineItemModel orderLineItem in this.OrderLineItems)
            {
                orderLineItem.OrderLineItemStateId = orderLineItem?.OrderLineItemStateId > 0 ? orderLineItem?.OrderLineItemStateId : order.OrderStateID;
                orderLineItem.OmsOrderDetailsId = this.Order.OmsOrderDetailsId;
                if (Convert.ToBoolean(orderLineItem.IsRecurringBilling))
                {
                    orderLineItem.TransactionNumber = _cart.Token;
                }
                orderLineItem.IsActive = true;
                orderLineItem.OmsOrderLineItemsId = orderHelper.SaveOrderLineItem(orderLineItem);
                orderDiscounts = GetOrderDiscountDetails(orderDiscounts, orderLineItem);
                ZnodeLogging.LogMessage("Order discounts details", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderDiscounts);
                if (orderLineItem.IsItemStateChanged && SendEmailNotification(orderLineItem.OrderLineItemStateId))
                    this.Order.SendEmailLineItemIds += orderLineItem.OmsOrderLineItemsId + ",";

                orderLineItem.Quantity = orderLineItem.OrderLineItemCollection.FirstOrDefault()?.Quantity ?? orderLineItem.Quantity;
            }

            if (OrderLineItems?.Count > 0 && order.TaxCost > 0)
                orderHelper.SaveTaxOrder(Order.OmsOrderDetailsId, order.OrderLineItems);

            //to save order discount for line item
            if (orderDiscounts?.Count > 0)
                orderHelper.SaveOrderDiscount(orderDiscounts);

            //Create new Customer Shipping.            
            shoppingCartModel.Shipping.UserId = shoppingCartModel.UserId != 0 ? shoppingCartModel?.UserId : order?.UserID;
            shoppingCartModel.Shipping.OmsOrderDetailsId = Order?.OmsOrderDetailsId;
            if (string.IsNullOrEmpty(shoppingCartModel.Shipping.ShippingTypeId.ToString()))
                shoppingCartModel.Shipping.ShippingTypeId = _znodeShippingRepository.Table.Where(p => p.ShippingId == shoppingCartModel.Shipping.ShippingId).Select(q => q.ShippingTypeId).FirstOrDefault();

            int shippingTypeId = _znodeShippingTypeRepository.Table.Where(x => x.ClassName == "ZnodeCustomerShipping").Select(y => y.ShippingTypeId).FirstOrDefault();
            ZnodeLogging.LogMessage("shippingTypeId", ZnodeLogging.Components.OMS.ToString(),TraceLevel.Verbose, shippingTypeId);
            if (shippingTypeId == shoppingCartModel.Shipping.ShippingTypeId)
                _omsCustomerShippingRepository.Insert(shoppingCartModel.Shipping.ToEntity<ZnodeOmsCustomerShipping>());
            return orderId;
        }

        // Add an entry to gift card history.       
        public bool AddToGiftCardHistory(ZnodeOrderFulfillment order, int? userId = 0)
        {
            bool isAdded = true;
            if (!string.IsNullOrEmpty(order?.GiftCardNumber) && order?.GiftCardAmount > 0)
            {
                return Convert.ToBoolean(orderHelper.AddToGiftCardHistory(order.Order.OmsOrderDetailsId, order.GiftCardAmount, order.GiftCardNumber, order.OrderDate, userId));
            }
            return isAdded;
        }

        #endregion

        #region Private Method

        //to set order fulfillment
        private void SetOrderFulfillment(ZnodeOrderFulfillment order)
        {
            int? defaultOrderStateId = order.PortalId > 0 ? orderHelper.GetorderDefaultStateId(order.PortalId) : ZnodeConfigManager.SiteConfig.DefaultOrderStateID.GetValueOrDefault(1);

            // Set order state if order is edited and status is updated then set updated status to new order else default status of order will be set
            if (order.OrderID > 0)
                this.OrderStateID = order.Order.OmsOrderStateId;
            else
                this.OrderStateID = (int)defaultOrderStateId;

            // Set order date and status
            this.OrderDate = OrderDate;
            this.DiscountAmount = DiscountAmount;
            this.CSRDiscountAmount = CSRDiscountAmount;
            this.CouponCode = CouponCode;
            this.AdditionalInstructions = AdditionalInstructions;
            this.Order.AddressId = order.BillingAddress.AddressId;
            this.Order.ReferralUserId = order.ReferralUserId;
            this.Order = order.Order;
            this.Order.TotalAdditionalCost = order.shoppingCart.TotalAdditionalCost;
        }

        //to get all line items discount 
        private List<OrderDiscountModel> GetOrderDiscount(OrderModel model)
        {
            if (IsNull(model.OrdersDiscount) || model.OrdersDiscount?.Count == 0)
            {
                model.OrdersDiscount = new List<OrderDiscountModel>();
            }
            else
                model.OrdersDiscount?.ForEach(item => { item.OmsOrderDetailsId = model.OmsOrderDetailsId; });

            return model.OrdersDiscount;
        }

        //to get all line items discount 
        private List<OrderDiscountModel> GetOrderDiscountDetails(List<OrderDiscountModel> orderDiscount, OrderLineItemModel orderLineItem)
        {
            //If Quantity of OrderLineItem is Zero then we will take Quantity from OrderLineItemCollection
            orderLineItem.Quantity = orderLineItem.Quantity == 0 ? orderLineItem.OrderLineItemCollection.FirstOrDefault().Quantity : orderLineItem.Quantity;

            orderDiscount = AddDiscountItem(orderLineItem.OmsOrderLineItemsId, orderLineItem.Quantity, orderDiscount, orderLineItem.OrdersDiscount, orderLineItem.OmsOrderDetailsId);

            if (IsNotNull(orderLineItem.OrderLineItemCollection) && orderLineItem.OrderLineItemCollection.Count > 0)
            {
                foreach (OrderLineItemModel lineItem in orderLineItem.OrderLineItemCollection)
                {
                    orderDiscount = AddDiscountItem(lineItem.OmsOrderLineItemsId, lineItem.Quantity, orderDiscount, lineItem.OrdersDiscount, orderLineItem.OmsOrderDetailsId);
                }
            }
            return orderDiscount;
        }

        //to add single line items discount
        private List<OrderDiscountModel> AddDiscountItem(int lineItemId, decimal quantity, List<OrderDiscountModel> orderDiscount, List<OrderDiscountModel> orderLineItem, int omsOrderDetailsId)
        {
            if (IsNotNull(orderLineItem) && orderLineItem.Count > 0)
            {
                foreach (OrderDiscountModel lineItemDiscount in orderLineItem)
                {
                    lineItemDiscount.OmsOrderLineItemId = lineItemId;
                    lineItemDiscount.OmsOrderDetailsId = omsOrderDetailsId;
                    lineItemDiscount.DiscountAmount = (quantity * lineItemDiscount.OriginalDiscount);
                    orderDiscount.Add(lineItemDiscount);
                }
            }
            return orderDiscount;
        }

        //to check email notification
        private bool SendEmailNotification(int? stateId)
            => IsNull(stateId) ? false : orderHelper.IsSendEmail(stateId.GetValueOrDefault());

        protected virtual void UpdateOrderLevelDiscountDetails(List<OrderDiscountModel> orderLevelDiscountDetails, List<OrderDiscountModel> orderDiscounts)
        {
            if (orderLevelDiscountDetails?.Count > 0)
            {
                foreach (OrderDiscountModel item in orderLevelDiscountDetails)
                {
                    item.OmsOrderDetailsId = this.Order.OmsOrderDetailsId;
                    item.OmsOrderLineItemId = null;
                    orderDiscounts.Add(item);
                }
            }
        }

        #endregion
    }
}
