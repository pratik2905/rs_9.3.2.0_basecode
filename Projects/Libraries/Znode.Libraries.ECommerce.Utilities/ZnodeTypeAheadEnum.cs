﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum ZnodeTypeAheadEnum
    {
        StoreList,
        CatalogList,
        ProductList,
        PIMCatalogList
    }
    public enum ZnodeTypeAheadTypeNameEnum
    {
        Stores,
        Catalogs,
        Products
    }

    public enum ZnodeTypeAheadListGenericOptions
    {
        No = -1,
        All = 0
    }
}
