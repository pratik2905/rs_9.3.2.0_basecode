﻿using System;
using System.Web;
using System.Web.SessionState;


namespace Znode.Libraries.ECommerce.Utilities
{
    public static class SessionHelper
    {
        public static void SaveDataInSession<T>(string key, T value)
        {
            //only InProc session mode will support CLR objects, hence no need of object serialization
            //but other modes (SQL or State server or any custom) may not support storing CLR objects, so in those cases serialization would be required

            switch (GetSessionStateMode())
            {
                case SessionStateMode.InProc:
                    if (HelperUtility.IsNotNull(HttpContext.Current.Session))
                        HttpContext.Current.Session[key] = value;
                    break;

                default:
                    ApplicationSessionConfiguration applicationSessionConfiguration = new ApplicationSessionConfiguration();
                    string sessionValue = applicationSessionConfiguration.GetSerializedData(value);
                    HttpContext.Current.Session[key] = sessionValue;
                    break;

            }
        }

        public static T GetDataFromSession<T>(string key)
        {
            //only InProc session mode will support CLR objects, hence no need of object deserialization
            //but other modes (SQL or State server or any custom) may not support storing CLR objects, so in those cases deserialization would be required

            switch (GetSessionStateMode())
            {
                case SessionStateMode.InProc:
                    var o = HttpContext.Current?.Session?[key];// ConvertValueToNullableType<T>(HttpContext.Current?.Session?[key]);
                    if (o is T)
                    {
                        return (T)o;
                    }
                    break;

                default:
                    ApplicationSessionConfiguration applicationSessionConfiguration = new ApplicationSessionConfiguration();
                    return applicationSessionConfiguration.GetDeSerializeData<T>(Convert.ToString(HttpContext.Current.Session[key]));
            }

            //NOTE: We can't return default, it may have negative effect on if "T" is bool which will always be false, also int may always return 0 as default
            //but this method have been called on many places to check existence rather then just "get", but then how we can return type T?

            return default(T);
        }

        public static void RemoveDataFromSession(string key)
        {
            var obj = GetDataFromSession<object>(key);
            if (obj == null) return;

            HttpContext.Current.Session.Remove(key);
        }

        public static SessionStateMode GetSessionStateMode() => HelperUtility.IsNull(HttpContext.Current?.Session) ? SessionStateMode.InProc : HttpContext.Current.Session.Mode;

        public static bool IsSessionObjectPresent()
        {
            return !Equals(HttpContext.Current?.Session, null);
        }

        public static void Abandon()
        {
            HttpContext.Current.Session.Abandon();
        }

        public static void Clear()
        {
            HttpContext.Current.Session.Clear();
        }

        public static string GetSessionId()
        {
            return HttpContext.Current.Session.SessionID;
        }
    }
}