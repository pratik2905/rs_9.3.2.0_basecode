﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum EntityTypeEnum
    {
        Store,
        User,
        Account
    }
}
