﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum OrderDiscountTypeEnum
    {
        PROMOCODE = 1,
        COUPONCODE = 2,
        GIFTCARD = 3,
        CSRDISCOUNT = 4,
        PARTIALREFUND = 5
    }
}
