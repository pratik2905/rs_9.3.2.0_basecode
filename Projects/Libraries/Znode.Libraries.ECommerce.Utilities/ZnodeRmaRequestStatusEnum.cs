﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum ZnodeRmaRequestStatusEnum
    {
        Authorised,
        Returned_Refunded,
        Void
    }
}
