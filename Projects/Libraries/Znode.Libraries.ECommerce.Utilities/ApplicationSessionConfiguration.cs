﻿using Newtonsoft.Json;

namespace Znode.Libraries.ECommerce.Utilities
{
    public class ApplicationSessionConfiguration
    {
        public string GetSerializedData<T>(T tClass) => JsonConvert.SerializeObject(tClass, Formatting.None);

        public T GetDeSerializeData<T>(string sessionString)
        {
            if (!string.IsNullOrEmpty(sessionString))
                return JsonConvert.DeserializeObject<T>(sessionString);

            return default(T);
        }
    }
}
