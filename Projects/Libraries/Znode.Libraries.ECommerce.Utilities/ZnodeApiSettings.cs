﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Znode.Libraries.ECommerce.Utilities
{
    public static class ZnodeApiSettings
    {
        private static NameValueCollection settings = ConfigurationManager.AppSettings;

        public static void SetConfigurationSettingSource(NameValueCollection settingSource)
        {
            settings = settingSource;
        }

        public static string ZnodeApiRootUri
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiRootUri"]);
            }
        }
        public static string AdminWebsiteUrl
        {
            get
            {
                return Convert.ToString(settings["AdminWebsiteUrl"]);
            }
        }
        public static string LicensePath
        {
            get
            {
                return Convert.ToString(settings["LicensePath"]);
            }
        }
        public static string EnableFileLogging
        {
            get
            {
                return Convert.ToString(settings["EnableFileLogging"]);
            }
        }
        public static string EnableDBLogging
        {
            get
            {
                return Convert.ToString(settings["EnableDBLogging"]);
            }
        }
        public static string ValidateAuthHeader
        {
            get
            {
                return Convert.ToString(settings["ValidateAuthHeader"]);
            }
        }
        public static string MaxInvalidPasswordAttempts
        {
            get
            {
                return Convert.ToString(settings["MaxInvalidPasswordAttempts"]);
            }
        }
        public static string ResetPassworLinkExpirationDuration
        {
            get
            {
                return Convert.ToString(settings["ResetPassworLinkExpirationDuration"]);
            }

        }
        public static string PasswordExpiration
        {
            get
            {
                return Convert.ToString(settings["PasswordExpiration"]);
            }
        }
        public static string FedExGatewayURL
        {
            get
            {
                return Convert.ToString(settings["FedExGatewayURL"]);
            }
        }
        public static string UPSGatewayURL
        {
            get
            {
                return Convert.ToString(settings["UPSGatewayURL"]);
            }
        }
        public static string UPSAddressValidationURL
        {
            get
            {
                return Convert.ToString(settings["UPSAddressValidationURL"]);
            }
        }
        public static string AllowedPromotions
        {
            get
            {
                return Convert.ToString(settings["AllowedPromotions"]);
            }
        }
        public static string USPSShippingAPIURL
        {
            get
            {
                return Convert.ToString(settings["USPSShippingAPIURL"]);
            }
        }
        public static string USPSWebToolsUserID
        {
            get
            {
                return Convert.ToString(settings["USPSWebToolsUserID"]);
            }
        }
        public static string USPSWeightLimitInLBS
        {
            get
            {
                return Convert.ToString(settings["USPSWeightLimitInLBS"]);
            }
        }
        public static string SiteMapNameSpace
        {
            get
            {
                return Convert.ToString(settings["SiteMapNameSpace"]);
            }
        }
        public static string GoogleProductFeedNameSpace
        {
            get
            {
                return Convert.ToString(settings["GoogleProductFeedNameSpace"]);
            }
        }
        public static string ProductFeedRecordCount
        {
            get
            {
                return Convert.ToString(settings["ProductFeedRecordCount"]);
            }
        }
        public static string ZnodeReportFolderName
        {
            get
            {
                return Convert.ToString(settings["ZnodeReportFolderName"]);
            }
        }
        public static string ZnodeApiUriKeyValueSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriKeyValueSeparator"]);
            }
        }
        public static string ZnodeApiUriItemSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriItemSeparator"]);
            }
        }

        public static string ReportServerDynamicReportFolderName
        {
            get
            {
                return Convert.ToString(settings["ReportServerDynamicReportFolderName"]);
            }
        }

        public static string WebstoreWebsiteName
        {
            get
            {
                return Convert.ToString(settings["WebstoreWebsiteName"]);
            }
        }
        public static string UseSynonym
        {
            get
            {
                return Convert.ToString(settings["UseSynonyms"]);
            }
        }
        public static string UseCustomAnalyzer
        {
            get
            {
                return Convert.ToString(settings["UseCustomAnalyzer"]);
            }
        }
        public static string DefaultTokenFilters
        {
            get
            {
                return Convert.ToString(settings["DefaultTokenFilters"]);
            }
        }
        public static string SearchIndexChunkSize
        {
            get
            {
                return Convert.ToString(settings["SearchIndexChunkSize"]);
            }
        }
        public static string RemovableProductAttributes
        {
            get
            {
                return Convert.ToString(settings["RemovableProductAttributes"]);
            }
        }
        public static string CreateSchedulerOnServer
        {
            get
            {
                return Convert.ToString(settings["CreateSchedulerOnServer"]);
            }
        }

        public static string CCHTestTransactionMode
        {
            get
            {
                return Convert.ToString(settings["CCHTestTransactionMode"]);
            }
        }

        public static string CCHEntityId
        {
            get
            {
                return Convert.ToString(settings["CCHEntityId"]);
            }
        }

        public static string CCHDivisionId
        {
            get
            {
                return Convert.ToString(settings["CCHDivisionId"]);
            }
        }

        public static string CCHSourceSystem
        {
            get
            {
                return Convert.ToString(settings["CCHSourceSystem"]);
            }
        }

        public static string CCHTransactionDescription
        {
            get
            {
                return Convert.ToString(settings["CCHTransactionDescription"]);
            }
        }
        public static string ProductPublishMaxTime
        {
            get
            {
                return Convert.ToString(settings["ProductPublishMaxTime"]);
            }
        }
        public static string ProductPublishSleepTime
        {
            get
            {
                return Convert.ToString(settings["ProductPublishSleepTime"]);
            }
        }

        public static string AllowedExtention
        {
            get
            {
                return Convert.ToString(settings["AllowedMediaExtention"]);
            }
        }

        public static string CCHWebServiceURL
        {
            get
            {
                return Convert.ToString(settings["CCHWebServiceURL"]);
            }
        }

        public static string CCHTransactionType
        {
            get
            {
                return Convert.ToString(settings["CCHTransactionType"]);
            }
        }

        public static string CCHCustomerType
        {
            get
            {
                return Convert.ToString(settings["CCHCustomerType"]);
            }
        }

        public static string CCHProviderType
        {
            get
            {
                return Convert.ToString(settings["CCHProviderType"]);
            }
        }

        public static string CCHTaxGroup
        {
            get
            {
                return Convert.ToString(settings["CCHTaxGroup"]);
            }
        }

        public static string CCHTaxItem
        {
            get
            {
                return Convert.ToString(settings["CCHTaxItem"]);
            }
        }

        public static string AvaTaxCode
        {
            get
            {
                return Convert.ToString(settings["AvaTaxCode"]);
            }
        }

        public static string AvaTaxClientHeader
        {
            get
            {
                return Convert.ToString(settings["AvaTaxClientHeader"]);
            }
        }

        public static string FlexFieldsToPassInVertex
        {
            get
            {
                return Convert.ToString(settings["FlexFieldsToPassInVertex"]);
            }
        }

        public static string GenerateRuntimeImages
        {
            get
            {
                return Convert.ToString(settings["GenerateRuntimeImages"]);
            }
        }

        public static string ElasticSearchRootUri
        {
            get
            {
                return Convert.ToString(settings["ElasticSearchRootUri"]);
            }
        }

        public static int TokenExpirationTime
        {
            get
            {
                return Convert.ToInt32(settings["TokenExpirationTime"]);
            }
        }

        public static bool EnableTokenBasedAuthorization
        {
            get
            {
                return Convert.ToBoolean(settings["EnableTokenBasedAuthorization"]);
            }
        }

        public static bool EnableBasicAuthorization
        {
            get
            {
                return Convert.ToBoolean(settings["EnableBasicAuthorization"]);
            }
        }

        public static bool CreateIndexAfterPublish
        {
            get
            {
                string createIndexAfterPublish = settings["CreateIndexAfterPublish"];
                if (string.IsNullOrEmpty(createIndexAfterPublish))
                    return true;
                return Convert.ToBoolean(settings["CreateIndexAfterPublish"]);
            }
        }

        public static string ExemptionNo
        {
            get
            {
                return Convert.ToString(settings["ExemptionNo"]);
            }
        }

        public static bool DisableTaxCommit
        {
            get
            {
                return Convert.ToBoolean(settings["DisableTaxCommit"]);
            }
        }


        public static string UseECertificateERPMode
        {
            get
            {
                return Convert.ToString(settings["UseECertificateERPMode"]);
            }
        }


        public static string UPSTrackingURL
        {
            get
            {
                return Convert.ToString(settings["UPSTrackingUrl"]);
            }
        }

        public static string FedExTrackingURL
        {
            get
            {
                return Convert.ToString(settings["FedExTrackingUrl"]);
            }
        }

        public static string USPSTrackingURL
        {
            get
            {
                return Convert.ToString(settings["USPSTrackingUrl"]);
            }
        }

        public static long ImageQualityFactor
        {
            get
            {
                return Convert.ToInt32(settings["ImageQualityFactor"]);
            }
        }
        public static string IsAllowAddOnQuantity
        {
            get
            {
                return Convert.ToString(settings["IsAllowAddOnQuantity"]);
            }
        }

        public static string SchdeulerPath
        {
            get
            {
                return Convert.ToString(settings["SchedulerPath"]);
            }
        }

        public static string IsCategoryMerchandise
        {
            get
            {
                return Convert.ToString(settings["IsCategoryMerchandise"]);
            }
        }
        public static string CalculateTaxAfterDiscount
        {
            get
            {
                return Convert.ToString(settings["CalculateTaxAfterDiscount"]);
            }
        }
        public static bool IsCookieHttpOnly
        {
            get
            {
                return Convert.ToBoolean(settings["IsCookieHttpOnly"]);
            }
        }
        public static bool IsCookieSecure
        {
            get
            {
                return Convert.ToBoolean(settings["IsCookieSecure"]);
            }
        }
        public static string ProductPublishChunkSize
        {
            get
            {
                return Convert.ToString(settings["ProductPublishChunkSize"]);
            }
        }

        public static string IndexChunkSizeOnCategoryPublish
        {
            get
            {
                return Convert.ToString(settings["IndexChunkSizeOnCategoryPublish"]);
            }
        }

        public static string CustomAssemblyLookupPrefix
        {
            get
            {
                string customAssemblyLookupPrefix = Convert.ToString(settings["CustomAssemblyLookupPrefix"]);
                return string.IsNullOrEmpty(customAssemblyLookupPrefix) ? string.Empty : customAssemblyLookupPrefix;
            }
        }

        public static string CacheControl
        {
            get
            {
                return Convert.ToString(settings["CacheControl"]);
            }
        }
        public static string DefaultCacheControl
        {
            get
            {
                return Convert.ToString(settings["DefaultCacheControl"]);
            }
        }
        public static string ClientId
        {
            get
            {
                return Convert.ToString(settings["ClientId"]);
            }
        }
        public static string ClientSecret
        {
            get
            {
                return Convert.ToString(settings["ClientSecret"]);
            }
        }
        public static string TenanatId
        {
            get
            {
                return Convert.ToString(settings["TenanatId"]);
            }
        }

        public static string SubscriptionId
        {
            get
            {
                return Convert.ToString(settings["SubscriptionId"]);
            }
        }
        public static string ResourceGroups
        {
            get
            {
                return Convert.ToString(settings["ResourceGroups"]);
            }
        }
        public static string Profiles
        {
            get
            {
                return Convert.ToString(settings["Profiles"]);
            }
        }
        public static string EndPoints
        {
            get
            {
                return Convert.ToString(settings["EndPoints"]);
            }
        }
        public static string RequestTimeout
        {
            get
            {
                return Convert.ToString(settings["RequestTimeout"]);
            }
        }
        public static string ZnodeCommaReplacer
        {
            get
            {
                return Convert.ToString(settings["ZnodeCommaReplacer"]);
            }
        }
        public static string BarcodeScannerLicenseKey
        {
            get
            {
                return Convert.ToString(settings["BarcodeScannerLicenseKey"]);
            }
        }
        public static bool EnableBarcodeSpecificSearch
        {
            get
            {
                string enableBarcodeSpecificSearch = settings["EnableBarcodeSpecificSearch"];
                if (string.IsNullOrEmpty(enableBarcodeSpecificSearch))
                    return false;
                return Convert.ToBoolean(settings["EnableBarcodeSpecificSearch"].ToLower());
            }
        }
        public static bool AllowPublishedEntityLogging
        {
            get
            {
                return Convert.ToBoolean(settings["AllowPublishedEntityLogging"]);
            }
        }
        public static int NoOfPublishedVersionsToBeLogged
        {
            get
            {
                return Convert.ToInt32(settings["NoOfPublishedVersionsToBeLogged"]);
            }
        }

        public static bool DisableRoutesForStaticFile
        {
            get
            {
                string disableRoutesForStaticFile = settings["DisableRoutesForStaticFile"];
                if (string.IsNullOrEmpty(disableRoutesForStaticFile))
                    return false;

                return Convert.ToBoolean(disableRoutesForStaticFile);
            }
        }

        public static bool EnableMongoIndexAfterPostPublish
        {
            get
            {
                string enableMongoIndexAfterPostPublish = settings["EnableMongoIndexAfterPostPublish"];
                if (string.IsNullOrEmpty(enableMongoIndexAfterPostPublish))
                    return false;
                return Convert.ToBoolean(enableMongoIndexAfterPostPublish.ToLower());
            }
        }

        public static bool EnableStopWordsForSearchIndex
        {
            get
            {
                return Convert.ToBoolean(settings["EnableStopWordsForSearchIndex"]);
            }
        }
    }
}
