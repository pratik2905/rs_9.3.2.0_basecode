﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum GlobalSettingEnum
    {
        Locale,
        DateFormat,
        TimeZone,
        WeightUnit,
        Currency,
        DisplayUnit,
        PriceRoundOff,
        InventoryRoundOff,
        AllowGlobalLevelUserCreation,
        SaveOrderAttribute,
        Country,
        BuildEnvironment,
        CartAttribute,
        GroupIdFormat,
        CurrentEnvironment,
        WebApiClientKey,
        WebsiteCode,
        DomainName,
        IsAllowWithOtherPromotionsAndCoupons,
        IsColumnEncryptionSettingEnabled,
        TimeFormat,      
        Culture,
        ClearLoadBalancerAPICacheIPs,
        ClearLoadBalancerWebStoreCacheIPs,
        ClearOnlyHttpsDomainCache,
        DefaultProductLimitForRecommendations
    }

    public enum LoggingSettingEnum
    {
        IsLoggingLevelsEnabledError,
        IsLoggingLevelsEnabledWarning,
        IsLoggingLevelsEnabledInfo,
        IsLoggingLevelsEnabledDebug,
        IsLoggingLevelsEnabledAll,
        IsDataBaseLoggingEnabled,
        IsErrorLoggingEnabled,
        IsEventLoggingEnabled,
        IsIntegrationLoggingEnabled,

    }
}
