﻿using System;
using System.Text;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    [Serializable()]
    public class ZnodeMultipleAddressCart : ZnodeShoppingCart, IZnodeMultipleAddressCart
    {
        public Guid AddressCartID { get; set; }

        public int AddressID { get; set; }

        public int OrderShipmentID { get; set; }

        public ZnodeMultipleAddressCart() : base()
        {
            AddressCartID = Guid.NewGuid();
        }

        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        public override void Calculate()
        {
            // Clear previous messages
            _ErrorMessage = new StringBuilder();

            // ShippingRules
            ZnodeShippingManager shipping = new ZnodeShippingManager(this);
            shipping.Calculate();

            ZnodeCartPromotionManager cartPromoManager = new ZnodeCartPromotionManager(this, null);
            cartPromoManager.Calculate();

            ZnodeTaxManager taxRules = new ZnodeTaxManager(this);
            taxRules.Calculate(this);
        }
    }
}
