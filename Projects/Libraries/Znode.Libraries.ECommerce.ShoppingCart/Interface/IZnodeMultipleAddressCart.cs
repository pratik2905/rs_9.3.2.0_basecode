﻿using System;
using System.Text;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    public interface IZnodeMultipleAddressCart
    {
        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        void Calculate();

    }
}
