﻿using System;
using System.Xml.Serialization;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Taxes;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents a product
    /// </summary>    

    public interface IZnodeProductBase 
    {
        #region Static Create      

        /// <summary>
        /// Apply Promotion Product if any.
        /// </summary>
        void ApplyPromotion();
        #endregion
    }
}