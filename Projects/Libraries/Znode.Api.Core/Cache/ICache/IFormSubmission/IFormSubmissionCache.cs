﻿namespace Znode.Engine.Api.Cache
{
    public interface IFormSubmissionCache
    {
        /// <summary>
        /// Get form submission list.
        /// </summary>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>Returns form submission list.</returns>
        string GetFormSubmissionList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Form Submission Details
        /// </summary>
        /// <param name="formSubmitId">int form submit Id</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns form submit details</returns>
        string GetFormSubmitDetails(int formSubmitId, string routeUri, string routeTemplate);

    }
}
