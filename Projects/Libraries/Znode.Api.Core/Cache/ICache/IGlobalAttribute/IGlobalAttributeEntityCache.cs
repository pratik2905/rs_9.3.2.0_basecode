﻿namespace Znode.Engine.Api.Cache
{
    public interface IGlobalAttributeEntityCache
    {
        /// <summary>
        /// Get all entity entity list.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAllEntityList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get assigned entity attribute groups.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAssignedEntityAttributeGroups(string routeUri, string routeTemplate);

        /// <summary>
        /// Get unassigned entity group attribute groups.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetUnAssignedEntityAttributeGroups(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Attribute Values based on the Entity Id.
        /// </summary>
        /// <param name="entityId">Entity Id.</param>
        /// <param name="entityType">Entity Type.</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetEntityAttributeDetails(int entityId, string entityType, string routeUri, string routeTemplate);

        /// <summary>
        /// Get publish global attributes.
        /// </summary>
        /// <param name="entityId">Entity Id.</param>
        /// <param name="entityType">Entity Type.</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Get publish attributes.</returns>
        string GetGlobalEntityAttributes(int entityId, string entityType, string routeUri, string routeTemplate);
    }
}
