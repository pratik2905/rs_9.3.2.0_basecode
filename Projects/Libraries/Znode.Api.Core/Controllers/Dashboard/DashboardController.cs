﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
    public class DashboardController : BaseController
    {
        #region private variables
        private readonly IDashboardCache _cache;
        private readonly IDashboardService _service;
        #endregion

        #region constructor
        public DashboardController(IDashboardService service)
        {
            _service = service;
            _cache = new DashboardCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get list of dashboard top brands
        /// </summary>
        /// <returns>Returns list of dashboard top brands.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardTopBrandsList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardTopBrands(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Get list of dashboard top categories
        /// </summary>
        /// <returns>Returns list of dashboard top categories.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardTopCategoriesList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardTopCategories(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Get list of dashboard top products
        /// </summary>
        /// <returns>Returns list of dashboard top products.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardTopProductsList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardTopProducts(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Get list of dashboard top searches
        /// </summary>
        /// <returns>Returns list of dashboard top searches.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardTopSearchesList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardTopSearches(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Gets list of total sales, orders, new customers and avg orders
        /// </summary>
        /// <returns>Returns total sales, orders, new customers and avg orders.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardSalesDetails()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardSalesDetails(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Gets dashboard low inventory product count
        /// </summary>
        /// <returns>Returns dashboard low inventory product count.</returns>
        [ResponseType(typeof(DashboardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetDashboardLowInventoryProductCount()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetDashboardLowInventoryProductCount(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<DashboardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DashboardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(),TraceLevel.Error);
            }
            return response;
        }
        #endregion
    }
}
