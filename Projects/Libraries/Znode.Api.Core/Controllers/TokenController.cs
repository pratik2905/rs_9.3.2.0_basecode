﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Models.Responses;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Core.Controllers
{
    public class TokenController : ApiController
    {
        private string _authValue;

        [AllowAnonymous]
        [HttpGet]
        [ResponseType(typeof(StringResponse))]
        public virtual HttpResponseMessage GenerateToken()
        {
            HttpResponseMessage response;
            try
            {
                response = GetTokenKey();
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, new StringResponse { Response = "Web API Key Not Found" });
            }
            return response;
        }

        //Get the token
        private HttpResponseMessage GetTokenKey()
        {            
            _authValue = HttpContext.Current.Request.Headers.AllKeys.Contains("Authorization") ? HttpContext.Current.Request.Headers["Authorization"] : string.Empty;

            string[] authHeader = ZnodeTokenHelper.GetAuthHeader(_authValue);

            // Strip off the "Basic " from "Authorization" header
            _authValue = _authValue.Remove(0, 6);

            if (!Equals(authHeader, null))
            {
                bool validFlag = ZnodeTokenHelper.CheckAuthHeader(authHeader[0], authHeader[1]);
                if (validFlag)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new StringResponse { Response = ZnodeTokenHelper.GenerateTokenKey(_authValue) });
                }
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, new StringResponse { Response = "Web API Key Not Found" });
        }       
    }
}
