﻿using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;

namespace Znode.Engine.Api
{
    public static class ApiHelper 
    {
        //Caches all active promotions in the application cache.
        public static void CacheActivePromotions() => ZnodePromotionManager.CacheActivePromotions();
    }
}
