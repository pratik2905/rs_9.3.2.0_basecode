﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api
{
    public struct APIConstant
    {
        public const string DefaultMediaClassName = "LocalAgent";
        public const string DefaultMediaServerName = "Local";
        public const string DefaultMediaFolder = "Data/Media";
        public const string ThumbnailFolderName = "Thumbnail";
        public const string TempImage = "TempImage";
    }
}