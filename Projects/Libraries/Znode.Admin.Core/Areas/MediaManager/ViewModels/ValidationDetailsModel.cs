﻿using System.Web.Mvc;

namespace Znode.Engine.Admin.ViewModels
{
    public class ValidationDetailsModel : BaseViewModel
    {
        public string LableName { get; set; }
        public MvcHtmlString ControlName { get; set; }
    }
}