﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Agents
{
    public interface IOrderAgent
    {
        /// <summary>
        /// Get order details by order id.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">SortCollection</param>
        /// <param name="pageIndex">Current index of page.</param>
        /// <param name="recordPerPage">record per page.</param>
        /// <param name="userId">userId</param>
        /// <param name="accountId">accountId</param>
        /// <param name="portalId">portalId</param>
        /// <param name="portalName">portalName</param>
        /// <returns>OrdersListViewModel</returns>
        OrdersListViewModel GetOrderList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null, int userId = 0, int accountId = 0, int portalId = 0, string portalName = null);

        /// <summary>
        /// Get list of Portals, Catalogs and Accounts on the basis of which create new order.
        /// </summary>
        /// <param name="portalId">Portal id</param>
        /// <returns>CreateOrderViewModel.</returns>
        CreateOrderViewModel GetCreateOrderDetails(int portalId = 0);

        /// <summary>
        /// Get the list of customer on the basis of portalId and accountId.
        /// </summary>
        /// <param name="portalId">Id of Portal</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Current Page Index</param>
        /// <param name="recordPerPage">Total Record count</param>
        /// <returns>CustomerListViewModel</returns>
        CustomerListViewModel GetCustomerList(int portalId, int accountId, bool isAccountCustomer, FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Get details of perticular user.
        /// </summary>
        /// <param name="portal">Id of portal.</param>
        /// <param name="userId">Id of user.</param>
        /// <returns>UserAddressDataViewModel</returns>
        UserAddressDataViewModel GetCustomerDetails(int portal, int userId);

        /// <summary>
        /// Create Update Customer Address.
        /// </summary>
        /// <param name="userAddressDataViewModel">UserAddressDataViewModel</param>
        /// <returns>UserAddressDataViewModel</returns>
        UserAddressDataViewModel CreateUpdateCustomerAddress(UserAddressDataViewModel userAddressDataViewModel);

        /// <summary>
        /// Get Country list to create new customer.
        /// </summary>
        /// <param name="portalId">Id of portal</param>
        /// <param name="userAddressDataViewModel">UserAddressDataViewModel</param>
        /// <returns>UserAddressDataViewModel</returns>
        UserAddressDataViewModel GetCountryList(int portalId, UserAddressDataViewModel userAddressDataViewModel = null);

        /// <summary>
        /// Get published product list.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">Current Page Index</param>
        /// <param name="pageSize">Total page size</param>
        /// <returns>PublishProductsListViewModel</returns>
        PublishProductsListViewModel GetPublishProducts(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get sorted customer list by name.
        /// </summary>
        /// <param name="portalId">current portal</param>
        /// <param name="customerName">searched customer name</param>
        /// <param name="isAccountCustomer">Is Account Customer</param>
        /// <returns></returns>
        List<CustomerViewModel> GetCustomerListByName(int portalId, string customerName, bool isAccountCustomer, int accountId);

        /// <summary>
        /// Sets the Selected Shipping Options and show the confirmation page details for the cart.
        /// </summary>
        /// <param name="cart">ShoppingCartModel</param>
        /// <param name="shippingAddress">AddressViewModel</param>
        /// <returns>Return the cart items, address as review order view model</returns>
        ReviewOrderViewModel GetCheckoutReview(ShoppingCartModel cart, AddressViewModel shippingAddress);

        /// <summary>
        /// Save user details in session.
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <param name="userId">user id</param>
        void SaveUserDetailsInSession(int portalId, int userId);

        /// <summary>
        /// Get calculated cart on shipping change.
        /// </summary>
        /// <param name="model">CreateOrderViewModel</param>
        /// <returns>ReviewOrderViewModel</returns>
        CartViewModel GetShippingChargesById(CreateOrderViewModel model);

        /// <summary>
        /// Get calculated cart on shipping change.
        /// </summary>
        /// <param name="UserId">UserId</param>
        /// <param name="ShippingId">ShippingId</param>
        /// <param name="omsOrderId">omsOrderId</param>
        /// <returns>CartViewModel</returns>
        CartViewModel GetShippingChargesForManage(int userId, int? shippingId, int? omsOrderId);

        /// <summary>
        /// Get user details by user id.
        /// </summary>
        /// <param name="userId">user Id</param>
        /// <returns>UserViewModel</returns>
        UserViewModel GetUserDetailsByUserId(int userId);

        /// <summary>
        /// Bind payment option list.
        /// </summary>
        /// <param name="userId">user id</param>
        ///  <param name="portalId">user id</param>
        /// <param name="paymentType">payment type name</param>
        /// <returns>List of payment option</returns>
        List<BaseDropDownOptions> BindPaymentList(int userId, int portalId = 0, string paymentType = "");

        /// <summary>
        /// Get User's full details like Shopping cart, shipping methods, payment option and review order.
        /// </summary>
        /// <param name="cartParameter">cartParameter model contains PortalId, UserId, CatalogId, CookieMappingId and LocaleId.</param>
        /// <returns>CreateOrderViewModel</returns>
        CreateOrderViewModel GetUserFullDetails(CartParameterModel cartParameter);

        /// <summary>
        /// Get Publish Product
        /// </summary>
        /// <param name="publishProductId">Publish Product Id</param>
        /// <param name="portalId">Portal Id</param>
        /// <returns>Publish Products View Model</returns>
        PublishProductsViewModel GetPublishProduct(int publishProductId, int localeId, int portalId, int userId, int catalogId);

        /// <summary>
        /// Get Bundle Products
        /// </summary>
        /// <param name="publishProductId">publish Product Id</param>
        /// <returns></returns>
        List<BundleProductViewModel> GetBundleProduct(int publishProductId);

        /// <summary>
        /// Submit the order.
        /// </summary>
        /// <param name="createOrderViewModel">CreateOrderViewModel</param>
        /// <returns>CreateOrderViewModel</returns>
        CreateOrderViewModel SubmitOrder(CreateOrderViewModel createOrderViewModel);

        /// <summary>
        /// Set portalCatalog id and portal Id as filter.
        /// </summary>
        /// <param name="model">FilterCollectionData Model</param>
        /// <param name="portalCatalogId">portal Catalog Id</param>
        /// <param name="portalId">portal Id</param>
        /// <param name="portalId">user Id</param>
        void SetProductListFilter(FilterCollectionDataModel model, int portalCatalogId, int portalId, int userId);

        /// <summary>
        /// Remove and add filter for customer name for search.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <param name="isAccountCustomer">Is Account Customer</param>
        void AddCustomerNameToFilterCollection(FilterCollectionDataModel model, bool isAccountCustomer);

        /// <summary>
        /// Get product price and inventory
        /// </summary>
        /// <param name="productSKU">product SKU</param>
        /// <param name="quantity">product quantity</param>
        /// <param name="addOnIds">addOn Ids</param>
        /// <param name="portalId">portal Id</param>
        /// <param name="parentProductId">parent product Id</param>
        /// <param name="omsOrderId">OMS Order Id</param>
        /// <returns>PublishProductsViewModel</returns>
        PublishProductsViewModel GetProductPriceAndInventory(string productSKU, string parentProductSKU, string quantity, string addOnIds, int portalId, int parentProductId, int omsOrderId = 0);

        /// <summary>
        /// Get Configurable product.
        /// </summary>
        /// <param name="parameters">model with parameter values.</param>
        /// <param name="codes">Attribute Codes</param>
        /// <param name="values">Attribute values</param>
        /// <returns>Return product view model.</returns>
        PublishProductsViewModel GetConfigurableProduct(ParameterProductModel parameters);

        /// <summary>
        /// Get ordered details of user for edit order.
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>CreateOrderViewModel</returns>
        CreateOrderViewModel EditOrder(int orderId, int userId = 0, int accountId = 0, string updatePageType = null);

        /// <summary>
        ///Get group product list.
        /// </summary>
        /// <param name="productId">product id.</param>
        /// <returns>list of associated products.</returns>
        List<GroupProductViewModel> GetGroupProductList(int productId, int localeId, int portalId, int userId, int? catalogId = 0);

        /// <summary>
        /// Get order review details.
        /// </summary>
        /// <param name="createOrderViewModel">CreateOrderViewModel</param>
        /// <returns>ReviewOrderViewModel</returns>
        ReviewOrderViewModel GetReviewOrder(CreateOrderViewModel createOrderViewModel);

        /// <summary>
        /// Update existing order.
        /// </summary>
        /// <param name="additionalNote">additionalNote</param>
        /// <returns>OrderViewModel</returns>
        OrderViewModel UpdateOrder(int orderId, string additionalNote);

        /// <summary>
        /// This method will use to call the payment and process the order
        /// </summary>
        /// <param name="submitPaymentViewModel">SubmitPaymentViewModel</param>
        /// <returns>Submit Order View Model</returns>
        SubmitOrderViewModel ProcessCreditCardPayment(SubmitPaymentViewModel submitPaymentViewModel, bool isPaypalExpressCheckout = false);

        /// <summary>
        /// To Process PayPal Express Checkout Payment
        /// </summary>
        /// <param name="paymentOptionId">int paymentOptionId</param>
        /// <param name="returnUrl">string returnUrl</param>
        /// <param name="cancelUrl">string cancelUrl</param>
        /// <returns>returns PayPal response</returns>
        List<string> ProcessPayPalCheckout(SubmitPaymentViewModel submitPaymentViewModel);

        /// <summary>
        /// Check Group Product Inventory
        /// </summary>
        /// <param name="productSKU">Product sku.</param>
        /// <param name="quantity">quantity selected</param>
        /// <param name="portalId">Portal Id.</param>
        /// <param name="localeId">Locale Id.</param>
        /// <returns>product view model</returns>
        GroupProductViewModel CheckGroupProductInventory(ProductParameterModel parameters, string productSKU, string quantity);

        /// <summary>
        /// Get order invoice details.
        /// </summary>
        /// <param name="orderIds">selected order ids</param>
        /// <returns>OrderInvoiceViewModel</returns>
        OrderInvoiceViewModel GetOrderInvoiceDetails(string orderIds);

        /// <summary>
        /// Capture payment
        /// </summary>
        /// <param name="OmsOrderId">OmsOrderId</param>
        /// <param name="paymentTransactionToken">paymentTransactionToken</param>
        /// <param name="isUpdateOrderHistory">true to update order history </param>
        /// <param name="errorMessage">out string error message.</param>
        /// <returns>Returns true if captured successfully else return false.</returns>
        bool CapturePayment(int OmsOrderId, string paymentTransactionToken, bool isUpdateOrderHistory, out string errorMessage);

        /// <summary>
        /// Void Payment
        /// </summary>
        /// <param name="OmsOrderId">OmsOrderId</param>
        /// <param name="paymentTransactionToken">paymentTransactionToken</param>
        /// <param name="errorMessage">out string error message.</param>
        /// <returns>Returns true if voided successfully else return false.</returns>
        bool VoidPayment(int OmsOrderId, string paymentTransactionToken, out string errorMessage);

        /// <summary>
        /// Get User Account Address By AddressId.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        /// <param name="isB2BCustomer">B2B customer flag</param>
        /// <param name="portalId">portal id</param>
        /// <returns>AddressViewModel</returns>
        AddressViewModel GetUserAccountAddressByAddressId(int addressId, bool isB2BCustomer, int portalId);

        /// <summary>
        /// Get User Address By addressId.
        /// </summary>
        /// <param name="addressId">address id.</param>
        /// <param name="userId">user id.</param>
        /// <param name="portalId">portal id</param>
        /// <returns>AddressViewModel</returns>
        AddressViewModel GetUserAddressById(int addressId, int userId, int portalId);

        /// <summary>
        /// Update Customer Address And Calculate.
        /// </summary>
        /// <param name="addressViewModel">AddressViewModel</param>
        /// <param name="IsManange">To know order call from manage or edit order</param>
        /// /// <param name="omsOrderId ">omsOrderId</param>
        /// <returns>CreateOrderViewModel</returns>
        CreateOrderViewModel UpdateCustomerAddressAndCalculate(AddressViewModel addressViewModel, bool isManage = false, int omsOrderId = 0);

        /// <summary>
        /// Adding additional notes into ordeModel
        /// </summary>
        /// <param name="model">OrderViewModel</param>
        bool ManageOrderNotes(OrderViewModel model);

        /// <summary>
        /// Submit quote for approval.
        /// </summary>
        /// <param name="createOrderViewModel">CreateOrderViewModel</param>
        /// <returns>Returns created order quote.</returns>
        CreateOrderViewModel SubmitQuote(CreateOrderViewModel createOrderViewModel);

        /// <summary>
        /// Manage order.
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>OrderViewModel</returns>
        OrderViewModel Manage(int orderId);

        /// <summary>
        /// Get order details.
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>OrderViewModel</returns>
        AddressViewModel GetorderdetailsById (int orderId, int shippingAddressId, int billingAddressId, string control, int portalId);

        /// <summary>
        /// Get Address Default Data.
        /// </summary>
        /// <param name="addressId">address Id</param>
        /// <param name="userId">user id</param>
        /// <param name="accountId">account id</param>
        /// <param name="portalId"> portal id</param>
        /// <returns>AddressViewModel</returns>
        AddressViewModel GetAddressDefaultData(int addressId, int userId, int accountId, int portalId);

        /// <summary>
        /// Get OrderLine Items With Refund payment left
        /// </summary>
        /// <param name="orderDetailsId">orderDetailsId</param>
        /// <returns>Refund Payment List View Model</returns>
        OrderItemsRefundViewModel GetOrderLineItemsWithRefund(int orderDetailsId);

        /// <summary>
        /// Add Refund Payment Details in admin and Refund the payment
        /// </summary>
        /// <param name="refundPaymentListViewModel">refundPaymentListViewModel</param>
        /// <param name="errorMessage">Error message is refund fails</param>
        /// <returns>Returns true if Refunded sucessfully else return false.</returns>
        bool AddRefundPaymentDetails(OrderItemsRefundViewModel refundPaymentListViewModel, out string errorMessage);

        /// <summary>
        /// Resend order confirmation email
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>Email message</returns>
        string ResendOrderConfirmationEmail(int orderId);

        /// <summary>
        /// Resend order confirmation email for singl cart item.
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="omsOrderLineItemId">omsOrderLineItemId</param>
        /// <returns>Email message</returns>
        string ResendOrderConfirmationEmailForCart(int orderId, int omsOrderLineItemId);

        /// <summary>
        /// Get Order Status Details.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        OrderStateParameterViewModel GetOrderStatusDetails(int orderId);

        /// <summary>
        /// Bind Order Status list.
        /// </summary>
        /// <returns>Order Status list</returns>
        List<SelectListItem> BindOrderStatus(FilterTuple filter = null);

        /// <summary>
        /// Update Order Status.
        /// </summary>
        /// <param name="model">OrderStateParameterViewModel</param>
        /// <returns>Returns true if updated sucessfully else return false.</returns>
        bool UpdateOrderStatus(OrderStateParameterViewModel model);

        /// <summary>
        /// Create quote for customer.
        /// </summary>
        /// <param name="userId">User id of the customer.</param>
        /// <param name="userId">Account id of the customer.</param>
        /// <returns>Returns CreateOrderViewModel.</returns>
        CreateOrderViewModel CreateQuoteForCustomer(int userId, int accountId);

        /// <summary>
        /// Add new order note.
        /// </summary>
        /// <param name="additionalNotes">additional Notes</param>
        /// <param name="omsOrderDetailsId">oms Order Details Id</param>
        /// <param name="omsOrderId">oms Order Id</param>
        /// <param name="omsQuoteId">oms Quote Id</param>
        /// <returns>List of OrderNotesViewModel </returns>
        List<OrderNotesViewModel> AddOrderNote(string additionalNotes, int? omsOrderDetailsId, int? omsQuoteId, int omsOrderId = 0);

        /// <summary>
        /// Get order data for receipt.
        /// </summary>
        /// <param name="omsOrderId">omsOrderId</param>
        /// <returns></returns>
        OrderViewModel GetDataForReceipt(int omsOrderId);

        /// <summary>
        /// Get PaymentGatway Name
        /// </summary>
        /// <param name="paymentSettingId">Payment Setting Id/param>
        /// <returns></returns>
        PaymentDetailsViewModel GetOrderPaymentDetails(int paymentSettingId);

        /// <summary>
        /// Get Attribute Validation By Codes
        /// </summary>
        /// <param name="attributecodes">Attribute Codes</param>
        /// <param name="productId">productId</param>
        /// <returns>PIMProductAttributeValuesViewModel list</returns>
        List<PIMProductAttributeValuesViewModel> GetAttributeValidationByCodes(Dictionary<string, string> personliseValues, int productId);

        /// <summary>
        /// Remove user details and cart from session.
        /// </summary>
        void RemoveUserDetailsFromSessions();

        /// <summary>
        /// Update order Payment Status.
        /// </summary>
        /// <param name="OmsOrderId">Order id</param>
        /// <param name="paymentstatus">Payment status</param>
        /// <returns>Return OrderViewModel</returns>
        OrderViewModel UpdateOrderPaymentStatus(int OmsOrderId, string paymentstatus);

        /// <summary>
        /// Update order Payment Status.
        /// </summary>
        /// <param name="TrackingNumber">Tracking Number</param>
        /// <returns>Return OrderViewModel</returns>
        string UpdateTrackingNumber(int orderId, string trackingNumber);

        /// <summary>
        /// Update order Shipping Account Number.
        /// </summary>
        /// <param name="ShippingAccountNumber">Shipping Account Number</param>
        /// <returns>Return OrderViewModel</returns>
        string UpdateShippingAccountNumber(int orderId, string shippingAccountNumber);

        /// <summary>
        /// Update order Shipping Method.
        /// </summary>
        /// <param name="ShippingMethod">ShippingMethod</param>
        /// <returns>Return OrderViewModel</returns>
        string UpdateShippingMethod(int orderId, string shippingMethod);

        /// <summary>
        /// Get Shipping options with Rates
        /// </summary>
        /// <returns>ShippingListViewModel</returns>
        ShippingListViewModel GetShippingListWithRates();

        /// <summary>
        /// Get Shipping options with Rates for manage
        /// </summary>
        /// <returns>ShippingListViewModel</returns>
        ShippingListViewModel GetShippingListForManange(int orderId);

        /// <summary>
        /// Get re ordered details of user for edit order.
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>CreateOrderViewModel</returns>
        CreateOrderViewModel ReOrder(int orderId, int userId = 0, int accountId = 0, string updatePageType = null);

        /// <summary>
        /// Get the list of publish products containing sku.
        /// </summary>
        /// <param name="sku">SKU to filter products.</param>
        /// <returns>List<AutoComplete> for products.</returns>
        List<AutoComplete> GetProductListBySKU(string sku);

        /// <summary>
        /// Get order related Information from session.
        /// </summary>
        /// <returns>OrderInfoViewModel</returns>
        OrderInfoViewModel GetOrderInformation(int orderId);

        /// <summary>
        /// Get Customer Information from session.
        /// </summary>
        /// <returns></returns>
        CustomerInfoViewModel GetCustomerInformation(int orderId);

        /// <summary>
        /// Get orderline items from session.
        /// </summary>
        /// <returns></returns>
        CartViewModel GetOrderLineItems(int orderId);

        /// <summary>
        ///  Get order history.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">SortCollection</param>
        /// <param name="pageIndex">Current index of page.</param>
        /// <param name="recordPerPage">record per page.</param>
        /// <param name="userId">userId</param>
        /// <param name="accountId">accountId</param>
        /// <returns>OrderHistoryListViewModel</returns>
        OrderHistoryListViewModel GetOrderHistory(int orderId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Update CSR Discount, Tax cost, shipping cost
        /// </summary>
        /// <param name="amount">amount to update</param>
        /// <param name="pagetype">tax/shipping/CSRDiscount to update.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel UpdateAmounts(int orderId, string amount, string pagetype);

        /// <summary>
        /// For binding order status list.
        /// </summary>
        /// <param name="omsOrderId">respective orderid</param>
        /// <param name="orderStatus">respective orderStatus</param>
        /// <param name="pageName">type of page</param>
        /// <returns></returns>
        OrderStatusList GetOrderStatus(int omsOrderId, string orderStatus, string pageName = null);

        /// <summary>
        /// For updating order session.
        /// </summary>
        /// <param name="OrderStatusList">depending on selection selecteditemid</param>
        /// <returns>OrderStatusList </returns>
        OrderStatusList UpdateOrderAndPaymentStatus(OrderStatusList orderStatus);

        /// <summary>
        /// For Maping and updating address
        /// </summary>
        /// <param name="createOrderViewModel">create Order View Model</param>
        /// <returns>Map CustomerInfoViewModel</returns>
        CustomerInfoViewModel MapAndUpdateCustomerAddress(CreateOrderViewModel createOrderViewModel);

        /// <summary>
        /// This method will use to call the payment and process the order
        /// </summary>
        /// <param name="submitPaymentViewModel">submitPaymentViewModel</param>
        /// <returns>Submit Order View Model</returns>
        SubmitOrderViewModel SubmitEditOrderpayment(SubmitPaymentViewModel submitPaymentViewModel);

        /// <summary>
        /// Updates the Quantity updated in the shopping cart page.
        /// </summary>
        /// <param name="orderDataModel">data to update.</param>
        /// <returns>Cart View Model.</returns>
        CartViewModel UpdateOrderLineItemDetails(ManageOrderDataModel orderDataModel);

        /// <summary>
        /// Get list of returned order line items.
        /// </summary>
        /// <param name="orderId">Id of Order,</param>
        /// <returns>ReturnOrderLineItemListViewModel</returns>
        ReturnOrderLineItemListViewModel GetReturnLineItems(int orderId);

        /// <summary>
        /// Get order state value by id
        /// </summary>
        /// <param name="omsOrderStateId">omsOrderStateId</param>
        /// <returns>isEdit</returns>
        bool GetOrderStateValueById(int omsOrderStateId);

        /// <summary>
        /// Send returned order mail.
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>true/false</returns>
        bool SendReturnedOrderEmail(int orderId);

        /// <summary>
        /// Get the list of return line items.
        /// </summary>
        /// <param name="orderId">Id of Order.</param>
        /// <returns>ReturnOrderLineItemListViewModel</returns>
        ReturnOrderLineItemListViewModel GetRetrunLineItemList(int orderId);

        /// <summary>
        /// Set TrackingUrl By omsOrderId.
        /// </summary>
        /// <param name="omsOrderId">omsOrderId</param>
        /// <returns></returns>
        void SetTrackingUrlByOrderId(int omsOrderId, List<CartItemViewModel> cartItemViewModel);

        /// <summary>
        /// Check the user is guest or not
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns>Depending on userid returns true or false</returns>
        bool CheckForGuestUser(int userId);

        /// <summary>
        /// Bind shipping list.
        /// </summary>
        /// <returns></returns>
        ShippingListViewModel BindShippingList();

        /// <summary>
        /// UpdateReturnShippingHistory in session
        /// </summary>
        /// <param name="omsOrderLineItemsId"></param>
        /// <param name="omsOrderId"></param>
        /// <param name="isInsert"></param>
        bool UpdateReturnShippingHistory(int omsOrderLineItemsId, int omsOrderId, bool isInsert);

        /// <summary>
        /// Send mail for Purchase order
        /// </summary>
        /// <param name="model">SendInvoiceViewModel</param>
        /// <returns></returns>
        bool SendPOEmail(SendInvoiceViewModel invoiceModel);

        /// <summary>
        /// Add custom shipping amount.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="estimateShippingCost"></param>
        /// <returns>true/false</returns>
        bool AddCustomShippingAmount(decimal? amount, decimal? estimateShippingCost);

        /// <summary>
        /// Get Payment API Header
        /// </summary>
        /// <returns>API Header</returns>
        AjaxHeadersModel GetPaymentAPIHeader();

        /// <summary>
        /// Generate order number.
        /// </summary>
        /// <param name="portalId">Id of portal.</param>
        /// <returns>Order Number</returns>
        string GenerateOrderNumber(int portalId);

        /// <summary>
        //Generate Single Order Line Item 
        /// </summary>
        /// <param name="orderModel"></param>
        /// <param name="orderLineItemListModel"></param>
        void CreateSingleOrderLineItem(OrderViewModel orderModel, List<OrderLineItemViewModel> orderLineItemListModel);
    }
}