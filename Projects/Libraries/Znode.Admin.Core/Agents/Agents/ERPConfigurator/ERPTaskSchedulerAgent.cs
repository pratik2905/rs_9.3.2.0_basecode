﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Maps;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    public class ERPTaskSchedulerAgent : BaseAgent, IERPTaskSchedulerAgent
    {
        #region Private Variables
        private readonly IERPTaskSchedulerClient _erpTaskSchedulerClient;
        private readonly IERPConfiguratorAgent _erpConfiguratorAgent;
        #endregion

        #region Constructor
        public ERPTaskSchedulerAgent(IERPTaskSchedulerClient erpTaskSchedulerClient)
        {
            _erpTaskSchedulerClient = GetClient<IERPTaskSchedulerClient>(erpTaskSchedulerClient);
            _erpConfiguratorAgent = new ERPConfiguratorAgent(GetClient<ERPConfiguratorClient>());
        }
        #endregion

        #region public virtual Methods
        //Get the list of ERP Task Scheduler.
        public virtual ERPTaskSchedulerListViewModel GetERPTaskSchedulerList(ExpandCollection expands, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters expands,filters and sorts:", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, new { expands = expands, sorts = sorts, filters = filters });
            ERPTaskSchedulerListModel erpTaskSchedulerList = _erpTaskSchedulerClient.GetERPTaskSchedulerList(expands, filters, sorts, pageIndex, pageSize);
            ERPTaskSchedulerListViewModel listViewModel = new ERPTaskSchedulerListViewModel { ERPTaskSchedulerList = erpTaskSchedulerList?.ERPTaskSchedulerList?.ToViewModel<ERPTaskSchedulerViewModel>().ToList() };
            SetListPagingData(listViewModel, erpTaskSchedulerList);

            //Set tool menu for ERP Task Scheduler list grid view.
            SetERPTaskSchedulerListToolMenus(listViewModel);
            return erpTaskSchedulerList?.ERPTaskSchedulerList?.Count > 0 ? listViewModel : new ERPTaskSchedulerListViewModel() { ERPTaskSchedulerList = new List<ERPTaskSchedulerViewModel>() };
        }

        //Get erpTaskScheduler by erpTaskScheduler id.
        public virtual ERPTaskSchedulerViewModel GetERPTaskScheduler(int erpTaskSchedulerId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ERPTaskSchedulerViewModel erpTaskSchedulerViewModel = new ERPTaskSchedulerViewModel();
            erpTaskSchedulerViewModel = _erpTaskSchedulerClient.GetERPTaskScheduler(erpTaskSchedulerId).ToViewModel<ERPTaskSchedulerViewModel>();
            erpTaskSchedulerViewModel = BindDropdown(erpTaskSchedulerViewModel);

            if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Weekly)
            {
                erpTaskSchedulerViewModel.DayWeekDisplayName = ERP_Resources.LableWeeks;
                erpTaskSchedulerViewModel.ERPTaskSchedulerWeekDaysList = GetERPTaskSchedulerWeekDaysList();
                erpTaskSchedulerViewModel.WeekDaysArray = erpTaskSchedulerViewModel.WeekDays?.Split(',');
            }
            else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Monthly)
            {
                erpTaskSchedulerViewModel = BindMonthsPartialDropdown(erpTaskSchedulerViewModel);
                erpTaskSchedulerViewModel.DaysArray = erpTaskSchedulerViewModel.Days?.Split(',');
                erpTaskSchedulerViewModel.WeekDaysArray = erpTaskSchedulerViewModel.WeekDays?.Split(',');
                erpTaskSchedulerViewModel.MonthsArray = erpTaskSchedulerViewModel.Months?.Split(',');
                erpTaskSchedulerViewModel.OnDaysArray = erpTaskSchedulerViewModel.OnDays?.Split(',');
            }
            else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Daily)
                erpTaskSchedulerViewModel.DayWeekDisplayName = ERP_Resources.LableDays;

            if (erpTaskSchedulerViewModel.RepeatTaskEvery != 0)
                erpTaskSchedulerViewModel.IsRepeatTaskEvery = true;
            erpTaskSchedulerViewModel.StartTime = GetseparatedTime(Convert.ToDateTime(erpTaskSchedulerViewModel.StartDate));
            erpTaskSchedulerViewModel.ExpireTime = GetseparatedTime(Convert.ToDateTime(erpTaskSchedulerViewModel.ExpireDate));
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return erpTaskSchedulerViewModel;
        }

        //Create ERP Task Scheduler.
        public virtual ERPTaskSchedulerViewModel Create(ERPTaskSchedulerViewModel erpTaskSchedulerViewModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            try
            {
                if ((!erpTaskSchedulerViewModel.IsAssignTouchPoint && erpTaskSchedulerViewModel.SchedulerType == ZnodeConstant.Scheduler)
                    || erpTaskSchedulerViewModel.SchedulerCallFor == ZnodeConstant.SearchIndex || erpTaskSchedulerViewModel.SchedulerCallFor == ZnodeConstant.ProductFeed)
                {
                    if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Weekly)
                        erpTaskSchedulerViewModel.WeekDays = string.Join(",", erpTaskSchedulerViewModel.WeekDaysArray);
                    else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Monthly)
                    {
                        erpTaskSchedulerViewModel.Months = !Equals(erpTaskSchedulerViewModel.MonthsArray, null) ? string.Join(",", erpTaskSchedulerViewModel.MonthsArray) : string.Empty;
                        if (erpTaskSchedulerViewModel.IsMonthlyDays)
                            erpTaskSchedulerViewModel.Days = !Equals(erpTaskSchedulerViewModel.DaysArray, null) ? string.Join(",", erpTaskSchedulerViewModel.DaysArray) : string.Empty;
                        else
                        {
                            erpTaskSchedulerViewModel.OnDays = !Equals(erpTaskSchedulerViewModel.OnDaysArray, null) ? string.Join(",", erpTaskSchedulerViewModel.OnDaysArray) : string.Empty;
                            erpTaskSchedulerViewModel.WeekDays = !Equals(erpTaskSchedulerViewModel.WeekDaysArray, null) ? string.Join(",", erpTaskSchedulerViewModel.WeekDaysArray) : string.Empty;
                        }
                    }
                    erpTaskSchedulerViewModel.SchedulerType = !string.IsNullOrEmpty(erpTaskSchedulerViewModel.SchedulerType) ? erpTaskSchedulerViewModel.SchedulerType : ZnodeConstant.Scheduler;
                    erpTaskSchedulerViewModel.StartDate = GetDateTime(Convert.ToDateTime(erpTaskSchedulerViewModel.StartDate), Convert.ToDateTime(erpTaskSchedulerViewModel.StartTime));
                    erpTaskSchedulerViewModel.RepeatTaskEvery = erpTaskSchedulerViewModel.RepeatTaskEvery != 0 ? erpTaskSchedulerViewModel.RepeatTaskEvery : 60;
                    erpTaskSchedulerViewModel.RepeatTaskForDuration = !string.IsNullOrEmpty(erpTaskSchedulerViewModel.RepeatTaskForDuration) ? erpTaskSchedulerViewModel.RepeatTaskForDuration : "1440m";
                }
                if (!erpTaskSchedulerViewModel.IsAssignTouchPoint)
                {
                    if (!Equals(erpTaskSchedulerViewModel.ExpireDate, null))
                        erpTaskSchedulerViewModel.ExpireDate = GetDateTime(Convert.ToDateTime(erpTaskSchedulerViewModel.ExpireDate), Convert.ToDateTime(erpTaskSchedulerViewModel.ExpireTime));
                    else
                        erpTaskSchedulerViewModel.ExpireDate = Convert.ToDateTime("2029-12-31 00:00:00");

                    if (erpTaskSchedulerViewModel.SchedulerType != ZnodeConstant.Scheduler)
                        erpTaskSchedulerViewModel.SchedulerFrequency = ZnodeConstant.OneTime;
                }

                switch (erpTaskSchedulerViewModel.SchedulerCallFor)
                {
                    case ZnodeConstant.ERP:
                        erpTaskSchedulerViewModel.SchedulerCallFor = ZnodeConstant.ERP;
                        break;
                    case ZnodeConstant.SearchIndex:
                        erpTaskSchedulerViewModel.SchedulerCallFor = ZnodeConstant.SearchIndex;
                        break;
                    case ZnodeConstant.ProductFeed:
                        erpTaskSchedulerViewModel.SchedulerCallFor = ZnodeConstant.ProductFeed;
                        break;
                    case ZnodeConstant.PublishCatalog:
                        erpTaskSchedulerViewModel.SchedulerCallFor = ZnodeConstant.PublishCatalog;
                        break;
                }

                ERPTaskSchedulerModel erpTaskSchedulerModel = _erpTaskSchedulerClient.Create(erpTaskSchedulerViewModel.ToModel<ERPTaskSchedulerModel>());
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return IsNotNull(erpTaskSchedulerModel) ? erpTaskSchedulerModel.ToViewModel<ERPTaskSchedulerViewModel>() : new ERPTaskSchedulerViewModel();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.AlreadyExistSchedulerName);
                    default:
                        if (erpTaskSchedulerViewModel.ERPTaskSchedulerId == 0)
                            return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, Admin_Resources.ErrorFailedToCreate);
                        else
                            return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, Admin_Resources.UpdateError);
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, Admin_Resources.UpdateError);
            }
            finally
            {
                if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Monthly)
                    erpTaskSchedulerViewModel = BindMonthsPartialDropdown(erpTaskSchedulerViewModel);
                else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Weekly)
                    erpTaskSchedulerViewModel.ERPTaskSchedulerWeekDaysList = GetERPTaskSchedulerWeekDaysList();

            }
        }

        //Delete erpTaskScheduler.
        public virtual bool Delete(string erpTaskSchedulerId, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            errorMessage = Admin_Resources.ErrorFailedToDelete;
            try
            {
                return _erpTaskSchedulerClient.Delete(new ParameterModel { Ids = erpTaskSchedulerId });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AssociationDeleteError:
                        errorMessage = ERP_Resources.ErrorDeleteERPTaskScheduler;
                        return false;
                    default:
                        errorMessage = Admin_Resources.ErrorFailedToDelete;
                        return false;
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                errorMessage = Admin_Resources.ErrorFailedToDelete;
                return false;
            }
        }

        //Bind Dropdowns.
        public virtual ERPTaskSchedulerViewModel BindDropdown(ERPTaskSchedulerViewModel erpTaskSchedulerViewModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            erpTaskSchedulerViewModel.RepeatTaskForDurationList = GetRepeatTaskForDurationList();
            erpTaskSchedulerViewModel.RepeatTaskEveryList = GetRepeatTaskEveryList();

            if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Weekly)
                erpTaskSchedulerViewModel.DayWeekDisplayName = ERP_Resources.LableWeeks;
            else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Daily)
                erpTaskSchedulerViewModel.DayWeekDisplayName = ERP_Resources.LableDays;
            return erpTaskSchedulerViewModel;
        }

        //Bind Month partial view Dropdowns.
        public virtual ERPTaskSchedulerViewModel BindMonthsPartialDropdown(ERPTaskSchedulerViewModel erpTaskSchedulerViewModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            erpTaskSchedulerViewModel.ERPTaskSchedulerDaysList = GetERPTaskSchedulerDaysList();
            erpTaskSchedulerViewModel.ERPTaskSchedulerWeekDaysList = GetERPTaskSchedulerWeekDaysList();
            erpTaskSchedulerViewModel.ERPTaskSchedulerMonthsList = GetERPTaskSchedulerMonthsList();
            erpTaskSchedulerViewModel.ERPTaskSchedulerOnDaysList = GetERPTaskSchedulerOnDaysList();
            return erpTaskSchedulerViewModel;
        }

        //Get ERP Task Scheduler WeekDay List. 
        public virtual List<SelectListItem> GetERPTaskSchedulerWeekDaysList() => ERPTaskSchedulerViewModelMap.GetERPTaskSchedulerWeekDays();

        //Method for  ERPTaskSchedulerId From Touch point name
        public virtual int GetSchedulerIdByTouchPointName(string erpTouchPointName, string schedulerCallFor)
           => _erpTaskSchedulerClient.GetSchedulerIdByTouchPointName(erpTouchPointName, schedulerCallFor);

        //Enable/disable ERP task scheduler from windows service.
        public virtual bool EnableDisableTaskScheduler(int ERPTaskSchedulerId, bool isActive, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            errorMessage = Admin_Resources.ErrorMessageEnableDisable;
            try
            {
                return _erpTaskSchedulerClient.EnableDisableTaskScheduler(ERPTaskSchedulerId, isActive);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.NotFound:
                        errorMessage = ERP_Resources.ErrorTaskSchedulerNotFound;
                        return false;
                    default:
                        errorMessage = Admin_Resources.ErrorMessageEnableDisable;
                        return false;
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                errorMessage = Admin_Resources.ErrorMessageEnableDisable;
                return false;
            }
        }

        //Delete erpTaskScheduler.
        public virtual ERPTaskSchedulerViewModel CheckValidation(ERPTaskSchedulerViewModel erpTaskSchedulerViewModel, out bool status)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            status = false;
            ZnodeLogging.LogMessage("ERPTaskSchedulerViewModel having ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info, new { SchedulerFrequency = erpTaskSchedulerViewModel?.SchedulerFrequency});
            if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Daily && erpTaskSchedulerViewModel.RecurEvery > 365)
                return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorRecurEveryForDaily);
            else if (erpTaskSchedulerViewModel.StartDate?.Date < DateTime.Today.Date)
                return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorStartDateGreaterThan);
            else if (erpTaskSchedulerViewModel.ExpireDate?.Date < DateTime.Now.Date)
                return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorExpireDateGreaterThan);
            else if (erpTaskSchedulerViewModel.StartDate?.Date > erpTaskSchedulerViewModel.ExpireDate?.Date)
                return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorStartDateExpireDateGreaterThan);
            else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Weekly)
            {
                if (erpTaskSchedulerViewModel.RecurEvery > 52)
                    return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorRecurEveryForWeekly);
                else if ((IsNull(erpTaskSchedulerViewModel?.WeekDaysArray)) || (erpTaskSchedulerViewModel?.WeekDaysArray?.Count() == 1 && string.IsNullOrEmpty(erpTaskSchedulerViewModel?.WeekDaysArray[0])))
                    return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorSelectWeekDays);
                else
                {
                    status = true;
                    return erpTaskSchedulerViewModel;
                }
            }
            else if (erpTaskSchedulerViewModel?.SchedulerFrequency == ZnodeConstant.Monthly)
            {
                if ((erpTaskSchedulerViewModel?.MonthsArray?.Length == 0 || IsNull(erpTaskSchedulerViewModel?.MonthsArray)) || (erpTaskSchedulerViewModel?.MonthsArray?.Count() == 1 && string.IsNullOrEmpty(erpTaskSchedulerViewModel?.MonthsArray[0])))
                    return (ERPTaskSchedulerViewModel)GetViewModelWithErrorMessage(erpTaskSchedulerViewModel, ERP_Resources.ErrorSelectMonths);
                else
                {
                    status = true;
                    return erpTaskSchedulerViewModel;
                }
            }
            else
            {
                status = true;
                return erpTaskSchedulerViewModel;
            }
        }

        //Set task scheduler data.
        public virtual ERPTaskSchedulerViewModel SetTaskSchedulerData(string ConnectorTouchPoints, string indexName, string schedulerCallFor, int portalId, int catalogId, int portalIndexId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ERPTaskSchedulerViewModel erpTaskSchedulerViewModel = new ERPTaskSchedulerViewModel();
            erpTaskSchedulerViewModel.ERPClassName = _erpConfiguratorAgent.GetERPClassName();
            erpTaskSchedulerViewModel.SchedulerFrequency = ZnodeConstant.OneTime;
            erpTaskSchedulerViewModel.SchedulerType = ZnodeConstant.Scheduler;
            erpTaskSchedulerViewModel.TouchPointName = ConnectorTouchPoints;
            erpTaskSchedulerViewModel.IsEnabled = true;
            erpTaskSchedulerViewModel.RepeatTaskEvery = 60;
            erpTaskSchedulerViewModel.RepeatTaskForDuration = "1440m";
            erpTaskSchedulerViewModel = BindDropdown(erpTaskSchedulerViewModel);

            erpTaskSchedulerViewModel.SchedulerCallFor = schedulerCallFor;
            erpTaskSchedulerViewModel.PortalId = portalId;
            erpTaskSchedulerViewModel.CatalogId = catalogId;
            erpTaskSchedulerViewModel.IndexName = indexName;
            erpTaskSchedulerViewModel.CatalogIndexId = portalIndexId;
            return erpTaskSchedulerViewModel;
        }

        //Get task scheduler data for update.
        public virtual ERPTaskSchedulerViewModel GetTaskSchedulerDataForUpdate(int erpTaskSchedulerId, string indexName, string schedulerCallFor, int portalId,int catalogId, int portalIndexId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ERPTaskSchedulerViewModel erpTaskSchedulerViewModel = new ERPTaskSchedulerViewModel();
            erpTaskSchedulerViewModel = GetERPTaskScheduler(erpTaskSchedulerId);
            erpTaskSchedulerViewModel.ERPClassName = _erpConfiguratorAgent.GetERPClassName();
            erpTaskSchedulerViewModel.RepeatTaskEvery = erpTaskSchedulerViewModel.RepeatTaskEvery != 0 ? erpTaskSchedulerViewModel.RepeatTaskEvery : 60;
            erpTaskSchedulerViewModel.RepeatTaskForDuration = !string.IsNullOrEmpty(erpTaskSchedulerViewModel.RepeatTaskForDuration) ? erpTaskSchedulerViewModel.RepeatTaskForDuration : "1440m";
            erpTaskSchedulerViewModel.SchedulerCallFor = string.IsNullOrEmpty(schedulerCallFor) ? erpTaskSchedulerViewModel.SchedulerCallFor : schedulerCallFor;
            erpTaskSchedulerViewModel.PortalId = portalId;
            erpTaskSchedulerViewModel.CatalogId = catalogId;
            erpTaskSchedulerViewModel.IndexName = indexName;
            erpTaskSchedulerViewModel.CatalogIndexId = portalIndexId;
            return erpTaskSchedulerViewModel;
        }
        #endregion

        #region Private Methods
        //Method for Set ERP TaskScheduler List Tool Menus
        private void SetERPTaskSchedulerListToolMenus(ERPTaskSchedulerListViewModel model)
        {
            if (IsNotNull(model))
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('ERPTaskSchedulerDeletePopup')", ControllerName = "ERPTaskScheduler", ActionName = "Delete" });
            }
        }

        //Get Repeat Task Every List. 
        private List<SelectListItem> GetRepeatTaskEveryList() => ERPTaskSchedulerViewModelMap.GetRepeatTaskEvery();

        //Get Repeat Task For Duration List. 
        private List<SelectListItem> GetRepeatTaskForDurationList() => ERPTaskSchedulerViewModelMap.GetRepeatTaskForDurations();

        //Get ERP Task Scheduler Days List. 
        private List<SelectListItem> GetERPTaskSchedulerDaysList() => ERPTaskSchedulerViewModelMap.GetERPTaskSchedulerDays();

        //Get ERP Task Scheduler Months List. 
        private List<SelectListItem> GetERPTaskSchedulerMonthsList() => ERPTaskSchedulerViewModelMap.GetERPTaskSchedulerMonths();

        //Get ERP Task Scheduler OnDays List. 
        private List<SelectListItem> GetERPTaskSchedulerOnDaysList() => ERPTaskSchedulerViewModelMap.GetERPTaskSchedulerOnDays();

        //Return Concated Date and time
        private DateTime GetDateTime(DateTime date, DateTime time) => (date.Date + time.TimeOfDay);

        //Return separate Time
        private string GetseparatedTime(DateTime dateTime) => (Convert.ToString(dateTime.TimeOfDay));
        #endregion
    }
}