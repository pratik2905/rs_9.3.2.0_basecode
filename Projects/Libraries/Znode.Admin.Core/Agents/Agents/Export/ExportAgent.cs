﻿using System.Collections.Generic;
using System.Web;
using Znode.Engine.Admin.Helpers;
using System.Dynamic;
using System.Linq;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Admin.ViewModels;
using System.Web.Mvc;
using System.Diagnostics;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Agents
{
    public class ExportAgent : BaseAgent, IExportAgent
    {
        #region Private Variables
        private IProductAgent _productAgent;
        private IMediaManagerAgent _mediaAgent;
        #endregion

        #region Public Method
        //Get Export Data List
        public virtual List<dynamic> GetExportList(string type, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int folderId = 0, int localId = 0, int pimCatalogId = 0, string catalogName = null)
        {
			ZnodeLogging.LogMessage("Agent method execution started.", string.Empty, TraceLevel.Info);
			ZnodeLogging.LogMessage("Input parameters :", string.Empty, TraceLevel.Verbose, new { filters = filters, sorts = sorts });
			List<dynamic> exportList = new List<dynamic>();
            switch (type)
            {
                case AdminConstants.Product:
                    _productAgent = DependencyResolver.Current.GetService<IProductAgent>();
                    ProductDetailsListViewModel productListModel = new ProductDetailsListViewModel() { TotalResults = 1 };
                    //PageIndex = 1 to get all product details by pageIndex
                    exportList.AddRange(GetAllProductList(productListModel, filters, sorts, 1, localId, pimCatalogId, catalogName));
                    break;
                case AdminConstants.Media:
                    _mediaAgent = DependencyResolver.Current.GetService<IMediaManagerAgent>();
                    MediaManagerListViewModel mediaManagerViewModel = _mediaAgent.GetMedias(filters, sorts, null, null, folderId);
                    exportList.AddRange(mediaManagerViewModel.MediaList);
                    break;
                default:
                    break;
            }
			ZnodeLogging.LogMessage("Agent method execution done.", string.Empty, TraceLevel.Info);
			return exportList;
        }

        //Create data source.
        public virtual List<dynamic> CreateDataSource(List<dynamic> dataModel)
        {
			ZnodeLogging.LogMessage("Agent method execution started.", string.Empty, TraceLevel.Info);

			List<dynamic> xmlConfigurableList = GetFromSession<List<dynamic>>(DynamicGridConstants.ColumnListSesionKey);
            List<dynamic> _sortedXmlList = GetFilteredXmlList(xmlConfigurableList);
            List<dynamic> _resultList = new List<dynamic>();

            dataModel.ForEach(row =>
            {
                IDictionary<string, object> columnObject = new ExpandoObject();
                _sortedXmlList.ForEach(col =>
                {
                    columnObject.Add(col.headertext, GetColumnData(row, col));
                });
                _resultList.Add(columnObject);
            });

			ZnodeLogging.LogMessage("Agent method execution done.", string.Empty, TraceLevel.Info);
			return _resultList;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get All Product List
        /// </summary>
        /// <param name="productListModel">ProductListModel</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <returns> </returns>
        private IEnumerable<dynamic> GetAllProductList(ProductDetailsListViewModel productListModel, FilterCollection filters, SortCollection sorts, int? pageIndex, int localId, int pimCatalogId = 0, string catalogName = null)
        {
            List<dynamic> exportList = new List<dynamic>();
            // Get the record from Product agent
            exportList = _productAgent.GetExportProductList(filters, sorts, pageIndex, ZnodeAdminSettings.ProductExportChunkSize, localId);
            return exportList;
        }
        /// <summary>
        /// Get Column Data by type
        /// </summary>
        /// <param name="row">row element</param>
        /// <param name="col">col element</param>
        /// <returns>row value</returns>
        private object GetColumnData(dynamic row, dynamic col)
        {
            if (HelperUtility.IsNotNull(row.GetType().GetProperty(col.name)))
                return row.GetType().GetProperty(col.name).GetValue(row, null);

            return row[col.name];
        }
        /// <summary>
        /// Get Filtered XmlList
        /// </summary>
        /// <param name="XMLConfigurableList">XmlConfigurationList</param>
        /// <returns>Filtered XmlConfigurationList</returns>
        private List<dynamic> GetFilteredXmlList(List<dynamic> XMLConfigurableList)
        {
            string[] excludedColumn = { "Action", "Checkbox" };
            return XMLConfigurableList?.FindAll(x => x.isvisible.Equals(DynamicGridConstants.Yes) && !excludedColumn.Contains((string)x.headertext)
                                               && !x.name.Equals("Image") && (!HelperUtility.IsPropertyExist(x, "isattributecolumn") || !x.isattributecolumn.Equals(DynamicGridConstants.Yes))
                                                );
        }
        #endregion


    }
}
