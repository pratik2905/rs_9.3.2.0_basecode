﻿using System.Web.Mvc;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Admin.Agents;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Controllers
{
    public class RecommendationController : BaseController
    {
        #region Private Variables
        private readonly IRecommendationAgent _recommendationAgent;
        private const string _ManagePartialView = "_RecommendationSettingPartial";
        #endregion

        #region Constructors
        public RecommendationController(IRecommendationAgent recommendationAgent)
        {
            _recommendationAgent = recommendationAgent;
        }
        #endregion

        #region Public Methods
        public virtual ActionResult GetRecommendationSetting(int portalId)
        {
            //Get the available WebSite Logo details.
            RecommendationSettingViewModel model = _recommendationAgent.GetRecommendationSetting(portalId);
            if (model?.PortalId > 0)
            {
                return ActionView(_ManagePartialView, model);
            }
            //To show message box on store experience list page.
            SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorPortalNotFound));
            return RedirectToAction<Znode.Admin.Core.Controllers.StoreExperienceController>(x => x.List(null));
        }

        [HttpPost]
        public virtual ActionResult SaveRecommendationSetting(RecommendationSettingViewModel model)
        {
            ActionResult action = GotoBackURL();
            if (ModelState.IsValid)
            {
                RecommendationSettingViewModel recommendationSettingsModel = _recommendationAgent.SaveRecommendationSetting(model);
                string message = IsNotNull(recommendationSettingsModel) && !recommendationSettingsModel.HasError ? Admin_Resources.UpdateMessage : Admin_Resources.UpdateErrorMessage;

                //To show message box on store experience list page after click on save & close.
                if (IsNotNull(action))
                {
                    SetNotificationMessage(IsNotNull(recommendationSettingsModel) && !recommendationSettingsModel.HasError ? GetSuccessNotificationMessage(Admin_Resources.UpdateMessage) : GetErrorNotificationMessage(Admin_Resources.UpdateErrorMessage));
                    message = "";
                }

                return Json(new { status = IsNotNull(recommendationSettingsModel) && !recommendationSettingsModel.HasError, message = message, Url = IsNull(action) ? "" : ((RedirectResult)action).Url }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //To show message box on store experience list page after click on save & close.
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.UpdateErrorMessage));
                return Json(new { status = false, Url = IsNull(action) ? "" : ((RedirectResult)action).Url }, JsonRequestBehavior.AllowGet);
            }            
        }
        #endregion
    }
}
