﻿using System;
using System.Net;
using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Controllers
{
    public class QuoteController : BaseController
    {
        #region Private ReadOnly members
        private readonly IAccountQuoteAgent _accountQuoteAgent;
        private readonly IOrderAgent _orderAgent;
        private readonly ICartAgent _cartAgent;
        #endregion

        #region Public Constructor
        public QuoteController(IOrderAgent orderAgent, IAccountQuoteAgent accountQuoteAgent, ICartAgent cartAgent)
        {
            _orderAgent = orderAgent;
            _accountQuoteAgent = accountQuoteAgent;
            _cartAgent = cartAgent;
        }
        #endregion

        #region Account Quote
        //Get account note list.
        public virtual ActionResult AccountQuoteList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeOmsQuote.ToString(), model);
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeOmsQuote.ToString(), model);
            AccountQuoteListViewModel quoteList = _accountQuoteAgent.GetAccountQuoteList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage, 0, 0);
            quoteList.GridModel = FilterHelpers.GetDynamicGridModel(model, quoteList?.AccountQuotes, GridListType.ZnodeOmsQuote.ToString(), string.Empty, null, true, true, null);
            quoteList.GridModel.TotalRecordCount = quoteList.TotalResults;
            return ActionView(quoteList);
        }

        // Checkout receipt after successfull order submit.
        [HttpPost]
        [ValidateInput(false)]
        public virtual ActionResult CheckoutReceipt(int orderId, string ReceiptHtml, bool? IsEmailSend)
        {
            //if order is not present then redirect to list page.
            if (orderId > 0)
            {
                CreateOrderViewModel model = new CreateOrderViewModel();
                //Decode the checkout receipt html.
                model.ReceiptHtml = WebUtility.HtmlDecode(ReceiptHtml);

                //Set order receipt Message if unable to send email receipt.
                if (!Convert.ToBoolean(IsEmailSend))
                    SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorFailedToSendOrderReceipt));

                return View("CheckoutReceipt", model);
            }
            return RedirectToAction<QuoteController>(x => x.AccountQuoteList(null));
        }

        // The index page for create quote.
        [HttpGet]
        public virtual ActionResult CreateQuote(int portalId = 0)
        {
            //Remove all cart items.
            _cartAgent.RemoveAllCartItems();

            CreateOrderViewModel createQuote = _orderAgent.GetCreateOrderDetails(portalId);
            createQuote.IsQuote = true;

            return ActionView(createQuote);
        }

        //Get Pending Payment List
        public virtual ActionResult PendingPaymentList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeOmsPendingPayment.ToString(), model);
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeOmsPendingPayment.ToString(), model);

            AccountQuoteListViewModel quoteList = _accountQuoteAgent.GetAccountQuoteList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage, 0, 0, true);
            quoteList.GridModel = FilterHelpers.GetDynamicGridModel(model, quoteList?.AccountQuotes, GridListType.ZnodeOmsPendingPayment.ToString(), string.Empty, null, true, true, quoteList?.GridModel?.FilterColumn?.ToolMenuList);
            quoteList.GridModel.TotalRecordCount = quoteList.TotalResults;
            return ActionView(quoteList);
        }

        //Convert To Order
        [HttpPost]
        public virtual ActionResult ConvertToOrder(AccountQuoteViewModel accountQuoteViewModel, int omsQuoteId = 0)
        {
            if (omsQuoteId > 0)
                accountQuoteViewModel.OmsQuoteId = omsQuoteId;
            OrderViewModel order = _accountQuoteAgent.ConvertToOrder(accountQuoteViewModel);

            if (HelperUtility.IsNull(order))
            {
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorUnablePlaceOrder));                
            }
            if (!order.HasError)
            {
                order.ReceiptHtml = WebUtility.HtmlEncode(order.ReceiptHtml);
                return new JsonResult { Data = order, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            if (!Equals(order, null) && order.HasError)
            {
                if (string.IsNullOrEmpty(order.ErrorMessage))
                    order.ErrorMessage = Admin_Resources.OrderSubmitError;
                SetNotificationMessage(GetErrorNotificationMessage(order.ErrorMessage));                
            }
            return RedirectToAction<QuoteController>(x => x.AccountQuoteList(null));
        }
        #endregion
    }
}


