﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;

namespace Znode.Engine.Admin.Controllers
{
    public class TypeaheadController : BaseController
    {
        #region Private ReadOnly members

        private readonly ITypeaheadAgent _typeaheadAgent;

        #endregion

        #region Constructor

        public TypeaheadController(ITypeaheadAgent typeahead)
        {
            _typeaheadAgent = typeahead;
        }
        #endregion

        #region Public methods

        //Get Suggestions.
        [HttpGet]
        public virtual JsonResult GetSuggestions(string type, string fieldname, string query, string additionalOptions = null)
        => Json(_typeaheadAgent.GetAutocompleteList(query, type, fieldname, additionalOptions), JsonRequestBehavior.AllowGet);        
        #endregion
    }
}
