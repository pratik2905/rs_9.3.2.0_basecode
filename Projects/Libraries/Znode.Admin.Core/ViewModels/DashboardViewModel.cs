﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.Admin.ViewModels
{
    public class DashboardViewModel : BaseViewModel
    {
        public List<SelectListItem> Portal { get; set; }
        public int PortalId { get; set; }

        public List<SelectListItem> Duration { get; set; }
    }
}