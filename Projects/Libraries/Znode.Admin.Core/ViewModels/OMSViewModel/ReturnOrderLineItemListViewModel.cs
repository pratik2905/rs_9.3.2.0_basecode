﻿using System.Collections.Generic;

namespace Znode.Engine.Admin.ViewModels
{
    public class ReturnOrderLineItemListViewModel : BaseViewModel
    {
        public List<ReturnOrderLineItemViewModel> ReturnItemList { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TaxCost { get; set; }
        public string ShippingCost { get; set; }
        public decimal Total { get; set; }
        public string CurrencyCode { get; set; }
        public string CultureCode { get; set; }
    }
}
