﻿namespace Znode.Engine.Admin.ViewModels
{
    public class LBDetailsViewModel : BaseViewModel
    {
        public bool IsAPILBActive { get; set; }
        public string APILBDetails { get; set; }
        public bool IsWebstoreLBActive { get; set; }
        public string WebstoreLBDetails { get; set; }
    }
}
