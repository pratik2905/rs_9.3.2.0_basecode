﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class AnalyticsViewModel : BaseViewModel
    {
        public int PortalId { get; set; }
        public TagManagerViewModel TagManager { get; set; }

        public PortalTrackingPixelViewModel TrackingPixel { get; set; }
    }
}
