﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Admin.Helpers;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class GiftCardViewModel : BaseViewModel
    {
        public int GiftCardId { get; set; }

        [Display(Name = ZnodeAdmin_Resources.TextStoreName, ResourceType = typeof(Admin_Resources))]
        public int PortalId { get; set; }

        [RegularExpression(AdminConstants.NumberExpression, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.MessageNumericValueAllowed)]
        [Display(Name = ZnodeAdmin_Resources.LabelCustomerId, ResourceType = typeof(Admin_Resources))]
        [Range(1, 99999999, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.rnvDisplayOrder)]
        public int? UserId { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelGiftCardName, ResourceType = typeof(Admin_Resources))]
        [MaxLength(100, ErrorMessageResourceName = ZnodeAdmin_Resources.GiftCardNameMaxLength, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.PleaseEnterGiftCardName)]
        public string Name { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelCardNumber, ResourceType = typeof(Admin_Resources))]
        public string CardNumber { get; set; }
        public string DisplayAmount { get; set; }
        public string CurrencyCode { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelGiftCardAmount, ResourceType = typeof(Admin_Resources))]
        [Range(0.01, 999999.00, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorRangeBetween)]
        public string Amount { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelAmountOwed, ResourceType = typeof(Admin_Resources))]
        public decimal OwedAmount { get; set; }
        public decimal LeftAmount { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelGiftCardAmount, ResourceType = typeof(Admin_Resources))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.PleaseEnterGiftCardNumber)]
        [Range(0.01, 999999.00, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorRangeBetween)]
        public decimal GiftCardAmount { get; set; }

        public int? AccountId { get; set; }
        public bool EnableToCustomerAccount { get; set; }
        public bool IsReferralCommission { get; set; } = false;

        [Display(Name = ZnodeAdmin_Resources.LabelExpirationDate, ResourceType = typeof(Admin_Resources))]
        public DateTime? ExpirationDate { get; set; }

        public bool SendMail { get; set; }

        public RMARequestViewModel RmaRequestModel { get; set; }

        public string StoreName { get; set; }
        [Display(Name = ZnodeAdmin_Resources.LblCustomerName, ResourceType = typeof(Admin_Resources))]
        public string CustomerName { get; set; }
        public string CultureCode { get; set; }
    }
}