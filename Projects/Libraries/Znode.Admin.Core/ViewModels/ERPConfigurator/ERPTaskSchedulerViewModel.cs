﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class ERPTaskSchedulerViewModel : BaseViewModel
    {
        public GridModel GridModel { get; set; }
        public List<SelectListItem> RepeatTaskForDurationList { get; set; }
        public List<SelectListItem> ERPTaskSchedulerDaysList { get; set; }
        public List<SelectListItem> ERPTaskSchedulerWeekDaysList { get; set; }
        public List<SelectListItem> ERPTaskSchedulerMonthsList { get; set; }
        public List<SelectListItem> ERPTaskSchedulerOnDaysList { get; set; }
        public List<SelectListItem> RepeatTaskEveryList { get; set; }

        public int ERPTaskSchedulerId { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerSchedulerName, ResourceType = typeof(ERP_Resources))]
        [Required(ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.RequiredField)]
        [MaxLength(100, ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.ERPTaskSchedulerSchedulerNameLengthErrorMessage)]
        [RegularExpression(AdminConstants.AlphanumericStartWithAlphabetValidation, ErrorMessageResourceName = ZnodeAdmin_Resources.AlphanumericStartWithAlphabet, ErrorMessageResourceType = typeof(Admin_Resources))]
        public string SchedulerName { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerTouchPointName, ResourceType = typeof(ERP_Resources))]
        [Required(ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.ERPTaskSchedulerTouchPointNameRequiredMessage)]
        [MaxLength(100, ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.ERPTaskSchedulerTouchPointNameLengthErrorMessage)]
        public string TouchPointName { get; set; }

        public string SchedulerFrequency { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerStartDate, ResourceType = typeof(ERP_Resources))]
        [Required(ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.RequiredField)]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ERP_Resources), ErrorMessageResourceName = ZnodeERP_Resources.StartTimeRequired)]
        public string StartTime { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerExpireDate, ResourceType = typeof(ERP_Resources))]
        public DateTime? ExpireDate { get; set; }

        public string ExpireTime { get; set; }

        public string ActiveERPClassName { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerRepeatTaskEvery, ResourceType = typeof(ERP_Resources))]
        [RegularExpression(AdminConstants.NumberValidation, ErrorMessageResourceName = ZnodeERP_Resources.InvalidRepeatTaskEvery, ErrorMessageResourceType = typeof(ERP_Resources))]
        public int RepeatTaskEvery { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerForDuration, ResourceType = typeof(ERP_Resources))]
        public string RepeatTaskForDuration { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerRecurEvery, ResourceType = typeof(ERP_Resources))]
        [RegularExpression(AdminConstants.NumberValidation, ErrorMessageResourceName = ZnodeERP_Resources.InvalidRecurEvery, ErrorMessageResourceType = typeof(ERP_Resources))]
        public int RecurEvery { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerWeekDays, ResourceType = typeof(ERP_Resources))]
        public string WeekDays { get; set; }

        public string[] WeekDaysArray { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerMonths, ResourceType = typeof(ERP_Resources))]
        public string Months { get; set; }

        public string[] MonthsArray { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerDays, ResourceType = typeof(ERP_Resources))]
        public string Days { get; set; }

        public string[] DaysArray { get; set; } 

        [Display(Name = ZnodeERP_Resources.LabelERPTaskSchedulerOnDays, ResourceType = typeof(ERP_Resources))]
        public string OnDays { get; set; }

        public string[] OnDaysArray { get; set; }

        public bool IsRepeatTaskEvery { get; set; }

        public string DayWeekDisplayName { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelIsEnabled, ResourceType = typeof(ERP_Resources))]
        public bool IsEnabled { get; set; }

        public bool IsMonthlyDays { get; set; }

        public string ExePath { get; set; }
        public string ExeParameters { get; set; }

        public int PortalId { get; set; }
        public string IndexName { get; set; }
        public int CatalogId { get; set; }
        public int CatalogIndexId { get; set; }
        public bool IsAssignTouchPoint { get; set; }

        [Display(Name = ZnodeERP_Resources.LabelSchedulerType, ResourceType = typeof(ERP_Resources))]
        public string SchedulerType { get; set; }
        public string ERPClassName { get; set; }
        public string SchedulerCallFor { get; set; }
        public string DomainName { get; set; }
    }
}