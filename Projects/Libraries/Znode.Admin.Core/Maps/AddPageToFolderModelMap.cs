﻿using Znode.Engine.Api.Models;

namespace Znode.Admin.Core.Maps
{
    public static class AddPageToFolderModelMap
    {
        public static AddPagetoFolderModel ToModel(int folderId, string pageIds)
        {
            AddPagetoFolderModel model = new AddPagetoFolderModel();
            model.FolderId = folderId;
            model.PageIds = pageIds;
            return model;
        }
    }
}
