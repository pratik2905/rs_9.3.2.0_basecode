//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Znode.Libraries.Data.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZnodePublishCatalog : ZnodeEntityBaseModel
    {
        public ZnodePublishCatalog()
        {
            this.ZnodeCatalogIndexes = new HashSet<ZnodeCatalogIndex>();
            this.ZnodePortalCatalogs = new HashSet<ZnodePortalCatalog>();
            this.ZnodePortalSearchProfiles = new HashSet<ZnodePortalSearchProfile>();
            this.ZnodePromotionCatalogs = new HashSet<ZnodePromotionCatalog>();
            this.ZnodePublishCatalogLogs = new HashSet<ZnodePublishCatalogLog>();
            this.ZnodePublishCatalogSearchProfiles = new HashSet<ZnodePublishCatalogSearchProfile>();
            this.ZnodePublishCategories = new HashSet<ZnodePublishCategory>();
            this.ZnodePublishCategoryProducts = new HashSet<ZnodePublishCategoryProduct>();
            this.ZnodePublishProducts = new HashSet<ZnodePublishProduct>();
            this.ZnodeSearchDocumentMappings = new HashSet<ZnodeSearchDocumentMapping>();
            this.ZnodeSearchGlobalProductBoosts = new HashSet<ZnodeSearchGlobalProductBoost>();
            this.ZnodeSearchGlobalProductCategoryBoosts = new HashSet<ZnodeSearchGlobalProductCategoryBoost>();
            this.ZnodeSearchKeywordsRedirects = new HashSet<ZnodeSearchKeywordsRedirect>();
            this.ZnodeSearchSynonyms = new HashSet<ZnodeSearchSynonym>();
        }
    
        public int PublishCatalogId { get; set; }
        public Nullable<int> PimCatalogId { get; set; }
        public string CatalogName { get; set; }
        public string ExternalId { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string Tokem { get; set; }
    
        public virtual ICollection<ZnodeCatalogIndex> ZnodeCatalogIndexes { get; set; }
        public virtual ICollection<ZnodePortalCatalog> ZnodePortalCatalogs { get; set; }
        public virtual ICollection<ZnodePortalSearchProfile> ZnodePortalSearchProfiles { get; set; }
        public virtual ICollection<ZnodePromotionCatalog> ZnodePromotionCatalogs { get; set; }
        public virtual ICollection<ZnodePublishCatalogLog> ZnodePublishCatalogLogs { get; set; }
        public virtual ICollection<ZnodePublishCatalogSearchProfile> ZnodePublishCatalogSearchProfiles { get; set; }
        public virtual ICollection<ZnodePublishCategory> ZnodePublishCategories { get; set; }
        public virtual ICollection<ZnodePublishCategoryProduct> ZnodePublishCategoryProducts { get; set; }
        public virtual ICollection<ZnodePublishProduct> ZnodePublishProducts { get; set; }
        public virtual ICollection<ZnodeSearchDocumentMapping> ZnodeSearchDocumentMappings { get; set; }
        public virtual ICollection<ZnodeSearchGlobalProductBoost> ZnodeSearchGlobalProductBoosts { get; set; }
        public virtual ICollection<ZnodeSearchGlobalProductCategoryBoost> ZnodeSearchGlobalProductCategoryBoosts { get; set; }
        public virtual ICollection<ZnodeSearchKeywordsRedirect> ZnodeSearchKeywordsRedirects { get; set; }
        public virtual ICollection<ZnodeSearchSynonym> ZnodeSearchSynonyms { get; set; }
    }
}
