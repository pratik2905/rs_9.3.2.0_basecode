﻿using System;
using System.Configuration;
using System.Web;
using Znode.Libraries.Data.DataModel;

namespace Znode.Libraries.Data.Helpers
{
    public static class HelperMethods
    {
        /// <summary>
        /// Get Login User Id from Request Headers
        /// </summary>
        /// <returns>Login User Id</returns>
        public static int GetLoginUserId()
        {
            int userId = 0;
            var headers = HttpContext.Current.Request.Headers;

            int.TryParse(headers["Znode-UserId"], out userId);

            int loginAs = GetLoginAdminUserId();
            if (loginAs > 0)
                return loginAs;

            return userId;
        }

        /// <summary>
        /// Get Logged in admin userId from Request Headers
        /// </summary>
        /// <returns>Login User Id</returns>
        public static int GetLoginAdminUserId()
        {
            int loginAs = 0;
            var headers = HttpContext.Current.Request.Headers;

            int.TryParse(headers["Znode-LoginAsUserId"], out loginAs);

            return loginAs;
        }

        /// <summary>
        /// Get database connection string
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString;
            }
        }

        /// <summary>
        /// Get SSRS Reports database connection string
        /// </summary>
        public static string GetSSRSReportConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ZnodeReportDB"].ConnectionString;
            }
        }

        //Gets current context object
        public static Znode_Entities CurrentContext
        {
            get
            {
                return GetObjectContext();
            }
        }

        // Get current datetime.
        public static DateTime GetEntityDateTime() => DateTime.Now;

        //Create the Context object, return the context.
        private static Znode_Entities GetObjectContext()
        {
            if (HttpContext.Current != null)
            {
                string objectContextKey = "ocm_" + HttpContext.Current.GetHashCode().ToString("x");
                if (!HttpContext.Current.Items.Contains(objectContextKey))
                    HttpContext.Current.Items.Add(objectContextKey, new Znode_Entities());
                return HttpContext.Current.Items[objectContextKey] as Znode_Entities;
            }
            else
                return new Znode_Entities();
        }

    }
}
