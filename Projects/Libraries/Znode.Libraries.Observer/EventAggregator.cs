﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Znode.Libraries.Observer
{
    public class EventAggregator
    {
        private readonly object lockObj = new object();
        private readonly Dictionary<Type, IList> TargetErp;

        public EventAggregator()
        {
            TargetErp = new Dictionary<Type, IList>();
        }

        public void Submit<TModelType>(TModelType message)
        {
            Type t = typeof(TModelType);
            IList sublst;
            if (TargetErp.ContainsKey(t))
            {
                lock (lockObj)
                {
                    sublst = new List<Connector<TModelType>>(TargetErp[t].Cast<Connector<TModelType>>());
                }

                foreach (Connector<TModelType> sub in sublst)
                {
                    var action = sub.CreatAction();
                    if (action != null)
                        action(message);
                }
            }
        }

        public Connector<TModelType> Attach<TModelType>(Action<TModelType> action)
        {
            Type t = typeof(TModelType);
            IList actionlst;
            var actiondetail = new Connector<TModelType>(action, this);

            lock (lockObj)
            {
                if (!TargetErp.TryGetValue(t, out actionlst))
                {
                    actionlst = new List<Connector<TModelType>>();
                    actionlst.Add(actiondetail);
                    TargetErp.Add(t, actionlst);
                }
                else
                {
                    actionlst.Add(actiondetail);
                }
            }

            return actiondetail;
        }

        public void Detach<TModelType>(Connector<TModelType> action)
        {
            Type t = typeof(TModelType);
            if (TargetErp.ContainsKey(t))
            {
                lock (lockObj)
                {
                    TargetErp[t].Remove(action);
                }
                action = null;
            }
        }

    }
}

