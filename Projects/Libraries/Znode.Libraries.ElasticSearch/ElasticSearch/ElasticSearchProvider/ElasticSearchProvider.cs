﻿using System.Collections.Generic;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{
    public class ElasticSearchProvider : IZnodeSearchProvider
    {
        private readonly ElasticSearchBase search;

        public ElasticSearchProvider()
        {
            search = new ElasticSearchBase();
        }

        #region Public Methods       

        /// <summary>
        /// Find terms matching the given partial word that appear in the highest number of documents.</summary>
        /// <param name="term">A word or part of a word</param>
        /// <returns>A list of suggested completions</returns>
        public IZnodeSearchResponse SuggestTermsFor(IZnodeSearchRequest request)
        {
            IZnodeSearchResponse elasticresponse = new ElasticSearchResponse();
            ElasticSuggestions suggestions = new ElasticSuggestions();

            elasticresponse = suggestions.SuggestTermsFromExistingIndex(request);
            return elasticresponse;
        }

        #region Elastic search
        /// <summary>
        /// Full text search for a search keyword.
        /// </summary>
        /// <param name="request">Request for search.</param>
        /// <returns>Search Response</returns>
        public IZnodeSearchResponse FullTextSearch(IZnodeSearchRequest request)
        {
            if (string.IsNullOrEmpty(request.SearchText)) request.SearchText = string.Empty;

            // Search the category / Sub-Cateogry for the given text.
            IZnodeSearchResponse searchResponse = search.FullTextSearch(request);

            GetsFacetsAndCategories(request, searchResponse);

            return searchResponse;
        }

        public List<string> FieldValueList(string indexName)
        {
            List<string> searchResponse = search.FieldValueList(indexName);
            return searchResponse;
        }

        /// <summary>
        /// Method to check if elastic search is working
        /// </summary>
        /// <returns></returns>
        public string CheckElasticSearch()
        {
            return search.CheckElasticSearch();
        }

        #endregion
        #endregion

        #region Private Methods
        //Populate the facets for the given input       
        private List<IZNodeSearchCategoryItem> PopulateCategoryFacets(IZnodeSearchResponse searchResponse)
        {
            return searchResponse.CategoryItems;
        }

        //Gets facets and categories for the search result.
        private void GetsFacetsAndCategories(IZnodeSearchRequest request, IZnodeSearchResponse searchResponse)
        {
            if (HelperUtility.IsNotNull(searchResponse) && request.GetCategoriesHeirarchy)
            {
                searchResponse.CategoryItems = PopulateCategoryFacets(searchResponse);
            }
        }

    }
    #endregion

}
