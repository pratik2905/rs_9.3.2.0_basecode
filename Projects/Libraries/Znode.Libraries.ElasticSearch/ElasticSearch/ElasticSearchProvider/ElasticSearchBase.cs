﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{
    public class ElasticSearchBase
    {
        public static ElasticClient ElasticSearchClient
        {
            get
            {
                string uri = ZnodeApiSettings.ElasticSearchRootUri;
                Uri node = new Uri(string.IsNullOrEmpty(uri) ? "http://localhost:9200" : uri);

                if (HttpContext.Current != null)
                {
                    string objectContextKey = "search_" + HttpContext.Current.GetHashCode().ToString("x");
                    if (!HttpContext.Current.Items.Contains(objectContextKey))
                    {
                        ConnectionSettings settings = new ConnectionSettings(node).DisableDirectStreaming().EnableHttpPipelining();
                        ElasticClient _client = new ElasticClient(settings);
                        HttpContext.Current.Items.Add(objectContextKey, _client);
                        return HttpContext.Current.Items[objectContextKey] as ElasticClient;
                    }
                    else
                        return HttpContext.Current.Items[objectContextKey] as ElasticClient;

                }
                else
                    return new ElasticClient(new ConnectionSettings(node).DisableDirectStreaming().EnableHttpPipelining());
            }
        }

        public ElasticSearchBase()
        {
        }

        #region Elastic search

        #region Text search

        /// <summary>
        /// Full text search (which contains the text of several text fields at once).
        /// </summary>
        /// <param name="request">Elastic search request.</param>
        /// <returns>Search response.</returns>
        public IZnodeSearchResponse FullTextSearch(IZnodeSearchRequest request)
        {
            bool isSortEnabled = request.SortCriteria?.Count > 0;

            //Checks if the index is prsent or not.
            CheckIsIndexExists(request.IndexName);

            //Get paging params
            request = GetPagingParameters(request);

            SetDefaultFilters(request);

            ISearchResponse<dynamic> searchResponse = GetSearchResponse(request);

            return ElasticProductMapper.ElasticProductMapToZNodeSearchResponse(searchResponse, request.HighlightFieldName, request);
        }

        public ISearchResponse<dynamic> GetSearchResponse(IZnodeSearchRequest request)
        {
            Assembly assembly = Assembly.Load("Znode.Libraries.ElasticSearch");
            Type className = assembly.GetTypes().FirstOrDefault(g => g.Name == request.QueryClass);

            //Create Instance of active class
            ISearchQuery instance = (ISearchQuery)Activator.CreateInstance(className);

            var _query = instance.GenerateQuery(request);

            var searchResponse = ElasticSearchClient.Search<dynamic>(_query);

            return searchResponse;
        }

        public List<string> FieldValueList(string indexName, string fieldType = "")
        {
            //Checks if the index is prsent or not.
            CheckIsIndexExists(indexName);

            //Get Index Mapping request.
            GetMappingRequest getMappingRequest = new GetMappingRequest(indexName, ElasticLibraryConstants.ElasticProductIndexType);

            IGetMappingResponse fieldMappings = ElasticSearchClient.GetMapping(getMappingRequest);

            List<string> listOfField = new List<string>();
            listOfField.Add("profileids");
            listOfField.Add("productindex");
            listOfField.Add("versionid");
            listOfField.Add("localeid");
            listOfField.Add("timestamp");
            listOfField.Add("znodeproductid");
            listOfField.Add("categoryid");
            listOfField.Add("znodecatalogid");
            listOfField.Add("attributes");
            listOfField.Add("outofstockoptions");
            listOfField.Add("isactive");
            listOfField.Add("callforpricing");
            listOfField.Add("producttype");
            listOfField.Add("displayorder");

            switch (fieldType)
            {
                case "number":
                    if (fieldMappings.Mapping?.Properties?.Count > 0)
                        return fieldMappings.Mapping.Properties.Where(x => x.Value.Type.Name == "float" || x.Value.Type.Name == "double" || x.Value.Type.Name == "long").Select(y => y.Value.Name).Select(z => z.Name).Where(y => !listOfField.Contains(y)).ToList();
                    break;
                case "all":
                    if (fieldMappings.Mapping?.Properties?.Count > 0)
                        return fieldMappings.Mapping.Properties.Select(y => y.Value.Name).Select(z => z.Name).Where(y => !listOfField.Contains(y)).ToList();
                    break;
            }

            return new List<string>();
        }


        //Get Auto suggestion for boost and bury.
        public List<string> GetBoostAndBuryAutoSuggestion(string indexName, int publishCatalogId, string fieldName, string searchTerm)
        {
            //Checks if the index is prsent or not.
            CheckIsIndexExists(indexName);

            List<QueryContainer> catalogIdLocaleIdContainerList = new List<QueryContainer>();
            catalogIdLocaleIdContainerList.Add(GetTermQuery("localeid", "1"));
            catalogIdLocaleIdContainerList.Add(GetTermQuery("znodecatalogid", publishCatalogId.ToString()));
            catalogIdLocaleIdContainerList.Add(GetTermQuery("isactive", "True"));
            catalogIdLocaleIdContainerList.Add(GetTermQuery("productindex", "1"));

            //Checks if the index is present or not.
            if (string.IsNullOrEmpty(indexName))
                throw new Exception("Index name cannot be blank.");
            if (!ElasticSearchClient.IndexExists(indexName).Exists)
                throw new Exception("Search index does not exist.");

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(indexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.From = 1;
            searchRequest.Size = 12;

            PrefixQuery prefixQuery = new PrefixQuery();
            prefixQuery.Field = $"{fieldName.ToLower()}.autocomplete";
            prefixQuery.Value = searchTerm;

            searchRequest.Query = prefixQuery;

            searchRequest.Aggregations = new TermsAggregation("autocomplete")
            {
                Field = $"{fieldName.ToLower()}.autocomplete",
                Include = new TermsIncludeExclude { Pattern = $"{searchTerm}.*" },
                Order = new List<TermsOrder> { TermsOrder.CountDescending },
                Aggregations = new TermsAggregation("name")
                {
                    Field = $"{fieldName.ToLower()}.keyword"
                }
            };

            var searchResponse = ElasticSearchClient.Search<dynamic>(searchRequest);

            List<string> suggestionList = new List<string>();

            TermsAggregate<string> suggestions = searchResponse.Aggs.Terms("autocomplete");

            var suggestionBucket = suggestions.Buckets;

            foreach (var bucket in suggestionBucket)
            {
                if (bucket.Terms("name").Buckets.Count > 0)
                {
                    string value = bucket.Terms("name").Buckets.FirstOrDefault().Key;
                    suggestionList.Add(value);
                }
            }

            return suggestionList;
        }

        /// <summary>
        /// Method to check if elastic search is working
        /// </summary>
        /// <returns></returns>
        public string CheckElasticSearch()
        {
            return ElasticSearchClient.ClusterHealth().Status;
        }
        #endregion Text search
        #region Create query

        private QueryContainer GetMatchQuery(string andItem, string itemValue, bool isKey = false) =>
         new QueryContainerDescriptor<SearchProduct>().Match(match => match.Field(andItem).Query(itemValue.ToLower()).Operator(Operator.And));

        // Get term query.
        private QueryContainer GetTermQuery(string andItem, string itemValue, double boost = 0.0, bool isSortEnabled = false)
        {
            boost = isSortEnabled ? 0.0 : boost;
            return new QueryContainerDescriptor<SearchProduct>().Term(s => s.Field(andItem).Value(itemValue.ToLower()).Boost(boost));
        }

        // Get terms query to find multiple values for a single field.
        private QueryContainer GetTermContainsQuery(string andItem, List<string> itemValues, bool isSortEnabled = false)
        => new QueryContainerDescriptor<SearchProduct>().Terms(s => s.Field(andItem).Terms(itemValues.ConvertAll(d => d.ToLower())));

        #endregion Create query

        #region Get paging params.

        //Get paging params.
        private IZnodeSearchRequest GetPagingParameters(IZnodeSearchRequest request)
        {
            //Condition to get all products.
            if (request.PageSize == -1)
            {
                //Maximum number of records elastic search can fetch.
                request.PageSize = 10000;
                request.StartIndex = 0;
                return request;
            }

            request.StartIndex = request.PageSize * (request.PageFrom - 1);
            return request;
        }

        #endregion Get paging params.

        #region Index Validation

        //Checks if an index exists.
        private void CheckIsIndexExists(string indexName)
        {
            if (string.IsNullOrEmpty(indexName))
                throw new ZnodeException(ErrorCodes.InvalidData, "Index name cannot be blank.");
            if (!ElasticSearchClient.IndexExists(indexName).Exists)
                throw new ZnodeException(ErrorCodes.NotFound, "Search index does not exist.");
        }

        #endregion Index Validation

        //Gets default filters for catalog, locale ID, etc.
        private List<QueryContainer> SetDefaultFilters(IZnodeSearchRequest request)
        {
            List<QueryContainer> catalogIdLocaleIdContainerList = new List<QueryContainer>();
            // Loop to add catalogId, locaelId and categoryId
            foreach (var andItem in request.CatalogIdLocalIdDictionary ?? new Dictionary<string, List<string>>())
            {// Added item to list to create "AND" query.
                if (andItem.Value.Count == 1)
                    catalogIdLocaleIdContainerList.Add(GetTermQuery(andItem.Key.ToString(), andItem.Value.FirstOrDefault()));
                else if (andItem.Value.Count > 1)
                    catalogIdLocaleIdContainerList.Add(GetTermContainsQuery(andItem.Key.ToString(), andItem.Value));
            }

            foreach (var andItem in request.Facets ?? new Dictionary<string, List<string>>())
            {
                List<QueryContainer> fieldValuesQuery = new List<QueryContainer>();

                foreach (var item in andItem.Value)
                {
                    fieldValuesQuery.Add(new QueryContainerDescriptor<SearchProduct>()
                    .Term(matchphrase => matchphrase.Field($"{andItem.Key.ToLower()}.keyword").Value(item)));
                }

                QueryContainer orQueryContainer = new QueryContainer();

                // Loop on item to get "OR" query which used for searchable field.
                foreach (QueryContainer item in fieldValuesQuery)
                    orQueryContainer |= +item;

                // Added combination of "AND" and "OR" query to create final query.
                catalogIdLocaleIdContainerList.Add(orQueryContainer);
            }

            request.FilterValues = catalogIdLocaleIdContainerList;
            return catalogIdLocaleIdContainerList;
        }
        #endregion Elastic search
    }
}