﻿using Nest;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Znode.Libraries.Search
{
    public class MatchPhraseQueryBuilder : BaseQuery, ISearchQuery
    {
        #region Variables
        readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        readonly BoolQuery finalBoolQuery = new BoolQuery();
        #endregion

        #region Public Methods
        //Generate query.
        public SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {
            //Match phrase query for partial match after the exact match.
            MatchPhraseQuery partialMatchPhraseQuery = BuildMatchPhraseQuery(request);

            functionScoreQuery.ScoreMode = FunctionScoreMode.Sum;
            functionScoreQuery.BoostMode = FunctionBoostMode.Sum;
            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { partialMatchPhraseQuery };

            //If boost and bury conditions are not present then add condition for exact macth first.
            if (request.BoostAndBuryItemLists.Count <= 0)
                CheckAndAddBoostQueryForExactMatchResult(request);

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;

            functionScoreQuery.Query = finalBoolQuery;
            AggregationBase aggregationBase = GetProductCountAggregation();

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation(aggregationBase);

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;
            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;

            searchRequest.Aggregations = aggregationBase;
            searchRequest.Suggest = GetSuggestionQuery(request);

            if (IsFeatureActive(request, "DfsQueryThenFetch"))
            {
                searchRequest.SearchType = Elasticsearch.Net.SearchType.DfsQueryThenFetch;
            }

            return searchRequest;

        }
        #endregion

        #region Private Method
        //Build Match Pharase Query
        private MatchPhraseQuery BuildMatchPhraseQuery(IZnodeSearchRequest request)
        {
            MatchPhraseQuery matchPharaseQuery = new MatchPhraseQuery();

            string searchField = "";

            if (request.SearchableAttibute?.Count > 0)
            {
                searchField = request.SearchableAttibute?.FirstOrDefault().AttributeCode.ToLower();
            }
            else
            {
                searchField = "productname";
            }

            matchPharaseQuery.Query = request.SearchText;
            matchPharaseQuery.Field = searchField;
            matchPharaseQuery.Operator = GetOperator(request);

            //Fuzziness
            string isAutocorrect = request.FeatureList.Find(x => x.FeatureCode == "AutoCorrect")?.SearchFeatureValue;
            if (!string.IsNullOrEmpty(isAutocorrect) && Convert.ToBoolean(isAutocorrect))
            {
                matchPharaseQuery.Fuzziness = Fuzziness.Auto;
            }

            return matchPharaseQuery;
        }
        #endregion
    }
}
