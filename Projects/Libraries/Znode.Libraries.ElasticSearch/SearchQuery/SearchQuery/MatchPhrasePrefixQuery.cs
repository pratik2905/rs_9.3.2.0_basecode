﻿using Nest;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Znode.Libraries.Search
{
    public class MatchPhrasePrefixQueryBuilder : BaseQuery, ISearchQuery
    {
        #region Private Fields
        readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        readonly BoolQuery finalBoolQuery = new BoolQuery();
        #endregion

        #region Public Methods
        //Generate query.
        public SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {

            //Macth phrase query for partial match after exact match.
            MatchPhrasePrefixQuery partialMatchPhrasePrefixQuery = BuildMatchPhrasePrefixQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { partialMatchPhrasePrefixQuery };

            //If boost and bury conditions are not present then add condition for exact macth first.
            if (request.BoostAndBuryItemLists.Count <= 0)
                CheckAndAddBoostQueryForExactMatchResult(request);

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;

            functionScoreQuery.ScoreMode = FunctionScoreMode.Sum;
            functionScoreQuery.BoostMode = FunctionBoostMode.Sum;
            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            functionScoreQuery.Query = finalBoolQuery;

            AggregationBase aggregationBase = GetProductCountAggregation();

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation(aggregationBase);

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;
            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;
            searchRequest.Aggregations = aggregationBase;
            searchRequest.Suggest = GetSuggestionQuery(request);

            if (IsFeatureActive(request, "DfsQueryThenFetch"))
            {
                searchRequest.SearchType = Elasticsearch.Net.SearchType.DfsQueryThenFetch;
            }
            return searchRequest;

        }

        #endregion

        #region Private Methods
        //Build match phrase prefix query.
        private MatchPhrasePrefixQuery BuildMatchPhrasePrefixQuery(IZnodeSearchRequest request)
        {
            MatchPhrasePrefixQuery matchPharasePrefixQuery = new MatchPhrasePrefixQuery();

            string searchField = "";

            if (request.SearchableAttibute?.Count > 0)
            {
                searchField = request.SearchableAttibute?.FirstOrDefault().AttributeCode.ToLower();
            }
            else
            {
                searchField = "productname";
            }

            matchPharasePrefixQuery.Query = request.SearchText;

            matchPharasePrefixQuery.Field = searchField;
            matchPharasePrefixQuery.Operator = GetOperator(request);
            //Fuzziness
            string isAutocorrect = request.FeatureList.Find(x => x.FeatureCode == "AutoCorrect")?.SearchFeatureValue;
            if (!string.IsNullOrEmpty(isAutocorrect) && Convert.ToBoolean(isAutocorrect))
            {
                matchPharasePrefixQuery.Fuzziness = Fuzziness.Auto;
            }

            return matchPharasePrefixQuery;
        }
        #endregion
    }
}
