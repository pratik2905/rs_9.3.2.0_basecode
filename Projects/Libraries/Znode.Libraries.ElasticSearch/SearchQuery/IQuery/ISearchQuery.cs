﻿using Nest;
using Znode.Libraries.Search;

namespace Znode.Libraries.Search
{
    public interface ISearchQuery
    {
        SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request);
    }
}
