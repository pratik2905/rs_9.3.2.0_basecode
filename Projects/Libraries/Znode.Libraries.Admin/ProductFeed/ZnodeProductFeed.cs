﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Libraries.Admin
{
    public class ZnodeProductFeedHelper
    {
        #region Private Variables
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IMongoRepository<VersionEntity> _versionMongoRepository;
        private readonly ZnodeRssWriter rssWriter;
        private const string SEOURLProductType = "Product";
        private const string SEOURLContentPageType = "ContentPage";
        private const string SEOURLCategoryType = "Category";
        private const string LastModifier = "Use the database update date";
        #endregion

        #region Public Constructor
        public ZnodeProductFeedHelper()
        {
            _productMongoRepository = new MongoRepository<ProductEntity>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _versionMongoRepository = new MongoRepository<VersionEntity>();
            rssWriter = new ZnodeRssWriter();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the Product list
        /// For more adding tags for the product, please refere to  http://www.google.com/support/merchants/bin/answer.py?answer=188494
        /// </summary>
        /// <param name="portalId">Portal Id to get the product</param>
        /// <param name="changeFreq">Change Frequency</param>
        /// <param name="priority">Priority of the page</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="xmlFileName"> XML File Name</param>
        /// <param name="lastModified">Last Modified date</param>
        /// <param name="localeId">locale id</param>
        /// <returns>Returns true if the XML file is created</returns>
        public virtual int GetProductList(string portalId, string rootTagValue, string xmlFileName, string googleFeedTitle, string googleFeedLink, string googleFeedDesc, string feedType, int localeId, string productFeedTimeStampName, string lastModifiedDate)
            => rssWriter.CreateGoogleSiteMap(GetProductFeedDataFromSQLAndMongo(portalId, localeId, feedType, productFeedTimeStampName, lastModifiedDate), rootTagValue, xmlFileName, googleFeedTitle, googleFeedLink, googleFeedDesc);

        /// <summary>
        /// Gets the Category List
        /// </summary>
        /// <param name="portalId">Portal Id to get category</param>
        /// <param name="model">Product Feed Model</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <returns>Returns the count of XML files created</returns>
        public virtual int GetCategoryList(string portalId, ProductFeedModel productFeedModel, string rootTag, string rootTagValue)
            => rssWriter.CreateXMLSiteMap(GetCategoryFeedDataFromSQLAndMongo(portalId, productFeedModel), rootTag, rootTagValue, productFeedModel.FileName);

        /// <summary>
        /// Gets the Content Page List
        /// </summary>
        /// <param name="portalId">Portal Id to get the contents</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="model">Product Feed Model</param>
        /// <returns>Returns the count of XML files created</returns>
        public virtual int GetContentPagesList(string portalIds, string rootTag, string rootTagValue, ProductFeedModel model)
            => rssWriter.CreateXMLSiteMap(GetContentPageFeedData(portalIds, model), rootTag, rootTagValue, model.FileName);

        /// <summary>
        /// Gets the list of all files generated
        /// </summary>
        /// <param name="model">Product Feed Model</param>
        /// <param name="portalId">Portal Id to get category</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="feedType">Type of Feed</param>
        /// <returns>Returns the count of XML files created</returns>
        public virtual int GetAllList(ProductFeedModel model, string portalId, string rootTag, string rootTagValue, string feedType)
            => rssWriter.CreateXMLSiteMapForAll(GetAllFeedDataFromSQLAndMongo(portalId, feedType, model), rootTag, rootTagValue, model.FileName, Convert.ToString(model.ProductFeedPriority));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameCount"></param>
        /// <param name="txtXMLFileName"></param>
        /// <returns></returns>
        public virtual string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName, string priority)
         => rssWriter.GenerateGoogleSiteMapIndexFiles(fileNameCount, txtXMLFileName, priority);

        /// <summary>
        /// Gets the count of XML file generated for product
        /// </summary>
        /// <param name="portalId">Portal Id to get category</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="feedType">Type of Feed</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="productFeedModel">Product Feed Model</param>
        /// <returns></returns>
        public virtual int GetProductXMLList(string portalId, string rootTagValue, string feedType, string rootTag, ProductFeedModel productFeedModel)
            => rssWriter.CreateProductSiteMap(GetProductFeedDataFromSQLAndMongo(portalId, productFeedModel.LocaleId, feedType, productFeedModel.ProductFeedTimeStampName, productFeedModel.Date), rootTag, rootTagValue, productFeedModel.FileName, Convert.ToString(productFeedModel.ProductFeedPriority));

        #endregion

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="portalIds"></param>
        /// <param name="feedType"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        protected virtual DataSet GetResultsFromSP(string spName, string portalIds, string feedType, int localeId, bool forProductData = false, string commaSeperatedIds = "")
        {
            DataTable skuData = GetSeoCodeInDataTable(commaSeperatedIds);
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            if (!forProductData)
            {
                executeSpHelper.GetParameter("@PortalID", portalIds, ParameterDirection.Input, SqlDbType.NVarChar);
                executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
                if (!string.IsNullOrEmpty(feedType))
                    executeSpHelper.GetParameter("@FeedType", feedType, ParameterDirection.Input, SqlDbType.NVarChar);
                if (!string.IsNullOrEmpty(commaSeperatedIds))
                    executeSpHelper.GetParameter("@CommaSeparatedId", commaSeperatedIds, ParameterDirection.Input, SqlDbType.NVarChar);
            }
            else
            {
                executeSpHelper.GetParameter("@PortalId", portalIds, ParameterDirection.Input, SqlDbType.NVarChar);
                executeSpHelper.SetTableValueParameter("@SKU", skuData, ParameterDirection.Input, SqlDbType.Structured, "dbo.SelectColumnList");
                executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@FeedType", feedType, ParameterDirection.Input, SqlDbType.NVarChar);
            }
            DataSet resultDataSet = executeSpHelper.GetSPResultInDataSet(spName);
            return resultDataSet.Tables.Count > 0 ? resultDataSet : null;
        }

        /// <summary>
        /// Get SEO Code in data table.
        /// </summary>
        /// <param name="commaSeperatedCodes"></param>
        /// <returns>Datatable with SKUs</returns>
        protected DataTable GetSeoCodeInDataTable(string commaSeperatedCodes)
        {
            DataTable table = new DataTable("SKU");
            table.Columns.Add("SKU", typeof(string));

            foreach (string model in commaSeperatedCodes?.Split(','))
                table.Rows.Add(model);

            return table;
        }

        /// <summary>
        /// This method will get the data from both Monogo and SQL.  Also it will convert the data in one DataSet. 
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="localeId">Locale Id</param>
        /// <param name="feedType">Feed Type For Ex Google, Bing etc.</param>
        /// <returns>DataSet of combination of SQL and Mongo</returns>
        protected virtual DataSet GetProductFeedDataFromSQLAndMongo(string portalId, int localeId, string feedType, string productFeedTimeStampName, string lastModifiedDate)
        {
            DataSet productDataFromSQL = null;
            DataSet finalMergedDataSet = null;
            List<SeoEntity> Seosettigs = null;
            try
            {
                List<ProductEntity> productDataFromMongo = null;
                string commaSeparatedProductIds = string.Empty;

                //Get the Published Catalog Ids from SQL on the basis of Portal and Locale
                string commaSeparatedCatalogIds = GetPublishedCatgalogIDsFromSQL(portalId, localeId);

                string versionIds = GetVersionIdsFromMongo(commaSeparatedCatalogIds, localeId);
                //Get the product details from Mongo
                if (!string.IsNullOrEmpty(commaSeparatedCatalogIds))
                    productDataFromMongo = GetProductDataFromMongo(commaSeparatedCatalogIds, localeId);

                List<string> productCodes = productDataFromMongo.Select(h => h.SKU)
         .Distinct().ToList();

                //Get te Product Ids from Mongo
                if (HelperUtility.IsNotNull(productDataFromMongo) && productDataFromMongo.Count > 0)
                    commaSeparatedProductIds = string.Join(",", productCodes);

                //Get the other details for ex. Price, Inventory etc from SQL for the selected product ids
                if (!string.IsNullOrEmpty(commaSeparatedProductIds))
                    productDataFromSQL = GetProductDataFromSQL(commaSeparatedProductIds, localeId, portalId, feedType);

                if (!string.IsNullOrEmpty(portalId))
                    Seosettigs = GetSeoSettigsList(localeId, portalId, ZnodeConstant.Product, productCodes, versionIds);

                //Merge the data from SQL and Mongo and return the merged data set for further processing
                if (HelperUtility.IsNotNull(productDataFromMongo) && productDataFromMongo.Count > 0 && HelperUtility.IsNotNull(productDataFromSQL) && HelperUtility.IsNotNull(productDataFromSQL.Tables)
                    && productDataFromSQL.Tables[0].Rows.Count > 0)
                    finalMergedDataSet = ConvertDataInDataSet(productDataFromSQL, productDataFromMongo, Seosettigs);
                return finalMergedDataSet;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
            finally
            {
                productDataFromSQL = null;
                finalMergedDataSet = null;
            }
        }

        private List<SeoEntity> GetSeoSettigsList(int localeId, string portalId, string seoType, List<string> commaSeparatedProductIds = null, string versionIds = null)
        {
            IMongoRepository<SeoEntity> _seoMongoRepository = new MongoRepository<SeoEntity>();

            List<int?> portalIds = portalId.Split(',').Select(n => (int?)Convert.ToInt32(n)).ToList();
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();

            if (!portalIds.Any(x => x.Value == 0))
                mongoQuery.Add(Query<SeoEntity>.In(pr => pr.PortalId, portalIds));

            if (!string.IsNullOrEmpty(versionIds)) {
                var versionId = versionIds?.Split(',').Select(n => (int?)Convert.ToInt32(n)).ToList();
                mongoQuery.Add(Query<SeoEntity>.In(pr => pr.VersionId, versionId));
            }

            if (commaSeparatedProductIds?.Count > 0)
                mongoQuery.Add(Query<SeoEntity>.In(pr => pr.SEOCode, commaSeparatedProductIds));

            mongoQuery.Add(Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoType));
            mongoQuery.Add(Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId));

            List<SeoEntity> publishSEOList = _seoMongoRepository.GetEntityList(Query.And(mongoQuery));
            if (publishSEOList?.Count > 0)
                return publishSEOList;

            return new List<SeoEntity>();
        }

        /// <summary>
        /// This method will get the data from both Monogo and SQL.  Also it will convert the data in one DataSet. 
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="localeId">Locale Id</param>
        /// <param name="changeFrequency">Change Frequency</param>
        /// <param name="priority">Priority</param>
        /// <returns>Datase of combination of SQL and Mongo</returns>
        protected virtual DataSet GetCategoryFeedDataFromSQLAndMongo(string portalId, ProductFeedModel productFeedModel)
        {
            //string portalId, int localeId, string changeFrequency, string priority
            DataSet categoryDataFromSQL = null;
            DataSet finalMergedDataSet = null;
            List<SeoEntity> Seosettigs = null;
            try
            {
                List<CategoryEntity> categoryDataFromMongo = null;
                string commaSeparatedCategoryIds = string.Empty;

                //Get the Published Catalog Ids from SQL on the basis of Portal and Locale
                string commaSeparatedCatalogIds = GetPublishedCatgalogIDsFromSQL(portalId, productFeedModel.LocaleId);

                //Get the category details from Mongo
                if (!string.IsNullOrEmpty(commaSeparatedCatalogIds))
                    categoryDataFromMongo = GetCategoryDataFromMongo(commaSeparatedCatalogIds, productFeedModel.LocaleId);

                //Get te Category Ids Ids from Mongo
                if (HelperUtility.IsNotNull(categoryDataFromMongo) && categoryDataFromMongo?.Count > 0)
                    commaSeparatedCategoryIds = string.Join(",", categoryDataFromMongo.Select(x => x.ZnodeCategoryId).ToList());

                string versionIds = GetVersionIdsFromMongo(commaSeparatedCatalogIds, productFeedModel.LocaleId);

                List<string> categoryCodes = categoryDataFromMongo.SelectMany(h => h.Attributes)
                                                            .Where(rt => rt.AttributeCode == "CategoryCode").Select(x => x.AttributeValues)
                                                            .Distinct().ToList();


                commaSeparatedCategoryIds = string.Join(",", categoryCodes);

                //Get the other details for ex. Category Name, id etc from SQL for the selected category ids
                if (!string.IsNullOrEmpty(commaSeparatedCategoryIds))
                    categoryDataFromSQL = GetCategoryDataFromSQL(commaSeparatedCategoryIds, portalId, productFeedModel.LocaleId);

                if (!string.IsNullOrEmpty(portalId))
                    Seosettigs = GetSeoSettigsList(productFeedModel.LocaleId, portalId, ZnodeConstant.Category, categoryCodes, versionIds);

                //Merge the data from SQL and Mongo and return the merged data set for further processing
                if (HelperUtility.IsNotNull(categoryDataFromMongo) && categoryDataFromMongo?.Count > 0 && HelperUtility.IsNotNull(categoryDataFromSQL) && HelperUtility.IsNotNull(categoryDataFromSQL?.Tables)
                    && categoryDataFromSQL?.Tables[0]?.Rows.Count > 0)
                    finalMergedDataSet = ConvertDataInCategoryDataSet(categoryDataFromSQL, categoryDataFromMongo, productFeedModel.ChangeFreq, Convert.ToString(productFeedModel.ProductFeedPriority), Seosettigs);

                finalMergedDataSet.Tables[0].TableName = "url";
                return finalMergedDataSet;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
            finally
            {
                categoryDataFromSQL = null;
                finalMergedDataSet = null;
            }
        }

        /// <summary>
        /// This method will gets the data for Content Pages
        /// </summary>
        /// <param name="portalIds">Portal Id to get the contents</param>
        /// <param name="model">Product Feed Model</param>
        /// <returns>Returns the dataset containing the Content Page Data</returns>
        protected virtual DataSet GetContentPageFeedData(string portalIds, ProductFeedModel model)
        {
            DataSet datasetContentPagesList = null;
            List<SeoEntity> seoSettings = null;
            try
            {
                datasetContentPagesList = GetResultsFromSP("Znode_GetContentFeedList", portalIds, string.Empty, model.LocaleId);
                seoSettings = GetSeoSettigsList(model.LocaleId, portalIds, ZnodeConstant.ContentPage);
                if (HelperUtility.IsNotNull(datasetContentPagesList))
                {
                    AddColumns(ref datasetContentPagesList, model.ChangeFreq, model.ProductFeedPriority);

                    foreach (DataRow dr in datasetContentPagesList.Tables[0].Rows)
                    {
                        int portalId = Convert.ToInt16(dr["PortalId"].ToString());
                        if (portalId > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                            break;
                        int id = Convert.ToInt32(dr["CMSContentPagesId"].ToString());

                        string seoUrl = seoSettings.FirstOrDefault(x => x.SEOId == id)?.SEOUrl;
                        string domainname = $"{HttpContext.Current.Request.Url.Scheme}://{dr["DomainName"].ToString()}";

                        dr["loc"] = string.IsNullOrEmpty(Convert.ToString(dr["loc"])) ? $"{domainname}{"/contentpage/"}{Convert.ToString(dr["CMSContentPagesId"])}"
      : $"{domainname}/{Convert.ToString(dr["loc"])}";
                        if (model.ProductFeedTimeStampName.Equals("None"))
                            dr["lastmod"] = System.DBNull.Value;
                        else if ((!model.ProductFeedTimeStampName.Equals(LastModifier)) && (!model.ProductFeedTimeStampName.Equals("None")))
                            dr["lastmod"] = Convert.ToDateTime(model.Date);
                    }
                    RemoveUnwantedColumnsFromDS(ref datasetContentPagesList);
                }
                return datasetContentPagesList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
            finally
            {
                datasetContentPagesList = null;
            }
        }

        /// <summary>
        /// Get all the feed data from Mongo and SQL for Product, Category and Content Pages
        /// </summary>
        /// <param name="portalId">Portal Id to get the data</param>
        /// <param name="feedType">Feed type</param>
        /// <param name="productFeedModel">Product Feed Model</param>
        /// <returns>Returns the dataset containing all the data</returns>
        protected virtual DataSet GetAllFeedDataFromSQLAndMongo(string portalId, string feedType, ProductFeedModel productFeedModel)
        {
            DataSet dataSet = new DataSet();
            int count = 0;
            DataTable categoryData = GetCategoryFeedDataFromSQLAndMongo(portalId, productFeedModel)?.Tables[0];
            if (categoryData != null)
            {
                dataSet.Tables.Add("Category");
                dataSet.Tables[count++].Merge(categoryData);
            }
            DataTable productData = GetProductFeedDataFromSQLAndMongo(portalId, productFeedModel.LocaleId, feedType, productFeedModel.ProductFeedTimeStampName, productFeedModel.Date)?.Tables[0];
            if (productData != null)
            {
                dataSet.Tables.Add("Product");
                dataSet.Tables[count++].Merge(productData);
            }
            DataTable contentData = GetContentPageFeedData(portalId, productFeedModel)?.Tables[0];
            if (contentData != null)
            {
                dataSet.Tables.Add("Content");
                dataSet.Tables[count++].Merge(contentData);
            }
            return dataSet;
        }

        /// <summary>
        /// Get the Products data from Mongo
        /// </summary>
        /// <param name="commaSeparatedProductIDs">Comma Separated Product Ids</param>
        /// <param name="localeId">Locale Id</param>
        /// <returns>List<ProductEntity></returns>
        protected virtual List<ProductEntity> GetProductDataFromMongo(string commaSepCatalogIds, int localeId)
        {
            //Get version id from the mongo
            string versionIds = GetVersionIdsFromMongo(commaSepCatalogIds, localeId);

            FilterCollection productFilter = new FilterCollection();
            productFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            productFilter.Add(FilterKeys.MongoLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            productFilter.Add(FilterKeys.VersionId, FilterOperators.In, versionIds);
            productFilter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            productFilter.Add(FilterKeys.MongoIsActive, FilterOperators.Equals, ZnodeConstant.True);
            //productFilter.Add(FilterKeys.ZnodeCategoryIds, FilterOperators.NotEquals, "0");
            NameValueCollection sortCollection = new NameValueCollection();

            return GetProductList(productFilter, sortCollection);
        }

        //Get product list.
        protected virtual List<ProductEntity> GetProductList(FilterCollection productFilter, NameValueCollection sortCollection)
        {
            List<ProductEntity> productEntityList = new List<ProductEntity>();

            try
            {
                int totalRowCount = 0;
                int totalRecordCount = 0;

                _productMongoRepository.GetPagedList(MongoQueryHelper.GenerateDynamicWhereClause(productFilter.ToFilterMongoCollection()), MongoQueryHelper.GenerateDynamicOrderByClause(sortCollection), 1, 10, out totalRecordCount);

                int chunkSize = 1000;
                int startIndex = 0;
                int totalRows = totalRecordCount;
                int totalRowsCount = totalRows / chunkSize;

                if (totalRows % chunkSize > 0)
                    totalRowsCount++;

                for (int iCount = 0; iCount < totalRowsCount; iCount++)
                {
                    startIndex = startIndex + chunkSize;
                    productEntityList.AddRange(_productMongoRepository.GetPagedList(MongoQueryHelper.GenerateDynamicWhereClause(productFilter.ToFilterMongoCollection()), MongoQueryHelper.GenerateDynamicOrderByClause(sortCollection), iCount + 1, chunkSize, out totalRowCount));
                }
            }
            catch (Exception ex)
            {
                productEntityList = new List<ProductEntity>();
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return productEntityList;
        }

        /// <summary>
        /// Get the list of version ids from catalog ids
        /// </summary>
        /// <param name="commaSepCatalogIds">Catalog Ids</param>
        /// <returns>version ids</returns>
        protected virtual string GetVersionIdsFromMongo(string commaSepCatalogIds, int localeId = 0)
        {
            IPublishProductHelper publishProductHelper = GetService<IPublishProductHelper>();
            FilterCollection filters = new FilterCollection();
            filters.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            filters.Add(FilterKeys.MongoLocaleId, FilterOperators.Equals, localeId.ToString());
            return string.Join(",", _versionMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()))?.
                Where(x => x.RevisionType == Convert.ToString(publishProductHelper.GetPortalPublishState())).Select(x => x.VersionId).ToList());
        }

        /// <summary>
        /// Get the Category data from Mongo
        /// </summary>
        /// <param name="commaSepCatalogIds">Comma Separated Catalog Id</param>
        /// <param name="localeId">Locale Id</param>
        /// <returns>List<CategoryEntity></returns>
        protected virtual List<CategoryEntity> GetCategoryDataFromMongo(string commaSepCatalogIds, int localeId)
        {
            //Get version id from the mongo
            string versionIds = GetVersionIdsFromMongo(commaSepCatalogIds, localeId);

            FilterCollection categoryFilter = new FilterCollection();
            categoryFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            categoryFilter.Add(FilterKeys.MongoLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            categoryFilter.Add(FilterKeys.VersionId, FilterOperators.In, versionIds);
            NameValueCollection sortCollection = new NameValueCollection();
            sortCollection.Add(FilterKeys.ZnodeCategoryId, "asc");

            int totalRowCount = 0;
            return _categoryMongoRepository.GetPagedList(MongoQueryHelper.GenerateDynamicWhereClause(categoryFilter.ToFilterMongoCollection()), MongoQueryHelper.GenerateDynamicOrderByClause(sortCollection), 0, 0, out totalRowCount).Distinct<CategoryEntity>().ToList();
        }

        /// <summary>
        /// Get the Product Data from SQL
        /// </summary>
        /// <param name="commaSeparatedProductIds">Comma Separated Product Ids</param>
        /// <param name="localeId">Locale Id</param>
        /// <param name="portalIds">Portal Ids</param>
        /// <param name="feedType">Feed Type</param>
        /// <returns>DataSet</returns>
        protected virtual DataSet GetProductDataFromSQL(string commaSeparatedProductIds, int localeId, string portalIds, string feedType)
            => GetResultsFromSP("Znode_GetProductFeedList", portalIds, feedType, localeId, true, commaSeparatedProductIds);

        /// <summary>
        /// Get the Category Data from SQL
        /// </summary>
        /// <param name="commaSeparatedCategoryIds">Comma Separated Category Ids</param>
        /// <param name="localeId">Locale Id</param>
        /// <returns>DataSet</returns>
        protected virtual DataSet GetCategoryDataFromSQL(string commaSeparatedCategoryIds, string portalIds, int localeId)
            => GetResultsFromSP("Znode_GetCategoryFeedList", portalIds, string.Empty, localeId, false, commaSeparatedCategoryIds);

        /// <summary>
        /// Merge Mongo entities with DataSet
        /// </summary>
        /// <param name="productDataFromSQL">SQL Product Data</param>
        /// <param name="productDataFromMongo">Mongo Product Data</param>
        /// <returns>DataSet</returns>
        protected virtual DataSet ConvertDataInDataSet(DataSet productDataFromSQL, List<ProductEntity> productDataFromMongo, List<SeoEntity> seoSettigs)
        {
            DataSet dsProduct = new DataSet();
            CreateDataSetColumns(ref dsProduct, SEOURLProductType);
            AddDataToDataSet(ref dsProduct, productDataFromSQL, productDataFromMongo, seoSettigs);

            return dsProduct;
        }

        /// <summary>
        /// Merge Mongo entities with DataSet of Category
        /// </summary>
        /// <param name="categoryDataFromSQL">SQL Category Data</param>
        /// <param name="categoryDataFromMongo">Mongo Category Data</param>
        /// <param name="changeFrequency">Change Frequency</param>
        /// <param name="priority">Priority</param>
        /// <returns>DataSet</returns>
        protected virtual DataSet ConvertDataInCategoryDataSet(DataSet categoryDataFromSQL, List<CategoryEntity> categoryDataFromMongo, string changeFrequency, string priority, List<SeoEntity> seoEntities)
        {
            DataSet dsCategory = new DataSet();
            CreateDataSetColumns(ref dsCategory, SEOURLCategoryType);
            AddDataToCategoryDataSet(ref dsCategory, categoryDataFromSQL, categoryDataFromMongo, changeFrequency, priority, seoEntities);

            return dsCategory;
        }

        /// <summary>
        /// This method will add the data in the Dataset for Product
        /// </summary>
        /// <param name="dsProduct">Product DataSet</param>
        /// <param name="productDataFromSQL">SQL Product DataSet</param>
        /// <param name="productDataFromMongo">Mongo Product Entity</param>
        protected virtual void AddDataToDataSet(ref DataSet dsProduct, DataSet productDataFromSQL, List<ProductEntity> productDataFromMongo, List<SeoEntity> seoSettigs)
        {
            try
            {
                int loopCount = productDataFromMongo.Count;
            if (productDataFromMongo.Count > productDataFromSQL.Tables[0].Rows.Count)
                loopCount = productDataFromSQL.Tables[0].Rows.Count;

            //Merge the Product Data from Mongo and SQLin one DataSet
            for (int iCount = 0; iCount < loopCount; iCount++)
            {
                int pid = Convert.ToInt16(productDataFromSQL.Tables[0].Rows[iCount]["PortalId"].ToString());
                if (pid > 0 && string.IsNullOrEmpty(productDataFromSQL.Tables[0].Rows[iCount]["DomainName"].ToString()))
                    break;
                int id = Convert.ToInt32(Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:id"]));
                ProductEntity mongoProduct = productDataFromMongo?.FirstOrDefault(x => x.ZnodeProductId == id);
                if (!Equals(mongoProduct, null))
                {
                    
                    string domainName = $"{HttpContext.Current.Request.Url.Scheme}://{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["DomainName"])}";

                    string seoUrl = seoSettigs?.Where(x => x.SEOCode == mongoProduct.SKU && x.VersionId == mongoProduct.VersionId)?.Select(x => x.SEOUrl)?.FirstOrDefault();

                    DataRow dr = dsProduct.Tables[0].NewRow();

                    dr["loc"] = string.IsNullOrEmpty(seoUrl) ? $"{domainName}{"/product/"}{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:id"])}"
                        : $"{domainName}/{seoUrl}";
                    dr["title"] = mongoProduct.Name;
                    dr["g:condition"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:condition"]);
                    dr["description"] = mongoProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ShortDescription)?.FirstOrDefault()?.AttributeValues;
                    dr["g:id"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:id"]);
                    dr["g:image_link"] = $"{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["MediaConfiguration"])}{mongoProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues}";
                    dr["link"] = string.IsNullOrEmpty(Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["loc"])) ? $"{domainName}{"/product/"}{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:id"])}"
                        : $"{domainName}/{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["loc"])}";
                    dr["g:price"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:price"]);
                    dr["g:identifier_exists"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:identifier_exists"]);
                    dr["g:availability"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:availability"]);
                    dsProduct.Tables[0].Rows.Add(dr);
                }
            }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
        }

        /// <summary>
        /// This method will add the data in the Dataset for Category
        /// </summary>
        /// <param name="dsCategory">Category DataSet</param>
        /// <param name="categoryDataFromSQL">SQL Category Data Set</param>
        /// <param name="categoryDataFromMongo">Mongo Category Entity</param>
        /// <param name="changeFrequency">Change Frequency</param>
        /// <param name="priority">Priority</param>
        protected virtual void AddDataToCategoryDataSet(ref DataSet dsCategory, DataSet categoryDataFromSQL, List<CategoryEntity> categoryDataFromMongo, string changeFrequency, string priority, List<SeoEntity> seoEntities)
        {
            //Merge the Product Data from Mongo and SQLin one DataSet
            for (int iCount = 0; iCount < categoryDataFromMongo.Count; iCount++)
            {
                int catID = categoryDataFromMongo[iCount].ZnodeCategoryId;
                string seoCode = categoryDataFromMongo[iCount].Attributes.FirstOrDefault(x => x.AttributeCode == "CategoryCode")?.AttributeValues;
                string seoUrl = seoEntities.FirstOrDefault(x => x.SEOCode == seoCode)?.SEOUrl;

                var result =
        categoryDataFromSQL.Tables[0].AsEnumerable().Where(dr => dr.Field<int>("id") == catID);

                foreach (DataRow row in result)
                {
                    string domainName = $"{HttpContext.Current.Request.Url.Scheme}://{Convert.ToString(row["DomainName"])}";
                    DataRow dr = dsCategory.Tables[0].NewRow();
                    dr["loc"] = string.IsNullOrEmpty(seoUrl) ? $"{domainName}{"/category/"}{Convert.ToString(row["id"])}" : $"{domainName}/{seoUrl}";
                    dr["changefreq"] = changeFrequency;
                    dr["priority"] = priority;
                    dsCategory.Tables[0].Rows.Add(dr);
                }
            }
        }

        /// <summary>
        /// This method will create data set depending upon provided type
        /// </summary>
        /// <param name="dsDataSet">DataSet</param>
        /// <param name="type">Type</param>
        protected virtual void CreateDataSetColumns(ref DataSet dsDataSet, string type)
        {
            dsDataSet.Tables.Add(new DataTable());
            switch (type)
            {
                case SEOURLProductType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("title", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:condition", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("description", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:id", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:image_link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:price", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:identifier_exists", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:availability", typeof(string));
                    break;
                case SEOURLCategoryType:                   
                case SEOURLContentPageType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("priority", typeof(string));
                    break;
            }
        }

        /// <summary>
        /// Get the Publish Catalog IDs on the basis of Portal and Locale
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="localeId">Locale Id</param>
        /// <returns>Comma Separated Catalog Ids</returns>
        protected virtual string GetPublishedCatgalogIDsFromSQL(string portalId, int localeId)
        {
            string whereClause = string.Empty;

            if (!portalId.Equals("0"))
            {
                FilterDataCollection filters = new FilterDataCollection();
                filters.Add(new FilterDataTuple(ZnodePortalEnum.PortalId.ToString(), FilterOperators.In, portalId));
                whereClause = DynamicClauseHelper.GenerateDynamicWhereClause(filters);
            }

            List<ZnodePortalCatalog> portalCatalogs = _portalCatalogRepository.GetEntityList(whereClause)?.ToList();
            return string.Join(",", portalCatalogs?.Select(x => x.PublishCatalogId).Distinct());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="seoURL"></param>
        /// <returns></returns>
        protected virtual string MakeURLforMVC(string id, string type, string seoURL)
        {
            var mapUrl = new StringBuilder();

            if (!string.IsNullOrEmpty(seoURL))
            {
                switch (type)
                {
                    case SEOURLProductType:
                        mapUrl.Append($"~/product/{seoURL}/{id}");
                        break;
                    case SEOURLCategoryType:
                        mapUrl.Append($"~/category/{seoURL}/{id}");
                        break;
                    case SEOURLContentPageType:
                        mapUrl.Append($"~/{seoURL}");
                        break;
                }
            }
            else
            {
                switch (type)
                {
                    case SEOURLProductType:
                        mapUrl.Append("~/product/" + id);
                        break;
                    case SEOURLCategoryType:
                        mapUrl.Append("~/category/" + id);
                        break;
                    case SEOURLContentPageType:
                        mapUrl.Append("~/content/" + id);
                        break;
                }
            }

            return mapUrl.ToString();
        }

        /// <summary>
        /// This method will remove the unwanted columns from ContentPage dataset
        /// </summary>
        /// <param name="datasetContentPagesList">DataSet</param>
        protected virtual void RemoveUnwantedColumnsFromDS(ref DataSet datasetContentPagesList)
        {
            datasetContentPagesList.Tables[0].Columns.Remove("Name");
            if (datasetContentPagesList.Tables[0].Columns.Contains("DomainName"))
                datasetContentPagesList.Tables[0].Columns.Remove("DomainName");
            if (datasetContentPagesList.Tables[0].Columns.Contains("PortalId"))
                datasetContentPagesList.Tables[0].Columns.Remove("PortalId");
            if (datasetContentPagesList.Tables[0].Columns.Contains("CMSContentPagesId"))
                datasetContentPagesList.Tables[0].Columns.Remove("CMSContentPagesId");
        }

        /// <summary>
        /// This method will add required columns in ContentPage dataset
        /// </summary>
        /// <param name="datasetContentPagesList">DataSet</param>
        /// <param name="changeFreq">Chnage Frequency</param>
        /// <param name="priority">Priority</param>
        protected virtual void AddColumns(ref DataSet datasetContentPagesList, string changeFreq, decimal priority)
        {
            datasetContentPagesList.Tables[0].TableName = "url";
            DataColumn dcolChangeFreq = new DataColumn("changefreq");
            dcolChangeFreq.DefaultValue = changeFreq;
            DataColumn dcolPriority = new DataColumn("priority");
            dcolPriority.DefaultValue = priority;

            datasetContentPagesList.Tables[0].Columns.Add(dcolChangeFreq);
            datasetContentPagesList.Tables[0].Columns.Add(dcolPriority);
        }

        #endregion
    }
}
