﻿using System.Web.Mvc;
namespace Znode.Engine.WebStore
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthenticationHelper());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
