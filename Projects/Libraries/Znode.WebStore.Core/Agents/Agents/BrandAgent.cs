﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.WebStore.Agents
{
    public class BrandAgent : BaseAgent, IBrandAgent
    {
        #region Private Variables
        private readonly IBrandClient _brandClient;
        #endregion

        #region Constructor
        public BrandAgent(IBrandClient brandClient)
        {
            _brandClient = GetClient<IBrandClient>(brandClient);
        }
        #endregion

        //Get Filtered Brand List By Brandname
        public virtual List<BrandViewModel> BrandList(string brandName)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add("BrandName", FilterOperators.StartsWith, brandName);
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, PortalAgent.LocaleId.ToString());
            filters.Add(ZnodeConstant.IsAssociated, FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(ZnodeBrandDetailEnum.IsActive.ToString(), FilterOperators.Is, ZnodeConstant.TrueValue);
            filters.Add(ZnodeConstant.IsWebstore, FilterOperators.Equals, ZnodeConstant.TrueValue);

            BrandListModel brandList = _brandClient.GetBrandList(null, filters, null, null, null);
            //Sort List In Alphabetical order.
            brandList?.Brands?.Sort((x, y) => string.Compare(x.BrandName, y.BrandName));
            return brandList?.Brands?.Count > 0 ? brandList?.Brands.ToViewModel<BrandViewModel>().ToList() : new List<BrandViewModel> { };
        }

        //Get brand details by id.
        public virtual BrandViewModel GetBrandDetails(int brandId)
        {
            BrandModel brand = _brandClient.GetBrand(brandId, PortalAgent.LocaleId);
            if (HelperUtility.IsNull(brand))
                throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorBrandNotFound);
            return brand.ToViewModel<BrandViewModel>();
        }

        //Set breadcrumb html.
        public virtual void SetBreadCrumbHtml(BrandViewModel brand)=>
            brand.BreadCrumbHtml = $"<a href='/'>Home</a> / <a href='/Brand/List'>{WebStore_Resources.TitleShopByBrand}</a> / {brand.BrandName}";
    }
}