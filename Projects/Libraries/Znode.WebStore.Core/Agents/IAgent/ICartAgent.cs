﻿using Znode.Engine.Api.Client.Sorts;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Agents
{
    public interface ICartAgent
    {
        /// <summary>
        /// Create new cart.
        /// </summary>
        /// <param name="cartItem">CartItemViewModel.</param>
        /// <returns>Returns CartViewModel created.</returns>
        CartViewModel CreateCart(CartItemViewModel cartItem);

        /// <summary>
        /// Add product in the cart.
        /// </summary>
        /// <param name="cartItem">AddToCartViewModel.</param>
        /// <returns>Returns AddToCartViewModel created.</returns>
        AddToCartViewModel AddToCartProduct(AddToCartViewModel cartItem);

        /// <summary>
        /// Get cart
        /// </summary>
        /// <returns>CartViewModel</returns>
        CartViewModel GetCart(bool isCalculateTaxAndShipping = true, bool isCalculateCart = true);

        /// <summary>
        /// Calculate shopping cart
        /// </summary>
        /// <returns></returns>
        CartViewModel CalculateCart();

        /// <summary>
        /// Update quantity of shopping cart item.
        /// </summary>
        /// <param name="guid">External Id of cart item.</param>
        /// <param name="quantity">Selected quantity to update.</param>
        /// <param name="productId">Id of Product.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel UpdateCartItemQuantity(string guid, string quantity, int productId);

        /// <summary>
        /// Update quantity of shopping cart item.
        /// </summary>
        /// <param name="guid">External Id of cart item.</param>
        /// <param name="quantity">Selected quantity to update.</param>
        /// <param name="productId">Id of Product.</param>
        /// <returns>AddToCartViewModel</returns>
        AddToCartViewModel UpdateQuantityOfCartItem(string guid, string quantity, int productId);

        /// <summary>
        /// Remove cart item from shopping cart.
        /// </summary>
        /// <param name="guiId">External Id.</param>
        /// <returns>True if removed else false.</returns>
        bool RemoveCartItem(string guiId);

        /// <summary>
        /// Remove cart items from shopping cart.
        /// </summary>
        /// <param name="guiId">Comma separated External Ids.</param>
        /// <returns>True if removed else false.</returns>
        bool RemoveCartItems(string[] guiId);

        /// <summary>
        /// Remove all cart items from shopping cart.
        /// </summary>
        /// <returns>True if removed else false.</returns>
        bool RemoveAllCartItems();

        /// <summary>
        /// Remove all cart items from shopping cart.
        /// </summary>
        /// <param name="omsOrderId">omsOrderId</param>
        /// <returns>True if removed else false.</returns>
        bool RemoveAllCartItems(int omsOrderId = 0);

        /// <summary>
        /// Get Cart count method to check Session or Cookie to get the existing cart item count.
        /// </summary>
        /// <returns>Returns count of cart items.</returns>
        decimal GetCartCount();

        /// <summary>
        /// Get Cart count method to check Session or Cookie to get the existing cart item count.
        /// </summary>
        /// <returns>Returns count of cart items.</returns>
        decimal GetCartCountAfterLogin(bool cartCountAfterLogin);

        /// <summary>
        /// Get cart count of a specific product.
        /// </summary>
        /// <param name="productId">productId</param>
        /// <returns>Return count of items in cart for the product.</returns>
        decimal GetCartCount(int productId);

        /// <summary>
        /// Get template cart model session.
        /// </summary>
        /// <returns>Returns count of cart items.</returns>
        TemplateViewModel GetTemplateCartModelSession();

        /// <summary>
        /// Get cart from cookie.
        /// </summary>
        /// <returns>ShoppingCartModel</returns>
        ShoppingCartModel GetCartFromCookie();

        /// <summary>
        /// Calculate shipping for shopping cart.
        /// </summary>
        /// <param name="shippingOptionId">shipping option id.</param>
        /// <param name="shippingAddressId">shipping address id.</param>
        /// <param name="shippingCode">shipping code.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel CalculateShipping(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "");

        /// <summary>
        /// Merge cart.
        /// </summary>
        /// <returns>true if merged else false.</returns>
        bool MergeCart();

        /// <summary>
        /// Apply coupon/giftcard to shopping cart.
        /// </summary>
        /// <param name="discountCode">Coupon Code/ Gift Card Number.</param>
        /// <param name="isGiftCard">flag to check if giftcard needs to apply or coupon.</param>
        /// <returns>CartViewModel.</returns>
        CartViewModel ApplyDiscount(string discountCode, bool isGiftCard);

        /// <summary>
        /// Removes the applied coupon from the cart.
        /// </summary>
        /// <param name="couponCode">coupon code to remove.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel RemoveCoupon(string couponCode);

        #region Template

        /// <summary>
        /// Add product to template.
        /// </summary>
        /// <param name="cartItem">TemplateItemViewModel</param>
        /// <returns>Returns TemplateViewModel.</returns>
        TemplateViewModel AddToTemplate(TemplateCartItemViewModel cartItem);

        /// <summary>
        /// Set template cart model session to null.
        /// </summary>
        void SetTemplateCartModelSessionToNull();

        /// <summary>
        /// Delete template on the basis of oms Template Id.
        /// </summary>
        /// <param name="omsTemplateId">omsTemplateId</param>        
        /// <returns>Returns status</returns>
        bool DeleteTemplate(string omsTemplateId);


        /// <summary>
        /// Get template list against a user.
        /// </summary>
        /// <param name="filters">Filters.</param>
        /// <param name="sortCollection">Sort.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="recordPerPage">Record per page.</param>
        /// <returns>Returns list of template.</returns>
        TemplateListViewModel GetTemplateList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);


        /// <summary>
        /// Create template.
        /// </summary>
        /// <param name="templateViewModel">Template view model to create.</param>
        /// <returns>Returns true if template created succesfully else false.</returns>
        bool CreateTemplate(TemplateViewModel templateViewModel);

        /// <summary>
        /// Get template.
        /// </summary>
        /// <param name="omsTemplateId">Oms template id.</param>
        /// <returns>Returns TemplateViewModel.</returns>
        TemplateViewModel GetTemplate(int omsTemplateId);

        /// <summary>
        /// Update the template item quantity.
        /// </summary>
        /// <param name="guid">Unique id.</param>
        /// <param name="quantity">Quantity.</param>
        /// <param name="productId">Product id.</param>
        /// <returns>Returns the template view model.</returns>
        TemplateViewModel UpdateTemplateItemQuantity(string guid, decimal quantity, int productId);

        /// <summary>
        /// Add the template to cart.
        /// </summary>
        /// <param name="omsTemplateId">OmsTemplateId</param>
        /// <returns>Returns error message if any.</returns>
        string AddTemplateToCart(int omsTemplateId);

        /// <summary>
        /// Remove cart item from template.
        /// </summary>
        /// <param name="guiId">External Id.</param>
        /// <returns>True if removed else false.</returns>
        bool RemoveTemplateCartItem(string guiId);

        /// <summary>
        /// Remove all cart items from template.
        /// </summary>
        /// <returns>True if removed else false.</returns>
        bool RemoveAllTemplateCartItems();

        /// <summary>
        /// Add multiple items to cart template
        /// </summary>
        /// <param name="cartItems">list of cart items</param>
        /// <returns>error/ success message.</returns>
        string AddMultipleProductsToCartTemplate(List<TemplateCartItemViewModel> cartItems);
        #endregion

        /// <summary>
        /// Add multiple items to cart
        /// </summary>
        /// <param name="cartItems">list of cart items</param>
        /// <returns>error/ success message.</returns>
        string AddMultipleProductsToCart(List<CartItemViewModel> cartItems);

        /// <summary>
        /// Get shipping estimates based on the zip code
        /// </summary>
        /// <param name="zipCode">ZipCode</param>
        /// <returns>ShippingOptionListViewModel</returns>
        ShippingOptionListViewModel GetShippingEstimates(string zipCode);

        /// <summary>
        /// Insert estimated shipping id in session
        /// </summary>
        /// <param name="shippingId">Shipping Id</param>
        void AddEstimatedShippingIdToCartViewModel(int shippingId);

        /// <summary>
        /// Insert estimated shipping details in session
        /// </summary>
        /// <param name="shippingId">Shipping Id</param>
        /// <param name="zipCode">Zip code.</param>
        void AddEstimatedShippingDetailsToCartViewModel(int shippingId, string zipCode);

        /// <summary>
        /// Add cart items to cart. 
        /// </summary>
        /// <param name="cartModel">ShoppingCartModel</param>
        /// <returns>Return the updated shopping cart item status.</returns>
        bool UpdateCart(ref ShoppingCartModel cartModel);

        /// <summary>
        /// Add cart items to cart.
        /// </summary>
        /// <param name="cartModel">ShoppingCartModel</param>
        /// <returns>Return the updated shopping cart items.</returns>
        ShoppingCartModel UpdateCartDetails(ShoppingCartModel cartModel);

        /// <summary>
        /// Merge Cart after login
        /// </summary>
        /// <returns>Return true if cart gets merged</returns>
        bool MergeGuestUserCart();


        /// <summary>
        /// Get cart
        /// </summary>
        /// <returns>CartViewModel</returns>
        CartViewModel SetQuoteCart(int omsQuoteId);

        #region AmazonPay

        /// <summary>
        /// Calculate shipping for AmazonPay shopping cart.
        /// </summary>
        /// <param name="shippingOptionId">shipping Option Id</param>
        /// <param name="shippingAddressId">shipping Address Id</param>
        /// <param name="shippingCode">shipping Code</param>
        /// <param name="addressViewModel">address View Model</param>
        /// <returns>CartViewModel</returns>
        CartViewModel CalculateAmazonShipping(int shippingOptionId, int shippingAddressId, string shippingCode, AddressViewModel addressViewModel);

        #endregion
    }
}
