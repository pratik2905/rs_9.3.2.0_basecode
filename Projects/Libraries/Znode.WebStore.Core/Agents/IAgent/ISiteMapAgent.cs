﻿using Znode.Engine.Api.Models;

namespace Znode.WebStore.Core.Agents
{
    public interface ISiteMapAgent
    {
        /// <summary>
        /// Get Publish Product List.
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Returns Publish Product List Model.</returns>
        PublishProductListModel GetPublishProductList(int? pageIndex, int? pageSize);
     }
}
