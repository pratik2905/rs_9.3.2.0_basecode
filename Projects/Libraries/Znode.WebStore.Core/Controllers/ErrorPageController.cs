﻿using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Controllers
{
    public class ErrorPageController : Controller
    {
        // GET: ErrorPage
        public virtual ActionResult Index()
        {
            HttpContext.Response.AddHeader("Location", "/Error");
            return new HttpStatusCodeResult(307);
        }
      /*Nivi 9.2.1.14 patch*/
        public virtual ActionResult PageNotFound()
        {
            //HttpContext.Response.AddHeader("Location", "/404");
            // return new HttpStatusCodeResult(307);
            //Getting Seo URL details for 404 seo slug
            SEOUrlViewModel model = GetSeoUrlDetails("404");

            if (HelperUtility.IsNotNull(model) && model.SEOId > 0 && model.IsActive)
            {
                //setting 404 reponse from CMS content page from passing 404 contagepageId
                Set404Response(model.SEOId);

                Response.StatusCode = 404;

                Response.TrySkipIisCustomErrors = true;

                Response.End();
            }
            else
            {
                Response.ContentType = "text/html;charset=UTF-8";
                return View("ErrorPage");
            }

            return null;
        }

        [AllowAnonymous]
        public virtual ActionResult UnAuthorizedErrorRequest()=>
            View("UnAuthorizedRequest");
        /*Nivi 9.2.1.14 patch*/
        //Get Seo Details by seo slug
        protected virtual SEOUrlViewModel GetSeoUrlDetails(string seoSlug)
        {
            IWebstoreHelper helper = ZnodeDependencyResolver.GetService<IWebstoreHelper>();

            return helper.GetSeoUrlDetail(seoSlug);
        }

        //Set 404 content page response of current portal
        protected virtual void Set404Response(int seoId)
        {
            var routeData = new RouteData();

            routeData.Values.Add("controller", "ContentPage");
            routeData.Values.Add("action", "ContentPage");
            routeData.Values.Add("contentPageId", seoId);

            var controller = ZnodeDependencyResolver.GetService<ContentPageController>();

            ((IController)controller).Execute(new RequestContext(HttpContext, routeData));
        }
        /*Nivi 9.2.1.14 patch*/
    }
}