﻿using System.Web.Mvc;
using Znode.WebStore.Core.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class SiteMapController : BaseController
    {
        #region Private Readonly members
        private readonly ISiteMapAgent _siteMapAgent;
        #endregion

        #region Public Constructor
        public SiteMapController(ISiteMapAgent siteMapAgent)
        {
            _siteMapAgent = siteMapAgent;
        }
        #endregion

        // GET: SiteMap
        [HttpGet]
        public virtual ActionResult Index() => ActionView("SiteMap");

        // GET : Get Publish Product.
        public virtual JsonResult GetPublishProduct(int? pageIndex, int? pageSize)
            => Json(new { result = _siteMapAgent.GetPublishProductList(pageIndex, pageSize)?.PublishProducts }, JsonRequestBehavior.AllowGet);
    }
}