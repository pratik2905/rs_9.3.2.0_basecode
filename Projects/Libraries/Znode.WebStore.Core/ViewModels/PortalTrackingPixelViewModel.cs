﻿namespace Znode.Engine.WebStore.ViewModels
{
    public class PortalTrackingPixelViewModel : BaseViewModel
    {
        public int PortalId { get; set; }
        public int PortalPixelTrackingId { get; set; }

        public string PixelId1 { get; set; }
        public string PixelId2 { get; set; }
        public string PixelId3 { get; set; }
        public string PixelId4 { get; set; }
        public string PixelId5 { get; set; }
        public string CodePixel1 { get; set; }
        public string CodePixel2 { get; set; }
        public string CodePixel3 { get; set; }
        public string CodePixel4 { get; set; }
        public string CodePixel5 { get; set; }
        public string HelpTextPixel1 { get; set; }
        public string HelpTextPixel2 { get; set; }
        public string HelpTextPixel3 { get; set; }
        public string HelpTextPixel4 { get; set; }
        public string HelpTextPixel5 { get; set; }
    }
}
