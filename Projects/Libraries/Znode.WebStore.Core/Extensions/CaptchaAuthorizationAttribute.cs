﻿using CaptchaMvc.HtmlHelpers;
using System;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Core.Extensions
{
    public class CaptchaAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => x.AttributeCode == "IsCaptchaRequired")?.AttributeValue))
                filterContext.Controller.IsCaptchaValid(WebStore_Resources.ErrorReuiredDetails);
        }
    }
}
