﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IDashboardService
    {
        /// <summary>
        /// Gets the list of top brands
        /// </summary>
        /// <param name="filters">filters for top brands list</param>
        /// <param name="sorts">sorts for top brands list</param>
        /// <param name="page">page for top brands list</param>
        /// <returns>returns the list of top brands list</returns>
        DashboardTopItemsListModel GetDashboardTopBrands(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the list of top categories
        /// </summary>
        /// <param name="filters">filters for top categories list</param>
        /// <param name="sorts">sorts for top categories list</param>
        /// <param name="page">page for top categories list</param>
        /// <returns>returns the list of top categories list</returns>
        DashboardTopItemsListModel GetDashboardTopCategories(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the list of top products
        /// </summary>
        /// <param name="filters">filters for top products list</param>
        /// <param name="sorts">sorts for top products list</param>
        /// <param name="page">page for top products list</param>
        /// <returns>returns the list of top products list</returns>
        DashboardTopItemsListModel GetDashboardTopProducts(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the list of top searches
        /// </summary>
        /// <param name="filters">filters for top searches list</param>
        /// <param name="sorts">sorts for top searches list</param>
        /// <param name="page">page for top searches list</param>
        /// <returns>returns the list of top searches list</returns>
        DashboardTopItemsListModel GetDashboardTopSearches(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets total sales, total orders, total customers and average orders
        /// </summary>
        /// <param name="filters">filters for total parameters</param>
        /// <param name="sorts">sorts for total parameters</param>
        /// <param name="page">page for top searches list</param>
        /// <returns>returns total sales, total orders, total customers and average orders</returns>
        DashboardTopItemsListModel GetDashboardSalesDetails(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets dashboard low inventory product count
        /// </summary>
        /// <param name="filters">filters for total parameters</param>
        /// <param name="sorts">sorts for total parameters</param>
        /// <param name="page">page for top searches list</param>
        /// <returns>returns dashboard low inventory product count</returns>
        DashboardTopItemsListModel GetDashboardLowInventoryProductCount(FilterCollection filters, NameValueCollection sorts, NameValueCollection page);
    }
}
