﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IFormSubmissionService
    {
        /// <summary>
        /// Get the list of form submission.
        /// </summary>
        /// <param name="expands">Expands to be retrieved along with form submission list.</param>
        /// <param name="filters">Filters to be applied on form submission list.</param>
        /// <param name="sorts">Sorting to be applied on form submission list.</param>
        /// <param name="page">Page index.</param>
        /// <returns>Returns list of form submission.</returns>
        FormSubmissionListModel GetFormSubmissionList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Form Submission Details.
        /// </summary>
        /// <param name="formSubmitId">int formSubmitId</param>
        /// <returns>retuns FormBuilderAttributeGroupModel</returns>
        FormBuilderAttributeGroupModel GetFormSubmitDetails(int formSubmitId);
    }
}
