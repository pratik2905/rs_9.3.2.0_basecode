﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public interface ICategoryMongo
    {
        /// <summary>
        /// Get Publish Category Details
        /// </summary>
        /// <param name="categoryId">Category Id to get category</param>
        /// <param name="localeId">Locale Id to get Locale specific category</param>
        /// <returns>CategoryEntity</returns>
        CategoryEntity GetCategory(int categoryId, int localeId);

        /// <summary>
        ///  Method Gets Publish Parent Category by publish Catalog Id and localeId
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Category</param>
        /// <param name="localeId">Locale Id to get Publish Category</param>
        /// <returns>CategoryEntity List</returns>
        List<CategoryEntity> GetCategoryByCatalogId(int catalogId, int localeId);

        /// <summary>
        ///  Method Gets all publish categories satisfying the query
        /// </summary>
        /// <param name="query">Query to get categories</param>
        /// <returns>CategoryEntity List</returns>
        List<CategoryEntity> GetPublishCategories(IMongoQuery query);

        /// <summary>
        ///  Method Gets Publish Category by publish Catalog Id, Publish Category Id and locale Id
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Category</param>
        /// <param name="categoryId">Publish Category Id to get Publish Category</param>
        /// <param name="localeId">Locale Id to get Publish Category</param>
        /// <returns>CategoryEntity List</returns>
        List<CategoryEntity> GetCategory(int catalogId, int categoryId, int localeId);
    }
}
