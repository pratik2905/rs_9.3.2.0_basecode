﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IGeneralSettingService
    {
        /// <summary>
        /// Method To get List Of All General Setting
        /// </summary>
        /// <returns>model of general setting list</returns>
        GeneralSettingModel List();

        /// <summary>
        /// Method To Update Existing General Setting
        /// </summary>
        /// <param name="model">general setting model with values to update</param>
        /// <returns>true/false</returns>
        bool Update(GeneralSettingModel model);

        /// <summary>
        /// Method to get Cache Management data
        /// </summary>
        /// <returns>model containing Cache Management Data</returns>
        CacheListModel GetCacheData();

        /// <summary>
        /// Updates provide Cache data if already exists, otherwise creates new entry.
        /// </summary>
        /// <param name="cacheListModel"></param>
        /// <returns>return true if updated or created successfully else return false.</returns>
        bool CreateUpdateCache(CacheListModel cacheListModel);

        /// <summary>
        /// Refresh cache data.
        /// </summary>
        /// <param name="cacheModel"></param>
        /// <returns>refresh cache cacheModel.</returns>
        CacheModel RefreshCacheData(CacheModel cacheModel);

        /// <summary>
        /// Method to get load balace environment data
        /// </summary>
        /// <returns>model containing Load balance environment Data</returns>
        LBDetailsModel GetLBDetails();

        /// <summary>
        /// Update the Load balance settings
        /// </summary>
        /// <param name="lbDetailsModel"></param>
        /// <returns>Updates the LB data</returns>
        bool UpdateLBDetails(LBDetailsModel lbDetailsModel);
    }
}
