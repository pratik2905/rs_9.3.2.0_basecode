﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using GenericParsing;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Resources;
using Znode.Libraries.Admin.Import;

namespace Znode.Engine.Services
{
    public class ImportService : BaseService, IImportService
    {
        #region Private Variables
        private ApplicationUserManager _userManager;
        private readonly IZnodeRepository<ZnodeImportHead> _znodeImportHead;
        private readonly IZnodeRepository<ZnodeImportTemplate> _znodeImportTemplate;
        private readonly IZnodeRepository<ZnodeImportTemplateMapping> _znodeImportTemplateMapping;
        private readonly IZnodeRepository<ZnodeImportAccountDefaultTemplate> _importAccountDefaultTemplate;
        private readonly IZnodeRepository<ZnodeImportLog> _importLogDetails;
        private readonly IZnodeRepository<ZnodeImportProcessLog> _importLogs;
        private readonly IZnodeRepository<ZnodePimAttributeFamily> _znodeFamily;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IImportHelper importHelper;
        private readonly IUserService _userService;
        #endregion

        #region Constructor
        public ImportService()
        {
            _znodeImportHead = new ZnodeRepository<ZnodeImportHead>();
            _znodeImportTemplate = new ZnodeRepository<ZnodeImportTemplate>();
            _znodeImportTemplateMapping = new ZnodeRepository<ZnodeImportTemplateMapping>();
            _importLogDetails = new ZnodeRepository<ZnodeImportLog>();
            _importLogs = new ZnodeRepository<ZnodeImportProcessLog>();
            _znodeFamily = new ZnodeRepository<ZnodePimAttributeFamily>();
            _importAccountDefaultTemplate = new ZnodeRepository<ZnodeImportAccountDefaultTemplate>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            importHelper = ZnodeDependencyResolver.GetService<IImportHelper>();
            _userService = ZnodeDependencyResolver.GetService<IUserService>();
        }
        #endregion

        #region Public Methods
        //get all templates with respect to import head id
        public virtual ImportModel GetAllTemplates(int importHeadId, int familyId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters importHeadId and familyId values: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, new object[] { importHeadId, familyId });

            if (importHeadId > 0)
            {
                //generate the filter for Import head id
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeImportTemplateEnum.ImportHeadId.ToString(), FilterOperators.Equals, Convert.ToString(importHeadId)));
                filters.Add(new FilterTuple(ZnodeImportTemplateEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
                if (familyId.Equals(0))
                    filters.Add(new FilterTuple(ZnodeImportTemplateEnum.PimAttributeFamilyId.ToString(), FilterOperators.Equals, "Null"));
                else
                    filters.Add(new FilterTuple(ZnodeImportTemplateEnum.PimAttributeFamilyId.ToString(), FilterOperators.Equals, Convert.ToString(familyId)));
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("WhereClause generated to get importTemplates list: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);

                //pass the filter to get list of templates
                List<ZnodeImportTemplate> importTemplates = _znodeImportTemplate.GetEntityList(whereClauseModel.WhereClause, string.Empty).ToList();
                ZnodeLogging.LogMessage("importTemplates list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importTemplates?.Count());

                ImportModel model = new ImportModel();

                //bind template list in model
                model.TemplateList = new ImportTemplateListModel { TemplateList = importTemplates.ToModel<ImportTemplateModel>().ToList() };
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
                return model;
            }
            else
            {
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
                return new ImportModel();
            }
        }

        //get complete list of import types available
        public virtual ImportModel GetImportTypeList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to get list of import types available: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            List<ZnodeImportHead> importHead = _znodeImportHead.GetEntityList(pageListModel?.EntityWhereClause?.WhereClause)?.ToList();
            ZnodeLogging.LogMessage("importHead list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importHead?.Count());

            ImportModel model = new ImportModel();
            model.ImportTypeList = new ImportTypeListModel { ImportTypeList = importHead.ToModel<ImportTypeModel>().ToList() };
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            return model;
        }

        //get all data of templates
        public virtual ImportModel GetTemplateData(int templateId, int importHeadId, int familyId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters templateId, importHeadId and familyId : ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, new object[] { templateId, importHeadId, familyId });

            //If template id greater than 0 then we will show template mappings.
            if (templateId > 0)
            {
                //generate the filter with Import template id
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeImportTemplateEnum.ImportTemplateId.ToString(), FilterOperators.Equals, Convert.ToString(templateId)));
                if (familyId.Equals(0))
                    filters.Add(new FilterTuple(ZnodeImportTemplateEnum.PimAttributeFamilyId.ToString(), FilterOperators.Equals, "Null"));
                else
                    filters.Add(new FilterTuple(ZnodeImportTemplateEnum.PimAttributeFamilyId.ToString(), FilterOperators.Equals, Convert.ToString(familyId)));
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("WhereClause generated to get templates data: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);

                //get the templates data from import template id
                ZnodeImportTemplate importTemplate = _znodeImportTemplate.GetEntity(whereClauseModel.WhereClause);

                filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeImportTemplateEnum.ImportTemplateId.ToString(), FilterOperators.Equals, Convert.ToString(templateId)));
                whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("WhereClause generated to get all mappings with respect to templateId: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);

                //use the above created filter to get all mappings with respect to template id
                List<ZnodeImportTemplateMapping> mappings = _znodeImportTemplateMapping.GetEntityList(whereClauseModel.WhereClause).ToList();

                //bind Template mappings and template data in model
                ImportModel model = new ImportModel();
                model.SelectedTemplate = importTemplate.ToModel<ImportTemplateModel>();
                model.TemplateMappingList = new ImportTemplateMappingListModel { Mappings = mappings.ToModel<ImportTemplateMappingModel>().ToList() };
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
                return model;
            }
            else
            {
                //If template id is 0 then we will show the attributes 

                //generate where clause
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeImportTemplateEnum.ImportHeadId.ToString(), FilterOperators.Equals, Convert.ToString(importHeadId)));

                //create object to fetch the records
                IZnodeViewRepository<ImportTemplateMappingModel> objStoredProc = new ZnodeViewRepository<ImportTemplateMappingModel>();
                objStoredProc.SetParameter("@ImportHeadId", importHeadId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@PimAttributeFamilyId", familyId, ParameterDirection.Input, DbType.Int32);

                //create list model
                ImportTemplateMappingListModel listModel = new ImportTemplateMappingListModel();

                //assign the SP result back to model
                List<ImportTemplateMappingModel> importTemplateMapping = objStoredProc.ExecuteStoredProcedureList("Znode_ImportGetDefaultFamilyAttribute @ImportHeadId, @PimAttributeFamilyId")?.ToList();
                ZnodeLogging.LogMessage("importTemplateMapping list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importTemplateMapping?.Count());

                listModel.Mappings = importTemplateMapping?.Count > 0 ? importTemplateMapping : new List<ImportTemplateMappingModel>();
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
                //return the model
                return new ImportModel { TemplateMappingList = listModel };
            }
        }

        // This method will fetch the data from file and insert it into DB and then inserted data will be processed.
        public virtual int ProcessData(ImportModel importModel)
        {
       try
          { 
                int userId = GetLoginUserId();
                if (Equals(importModel.ImportType, ZnodeConstant.AdminUser))
                    return ImportAdminUsers(importModel.FileName);
                else
                    return importHelper.ProcessData(importModel, userId);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Failed to Process Import Data", ZnodeLogging.Components.Import.ToString(), TraceLevel.Error, ex);
                if (Equals(importModel.ImportType, ZnodeConstant.AdminUser))
                {
                    IZnodeRepository<ZnodeImportProcessLog> _importProcessLogRepository = new ZnodeRepository<ZnodeImportProcessLog>();
                    IZnodeRepository<ZnodeImportTemplate> _importTemplateRepository = new ZnodeRepository<ZnodeImportTemplate>();
                    IZnodeRepository<ZnodeImportLog> _importLogRepository = new ZnodeRepository<ZnodeImportLog>();

                    var importProcessLog = _importProcessLogRepository.Insert(new ZnodeImportProcessLog
                    {
                        ImportTemplateId = _importTemplateRepository.Table.FirstOrDefault(x => x.TemplateName == "AdminUserTemplate")?.ImportTemplateId,
                        Status = ZnodeConstant.SearchIndexFailedStatus,
                        ProcessStartedDate = DateTime.UtcNow,
                        ProcessCompletedDate = DateTime.UtcNow.AddMinutes(1.0)
                    });

                    _importLogRepository.Insert(new ZnodeImportLog
                    {
                        ImportProcessLogId = importProcessLog.ImportProcessLogId,
                        ErrorDescription = ex.Message,
                        Guid = Convert.ToString(new Guid()),

                    });
                }
                throw new ZnodeException(ErrorCodes.ImportError, ex.Message);
            }
        }

        // Downloads the model
        public virtual DownloadModel DownLoadTemplate(int importHeadId, int familyId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters importHeadId and familyId values: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, new object[] { importHeadId, familyId });
            DownloadModel model = new DownloadModel();
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@importHeadId", importHeadId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@PimAttributeFamilyId", familyId, ParameterDirection.Input, SqlDbType.Int);
            DataSet ds = executeSpHelper.GetSPResultInDataSet("Znode_ImportGetDefaultFamilyAttribute");
            model.data = ConvertImportDataSetToList(ds);
            ZnodeLogging.LogMessage("ConvertImportDataSetToList list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, model?.data?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            return model;
        }

        //Get the import log details on the basis of Import Log Id
        public virtual ImportLogDetailsListModel GetImportLogDetails(int importProcessLogId, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter importProcessLogId: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importProcessLogId);

            //check if filter is blank.
            if (IsNull(filters))
                filters = new FilterCollection();

            //add the filter
            filters.Add(new FilterTuple(ZnodeImportProcessLogEnum.ImportProcessLogId.ToString(), FilterOperators.Equals, Convert.ToString(importProcessLogId)));

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            IZnodeViewRepository<ImportLogDetailsModel> objStoredProc = new ZnodeViewRepository<ImportLogDetailsModel>();

            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            IList<ImportLogDetailsModel> importLogList = objStoredProc.ExecuteStoredProcedureList("Znode_GetImportProcessLog @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("ImportLogDetailsModel list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importLogList?.Count());

            ImportLogDetailsListModel model = new ImportLogDetailsListModel { ImportLogDetails = importLogList?.ToList() };
            model.ImportLogs = GetCurrentLogStatus(importProcessLogId);
            model.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            return model;
        }

        //Get the Import Logs - Get the template name and ImportProcessLogId order by ImportProcessLogId desc
        public virtual ImportLogsListModel GetImportLogs(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            if (IsNull(expands))
                expands = new NameValueCollection();

            List<string> navigationProperties = new List<string>();

            SetExpands(ExpandKeys.ZnodeImportTemplate, navigationProperties);

            IZnodeViewRepository<ImportLogsModel> objStoredProc = new ZnodeViewRepository<ImportLogsModel>();

            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            IList<ImportLogsModel> importLogList = objStoredProc.ExecuteStoredProcedureList("Znode_GetImportTemplateLogs @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("ImportLogsModel list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importLogList?.Count());
            ImportLogsListModel model = new ImportLogsListModel { ImportLogs = importLogList?.ToList() };
            model.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);

            return model;
        }

        //Get the import log status
        public virtual ImportLogsListModel GetLogStatus(int importProcessLogId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter importProcessLogId: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importProcessLogId);

            ImportLogsModel model = new ImportLogsModel();
            model = _importLogs.GetById(importProcessLogId).ToModel<ImportLogsModel>();
            ZnodeLogging.LogMessage("ImportLogsModel: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, model);
            ImportLogsListModel listModel = new ImportLogsListModel();
            listModel.ImportLogs = new List<ImportLogsModel>();
            listModel.ImportLogs.Add(model);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            return listModel;
        }

        public virtual ImportLogsModel GetCurrentLogStatus(int importProcessLogId)
        {
            ImportLogsModel model = new ImportLogsModel();
            model = _importLogs.GetById(importProcessLogId).ToModel<ImportLogsModel>();
            return model;
        }

        // Delete the import logs
        public virtual bool DeleteLogDetails(ParameterModel importProcessLogIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(importProcessLogIds?.Ids))
                return false;
            ZnodeLogging.LogMessage("Input parameter importProcessLogIds to be deleted: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, importProcessLogIds?.Ids);

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("@ImportProcessLogId", importProcessLogIds.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            IList<View_ReturnBoolean> result = objStoredProc.ExecuteStoredProcedureList("Znode_ImportRemoveLogs @ImportProcessLogId,  @Status OUT", 1, out status);
            ZnodeLogging.LogMessage("Deleted result count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, result?.Count());

            return result.FirstOrDefault().Status.Value;
        }

        //Get all families for product import
        public virtual ImportModel GetFamilies(bool isCategory)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimAttributeFamilyEnum.IsCategory.ToString(), FilterOperators.Equals, Convert.ToString(isCategory)));

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated to get familyEntityList: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);
            List<ZnodePimAttributeFamily> familyEntityList = _znodeFamily.GetEntityList(whereClauseModel.WhereClause)?.ToList();
            ZnodeLogging.LogMessage("familyEntityList list count: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, familyEntityList?.Count());

            ImportModel model = new ImportModel();
            model.FamilyList = new ImportProductFamilyListModel { FamilyList = familyEntityList.ToModel<ImportProductFamilyModel>().ToList() };
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            return model;
        }

        public virtual int UpdateTemplateMappings(ImportModel model) => importHelper.UpdateTemplateData(model);

        //check the import status
        public virtual bool CheckImportStatus() => importHelper.CheckImportStatus();

        public virtual ImportModel GetDefaultTemplate(string templateName)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);

            if (string.IsNullOrEmpty(templateName))
                throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.ImportTypeNotEmpty);
            ZnodeLogging.LogMessage("Input parameter templateName: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, templateName);

            ImportModel importModel = (from defaultTemplate in _importAccountDefaultTemplate.Table
                                       join importHead in _znodeImportHead.Table on defaultTemplate.ImportHeadId equals importHead.ImportHeadId
                                       join importTemplate in _znodeImportTemplate.Table on importHead.ImportHeadId equals importTemplate.ImportHeadId
                                       where defaultTemplate.TemplateName == templateName.Trim()
                                       select new ImportModel
                                       {
                                           ImportTypeId = defaultTemplate.ImportHeadId,
                                           TemplateId = importTemplate.ImportTemplateId,
                                           TemplateName = importTemplate.TemplateName,
                                           ImportType = importHead.Name

                                       })?.FirstOrDefault() ?? new ImportModel();
            ZnodeLogging.LogMessage("TemplateId, ImportType and ImportTypeId properties of importModel: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, new object[] { importModel?.TemplateId, importModel?.ImportType, importModel?.ImportTypeId });

            importModel.TemplateMappingList = new ImportTemplateMappingListModel { Mappings = _znodeImportTemplateMapping.Table.Where(w => w.ImportTemplateId == importModel.TemplateId).ToModel<ImportTemplateMappingModel>().ToList() };
            return importModel ?? new ImportModel();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// This method convert the Dataset to List
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List<dynamic></returns>
        private List<dynamic> ConvertImportDataSetToList(DataSet ds)
        {
            var tblPivot = new DataTable();
            for (int iCount = 0; iCount < ds.Tables[0].Rows.Count; iCount++)
                tblPivot.Columns.Add(Convert.ToString(ds.Tables[0].Rows[iCount][0]));

            return ConvertToList(tblPivot);
        }

        /// <summary>
        /// This method will convert Datatable to list
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <returns>List<dynamic></returns>
        private List<dynamic> ConvertToList(DataTable dt)
        {
            var dynamicDt = new List<dynamic>();

            for (int i = 0; i < 1; i++)
            {
                dynamic dyn = new ExpandoObject();
                dynamicDt.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    dic[column.ColumnName] = "";
                }
            }
            return dynamicDt;
        }

        private int ImportAdminUsers(string fileName)
        {
            DataTable dt = new DataTable();
            try
            {
                //Read data from file and create Data Table.
                using (GenericParserAdapter parser = new GenericParserAdapter(fileName))
                {
                    parser.ColumnDelimiter = ZnodeConstant.ColumnDelimiter;
                    parser.FirstRowHasHeader = true;
                    dt = parser.GetDataTable();
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Error);
                throw ex;
            }
            //throw ZnodeException if more than 100 Admin Users are getting imported at a time.
            if (dt?.Rows.Count > 100)
                throw new ZnodeException(ErrorCodes.ImportError, "It is recommended to import 100 Admin Users at a time.");

            //Import Admin Users.
            foreach (var item in dt.ToList<UserModel>())
            {
                item.User = new LoginUserModel { Username = item.UserName, Email = item.UserName };
                item.Email = item.UserName;

                if (!string.IsNullOrEmpty(item.StoreCode))
                {
                    FilterCollection filters = new FilterCollection();
                    filters.Add(ZnodePortalEnum.StoreCode.ToString(), FilterOperators.In, !string.IsNullOrEmpty(item.StoreCode) ? string.Join(",", item.StoreCode.Split(',')?.ToArray()?.Select(x => $"\"{x}\"")) : null);
                    item.PortalIds = _portalRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause)?.ToList()?.Select(x => x.PortalId.ToString())?.ToArray();
                }
                else
                    item.PortalIds = new string['0'];

                _userService.CreateAdminUser(item);
            }

            IZnodeRepository<ZnodeImportProcessLog> _importProcessLogRepository = new ZnodeRepository<ZnodeImportProcessLog>();
            IZnodeRepository<ZnodeImportTemplate> _importTemplateRepository = new ZnodeRepository<ZnodeImportTemplate>();

            _importProcessLogRepository.Insert(new ZnodeImportProcessLog
            {
                ImportTemplateId = _importTemplateRepository.Table.FirstOrDefault(x => x.TemplateName == "AdminUserTemplate")?.ImportTemplateId,
                Status = ZnodeConstant.CompletedStatus,
                ProcessStartedDate = DateTime.UtcNow,
                ProcessCompletedDate = DateTime.UtcNow.AddMinutes(1.0)
            });
            return 1;
        }
        #endregion

    }
}
