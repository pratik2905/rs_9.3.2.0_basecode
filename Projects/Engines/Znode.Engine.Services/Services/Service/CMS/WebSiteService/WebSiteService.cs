﻿using EntityFramework.Extensions;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Xml;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Helper;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.MongoDB.Data.DataModel;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class WebSiteService : BaseService, IWebSiteService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodeCMSPortalTheme> _cmsPortalThemeRepository;
        private readonly IZnodeRepository<ZnodeCMSTheme> _cmsThemeRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPage> _contentPageRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPagesProfile> _cmsContentPagesProfileRepository;
        private readonly IZnodeRepository<ZnodeCMSPortalProductPage> _cmsPortalProductPageRepository;
        private readonly IZnodeRepository<ZnodeMedia> _mediaManagerRepository;
        private readonly IZnodeRepository<ZnodeCMSSlider> _cmsSliderRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPagesLocale> _contentPagesLocaleRepository;
        private readonly IZnodeRepository<ZnodeCMSWidget> _widgetRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetCategory> _cmsWidgetCategoryRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetBrand> _cmsWidgetBrandRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetProduct> _cmsWidgetProductRepository;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;
        private readonly IZnodeRepository<ZnodePimCatalog> _catalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeCMSThemeCSS> _cmsThemeCSSRepository;
        private readonly IZnodeRepository<ZnodeCMSTemplate> _cmsTemplateRepository;
        private readonly IZnodeRepository<ZnodeCMSMessage> _cmsMessageRepository;

        private readonly IMongoRepository<ProductPageEntity> _cmsProductPageMongoRepository;
        private readonly IMongoRepository<DynamicStyleEntity> _dynamicStyleMongoRepository;
        private readonly IMongoRepository<WebStoreEntity> _cmsWebStoreMongoRepository;
        private readonly IMongoRepository<WidgetCategoryEntity> _cmsWidgetCategoryMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository;
        private readonly IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository;
        private readonly IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository;
        private readonly IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository;
        private readonly IMongoRepository<MediaWidgetEntity> _cmsMediaWidgetMongoRepository;
        private readonly IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository;
        private readonly IMongoRepository<MessageEntity> _cmsMessageMongoRepository;
        private readonly IMongoRepository<WidgetBrandEntity> _cmsWidgetBrandMongoRepository;
        private readonly IMongoRepository<BlogNewsEntity> _blogNewsMongoRepository;
        private readonly IMongoRepository<PortalGlobalAttributeEntity> _portalAttributMongoRepository;
        private readonly IMongoRepository<SeoEntity> _seoMongoRepository;
        private readonly IMongoRepository<_LogSeoEntity> log_seoMongoRepository;
        private readonly IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository;
        private readonly IZnodeRepository<ZnodePortalCustomCss> _portalCustomCssRepository;
        #endregion

        #region Constructor
        public WebSiteService()
        {
            _cmsPortalThemeRepository = new ZnodeRepository<ZnodeCMSPortalTheme>();
            _contentPageRepository = new ZnodeRepository<ZnodeCMSContentPage>();
            _cmsContentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();
            _cmsThemeRepository = new ZnodeRepository<ZnodeCMSTheme>();
            _cmsPortalProductPageRepository = new ZnodeRepository<ZnodeCMSPortalProductPage>();
            _mediaManagerRepository = new ZnodeRepository<ZnodeMedia>();
            _cmsSliderRepository = new ZnodeRepository<ZnodeCMSSlider>();
            _widgetRepository = new ZnodeRepository<ZnodeCMSWidget>();
            _cmsWidgetCategoryRepository = new ZnodeRepository<ZnodeCMSWidgetCategory>();
            _cmsWidgetBrandRepository = new ZnodeRepository<ZnodeCMSWidgetBrand>();
            _cmsWidgetProductRepository = new ZnodeRepository<ZnodeCMSWidgetProduct>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
            _catalogRepository = new ZnodeRepository<ZnodePimCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _cmsThemeCSSRepository = new ZnodeRepository<ZnodeCMSThemeCSS>();
            _cmsTemplateRepository = new ZnodeRepository<ZnodeCMSTemplate>();
            _contentPagesLocaleRepository = new ZnodeRepository<ZnodeCMSContentPagesLocale>();
            _cmsMessageRepository = new ZnodeRepository<ZnodeCMSMessage>();

            _cmsProductPageMongoRepository = new MongoRepository<ProductPageEntity>();
            _dynamicStyleMongoRepository = new MongoRepository<DynamicStyleEntity>();
            _cmsWebStoreMongoRepository = new MongoRepository<WebStoreEntity>();
            _cmsWidgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();
            _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>();
            _cmsMediaWidgetMongoRepository = new MongoRepository<MediaWidgetEntity>();
            _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            _cmsMessageMongoRepository = new MongoRepository<MessageEntity>();
            _cmsWidgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>();
            _blogNewsMongoRepository = new MongoRepository<BlogNewsEntity>();
            _portalAttributMongoRepository = new MongoRepository<PortalGlobalAttributeEntity>();
            _seoMongoRepository = new MongoRepository<SeoEntity>();
            log_seoMongoRepository = new MongoRepository<_LogSeoEntity>();
            _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>();
            _portalCustomCssRepository = new ZnodeRepository<ZnodePortalCustomCss>();
        }
        #endregion

        #region Public Methods

        #region Web Site Logo
        //Get the Portal List, for which the Themes are assigned.
        public virtual PortalListModel GetPortalList(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { pageListModel?.ToDebugString() });

            //Method to get locale id from filters.
            int localeId = 0;
            GetLocaleId(filters, ref localeId);

            IZnodeViewRepository<PortalModel> objStoredProc = new ZnodeViewRepository<PortalModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_BY", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowsCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);
            PortalListModel portalListModel = new PortalListModel() { PortalList = objStoredProc.ExecuteStoredProcedureList("Znode_GetCmsWebsiteConfiguration  @WhereClause,@Rows,@PageNo,@Order_BY,@RowsCount OUT,@LocaleId", 4, out pageListModel.TotalRowCount)?.ToList() };
            portalListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return portalListModel;
        }

        //Get Web Site Logo Details by Portal Id.
        public virtual WebSiteLogoModel GetWebSiteLogoDetails(int portalId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (portalId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.PortalNotIdLessThanOne);

            ZnodeLogging.LogMessage("Input parameter portalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId });

            //Check for User Portal Access.
            CheckUserPortalAccess(portalId);

            WebSiteLogoModel model = new WebSiteLogoModel();
            //Get the Portal Theme Information.
            ZnodeCMSPortalTheme portalTheme = _cmsPortalThemeRepository.Table.FirstOrDefault(x => x.PortalId == portalId);
            if (IsNotNull(portalTheme))
            {
                model = portalTheme.ToModel<WebSiteLogoModel>();
                //Get Theme name from CMSThemeId.               
                FilterCollection filter = new FilterCollection() { new FilterTuple(ZnodeCMSThemeEnum.CMSThemeId.ToString(), FilterOperators.Equals, portalTheme.CMSThemeId.ToString()) };
                ZnodeCMSTheme cmsTheme = _cmsThemeRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause, GetExpandsForParentTheme());
                model.ThemeName = cmsTheme?.Name;
                model.ParentThemeName = cmsTheme?.ZnodeCMSTheme2?.Name;
                ZnodeLogging.LogMessage("MediaId, ThemeName, ParentThemeName :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.MediaId, model?.ThemeName, model?.ParentThemeName });

                if (model?.MediaId > 0)
                {
                    IMediaManagerServices mediaService = GetService<IMediaManagerServices>();
                    MediaManagerModel mediaData = mediaService.GetMediaByID(Convert.ToInt32(portalTheme.MediaId), null);
                    MediaManagerModel faviconData = model?.FaviconId > 0 ? mediaService.GetMediaByID(Convert.ToInt32(portalTheme.FavIconId), null) : null;
                    model.LogoUrl = IsNotNull(mediaData) ? mediaData.MediaServerThumbnailPath : string.Empty;
                    model.FaviconUrl = IsNotNull(faviconData) ? faviconData?.MediaServerThumbnailPath : string.Empty;
                }
            }
            ZnodePortalCustomCss znodePortalCustomCss = _portalCustomCssRepository.Table.FirstOrDefault(x => x.PortalId == portalId);
            model.DynamicContent = znodePortalCustomCss.ToModel<DynamicContentModel>();

            IZnodeRepository<ZnodePortal> _portalRepository = new ZnodeRepository<ZnodePortal>();
            //Get the Portal Information.
            ZnodePortal portal = _portalRepository.Table.FirstOrDefault(x => x.PortalId == portalId);
            if (IsNotNull(portal))
                model.PortalName = portal.StoreName;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }

        //Save the WebSite Logo Details
        public virtual bool SaveWebSiteLogo(WebSiteLogoModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            ZnodeLogging.LogMessage("WebSiteLogoModel with CMSThemeId  :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.CMSThemeId });

            bool status = false;
            if (model.CMSThemeId > 0)
            {
                //Save the Web Site Logo information.
                status = _cmsPortalThemeRepository.Update(model.ToEntity<ZnodeCMSPortalTheme>());
                if (status)
                    ZnodeLogging.LogMessage(Admin_Resources.SuccessWebSiteLogoSave, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                else
                {
                    throw new ZnodeException(ErrorCodes.ExceptionalError, Admin_Resources.ErrorWhileWebSiteLogoDetailsSave);
                }
            }
            model.DynamicContent.PortalId = model.PortalId;
            
            SaveUpdateDynamicContent(model.DynamicContent);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return status;
        }

        protected virtual DynamicContentModel SaveUpdateDynamicContent(DynamicContentModel dynamicContentModel)
        {
            ZnodeLogging.LogMessage("SaveDynamicContent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (IsNull(dynamicContentModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorDynamicContentModelNull);

            ZnodePortalCustomCss znodePortalCustomCss = _portalCustomCssRepository.Table.FirstOrDefault(x=>x.PortalId == dynamicContentModel.PortalId);

            if(IsNotNull(znodePortalCustomCss))
            {
                dynamicContentModel.PortalCustomCssId = znodePortalCustomCss.PortalCustomCssId;
                _portalCustomCssRepository.Update(dynamicContentModel.ToEntity<ZnodePortalCustomCss>());
                return dynamicContentModel;
            }
            else
            {
                ZnodePortalCustomCss savePortalCustomCssEntity = null;
                if (IsNotNull(dynamicContentModel.DynamicCssStyle) || IsNotNull(dynamicContentModel.WYSIWYGFormatStyle))
                    savePortalCustomCssEntity = _portalCustomCssRepository.Insert(dynamicContentModel.ToEntity<ZnodePortalCustomCss>());
                return IsNotNull(savePortalCustomCssEntity) ? savePortalCustomCssEntity.ToModel<DynamicContentModel>() : dynamicContentModel;
            }
        }

        #endregion

        #region Portal Product Page
        //Get the list of portal page product associated to selected store in website configuration.
        public virtual PortalProductPageModel GetPortalProductPageList(int portalId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter portalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId });

            //Check for User Portal Access.
            CheckUserPortalAccess(portalId);

            List<ZnodeCMSPortalProductPage> portalProductPageList = (from list in _cmsPortalProductPageRepository.Table
                                                                     where list.PortalId == portalId
                                                                     select list)?.OrderBy(x => x.ProductType)?.ToList();

            ZnodeLogging.LogMessage("portalProductPageList count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalProductPageList?.Count });

            PortalProductPageModel model = new PortalProductPageModel();
            if (portalProductPageList?.Count > 0)
            {
                model.PortalProductPageList = new List<PortalProductPageModel>();
                foreach (ZnodeCMSPortalProductPage portalProductPage in portalProductPageList)
                    model.PortalProductPageList.Add(new PortalProductPageModel { PortalId = portalId, ProductType = portalProductPage.ProductType, TemplateName = portalProductPage.TemplateName });
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }


        //Assign new pdp template to product type.
        public virtual bool UpdatePortalProductPage(PortalProductPageModel portalProductPageModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            bool isUpdated = false;
            if (IsNull(portalProductPageModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            if (portalProductPageModel.PortalId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.PortalNotIdLessThanOne);

            ZnodeLogging.LogMessage("portalProductPageModel with PortalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalProductPageModel?.PortalId });

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeCMSPortalProductPageEnum.PortalId.ToString(), FilterOperators.Equals, portalProductPageModel.PortalId.ToString()));

            string whereClause = DynamicClauseHelper.GenerateDynamicWhereClause(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("whereClause for delete:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { whereClause });

            //Check if portal product page details already exists for current portal id.
            if (IsProductPageDetailsAvailable(portalProductPageModel.PortalId) && !_cmsPortalProductPageRepository.Delete(whereClause))
                throw new ZnodeException(ErrorCodes.AssociationDeleteError, Admin_Resources.ErrorDataAssociatedToPortalProductPageDelete);
            List<ZnodeCMSPortalProductPage> productPageList = new List<ZnodeCMSPortalProductPage>();

            for (int iLength = 0; iLength < portalProductPageModel.ProductTypeList.Count; iLength++)
                //Bind the newly updated date to entity list object.
                productPageList.Add(new ZnodeCMSPortalProductPage { PortalId = portalProductPageModel.PortalId, ProductType = portalProductPageModel.ProductTypeList[iLength], TemplateName = Convert.ToString(portalProductPageModel.TemplateNameList[iLength]) });

            //Insert new data. 
            IList<ZnodeCMSPortalProductPage> portalProductPageList = _cmsPortalProductPageRepository.Insert(productPageList)?.ToList();
            ZnodeLogging.LogMessage("portalProductPageList count:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalProductPageList.Count });

            isUpdated = portalProductPageList?.FirstOrDefault().CMSPortalProductPageId > 0;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return isUpdated;
        }
        #endregion

        #region Publish CMS

        [Obsolete("This method will be discontinued in one of the upcoming versions.")]
        // Publish CMS configuration
        public virtual bool Publish(int portalId)
        {
            ZnodePublishPortalLog znodePublishPortalLog = new ZnodePublishPortalLog();
            try
            {
                int userId = GetLoginUserId();

                //Insert null to the IsPortalPublished for first time. it means publish process is in progress.
                znodePublishPortalLog = _publishPortalLogRepository.Insert(new ZnodePublishPortalLog { IsPortalPublished = null, PortalId = portalId });

                //Active Content Page Ids Associated with Portal ID
                List<int> contentPageIds = _contentPageRepository.Table.Where(x => x.PortalId == portalId && x.IsActive).Select(x => x.CMSContentPagesId)?.ToList() ?? new List<int>();

                //Publish CMS Website Configuration to mongo
                PublishWebsiteConfiguration(portalId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Portal Product Page to mongo
                PublishPortalProductPagePages(portalId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Widget Category to mongo
                PublishWidgetCategory(portalId, contentPageIds, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Widget Product to mongo
                PublishWidgetProduct(portalId, contentPageIds, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Widget Slider Banner to mongo
                PublishWidgetSliderBanner(portalId, userId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Widget Title to mongo
                PublishTitleWidget(portalId, contentPageIds, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Text Widget Configuration to mongo
                PublishTextWidgetConfiguration(portalId, userId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Text Widget Configuration to mongo
                PublishSearchWidgetConfiguration(portalId, userId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Messages to mongo
                PublishMessages(portalId, userId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Content Page Configuration to mongo
                PublishContentPageConfiguration(portalId, znodePublishPortalLog.PublishPortalLogId);

                //Publish Blog/News to mongo.
                PublishBlogNews(portalId, znodePublishPortalLog.PublishPortalLogId);

                //Publish CMS Widget Brand to mongo
                PublishWidgetBrand(portalId, contentPageIds, znodePublishPortalLog.PublishPortalLogId);

                //Publish Portal Associted Global Attribute Details to Mongo
                PublishPortalAssociateAttributes(portalId, userId, znodePublishPortalLog.PublishPortalLogId);

                //Publish Seo to mongo 
                PublishSeo(portalId);

                //Delete Already Published Configurations
                DeletePublishConfiguration(Query.And(
                    Query<WebStoreEntity>.EQ(p => p.PortalId, portalId),
                    Query<WebStoreEntity>.NE(x => x.VersionId, znodePublishPortalLog.PublishPortalLogId)
                    ));

                //Update publish status false means portal published successfully.
                UpdatePublishedPortalStatus(znodePublishPortalLog, true);

                //Clear Cache of portal after store publish.
                ClearCacheHelper.ClearCacheAfterPublish(Convert.ToString(portalId));

                return true;
            }
            catch (ZnodeException)
            {
                throw;
            }
            catch (Exception)
            {
                DeletePublishConfiguration(Query.And(
                    Query<WebStoreEntity>.EQ(p => p.PortalId, portalId),
                    Query<WebStoreEntity>.EQ(x => x.VersionId, znodePublishPortalLog.PublishPortalLogId)
                    ));

                //Update publish status false means failed to publish portal.
                UpdatePublishedPortalStatus(znodePublishPortalLog, false);
                throw;
            }
        }

        public virtual bool Publish(int portalId, string targetPublishState = null, string publishContent = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Input parameters portalId, targetPublishState :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, targetPublishState });

            bool result = false;

            PublishProcessor processor = new PublishProcessor();
            result = processor.PublishCompleteWebstore(portalId, CopyWebstoreWithinMongo, CopyWebstoreFromSQLToMongo, targetPublishState, publishContent, takeFromDraftFirst);
            ClearCacheHelper.ClearCacheAfterPublish(Convert.ToString(portalId));

            return result;
        }

        public virtual bool PublishAsync(int portalId, string targetPublishState = null, string publishContent = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            HttpContext httpContext = HttpContext.Current;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = httpContext;

                PublishProcessor processor = new PublishProcessor();

                processor.PublishCompleteWebstore(portalId, CopyWebstoreWithinMongo, CopyWebstoreFromSQLToMongo, targetPublishState, publishContent, takeFromDraftFirst, true);

                ClearCacheHelper.ClearCacheAfterPublish(Convert.ToString(portalId));
            }));
            thread.Start();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return true;
        }

        #endregion

        //Get the widget id by its code.
        public virtual int GetWidgetIdByCode(string widgetCode)
        => _widgetRepository.Table.FirstOrDefault(x => x.Code == widgetCode).CMSWidgetsId;

        public virtual int GetAssociatedCatalogId(int portalId)
        {
            return (from portalCatalog in _portalCatalogRepository.Table
                    join publishCatalog in _publishCatalogRepository.Table on portalCatalog.PublishCatalogId equals publishCatalog.PublishCatalogId
                    join catalog in _catalogRepository.Table on publishCatalog.PimCatalogId equals catalog.PimCatalogId
                    where portalCatalog.PortalId == portalId
                    select catalog.PimCatalogId).FirstOrDefault();
        }

        #endregion

        #region Private Methods 
        //Check if portal product page details already exists for current portal id.
        private bool IsProductPageDetailsAvailable(int portalId)
         => _cmsPortalProductPageRepository.Table.Any(x => x.PortalId == portalId);

        //Get the Portal Publish Attributes And Groups data from mongo.
        public virtual GlobalAttributeEntityDetailsModel GetPortalPublishAttributes(int portalId, int localeId, string entityType, string groupCode)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            GlobalAttributeEntityDetailsModel model = new GlobalAttributeEntityDetailsModel();
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query.And(Query<PortalGlobalAttributeEntity>.EQ(ga => ga.PortalId, portalId)));
            query.Add(Query.And(Query<PortalGlobalAttributeEntity>.EQ(ga => ga.LocaleId, localeId)));

            PortalGlobalAttributeEntity portalAttribute = _portalAttributMongoRepository.Table.MongoCollection.FindOne(Query.And(query));

            if (!string.IsNullOrEmpty(groupCode) && HelperUtility.IsNotNull(portalAttribute))
            {
                string[] groupList = groupCode.ToLower().Split(',');
                portalAttribute.GlobalAttributeGroups = portalAttribute.GlobalAttributeGroups.Where(x => groupList.Contains(x.GroupCode.ToLower())).ToList();
                ZnodeLogging.LogMessage("GlobalAttributeGroups count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalAttribute?.GlobalAttributeGroups?.Count });

            }

            if (HelperUtility.IsNotNull(portalAttribute))
            {
                model.EntityId = portalId;
                model.EntityType = entityType;
                model.Groups = portalAttribute?.GlobalAttributeGroups?.ToModel<GlobalAttributeGroupModel>()?.ToList() ?? new List<GlobalAttributeGroupModel>();
                model.Attributes = GlobalAttributeGroupMap.ToAttributeModel(portalAttribute?.GlobalAttributeGroups);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return model;
        }

        //Method to get locale id from filters.
        private void GetLocaleId(FilterCollection filters, ref int localeId)
        {
            if (filters?.Count > 0)
            {
                localeId = Convert.ToInt32(filters.FirstOrDefault(x => x.FilterName == FilterKeys.LocaleId.ToLower()).FilterValue);
                filters.RemoveAll(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower());
            }
        }

        //Method to get expands for parent theme navigation property in ZnodeCMSTheme.
        private List<string> GetExpandsForParentTheme()
        {
            List<string> navigationProperties = new List<string>();
            SetExpands(ZnodeCMSThemeEnum.ZnodeCMSTheme2.ToString(), navigationProperties);
            return navigationProperties;
        }

        #region CMS Publish

        private List<ContentPageConfigEntity> GetContentPagesForLocale(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            int defaultLocaleId = GetDefaultLocaleId();
            return (from contentpage in _contentPageRepository.Table
                    join template in _cmsTemplateRepository.Table on contentpage.CMSTemplateId equals template.CMSTemplateId
                    join contentPagesLocale in _contentPagesLocaleRepository.Table on contentpage.CMSContentPagesId equals contentPagesLocale.CMSContentPagesId
                    where contentpage.PortalId == portalId && (contentPagesLocale.LocaleId == localeId || contentPagesLocale.LocaleId == defaultLocaleId)
                    select new ContentPageConfigEntity
                    {
                        ContentPageId = contentpage.CMSContentPagesId,
                        PortalId = portalId,
                        FileName = template.FileName,
                        PageName = contentpage.PageName,
                        PageTitle = contentPagesLocale.PageTitle,
                        ActivationDate = contentpage.ActivationDate,
                        ExpirationDate = contentpage.ExpirationDate,
                        VersionId = versionId,
                        LocaleId = localeId,
                        IsActive = contentpage.IsActive
                    }).ToList();
        }

        //Save CMS Portal Product Page to mongo as preview.
        protected virtual void SaveDynamicStylesToMongo(int portalId, int versionId, int localeId, ZnodePublishStatesEnum targetPublishState)
        {
            ZnodeLogging.LogMessage("Input parameters portalId, versionId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId });

            DynamicStyleEntity dynamicStyles = _portalCustomCssRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.ToModel<DynamicStyleEntity>();
            
            if (IsNotNull(dynamicStyles))
            {
                dynamicStyles.VersionId = versionId;
                dynamicStyles.LocaleId = localeId;
                dynamicStyles.PublishState = targetPublishState.ToString();
                _dynamicStyleMongoRepository.Create(dynamicStyles);
            }
        }

        //Save CMS Portal Product Page to mongo as preview.
        private void SavePortalProductPagesToMongo(int portalId, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId });

            List<ProductPageEntity> portalProductPages = _cmsPortalProductPageRepository.Table.Where(x => x.PortalId == portalId)?.ToModel<ProductPageEntity>()?.ToList();
            ZnodeLogging.LogMessage("portalProductPages count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalProductPages?.Count });

            if (portalProductPages?.Count > 0)
                _cmsProductPageMongoRepository.Create(portalProductPages.Select(c => { c.VersionId = versionId; return c; }).ToList());
        }

        //Publish CMS Website Configuration to mongo
        private void PushWebsiteConfigurationData(int portalId, int versionId, int localeId, ZnodePublishStatesEnum targetPublishState)
        {
            WebStoreEntity websiteConfiguration = (from cmsPortalTheme in _cmsPortalThemeRepository.Table
                                                   join cmsTheme in _cmsThemeRepository.Table on cmsPortalTheme.CMSThemeId equals cmsTheme.CMSThemeId
                                                   join cmsThemeCSS in _cmsThemeCSSRepository.Table on cmsPortalTheme.CMSThemeCSSId equals cmsThemeCSS.CMSThemeCSSId
                                                   join mediaManager in _mediaManagerRepository.Table on cmsPortalTheme.MediaId equals mediaManager.MediaId into media
                                                   join faviconMedia in _mediaManagerRepository.Table on cmsPortalTheme.FavIconId equals faviconMedia.MediaId into favicon
                                                   from mediaManager in media.DefaultIfEmpty()
                                                   from faviconMedia in favicon.DefaultIfEmpty()
                                                   where cmsPortalTheme.PortalId == portalId
                                                   select new WebStoreEntity
                                                   {
                                                       PortalThemeId = cmsPortalTheme.CMSPortalThemeId,
                                                       PortalId = cmsPortalTheme.PortalId,
                                                       WebsiteLogo = mediaManager.Path,
                                                       ThemeId = cmsPortalTheme.CMSThemeId,
                                                       ThemeName = cmsTheme.Name,
                                                       WebsiteTitle = cmsPortalTheme.WebsiteTitle,
                                                       WebsiteDescription = cmsPortalTheme.WebsiteDescription,
                                                       CSSId = cmsThemeCSS.CMSThemeCSSId,
                                                       CSSName = cmsThemeCSS.CSSName,
                                                       FaviconImage = faviconMedia.Path,
                                                       VersionId = versionId,
                                                       PublishState = targetPublishState.ToString(),
                                                       LocaleId = localeId
                                                   }).FirstOrDefault();

            if (IsNotNull(websiteConfiguration))
                _cmsWebStoreMongoRepository.Create(websiteConfiguration);
        }

        //Publish CMS Widget Category to mongo
        private void SaveWidgetCategoryToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            if (_cmsWidgetCategoryRepository.Table.Any(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId))))
            {
                List<WidgetCategoryEntity> widgetCategoryConfigurations = _cmsWidgetCategoryRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetCategoryEntity>().ToList();

            if (widgetCategoryConfigurations?.Count > 0)
                _cmsWidgetCategoryMongoRepository.Create(widgetCategoryConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
            }
        }

        //Publish CMS Widget Brand to mongo
        private void SaveWidgetBrandToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            List<WidgetBrandEntity> widgetBrandConfigurations = _cmsWidgetBrandRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetBrandEntity>()?.ToList();
            if (widgetBrandConfigurations?.Count > 0)
                _cmsWidgetBrandMongoRepository.Create(widgetBrandConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        //Publish CMS Widget Product to mongo
        private void SaveWidgetProductToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            List<WidgetProductEntity> widgetProductConfigurations = _cmsWidgetProductRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetProductEntity>()?.ToList();

            if (widgetProductConfigurations?.Count > 0)
                _cmsWidgetProductMongoRepository.Create(widgetProductConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        //Publish CMS Widget Slider Banner to mongo
        private void SaveWidgetSliderBannerToMongo(int portalId, int userId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, userId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, versionId, localeId });

            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSWidgetSliderBanner", null, "", 0, "@LocaleId", localeId);
            DataTable dataTable = dataSet.Tables[0];
            PublishHelper.SaveToMongo<WidgetSliderBannerEntity>(dataSet, _cmsWidgetSliderBannerMongoRepository, versionId);
        }
        //Publish CMS Widget Title to mongo
        private void SaveTitleWidgetToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            IZnodeViewRepository<WidgetTitleEntity> objStoredProc = new ZnodeViewRepository<WidgetTitleEntity>();

            //SP parameters for getting date to be publish.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            List<WidgetTitleEntity> widgetTitleConfigurations;
            widgetTitleConfigurations = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCMSWidgetTitle  @PortalId, @LocaleId").ToList();

            widgetTitleConfigurations?.ForEach(x => x.VersionId = versionId);
            if (widgetTitleConfigurations?.Count > 0)
                _cmsWidgetTitleMongoRepository.Create(widgetTitleConfigurations);
        }
        //Publish CMS Text Widget Configuration to mongo
        private void SaveTextWidgetConfigurationToMongo(int portalId, int userId, int versionId, int localeId)
            => PublishHelper.SaveToMongo<TextWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetTextWidgetConfiguration", null, "", 0, "@LocaleId", localeId), _cmsTextWidgetMongoRepository, versionId);

        //Publish Media Configurations
        private void SaveMediaWidgetConfiguration(int portalId, int userId, int versionId)
             => PublishHelper.SaveToMongo<MediaWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetMediaWidgetConfiguration", null, "", 0), _cmsMediaWidgetMongoRepository, versionId);


        //Publish CMS Search widget data.
        private void SaveSearchWidgetConfigurationToMongo(int portalId, int userId, int versionId, int localeId)
            => PublishHelper.SaveToMongo<SearchWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetSearchWidgetConfiguration", null, "", 0, "@LocaleId", localeId), _cmsSearchWidgetMongoRepository, versionId);

        //Publish CMS Message Configuration to mongo
        private void SaveMessagesToMongo(int portalId, int userId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, userId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, versionId, localeId });

            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSMessageConfiguration", null, "", 0, "@LocaleId", localeId);
            DataTable dataTable = dataSet.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                var doc = new XmlDocument();

                doc.LoadXml(Convert.ToString(row["ReturnXML"]));
            }
            PublishHelper.SaveToMongo<MessageEntity>(dataSet, _cmsMessageMongoRepository, versionId);           
        }
      
        //Publish CMS Content Page Configuration to mongo
        private void SaveContentPageConfigurationToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            //Get content pages associated to portalId
            List<ContentPageConfigEntity> ContentPageConfigconfigurations = GetContentPagesForLocale(portalId, versionId, localeId);
            List<int> contentPageIds = ContentPageConfigconfigurations.Select(x => x.ContentPageId).ToList();
            ZnodeLogging.LogMessage("contentPageIds associated to portal:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageIds });

            //Get porfiles associated to content pages
            List<ZnodeCMSContentPagesProfile> contentPagesProfiles =
                _cmsContentPagesProfileRepository.Table.Where(x => contentPageIds.Contains(x.CMSContentPagesId)).ToList();

            ZnodeLogging.LogMessage("profiles associated to content pages:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPagesProfiles });

            foreach (ContentPageConfigEntity ContentPageConfigconfiguration in ContentPageConfigconfigurations)
            {
                //Profileds Associated to content page
                int?[] profileIds = contentPagesProfiles.Where(x => x.CMSContentPagesId == ContentPageConfigconfiguration.ContentPageId && x.ProfileId != null).Select(x => x.ProfileId).ToArray();
                ContentPageConfigconfiguration.ProfileId = profileIds.Length > 0 ? profileIds : null;
            }

            if (ContentPageConfigconfigurations?.Count > 0)
                _cmsContentPageMongoRepository.Create(ContentPageConfigconfigurations);
        }


        //Publish blog/news to mongo.
        private void SaveBlogNewsToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            IZnodeViewRepository<BlogNewsEntity> objStoredProc = new ZnodeViewRepository<BlogNewsEntity>();

            //SP parameters for getting data to be published.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            List<BlogNewsEntity> blogNewsEntities;
            blogNewsEntities = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishBlogNews  @PortalId, @LocaleId").ToList();
            ZnodeLogging.LogMessage("blogNewsEntities count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { blogNewsEntities?.Count });

            blogNewsEntities?.ForEach(x => x.VersionId = versionId);
            if (blogNewsEntities?.Count > 0)
                _blogNewsMongoRepository.Create(blogNewsEntities);
        }

        //Publish the Portal associated Global Attribute/Attribute Groups to mongo.
        public virtual void SavePortalAssociateAttributesToMongo(int portalId, int userId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, userId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, versionId, localeId });

            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            DataSet resultDataSet = executeSpHelper.GetSPResultInDataSet("Znode_GetPublishPortalAttribute");

            if (resultDataSet?.Tables?.Count > 0 && resultDataSet?.Tables[0].Rows.Count > 0)
            {
                string portalXML = resultDataSet.Tables[0].AsEnumerable().Select(dataRow => dataRow.Field<string>("PortalXml")).FirstOrDefault();
                PortalGlobalAttributeEntity portalAttributeEntity = !string.IsNullOrEmpty(portalXML) ? ConvertXMLStringToModel<PortalGlobalAttributeEntity>(portalXML) : null;
                if (IsNotNull(portalAttributeEntity))
                {
                    portalAttributeEntity.VersionId = versionId;
                    portalAttributeEntity.LocaleId = localeId;
                    _portalAttributMongoRepository.Create(portalAttributeEntity);
                }
                else
                {
                    string logMessage = $"One of the portal associated attributes failed to publish to Mongo due to the blank XML. Method name - SavePortalAssociateAttributesToMongo. Class Name - WebsiteService. Parameters - portalId:{portalId}, userId:{userId}, versionId:{versionId}, localeId:{localeId}. Object portalXML: {portalXML}";
                    ZnodeLogging.LogMessage(logMessage, "Publish", TraceLevel.Warning);
                }
            }
        }

        //Publish Seo associated with portalId
        public virtual void SaveSeoToMongo(int portalId, int localeId, int currentlyPreviewedVersionId, int previousPreviewVersionId, ZnodePublishStatesEnum contentState, SEODetailsEnum[] seoTypes = null, string seoCode = null, bool deleteHistoricalDataAfterCompletion = true)
        {
            List<SeoEntity> getSeoLocaleDetailList = GetSeoData(portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId, seoTypes, seoCode);

            //Publish Seo Locale Detail to mongo           
            if (getSeoLocaleDetailList?.Count > 0)
            {
                getSeoLocaleDetailList.ForEach(s =>
                {
                    s.VersionId = currentlyPreviewedVersionId;
                    s.SEOCode = (string.IsNullOrEmpty(s.SEOCode) ? s.SEOTitle : s.SEOCode);
                });

                try
                {
                    //Delete Publish Category SEO Details associated with given catalog.
                    CommitSeoDetailToMongo(portalId, previousPreviewVersionId, localeId, seoTypes, seoCode);
                    //Create Publish Category SEO Details associated with given catalog.
                    _seoMongoRepository.Create(getSeoLocaleDetailList);
                }
                catch
                {
                    RollbackSeoDetailFromMongo(portalId, currentlyPreviewedVersionId, localeId, seoTypes, seoCode);
                }
            }
        }

        //Publish Seo associated with portalId
        public virtual void SaveSeoToMongoCatalog(int portalId, int localeId, int currentlyPreviewedVersionId, int previousPreviewVersionId, ZnodePublishStatesEnum contentState, SEODetailsEnum[] seoTypes = null, string seoCode = null, bool deleteHistoricalDataAfterCompletion = true)
        {
            ZnodeLogging.LogMessage($"Execution started for portalId: {portalId} :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId });

            List<SeoEntity> getSeoLocaleDetailList = GetSeoData(portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId, seoTypes, seoCode);

            //Publish Seo Locale Detail to mongo           
            if (getSeoLocaleDetailList?.Count > 0)
            {
                getSeoLocaleDetailList.ForEach(s =>
                {
                    s.VersionId = currentlyPreviewedVersionId;
                    s.SEOCode = (string.IsNullOrEmpty(s.SEOCode) ? s.SEOTitle : s.SEOCode);
                });

                try
                {
                    //Create Publish Category SEO Details associated with given catalog.
                    _seoMongoRepository.Create(getSeoLocaleDetailList);
                    ZnodeLogging.LogMessage($"Execution Ended for portalId: {portalId} :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                }
                catch
                {
                    ZnodeLogging.LogMessage($"Execution failed for portalId: {portalId} :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                    RollbackSeoDetailFromMongo(portalId, currentlyPreviewedVersionId, localeId, seoTypes, seoCode);
                }
            }
        }

        private List<SeoEntity> GetSeoData(int portalId, int localeId, int currentlyPreviewedVersionId, int previousPreviewVersionId, SEODetailsEnum[] seoTypes, string seoCode)
        {
            ZnodeLogging.LogMessage("Seolist fetching started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            string commandText = "Znode_GetSeoDetailsForPublish @PortalId, @IsBrand, @SeoCode, @SeoType, @LocaleId";

            IZnodeViewRepository<SeoEntity> objStoredProc = new ZnodeViewRepository<SeoEntity>();
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsBrand", true, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@SeoCode", (!string.IsNullOrEmpty(seoCode) && !string.IsNullOrWhiteSpace(seoCode)) ? seoCode : string.Empty, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SeoType", (IsNotNull(seoTypes) && seoTypes.Length > 0) ? string.Join(",", seoTypes).Replace("_", " ") : string.Empty, ParameterDirection.Input, DbType.String);

            List<SeoEntity> getSeoLocaleDetailList = objStoredProc.ExecuteStoredProcedureList(commandText).ToList();
            ZnodeLogging.LogMessage("getSeoLocaleDetailList count:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { getSeoLocaleDetailList?.Count });

            //Fail safe step in case the stored procedure fails to filter out as per locale.
            getSeoLocaleDetailList = getSeoLocaleDetailList?.Where(x => x.LocaleId == localeId)?.ToList();
            ZnodeLogging.LogMessage("Seolist fetching Ended:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return getSeoLocaleDetailList;
        }

        public virtual void CommitSeoDetailToMongo(int portalId, int versionId, int localeId, SEODetailsEnum[] seoTypes, string seoCode = null)
        {
            foreach (int seoType in seoTypes)
                PreviewHelper.DeleteRecords(_seoMongoRepository, log_seoMongoRepository, versionId, Query.And(Query.Or(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId), Query<SeoEntity>.EQ(pr => pr.PortalId, null)), Query<SeoEntity>.EQ(x => x.LocaleId, localeId), Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, seoType), (string.IsNullOrEmpty(seoCode) ? Query.Empty : Query<SeoEntity>.EQ(x => x.SEOCode, seoCode))));
        }

        public virtual void RollbackSeoDetailFromMongo(int portalId, int versionId, int localeId, SEODetailsEnum[] seoTypes, string seoCode = null)
        {
            foreach (int seoType in seoTypes)
                PreviewHelper.DeleteRecords(_seoMongoRepository, versionId, Query.And(Query.Or(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId), Query<SeoEntity>.EQ(pr => pr.PortalId, null)), Query<SeoEntity>.EQ(x => x.LocaleId, localeId), Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, seoType), (string.IsNullOrEmpty(seoCode) ? Query.Empty : Query<SeoEntity>.EQ(x => x.SEOCode, seoCode))));
        }

        

        //Update Publish Portal Status
        private void UpdatePortalPublishState(ZnodePublishPortalLog znodePublishPortalLog, ZnodePublishStatesEnum targetContentState)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            znodePublishPortalLog.PublishStateId = ((byte)targetContentState);
            _publishPortalLogRepository.Update(znodePublishPortalLog);
        }

        

        private void UpdatePublishStateForCompleteCMS(int portalId, int localeId, List<byte> sourceContentStates, ZnodePublishStatesEnum targetContentState)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, localeId.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose,new object[] { portalId, localeId });

            bool? status = null;

            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CurrentPublishStateId", string.Join(",", sourceContentStates), ParameterDirection.Input, SqlDbType.VarChar);
            executeSpHelper.GetParameter("@PublishStateId", (byte)targetContentState, ParameterDirection.Input, SqlDbType.TinyInt);
            executeSpHelper.GetParameter("@Status", status, ParameterDirection.Output, SqlDbType.Bit);
            executeSpHelper.GetSPResultInObject("Znode_UpdateStorePublishState");
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

        }

        //Makes a copy of existing Store and CMS content to the given target publish state.
        private bool CopyWebstoreWithinMongo(WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, List<string> publishContent)
        {
            bool isSuccessful = false;
            bool publishCMSContent = publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString());
            Exception exception = new Exception();

            int sourceVersionId = existingWebstoreCopy.VersionId;

            int portalId = existingWebstoreCopy.PortalId;
            int localeId = existingWebstoreCopy.LocaleId;
            ZnodePublishStatesEnum sourceContentState;
            Enum.TryParse(existingWebstoreCopy.PublishState, true, out sourceContentState);
            ZnodeLogging.LogMessage("sourceVersionId, portalId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { sourceVersionId, portalId, localeId });

            ZnodePublishPortalLog znodePublishPortalLog = null;

            ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.GetById(sourceVersionId);
            if (HelperUtility.IsNotNull(previewedPortalLog))
                znodePublishPortalLog = _publishPortalLogRepository.Insert(
                    new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

            int currentPublishedVersionId = 0;
            if (IsNotNull(znodePublishPortalLog))
                currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

            WebStoreEntity previousPublishedWebstore = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);
            DynamicStyleEntity previousPublishedDynamicStyle = PreviewHelper.GetDynamicStyle(portalId, targetPublishState, localeId);

            try
            {
                PreviewHelper.CopyAllCMSEntitiesToVersion(sourceVersionId, currentPublishedVersionId, portalId, targetPublishState);

                isSuccessful = true;
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                //Update CMS content publish status only if included in publish.
                if (IsNull(publishContent) || publishContent.Count == 0 || publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString()))
                    UpdatePublishStateForCompleteCMS(portalId, localeId, Enum.GetValues(typeof(ZnodePublishStatesEnum)).Cast<ZnodePublishStatesEnum>().Select(x => (byte)x).ToList(), targetPublishState);

                //Update Store publish status only if included in publish.
                if (IsNull(publishContent) || publishContent.Count == 0 || publishContent.Contains(ZnodePublishContentTypeEnum.StoreSettings.ToString()))
                    UpdatePortalPublishState(znodePublishPortalLog, targetPublishState);

                //Delete previous published copy from mongo at last.
                if (IsNotNull(previousPublishedWebstore))
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousPublishedWebstore.VersionId, portalId, currentPublishedVersionId, localeId, true);

                if (publishCMSContent && IsNotNull(previousPublishedDynamicStyle))
                    PreviewHelper.DeleteDynamicStyleEntityFromMongo(previousPublishedDynamicStyle.VersionId, null, true);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, IsNotNull(previousPublishedWebstore) ? previousPublishedWebstore.VersionId : 0, portalId, "Webstore", localeId, GetLocaleName(localeId));

                return true;
            }
        }

        // Pushes the Store and CMS content found in SQL database to the supplied target state in Mongo database.
        private bool CopyWebstoreFromSQLToMongo(int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, List<string> publishContent)
        {
            bool isSuccessful = false;
            bool publishCMSContent = publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString());
            Exception exception = new Exception();

            ZnodePublishPortalLog znodePublishPortalLog;

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);
            DynamicStyleEntity existingDynamicStyleCopy = PreviewHelper.GetDynamicStyle(portalId, targetPublishState, localeId);

            int? previousWebstoreVersion = existingWebstoreCopy?.VersionId;
            int? previousDynamicStyleVersion = existingDynamicStyleCopy?.VersionId;
            ZnodeLogging.LogMessage("previousWebstoreVersion :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { previousWebstoreVersion });

            int newWebstoreVersion = 0;

            SEODetailsEnum[] seoTypes = new SEODetailsEnum[] { SEODetailsEnum.BlogNews, SEODetailsEnum.Content_Page };

            try
            {
                int userId = GetLoginUserId();
                ZnodeLogging.LogMessage("userId returned from GetLoginUserId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { userId });

                //Insert the PublishPortalLog for webstore preview for the first time cases.
                znodePublishPortalLog = _publishPortalLogRepository.Insert(new ZnodePublishPortalLog { PublishStateId = (byte)ZnodePublishStatesEnum.DRAFT, PortalId = portalId });
                newWebstoreVersion = znodePublishPortalLog.PublishPortalLogId;

                // Publishing Store Settings.
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishContent.Contains(ZnodePublishContentTypeEnum.StoreSettings.ToString()) || !previousWebstoreVersion.HasValue)
                {
                    //Insert CMS Website Configuration to mongo for preview.
                    PushWebsiteConfigurationData(portalId, newWebstoreVersion, localeId, targetPublishState);
                    UpdatePortalPublishState(znodePublishPortalLog, targetPublishState);
                }
                else
                    PreviewHelper.CopyWebstoreToState(_cmsWebStoreMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId, targetPublishState);


                //Publishing CMS and SEO content.
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishCMSContent || !previousWebstoreVersion.HasValue)
                {
                    //Active Content Page Ids Associated with Portal ID
                    List<int> contentPageIds = _contentPageRepository.Table.Where(x => x.PortalId == portalId && x.IsActive).Select(x => x.CMSContentPagesId)?.ToList() ?? new List<int>();
                    ZnodeLogging.LogMessage("Active Content Page Ids Associated with Portal ID :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageIds });

                    //To avoid dynamic styles publish operation when there is no previousWebstoreVersion available.
                    if (publishCMSContent)
                    {
                        //Publish dynamic styles to mongo.
                        SaveDynamicStylesToMongo(portalId, newWebstoreVersion, localeId, targetPublishState);
                    }

                    //Publish CMS Portal Product Page to mongo
                    SavePortalProductPagesToMongo(portalId, newWebstoreVersion);

                    //Publish CMS Widget Category to mongo
                    SaveWidgetCategoryToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish CMS Widget Product to mongo
                    SaveWidgetProductToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish CMS Widget Title to mongo
                    SaveTitleWidgetToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish CMS Widget Slider Banner to mongo
                    SaveWidgetSliderBannerToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Text Widget Configuration to mongo
                    SaveTextWidgetConfigurationToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish Media Configurations
                    SaveMediaWidgetConfiguration(portalId, userId, newWebstoreVersion);

                    //Publish CMS Text Widget Configuration to mongo
                    SaveSearchWidgetConfigurationToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Messages to mongo
                    SaveMessagesToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Content Page Configuration to mongo
                    SaveContentPageConfigurationToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish Blog/News to mongo.
                    SaveBlogNewsToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish CMS Widget Brand to mongo
                    SaveWidgetBrandToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish Portal Associted Global Attribute Details to Mongo
                    SavePortalAssociateAttributesToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish Seo to mongo 
                    SaveSeoToMongo(portalId, localeId, newWebstoreVersion, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), targetPublishState, seoTypes, null, false);
                }
                else
                {
                    PreviewHelper.CopyEntityFromVersion(_cmsProductPageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetCategoryMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetProductMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetSliderBannerMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetTitleMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsTextWidgetMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsMediaWidgetMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);                  
                    PreviewHelper.CopyEntityFromVersion(_cmsContentPageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsMessageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetBrandMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_blogNewsMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_portalAttributMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    foreach (int seoType in seoTypes)
                    {
                        PreviewHelper.CopyEntityFromVersion(_seoMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId, Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, seoType));
                    }

                    PreviewHelper.CopyEntityFromVersion(_cmsSearchWidgetMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                }
                isSuccessful = true;
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString()) || !previousWebstoreVersion.HasValue)
                {
                    CommitSeoDetailToMongo(portalId, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), localeId, seoTypes);

                    UpdatePublishStateForCompleteCMS(portalId, localeId, Enum.GetValues(typeof(ZnodePublishStatesEnum)).Cast<ZnodePublishStatesEnum>().Select(x => (byte)x).ToList(), targetPublishState);
                }

                //Delete all the previously previewed Configurations.
                if (previousWebstoreVersion.HasValue)
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousWebstoreVersion.Value, portalId, newWebstoreVersion, localeId, true);

                if (publishCMSContent && previousDynamicStyleVersion.HasValue)
                    PreviewHelper.DeleteDynamicStyleEntityFromMongo(previousDynamicStyleVersion.Value, null, true);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), newWebstoreVersion, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), portalId, "Webstore", localeId, GetLocaleName(localeId));

                return true;
            }
        }

        private void RollbackPublishedCMSEntities(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            PreviewHelper.DeleteAllCMSEntitiesFromMongo(versionToRollback);
        }

        #endregion

        #region CMS Publish (Old Implementation)

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Portal Product Page to mongo
        private void PublishPortalProductPagePages(int portalId, int versionId)
        {
            List<ProductPageEntity> portalProductPages = _cmsPortalProductPageRepository.Table.Where(x => x.PortalId == portalId)?.ToModel<ProductPageEntity>()?.ToList();

            if (portalProductPages?.Count > 0)
                _cmsProductPageMongoRepository.Create(portalProductPages.Select(c => { c.VersionId = versionId; return c; }).ToList());
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Website Configuration to mongo
        private void PublishWebsiteConfiguration(int portalId, int versionId)
        {
            IZnodeRepository<ZnodeCMSPortalTheme> cmsPortalThemeRepository = new ZnodeRepository<ZnodeCMSPortalTheme>();
            IZnodeRepository<ZnodeCMSTheme> cmsThemeRepository = new ZnodeRepository<ZnodeCMSTheme>();
            IZnodeRepository<ZnodeCMSThemeCSS> cmsThemeCSSRepository = new ZnodeRepository<ZnodeCMSThemeCSS>();

            WebStoreEntity websiteConfiguration = (from cmsPortalTheme in cmsPortalThemeRepository.Table
                                                   join cmsTheme in cmsThemeRepository.Table on cmsPortalTheme.CMSThemeId equals cmsTheme.CMSThemeId
                                                   join cmsThemeCSS in cmsThemeCSSRepository.Table on cmsPortalTheme.CMSThemeCSSId equals cmsThemeCSS.CMSThemeCSSId
                                                   join mediaManager in _mediaManagerRepository.Table on cmsPortalTheme.MediaId equals mediaManager.MediaId into media
                                                   join faviconMedia in _mediaManagerRepository.Table on cmsPortalTheme.FavIconId equals faviconMedia.MediaId into favicon
                                                   from mediaManager in media.DefaultIfEmpty()
                                                   from faviconMedia in favicon.DefaultIfEmpty()
                                                   where cmsPortalTheme.PortalId == portalId
                                                   select new WebStoreEntity
                                                   {
                                                       PortalThemeId = cmsPortalTheme.CMSPortalThemeId,
                                                       PortalId = cmsPortalTheme.PortalId,
                                                       WebsiteLogo = mediaManager.Path,
                                                       ThemeId = cmsPortalTheme.CMSThemeId,
                                                       ThemeName = cmsTheme.Name,
                                                       WebsiteTitle = cmsPortalTheme.WebsiteTitle,
                                                       CSSId = cmsThemeCSS.CMSThemeCSSId,
                                                       CSSName = cmsThemeCSS.CSSName,
                                                       FaviconImage = faviconMedia.Path,
                                                       VersionId = versionId
                                                   }).FirstOrDefault();

            if (IsNotNull(websiteConfiguration))
                _cmsWebStoreMongoRepository.Create(websiteConfiguration);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Widget Category to mongo
        private void PublishWidgetCategory(int portalId, List<int> contentPageIds, int versionId)
        {
            List<WidgetCategoryEntity> widgetCategoryConfigurations = _cmsWidgetCategoryRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetCategoryEntity>().ToList();

            if (widgetCategoryConfigurations?.Count > 0)
                _cmsWidgetCategoryMongoRepository.Create(widgetCategoryConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Widget Brand to mongo
        private void PublishWidgetBrand(int portalId, List<int> contentPageIds, int versionId)
        {
            List<WidgetBrandEntity> widgetBrandConfigurations = _cmsWidgetBrandRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetBrandEntity>()?.ToList();
            if (widgetBrandConfigurations?.Count > 0)
                _cmsWidgetBrandMongoRepository.Create(widgetBrandConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Widget Product to mongo
        private void PublishWidgetProduct(int portalId, List<int> contentPageIds, int versionId)
        {
            List<WidgetProductEntity> widgetProductConfigurations = _cmsWidgetProductRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetProductEntity>()?.ToList();

            if (widgetProductConfigurations?.Count > 0)
                _cmsWidgetProductMongoRepository.Create(widgetProductConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Widget Slider Banner to mongo
        private void PublishWidgetSliderBanner(int portalId, int userId, int versionId)
        {
            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSWidgetSliderBanner", null, "", "");
            DataTable dataTable = dataSet.Tables[0];
            int cmsSliderId = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                WidgetSliderBannerEntity entity = ConvertXMLStringToModel<WidgetSliderBannerEntity>(Convert.ToString(row["ReturnXML"]));
                if (IsNotNull(entity))
                {
                    cmsSliderId = Convert.ToInt32(entity.SliderId);
                    UpdatePublishSliderStatus(cmsSliderId, true);
                }
                else
                {
                    string logMessage = $"One of the widget slider banners failed to publish to Mongo due to the blank XML. Method name - PublishWidgetSliderBanner. Class Name - WebsiteService. Parameter - portalId:{portalId}, userId:{userId}, versionId:{versionId}. Object row: {Convert.ToString(row["ReturnXML"])}";
                    ZnodeLogging.LogMessage(logMessage, "Publish", TraceLevel.Warning);
                }

            }
            PublishHelper.SaveToMongo<WidgetSliderBannerEntity>(dataSet, _cmsWidgetSliderBannerMongoRepository, versionId);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Widget Title to mongo
        private void PublishTitleWidget(int portalId, List<int> contentPageIds, int versionId)
        {
            IZnodeViewRepository<WidgetTitleEntity> objStoredProc = new ZnodeViewRepository<WidgetTitleEntity>();

            //SP parameters for getting date to be publish.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);

            List<WidgetTitleEntity> widgetTitleConfigurations;
            widgetTitleConfigurations = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCMSWidgetTitle  @PortalId").ToList();

            widgetTitleConfigurations?.ForEach(x => x.VersionId = versionId);
            if (widgetTitleConfigurations?.Count > 0)
                _cmsWidgetTitleMongoRepository.Create(widgetTitleConfigurations);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Text Widget Configuration to mongo
        private void PublishTextWidgetConfiguration(int portalId, int userId, int versionId)
            => PublishHelper.SaveToMongo<TextWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetTextWidgetConfiguration", null, "", ""), _cmsTextWidgetMongoRepository, versionId);

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Search widget data.
        private void PublishSearchWidgetConfiguration(int portalId, int userId, int versionId)
            => PublishHelper.SaveToMongo<SearchWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetSearchWidgetConfiguration", null, "", ""), _cmsSearchWidgetMongoRepository, versionId);

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Message Configuration to mongo
        private void PublishMessages(int portalId, int userId, int versionId)
        {
            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSMessageConfiguration", null, "", "");
            DataTable dataTable = dataSet.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                var doc = new XmlDocument();

                doc.LoadXml(Convert.ToString(row["ReturnXML"]));

                int cmsMessageId = Convert.ToInt32(doc.GetElementsByTagName("CMSMessageId").Item(0).InnerText);
                int localeId = Convert.ToInt32(doc.GetElementsByTagName("LocaleId").Item(0).InnerText);

                if (IsNotNull(cmsMessageId))
                    UpdatePublishMessagesStatus(cmsMessageId, localeId);
            }
            PublishHelper.SaveToMongo<MessageEntity>(dataSet, _cmsMessageMongoRepository, versionId);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish CMS Content Page Configuration to mongo
        private void PublishContentPageConfiguration(int portalId, int versionId)
        {
            IZnodeRepository<ZnodeCMSContentPagesProfile> contentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();

            //Get content pages associated to portalId
            List<ContentPageConfigEntity> ContentPageConfigconfigurations = GetContentPages(portalId, versionId);
            List<int> contentPageIds = ContentPageConfigconfigurations.Select(x => x.ContentPageId).ToList();

            //Get porfiles associated to content pages
            List<ZnodeCMSContentPagesProfile> contentPagesProfiles =
                contentPagesProfileRepository.Table.Where(x => contentPageIds.Contains(x.CMSContentPagesId)).ToList();

            foreach (ContentPageConfigEntity ContentPageConfigconfiguration in ContentPageConfigconfigurations)
            {
                //Profileds Associated to content page
                int?[] profileIds = contentPagesProfiles.Where(x => x.CMSContentPagesId == ContentPageConfigconfiguration.ContentPageId && x.ProfileId != null).Select(x => x.ProfileId).ToArray();
                ContentPageConfigconfiguration.ProfileId = profileIds.Length > 0 ? profileIds : null;
            }

            if (ContentPageConfigconfigurations?.Count > 0)
                _cmsContentPageMongoRepository.Create(ContentPageConfigconfigurations);
            UpdateContentPageAfterPublish(contentPageIds);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Get content pages associated to portalId
        private List<ContentPageConfigEntity> GetContentPages(int portalId, int versionId)
        => (from contentpage in _contentPageRepository.Table
            join template in new ZnodeRepository<ZnodeCMSTemplate>().Table on contentpage.CMSTemplateId equals template.CMSTemplateId
            join contentPagesLocale in _contentPagesLocaleRepository.Table on contentpage.CMSContentPagesId equals contentPagesLocale.CMSContentPagesId
            where contentpage.PortalId == portalId
            select new ContentPageConfigEntity
            {
                ContentPageId = contentpage.CMSContentPagesId,
                PortalId = portalId,
                FileName = template.FileName,
                PageName = contentpage.PageName,
                PageTitle = contentPagesLocale.PageTitle,
                ActivationDate = contentpage.ActivationDate,
                ExpirationDate = contentpage.ExpirationDate,
                VersionId = versionId,
                LocaleId = contentPagesLocale.LocaleId.Value,
                IsActive = contentpage.IsActive
            }).ToList();

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        private void UpdateContentPageAfterPublish(List<int> contentPageIds)
        {
            foreach (int contentPageId in contentPageIds)
            {
                //Updating the IsPublished flag for Content Page
                var entity = _contentPageRepository.GetById(contentPageId);
                entity.IsPublished = true;
                _contentPageRepository.Update(entity);
            }
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Update  seo status of ispublish to true according to seourl.If the seourl is there then published else draft.
        protected virtual void UpdateSEOStatusAfterPublish(int portalId)
        {
            HelperMethods.CurrentContext.ZnodeCMSSEODetails.Where(x => string.IsNullOrEmpty(x.SEOUrl) && x.PortalId == portalId).Update(y => new ZnodeCMSSEODetail() { IsPublish = false });
            HelperMethods.CurrentContext.ZnodeCMSSEODetails.Where(x => !string.IsNullOrEmpty(x.SEOUrl) && x.PortalId == portalId).Update(y => new ZnodeCMSSEODetail() { IsPublish = true });

        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish blog/news to mongo.
        private void PublishBlogNews(int portalId, int versionId)
        {
            IZnodeViewRepository<BlogNewsEntity> objStoredProc = new ZnodeViewRepository<BlogNewsEntity>();

            //SP parameters for getting data to be published.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);

            List<BlogNewsEntity> blogNewsEntities;
            blogNewsEntities = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishBlogNews  @PortalId").ToList();

            blogNewsEntities?.ForEach(x => x.VersionId = versionId);
            if (blogNewsEntities?.Count > 0)
                _blogNewsMongoRepository.Create(blogNewsEntities);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish the Portal associated Global Attribute/Attribute Groups to mongo.
        public virtual void PublishPortalAssociateAttributes(int portalId, int userId, int versionId)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            DataSet resultDataSet = executeSpHelper.GetSPResultInDataSet("Znode_GetPublishPortalAttribute");

            if (resultDataSet?.Tables?.Count > 0 && resultDataSet?.Tables[0].Rows.Count > 0)
            {
                string portalXML = resultDataSet.Tables[0].AsEnumerable().Select(dataRow => dataRow.Field<string>("PortalXml")).FirstOrDefault();
                PortalGlobalAttributeEntity portalAttributeEntity = !string.IsNullOrEmpty(portalXML) ? ConvertXMLStringToModel<PortalGlobalAttributeEntity>(portalXML) : null;
                if (IsNotNull(portalAttributeEntity))
                {
                    portalAttributeEntity.VersionId = versionId;
                    portalAttributeEntity.LocaleId = Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale);
                    _portalAttributMongoRepository.Create(portalAttributeEntity);
                }
                else
                {
                    string logMessage = $"One of the portal associated attributes failed to publish to Mongo due to the blank XML. Method name - PublishPortalAssociateAttributes. Class Name - WebsiteService. Parameters - portalId:{portalId}, userId:{userId}, versionId:{versionId}. Object portalXML: {portalXML}";
                    ZnodeLogging.LogMessage(logMessage, "Publish", TraceLevel.Warning);
                }
            }
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Publish Seo associated with portalId
        public virtual void PublishSeo(int portalId)
        {
            //Publish Seo Locale Detail to mongo
            IZnodeViewRepository<SeoEntity> objStoredProc = new ZnodeViewRepository<SeoEntity>();
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsBrand", true, ParameterDirection.Input, DbType.Boolean);
            List<SeoEntity> getSeoLocaleDetailList = objStoredProc.ExecuteStoredProcedureList("Znode_GetSeoDetailsForPublish @PortalId, @IsBrand").ToList();
            if (getSeoLocaleDetailList?.Count > 0)
            {
                //Delete Seo Locale Detail from mongo
                List<IMongoQuery> query = new List<IMongoQuery>();
                query.Add(Query.Or(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId), Query<SeoEntity>.EQ(pr => pr.PortalId, null)));
                _seoMongoRepository.DeleteByQuery(Query.And(query), true);
                _seoMongoRepository.Create(getSeoLocaleDetailList);
            }
            UpdateSEOStatusAfterPublish(portalId);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        //Delete Already Published Configurations
        private void DeletePublishConfiguration(IMongoQuery query)
        {
            //Delete CMS Website Configuration
            _cmsWebStoreMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Portal Product Page
            _cmsProductPageMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Widget Category
            _cmsWidgetCategoryMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Widget Product
            _cmsWidgetProductMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Widget Slider Banner
            _cmsWidgetSliderBannerMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Title Widget
            _cmsWidgetTitleMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Text Widget
            _cmsTextWidgetMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Search Widget
            _cmsSearchWidgetMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Message Configuration
            _cmsMessageMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Content Page Configuration
            _cmsContentPageMongoRepository.DeleteByQuery(query, true);

            //Delete CMS Widget Brand
            _cmsWidgetBrandMongoRepository.DeleteByQuery(query, true);

            //Delete Blog/News.
            _blogNewsMongoRepository.DeleteByQuery(query, true);

            //Delete Portal Associated Attribute Group/ Attrribute Details.
            _portalAttributMongoRepository.DeleteByQuery(query, true);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        private void UpdatePublishedPortalStatus(ZnodePublishPortalLog znodePublishPortalLog, bool isPortalPublished)
        {
            znodePublishPortalLog.PublishStateId = isPortalPublished ? (byte)ZnodePublishStatesEnum.PRODUCTION : (byte)ZnodePublishStatesEnum.DRAFT;
            znodePublishPortalLog.IsPortalPublished = isPortalPublished;
            _publishPortalLogRepository.Update(znodePublishPortalLog);
        }

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        private void UpdatePublishSliderStatus(int cmsSliderId, bool isPublished)
            => _cmsSliderRepository.Update(new ZnodeCMSSlider
            {
                CMSSliderId = cmsSliderId,
                Name = _cmsSliderRepository.Table.Where(x => x.CMSSliderId == cmsSliderId)?.FirstOrDefault().Name,
                IsPublished = isPublished
            });

        [Obsolete("This method was being used with deprecated publish flow. This will be removed in one of the upcoming versions.")]
        private void UpdatePublishMessagesStatus(int cmsMessageId, int localeId)
        {
            var model = _cmsMessageRepository.Table.FirstOrDefault(x => x.CMSMessageId == cmsMessageId && x.LocaleId == localeId);

            if (!Equals(model, null))
            {
                model.IsPublished = true;
                _cmsMessageRepository.Update(model);
            }
        }
        #endregion
        #endregion
    }
}
