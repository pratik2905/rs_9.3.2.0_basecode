﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;

namespace Znode.Engine.Services
{
    public class GlobalMessageService : BaseService, IGlobalMessageService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeCMSMessage> _cmsMessageRepository;
        private readonly IZnodeRepository<ZnodeCMSPortalMessage> _cmsPortalMessageRepository;
        private readonly IZnodeRepository<ZnodeCMSMessageKey> _cmsMessageKeyRepository;
        private readonly IZnodeRepository<ZnodeCMSArea> _cmsAreaRepository;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;        
        private readonly IMongoRepository<GlobalMessageEntity> _cmsGlobalMessageMongoRepository;
        private readonly IMongoRepository<GlobalVersionEntity> _cmsGlobalVersionMongoRepository;
        private readonly IMongoRepository<_LogMessageEntity> log_cmsMessageMongoRepository;
        #endregion

        #region Constructor
        public GlobalMessageService()
        {
            _cmsMessageRepository = new ZnodeRepository<ZnodeCMSMessage>();
            _cmsMessageKeyRepository = new ZnodeRepository<ZnodeCMSMessageKey>();
            _cmsPortalMessageRepository = new ZnodeRepository<ZnodeCMSPortalMessage>();
            _cmsAreaRepository = new ZnodeRepository<ZnodeCMSArea>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();            
            _cmsGlobalMessageMongoRepository = new MongoRepository<GlobalMessageEntity>();
            _cmsGlobalVersionMongoRepository = new MongoRepository<GlobalVersionEntity>();
            log_cmsMessageMongoRepository = new MongoRepository<_LogMessageEntity>();
        }
        #endregion

        #region Manage Messages
        //Publish the global message
        public virtual PublishedModel PublishManageMessage(string cmsMessageKeyId, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, localeId, targetPublishState, takeFromDraftFirst: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, localeId, targetPublishState, takeFromDraftFirst });
            

            if (string.IsNullOrEmpty(cmsMessageKeyId) || string.IsNullOrWhiteSpace(cmsMessageKeyId))
                throw new ZnodeException(ErrorCodes.InvalidEntityPassedDuringPublish, Api_Resources.InvalidEntityMessageDuringPublish);

            bool result = PublishIndividualCMSEntity(cmsMessageKeyId, localeId, targetPublishState, takeFromDraftFirst);

            return new PublishedModel { IsPublished = result, ErrorMessage = result ? String.Empty : Api_Resources.GenericExceptionMessageDuringPublish };
        }        

        #endregion

        #region Private Methods
        private bool CopyEntityWithinMongo(string id, GlobalVersionEntity existingGlobalVersionEntityCopy, ZnodePublishStatesEnum targetPublishState, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters id, targetPublishState, additionalParameter: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { id, targetPublishState, additionalParameter });
            ZnodeLogging.LogMessage("LocaleId, VersionId, PublishState properties of existingGlobalVersionEntityCopy: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { existingGlobalVersionEntityCopy?.LocaleId, existingGlobalVersionEntityCopy?.VersionId, existingGlobalVersionEntityCopy?.PublishState });

            int cmsMessageKeyId = int.Parse(id);
            bool isSuccessful = false;
            Exception exception = new Exception();

            int sourceVersionId = existingGlobalVersionEntityCopy.VersionId;
            int localeId = existingGlobalVersionEntityCopy.LocaleId;

            Enum.TryParse(existingGlobalVersionEntityCopy.PublishState, true, out ZnodePublishStatesEnum sourceContentState);

            int currentPublishedVersionId =  GetCurrentGlobalVersionId(targetPublishState, localeId);

            try
            {
                if (currentPublishedVersionId !=0)
                {
                    string messageKey = _cmsMessageKeyRepository.Table.Where(x => x.CMSMessageKeyId == cmsMessageKeyId).FirstOrDefault()?.MessageKey;
                    
                    //Content Blocks
                    List<GlobalMessageEntity> messagesToCopy = _cmsGlobalMessageMongoRepository.GetEntityList(Query.And(
                        Query<GlobalMessageEntity>.EQ(x => x.MessageKey, messageKey),                        
                        Query<GlobalMessageEntity>.EQ(x => x.VersionId, sourceVersionId)), true);

                    foreach (GlobalMessageEntity message in messagesToCopy)
                    {
                        message.VersionId = currentPublishedVersionId;

                        if (_cmsGlobalMessageMongoRepository.GetEntityList(Query.And(
                            Query<GlobalMessageEntity>.EQ(x => x.MessageKey, messageKey),                            
                            Query<GlobalMessageEntity>.EQ(x => x.VersionId, currentPublishedVersionId)
                            ), true).Count > 0)
                        {
                            _cmsGlobalMessageMongoRepository.UpdateEntity(Query.And(
                                Query<GlobalMessageEntity>.EQ(x => x.MessageKey, messageKey),
                                Query<GlobalMessageEntity>.EQ(x => x.VersionId, currentPublishedVersionId)
                                ), message, true);
                        }
                        else
                        {
                            //Creating a new entry if the existing version of same publish state doesn't already have it.
                             message.Id = ObjectId.GenerateNewId();
                            _cmsGlobalMessageMongoRepository.Create(message);
                        }                        
                    }
                    isSuccessful = true;
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content block"));
                }
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                BatchUpdateContentBlockPreviewStatus(cmsMessageKeyId, localeId, targetPublishState, true);
                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, existingGlobalVersionEntityCopy.VersionId, cmsMessageKeyId, "Content block", localeId, GetLocaleName(localeId));
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
        }

        private bool CopyEntityFromSQLToMongo(string id, int localeId, ZnodePublishStatesEnum targetPublishState, ref bool breakWithSuccess, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters id, localeId, targetPublishState, additionalParameter: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { id, localeId, targetPublishState, additionalParameter });

            int cmsMessageKeyId = int.Parse(id);

            bool isSuccessful = false;
            Exception exception = new Exception();
            List<GlobalMessageEntity> messagesToPublish = null;
            int sourceVersionId = 0;

            int currentPublishedVersionId = GetCurrentGlobalVersionId(targetPublishState, localeId);
            try
            {
                messagesToPublish = GetMessagesToPublish(cmsMessageKeyId, currentPublishedVersionId, localeId).ToList();
                ZnodeLogging.LogMessage("messagesToPublish list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, messagesToPublish?.Count);

                //Check if the configuration data is found for locale in the current iteration.
                if (HelperUtility.IsNotNull(messagesToPublish) && messagesToPublish.Any())
                {                    
                    isSuccessful= SaveGlobalMessageToMongo(messagesToPublish,localeId);                    
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.EntityNotFoundDuringPublish, string.Format(Api_Resources.EntityNotFoundMessageDuringPublish, "content block"));
                }
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackGlobalContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                if (HelperUtility.IsNotNull(messagesToPublish) && messagesToPublish.Count > 0)
                {
                    BatchUpdateContentBlockPreviewStatus(cmsMessageKeyId, localeId, targetPublishState, true);
                    PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, sourceVersionId, cmsMessageKeyId, "Content block", localeId, GetLocaleName(localeId));
                    
                }
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
        }

        private bool SaveGlobalMessageToMongo(List<GlobalMessageEntity> messagesToPublish, int localId)
        {
            messagesToPublish.ForEach(x =>
            {
                if(x.LocaleId != localId)
                {
                    x.LocaleId = localId;
                }
                var globalEntity = _cmsGlobalMessageMongoRepository.GetEntity(Query.And(Query<GlobalMessageEntity>.EQ(pr => pr.LocaleId, x.LocaleId), Query<GlobalMessageEntity>.EQ(pr => pr.VersionId, x.VersionId), Query<GlobalMessageEntity>.EQ(pr => pr.MessageKey, x.MessageKey)));
                if (HelperUtility.IsNull(globalEntity))
                {
                    x.Id = ObjectId.GenerateNewId();
                    _cmsGlobalMessageMongoRepository.Create(x);
                }
                else
                {
                    _cmsGlobalMessageMongoRepository.UpdateEntity(Query.And(Query<GlobalMessageEntity>.EQ(pr => pr.LocaleId, x.LocaleId), Query<GlobalMessageEntity>.EQ(pr => pr.VersionId, x.VersionId), Query<GlobalMessageEntity>.EQ(pr => pr.MessageKey, x.MessageKey)), x);
                }
            });
            
            return true;
        }

        private int GetCurrentGlobalVersionId(ZnodePublishStatesEnum targetPublishState, int localeId)
        {
            int currentVersionId = 0;
            GlobalVersionEntity globalVerionEntity = GetGlobalVersionEntity(targetPublishState, localeId);
            if (HelperUtility.IsNull(globalVerionEntity))
            {
                if (_cmsGlobalVersionMongoRepository.GetEntityList(null).Count > 0)
                    currentVersionId = _cmsGlobalVersionMongoRepository.GetEntityList(null).OrderByDescending(x => x.VersionId).FirstOrDefault().VersionId + 1;
                else
                    currentVersionId = 1;
                globalVerionEntity = new GlobalVersionEntity()
                {
                    Id = ObjectId.GenerateNewId(),
                    VersionId = currentVersionId,   
                    LocaleId = localeId,
                    PublishState = targetPublishState.ToString()
                };
                _cmsGlobalVersionMongoRepository.Create(globalVerionEntity);
            }
            else
            {
                currentVersionId = globalVerionEntity.VersionId;
            }
            return currentVersionId;
        }

        private GlobalVersionEntity GetGlobalVersionEntity(ZnodePublishStatesEnum targetPublishState, int localeId)
        {
            return _cmsGlobalVersionMongoRepository.GetEntity(Query.And(Query<GlobalVersionEntity>.EQ(pr => pr.PublishState, targetPublishState.ToString()),
                                                Query<GlobalVersionEntity>.EQ(pr => pr.LocaleId, localeId)));
        }

        private void RollbackGlobalContentBlock(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            _cmsGlobalMessageMongoRepository.DeleteByQuery(Query.And(Query<GlobalMessageEntity>.EQ(pr => pr.VersionId, versionToRollback)));
        }

        private IQueryable<GlobalMessageEntity> GetMessagesToPublish(int cmsMessageKeyId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, versionId, localeId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, versionId, localeId });

            int defaultLocaleId = GetDefaultLocaleId();

            ZnodeLogging.LogMessage("defaultLocaleId returned from GetDefaultLocaleId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, defaultLocaleId);

            IQueryable<GlobalMessageEntity> dataSelectQuery = (from contentblockmessage in _cmsMessageRepository.Table
                                                         join template in new ZnodeRepository<ZnodeCMSPortalMessage>().Table on contentblockmessage.CMSMessageId equals template.CMSMessageId
                                                         join cmsmessagekey in _cmsMessageKeyRepository.Table on template.CMSMessageKeyId equals cmsmessagekey.CMSMessageKeyId
                                                         where cmsmessagekey.CMSMessageKeyId == cmsMessageKeyId && (contentblockmessage.LocaleId == localeId || contentblockmessage.LocaleId == defaultLocaleId) && template.PortalId == null
                                                         select new GlobalMessageEntity
                                                         {
                                                             CMSMessageId = contentblockmessage.CMSMessageId,
                                                             LocaleId = contentblockmessage.LocaleId,
                                                             MessageKey = cmsmessagekey.MessageKey,
                                                             Message = contentblockmessage.Message,
                                                             VersionId = versionId
                                                         });

            IQueryable<GlobalMessageEntity> resultsWithPassedLocale = dataSelectQuery.Where(x => x.LocaleId == localeId);
            IQueryable<GlobalMessageEntity> resultWithDefaultLocale = dataSelectQuery.Where(x => x.LocaleId == defaultLocaleId);

            dataSelectQuery = resultsWithPassedLocale.Concat(resultWithDefaultLocale.Where(x => !resultsWithPassedLocale.Any(y => y.MessageKey == x.MessageKey)));

            return dataSelectQuery;
        }
        private void BatchUpdateContentBlockPreviewStatus(int cmsMessageKeyId, int localeId, ZnodePublishStatesEnum targetPublishState, bool isPortalPublished)
        {
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, localeId, targetPublishState, isPortalPublished: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, localeId, targetPublishState, isPortalPublished });

            List<ZnodeCMSMessage> messages = (from message in _cmsMessageRepository.Table
                                              join portalMessage in _cmsPortalMessageRepository.Table on message.CMSMessageId equals portalMessage.CMSMessageId
                                              join messagekey in _cmsMessageKeyRepository.Table on portalMessage.CMSMessageKeyId equals messagekey.CMSMessageKeyId
                                              where portalMessage.CMSMessageKeyId == cmsMessageKeyId && portalMessage.PortalId == null && message.LocaleId == localeId
                                              select message).ToList();
            ZnodeLogging.LogMessage("messages list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, messages?.Count);

            if (HelperUtility.IsNotNull(messages) && messages.Count > 0)
            {
                messages.ForEach(x => x.PublishStateId = isPortalPublished ? (byte)targetPublishState : (byte)ZnodePublishStatesEnum.DRAFT);
                _cmsMessageRepository.BatchUpdate(messages);
            }
        }

        private List<int> GetAvailableLocalesForPortal()
        {
            return new ZnodeRepository<ZnodePortalLocale>()?.Table?.Select(x => x.LocaleId).Distinct()?.ToList();
        }

        private bool PublishIndividualCMSEntity(string id, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false, int additionalParameter = 0)
        {
            List<bool> result = new List<bool>();

            List<int> activeLocaleIds = localeId > 0 ? new List<int>() { localeId } : GetAvailableLocalesForPortal();

            bool previewEnabled = IsWebstorePreviewEnabled();

            bool breakWithSuccess = false;

            foreach (int currentLocaleId in activeLocaleIds)
            {
                if (previewEnabled)
                {
                    if (string.IsNullOrEmpty(targetPublishState) || GetDefaultPublishState().ToString() == targetPublishState || !Enum.TryParse(targetPublishState, out ZnodePublishStatesEnum targetStateEnum))
                    {
                        //Get the Non-Default State.

                        if (NonDefaultPublishStateExists(out ZnodePublishStatesEnum nonDefaultPublishState))
                        {
                            result.Add(PublishIndividualCMSEntityToDefault(currentLocaleId, id, nonDefaultPublishState, takeFromDraftFirst, additionalParameter));
                        }
                        else
                        {
                            result.Add(CopyEntityFromSQLToMongo(id, currentLocaleId, GetDefaultPublishState(), ref breakWithSuccess, additionalParameter));
                        }
                    }
                    else
                    {
                        result.Add(CopyEntityFromSQLToMongo(id, currentLocaleId, targetStateEnum, ref breakWithSuccess, additionalParameter));
                    }
                }
                else
                {
                    result.Add(CopyEntityFromSQLToMongo(id, currentLocaleId, GetDefaultPublishState(), ref breakWithSuccess, additionalParameter));
                }
            }
            var clearCacheInitializer = new ZnodeEventNotifier<GlobalMessageEntity>(new GlobalMessageEntity());          
            return result.Contains(false) ? false : true;
        }

        private bool PublishIndividualCMSEntityToDefault(int localeId, string entityId, ZnodePublishStatesEnum sourceContentState, bool publishFromDraft = false, int additionalParameter = 0)
        {
            //Publish from any other content state to default publish state (PRODUCTION in most cases).
            bool publishResult = false;
            bool breakWithSuccess = false;
            ZnodePublishStatesEnum defaultPublishState = GetDefaultPublishState();

            GlobalVersionEntity existingGlobalVersionEntityCopy = GetGlobalVersionEntity(sourceContentState, localeId);
            if (publishFromDraft)
            {
                //If source does not exist. Publish to source first.
                bool isPublished = CopyEntityFromSQLToMongo(entityId, localeId, sourceContentState, ref breakWithSuccess, additionalParameter);
                existingGlobalVersionEntityCopy = GetGlobalVersionEntity(sourceContentState, localeId);
            }

            if (breakWithSuccess)
            {
                publishResult = true;
            }
            else
            {
                if (HelperUtility.IsNotNull(existingGlobalVersionEntityCopy))
                {
                    publishResult = CopyEntityWithinMongo(entityId, existingGlobalVersionEntityCopy, defaultPublishState, additionalParameter);
                }
            }
            return publishResult;
        }
        #endregion
    }
}
