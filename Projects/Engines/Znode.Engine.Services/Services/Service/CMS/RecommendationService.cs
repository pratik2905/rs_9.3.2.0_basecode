﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Resources;
using System.Diagnostics;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using System.Collections.Generic;
using Znode.Engine.Recommendations;
using Znode.Engine.Recommendations.Models;
using System.Collections.Specialized;
using System;

namespace Znode.Engine.Services
{
    public class RecommendationService : BaseService, IRecommendationService
    {
        #region Private Variables
        private readonly IPublishProductService _publishProductService;
        private readonly IZnodeRepository<ZnodePortalRecommendationSetting> _portalRecommendationSettingRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<ZnodeOmsOrder> _orderRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailsRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemsRepository;
        #endregion

        public RecommendationService(IPublishProductService publishProductService)
        {
            _publishProductService = publishProductService;
            _portalRecommendationSettingRepository = new ZnodeRepository<ZnodePortalRecommendationSetting>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _orderRepository = new ZnodeRepository<ZnodeOmsOrder>();
            _orderDetailsRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _orderLineItemsRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
        }

        //To get product recommendation setting against the portal Id.
        public RecommendationSettingModel GetRecommendationSetting(int portalId)
        {
            ZnodeLogging.LogMessage("Get recommendation settings for portalId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, portalId);

            if (portalId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.PortalIdNotLessThanOne);

            RecommendationSettingModel recommendationSettingModel = new RecommendationSettingModel();

            //To get record from ZnodePortalRecommendationSetting entity.
            ZnodePortalRecommendationSetting recommendationSettingEntity = _portalRecommendationSettingRepository.Table.FirstOrDefault(x => x.PortalId == portalId);

            if(IsNotNull(recommendationSettingEntity))
                recommendationSettingModel = recommendationSettingEntity.ToModel<RecommendationSettingModel>();

            //Portal id will be used in save event.
            recommendationSettingModel.PortalId = portalId;

            //Portal name to be displayed on the page.
            recommendationSettingModel.PortalName = _portalRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.StoreName;
            return recommendationSettingModel;
        }

        //To save the product recommendation setting.
        public RecommendationSettingModel SaveRecommendationSetting(RecommendationSettingModel recommendationSettingModel)
        {
            if (IsNull(recommendationSettingModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorRecommendationSettingModelNull);

            //To check recommendation setting is present for portal or not, if present update the record else insert.
            ZnodePortalRecommendationSetting recommendationSettingEntity = _portalRecommendationSettingRepository.Table.FirstOrDefault(x => x.PortalId == recommendationSettingModel.PortalId);
            if (IsNotNull(recommendationSettingEntity))
            {
                //Because of ajax request might be possible that model does not contain value for the PortalRecommendationSettingId property. 
                recommendationSettingModel.PortalRecommendationSettingId = recommendationSettingEntity.PortalRecommendationSettingId;
                UpdateRecommendationSetting(recommendationSettingModel);
                return recommendationSettingModel;
            }                       
            else
                return InsertRecommendationSetting(recommendationSettingModel);            
        }

        //To update the recommendation setting.
        protected virtual bool UpdateRecommendationSetting(RecommendationSettingModel recommendationSettingModel)
        {
            bool isUpdated = _portalRecommendationSettingRepository.Update(recommendationSettingModel.ToEntity<ZnodePortalRecommendationSetting>());

            //Clear webstore cache if product recommendation setting updated successfully.
            if (isUpdated)
                ClearWebstoreCache(recommendationSettingModel);

            return isUpdated;
        }

        //To insert the recommendation setting.
        protected virtual RecommendationSettingModel InsertRecommendationSetting(RecommendationSettingModel recommendationSettingModel)
        {
            //Used recommendationSettingEntity variable to hold inserted entity for better readability.
            ZnodePortalRecommendationSetting recommendationSettingEntity = _portalRecommendationSettingRepository.Insert(recommendationSettingModel.ToEntity<ZnodePortalRecommendationSetting>());

            //Clear webstore cache if product recommendation setting saved successfully.
            if (recommendationSettingEntity?.PortalRecommendationSettingId > 0)
                ClearWebstoreCache(recommendationSettingModel);

            return recommendationSettingEntity.ToModel<RecommendationSettingModel>();
        }

        //To clear webstore cache using ZnodeEventNotifier and ZnodeEventObserver.
        protected virtual void ClearWebstoreCache(RecommendationSettingModel recommendationSettingModel)
        {
            //Initialization to notify event. 
            var clearCacheInitializer = new ZnodeEventNotifier<RecommendationSettingModel>(recommendationSettingModel);
        }


        #region Recommendations
        //To get the recommendations based on recommendation request.
        public virtual RecommendationModel GetRecommendation(RecommendationRequestModel recommendationRequestModel)
        {
            // TODO - We should not inject historical context
            IRecommendationEngine engine = new RecommendationEngine(Mappers.MapToHistoricalContext(GetPlacedOrderDetails()));

            Recommendation engineRecommendation = engine.GetProductRecommendations(recommendationRequestModel.ToModel<RecommendationContext, RecommendationRequestModel>());
                        
            return new RecommendationModel
            {
                RecommendedProducts = PublishProductModels(engineRecommendation.ProductSkus, recommendationRequestModel)
            };
        }

        //To get the historical context(placed order data).
        protected virtual List<List<Product>> GetPlacedOrderDetails()
        {
            List<List<Product>> lineItemsSkuList = new List<List<Product>>();

            //To get the order line items for each and every order being placed.
            var orderLineItemsListForEachOrderDetail = (from order in _orderRepository.Table
                                                        join orderDetails in _orderDetailsRepository.Table on order.OmsOrderId equals orderDetails.OmsOrderId
                                                        join orderLineItems in _orderLineItemsRepository.Table on orderDetails.OmsOrderDetailsId equals orderLineItems.OmsOrderDetailsId
                                                        group orderLineItems by orderLineItems.OmsOrderDetailsId into data
                                                        select data ).ToList();

            //To get SKU and Quantity of products of every order.
            orderLineItemsListForEachOrderDetail.ForEach(x => {
                lineItemsSkuList.Add(x.Select(y => new Product { SKU = y.Sku, Quantity = y.Quantity }).DistinctBy(z => z.SKU).ToList());
            });

            ZnodeLogging.LogMessage("lineItemsSkuList count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, lineItemsSkuList.Count);
            return lineItemsSkuList;
        }

        //To get the recommended published products.
        protected virtual List<PublishProductModel> PublishProductModels(List<string> productSkus, RecommendationRequestModel recommendationRequestModel)
        {
            if (productSkus?.Count > 0)
            {
                ZnodeLogging.LogMessage("Count of recommended products derived by engine: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, productSkus.Count);

                //DefaultGlobalConfigSettingHelper.DefaultProductLimitForRecommendations is used to get the top ranked products according to the product limit specified in global setting.
                FilterCollection filters = GetRequiredFilters(productSkus, recommendationRequestModel);

                PublishProductListModel listModel = _publishProductService.GetPublishProductList(null, filters, null, null);

                if (listModel?.PublishProducts?.Count > 0)
                {
                    //Distinct clause is used to avoid duplicate products in case of same product is associated to many categories and
                    //OrderBy clause is used to change the order of published products list according to recommended products SKU list(ordered by ranking).
                    listModel.PublishProducts = listModel.PublishProducts.DistinctBy(product => product.SKU).OrderBy(product => productSkus.IndexOf(product.SKU)).Take(DefaultGlobalConfigSettingHelper.DefaultProductLimitForRecommendations).ToList();

                    ZnodeLogging.LogMessage("Recommended products count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, listModel.PublishProducts?.Count);
                    return listModel.PublishProducts;
                }
            }            
            return new List<PublishProductModel>();
        }
        
        //Filters to get the desired publish products.
        protected virtual FilterCollection GetRequiredFilters(List<string> productSkus, RecommendationRequestModel recommendationRequestModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(recommendationRequestModel.CatalogId));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, recommendationRequestModel.LocaleId.ToString());
            filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, recommendationRequestModel.PortalId.ToString());
            filters.Add(ZnodeConstant.SkuLower, FilterOperators.In, String.Join(",", productSkus.ConvertAll(sku => sku.ToLower())));
            //To avoid those products which are not available in any category.
            filters.Add(FilterKeys.ZnodeCategoryIds.ToString(), FilterOperators.NotEquals, ZnodeConstant.Zero);

            //Set profile Id in filters to get the products according to the user profile.
            if (recommendationRequestModel.ProfileId.HasValue)
                filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(recommendationRequestModel.ProfileId));
            
            return filters;
        }
        #endregion
    }
}
