﻿using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Engine.Services.Helper;

namespace Znode.Engine.Services
{
    public partial class CategoryService : BaseService, ICategoryService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodePimCategory> _pimCategory;
        private readonly IZnodeRepository<ZnodePimCategoryProduct> _pimCategoryProductRepository;
        private readonly IZnodeRepository<ZnodePimCategoryHierarchy> _pimCategoryHierarchyRepository;
        private IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IZnodeRepository<ZnodeLocale> _localeRepository;
        private readonly IMongoRepository<CategoryEntity> _CategoryMongoRepository;
        private readonly IMongoRepository<SeoEntity> _seoProductMongoRepository;
        private readonly IZnodeRepository<ZnodeCMSSEODetail> _seoDetailRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IWebSiteService webSiteService;
        private ProductAssociationHelper productAssociationHelper;
        #endregion

        #region Constructor
        public CategoryService()
        {
            _pimCategory = new ZnodeRepository<ZnodePimCategory>();
            _pimCategoryProductRepository = new ZnodeRepository<ZnodePimCategoryProduct>();
            _pimCategoryHierarchyRepository = new ZnodeRepository<ZnodePimCategoryHierarchy>();
            _localeRepository = new ZnodeRepository<ZnodeLocale>();
            _CategoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _productMongoRepository = new MongoRepository<ProductEntity>();
            _seoProductMongoRepository = new MongoRepository<SeoEntity>();
            _seoDetailRepository = new ZnodeRepository<ZnodeCMSSEODetail>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            webSiteService = ZnodeDependencyResolver.GetService<IWebSiteService>();
            productAssociationHelper = new ProductAssociationHelper();
        }
        #endregion

        #region Public Methods

        //Get a list of Categories
        public virtual CategoryListModel GetCategories(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {       
            

            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("pageListModel for GetXmlCategory:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            //gets the order by clause
            string orderBy = DynamicClauseHelper.GenerateDynamicOrderByClause(sorts);
            
            //This condition execute only when Category list is required for Catalog category association.
            if (filters.Any(filterTuple => filterTuple.FilterName.Equals(FilterKeys.IsAssociatedProducts)))
                {
                    int catalogId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodePimCatalogEnum.PimCatalogId.ToString().ToLower())?.FirstOrDefault()?.Item3);
                    ZnodeLogging.LogMessage("catalogId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, catalogId );

                    List<int?> categoryIds = (from catalogHierarchy in _pimCategoryHierarchyRepository.Table
                                              where catalogHierarchy.PimCatalogId == catalogId
                                              select catalogHierarchy.PimCategoryId).ToList();
                    ZnodeLogging.LogMessage("categoryIds list :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose,  categoryIds?.Count() );

                    filters.Remove(filters.Where(x => x.FilterName == ZnodePimCatalogEnum.PimCatalogId.ToString().ToLower())?.FirstOrDefault());
                    filters.Remove(filters.Where(x => x.FilterName == FilterKeys.IsAssociatedProducts.ToLower())?.FirstOrDefault());

                    if (categoryIds?.Count > 0) { filters.Add(new FilterTuple(ZnodePimCategoryEnum.PimCategoryId.ToString(), FilterOperators.NotIn, string.Join(",", categoryIds))); }
                }

            var xmlList = GetXmlCategory(filters, pageListModel, orderBy);

            CategoryListModel categoryList = new CategoryListModel
                {
                    Categories = new List<CategoryModel>(),
                    Locale = GetActiveLocaleList(),
                    AttrubuteColumnName = xmlList.AttrubuteColumnName,
                    XmlDataList = xmlList.XmlDataList
                };

            categoryList.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return categoryList;
        }
    
        // Get category details for edit
        public virtual PIMFamilyDetailsModel GetCategory(int categoryId, int familyId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters categoryId, familyId, localeId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { categoryId, familyId, localeId });

            //Check category id exist in database or not.
            int categoryexsist = _pimCategory.Table.Where(x => x.PimCategoryId == categoryId).Select(x => x.PimCategoryId).FirstOrDefault();

            if (categoryId != 0 && categoryexsist == 0)
                return null;

            //Get all attributes associated with CategoryId and with familyId associated with categoryId
            IZnodeViewRepository<View_PimCategoryAttributeValues> pimAttributeValues = new ZnodeViewRepository<View_PimCategoryAttributeValues>();
            pimAttributeValues.SetParameter("ChangeFamilyId", familyId, ParameterDirection.Input, DbType.Int32);
            pimAttributeValues.SetParameter("PimCategoryId", categoryId, ParameterDirection.Input, DbType.Int32);
            pimAttributeValues.SetParameter(ZnodePimAttributeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            var pimAttributes = pimAttributeValues.ExecuteStoredProcedureList("Znode_GetPimCategoryAttributeValues @ChangeFamilyId, @PimCategoryId, @LocaleId");
            ZnodeLogging.LogMessage("All attributes associated with familyId and categoryId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pimAttributes?.Count());
    
            int associatedFamilyId = pimAttributes.FirstOrDefault(e => e.FamilyCode != "DefaultCategory")?.PimAttributeFamilyId ?? 0;
            ZnodeLogging.LogMessage("Associated Family with Id :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, associatedFamilyId);

            //Get all groups associated with default family and familyId

            IList<View_PimAttributeGroupbyFamily> pimAttributeGroups = GetGroupsAssociatedWithFamily(associatedFamilyId, true, localeId);
            ZnodeLogging.LogMessage("Groups associated with family :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pimAttributeGroups?.Count);

            //Get All Pim category Families except the default family
            IList<PIMAttributeFamilyModel> pimAttributeFamilies = GetCategoryFamilies(localeId);
            ZnodeLogging.LogMessage("Category Families :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pimAttributeFamilies?.Count);

            return IsNotNull(pimAttributeGroups) && IsNotNull(pimAttributes) && IsNotNull(pimAttributeFamilies)
                ? MapToPIMFamilyDetailsModel(associatedFamilyId, categoryId, pimAttributes, pimAttributeGroups, pimAttributeFamilies) : null;
        }

        //Creates a new Category
        public virtual CategoryValuesListModel CreateCategory(CategoryValuesListModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            if (IsNull(model?.AttributeValues))
                    throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryModelNull);

            ZnodeLogging.LogMessage("SaveUpdateAttributeValues method with parameters.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, model?.AttributeValues);
            IList<View_ReturnBoolean> SaveStatus = SaveUpdateAttributeValues(model?.AttributeValues);
            
            if (SaveStatus.FirstOrDefault().Status.Value)
                {
                ZnodeLogging.LogMessage(string.Format(PIM_Resources.SuccessSaveCategory, model.AttributeValues.FirstOrDefault().AttributeValue), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                if (IsNotNull(model))
                        model.PimCategoryId = SaveStatus.FirstOrDefault().Id;

                ZnodeLogging.LogMessage("InsertCatalogData with parameters :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, model);

                //Inserts into mapper table.
                InsertCatalogData(model);
                ZnodeLogging.LogMessage("Catalog data inserted.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, null);

                return model;
                }
                else
                {
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorSaveCategory, model.AttributeValues.FirstOrDefault().AttributeValue), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return model;
                }
        }

        public virtual bool IsCategoryMerchandise { get => Convert.ToBoolean(ZnodeApiSettings.IsCategoryMerchandise); }

        //Updates an already existing Category 
        public virtual bool UpdateCategory(CategoryValuesListModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            if (IsNull(model?.AttributeValues))
                    throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryModelNull);

                ZnodeLogging.LogMessage("SaveUpdateAttributeValues with parameters :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, model?.AttributeValues);
                IList<View_ReturnBoolean> SaveStatus = SaveUpdateAttributeValues(model?.AttributeValues);

            if (SaveStatus.FirstOrDefault()?.Status.GetValueOrDefault() ?? false)
                {
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.SuccessUpdateCategory, model.AttributeValues.FirstOrDefault().AttributeValue), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return true;
                }
                else
                {
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorUpdateCategory, model.AttributeValues.FirstOrDefault().AttributeValue), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return false;
                }

        }

        //Deletes a Category using catergoryId
        public virtual bool DeleteCategory(ParameterModel categoryIds)
        {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("categoryIds to be deleted :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, categoryIds?.Ids);

                IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
                objStoredProc.SetParameter("PimCategoryIds", categoryIds.Ids, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
                int status = 0;
                IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCategory @PimCategoryIds, @Status OUT", 1, out status);
                 ZnodeLogging.LogMessage("Deleted result count :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, deleteResult?.Count);

                if (deleteResult.FirstOrDefault().Status.Value)
                {
                    ZnodeLogging.LogMessage(PIM_Resources.SuccessDeleteCategory, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return true;
                }
                else
                {
                    ZnodeLogging.LogMessage(PIM_Resources.ErrorFailToDeleteCategory, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return false;
                }
        }

        //Deletes a products associated to category.
        public virtual bool DeleteCategoryAssociatedProducts(ParameterModel PimCategoryProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (IsCategoryMerchandise)
            {
                productAssociationHelper.DeleteProductfromAllCatalog(PimCategoryProductId);
                return productAssociationHelper.UnassociateProductFromCategory(PimCategoryProductId);
            }
            else
                return DeleteCategoryAssociatedProduct(PimCategoryProductId);

        }

        protected virtual bool DeleteCategoryAssociatedProduct(ParameterModel PimCategoryProductId)
        {
            ZnodeLogging.LogMessage("PimCategoryProductId to be deleted :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, PimCategoryProductId?.Ids);

            if (PimCategoryProductId.Ids.Count() < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorPimCategoryProductIdLessThanOne);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimCategoryProductEnum.PimCategoryProductId.ToString(), ProcedureFilterOperators.In, PimCategoryProductId.Ids.ToString()));

            return _pimCategoryProductRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
        }

        // Delete associated categories to product.
        public virtual bool DeleteAssociatedCategoriesToProduct(ParameterModel PimCategoryProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (PimCategoryProductId.Ids.Count() < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryProductIdLessThanOne);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimCategoryProductEnum.PimCategoryProductId.ToString(), ProcedureFilterOperators.In, PimCategoryProductId.Ids.ToString()));

            return _pimCategoryProductRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
        }

        //Get associate products to Category
        public virtual bool AssociateCategoryProduct(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            if (!IsCategoryMerchandise)
                return AssociateCategoryProducts(categoryProductModel);
            else
                return productAssociationHelper.AssociateProductToCategory(categoryProductModel);

        }

        public virtual bool AssociateCategoryProducts(List<CategoryProductModel> categoryProductModel)
        {
            if (IsNull(categoryProductModel))
                throw new ZnodeException(ErrorCodes.NullModel, PIM_Resources.ErrorModelNull);

            if (categoryProductModel.Count < 1)
                throw new ZnodeException(ErrorCodes.InvalidData,PIM_Resources.ErrorCategoryProductModelCount);

            int categoryId = Convert.ToInt32(categoryProductModel[0].PimCategoryId.ToString());
            ZnodeLogging.LogMessage("categoryId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, categoryId);

            var associatedProductList = _pimCategoryProductRepository.Table?.Where(x => x.PimCategoryId == categoryId).Select(y => y.PimProductId);

            categoryProductModel = associatedProductList.Any() ? categoryProductModel?.Where(i => !associatedProductList.Contains(i.PimProductId)).ToList() : categoryProductModel;

            if (categoryProductModel.Count == 0)
                return true;

            IEnumerable<ZnodePimCategoryProduct> associateCategoryProducts = _pimCategoryProductRepository.Insert(categoryProductModel.ToEntity<ZnodePimCategoryProduct>().ToList());
            return associateCategoryProducts?.Count() > 0;
        }

        public virtual bool AssociateCategoriesToProduct(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (!IsCategoryMerchandise)
                return AssociateProductOnCategoryMerchandise(categoryProductModel);
            else
                return AssociateProductOnCategory(categoryProductModel);
        }

        protected virtual bool AssociateProductOnCategory(List<CategoryProductModel> categoryProductModel)
        {
            if (IsNull(categoryProductModel))
                throw new ZnodeException(ErrorCodes.NullModel, PIM_Resources.ErrorModelNull);

            if (categoryProductModel.Count < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryProductModelCount);

            return productAssociationHelper.AssociatePimProductToCategory(categoryProductModel);
        }

        protected virtual bool AssociateProductOnCategoryMerchandise(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            if (IsNull(categoryProductModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelNotNull);

            if (categoryProductModel.Count < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryProductModelCount);

            int categoryId = Convert.ToInt32(categoryProductModel[0].PimCategoryId.ToString());
            ZnodeLogging.LogMessage("Category Id :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, categoryId);

            var associatedProductList = _pimCategoryProductRepository.Table?.Where(x => x.PimCategoryId == categoryId).Select(y => y.PimProductId);
            ZnodeLogging.LogMessage("associatedProductList :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, associatedProductList.Count());

            categoryProductModel = associatedProductList.Any() ? categoryProductModel?.Where(i => !associatedProductList.Contains(i.PimProductId)).ToList() : categoryProductModel;
           
            if (categoryProductModel.Count == 0)
                return true;

            IEnumerable<ZnodePimCategoryProduct> associateCategoryProducts = _pimCategoryProductRepository.Insert(categoryProductModel.ToEntity<ZnodePimCategoryProduct>().ToList());
            ZnodeLogging.LogMessage("associateCategoryProducts :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, associateCategoryProducts.Count());

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return associateCategoryProducts?.Count() > 0;
        }

        //Get Product By Category 
        public virtual CategoryProductListModel GetAssociatedUnAssociatedCategoryProducts(int categoryId, bool associatedProduts, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            int catalogId = 0;
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel for ProductList:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            var list = ProductList(filters, pageListModel, categoryId, associatedProduts, catalogId);
            ZnodeLogging.LogMessage("product list count :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, list.Count);

            CategoryProductListModel categoryProductListModel = new CategoryProductListModel { CategoryProducts = list?.Count > 0 ? list.ToList() : null };
            
            //Get PimCategoryProductId 
            if (categoryProductListModel.CategoryProducts?.Count > 0)
                foreach (var item in categoryProductListModel.CategoryProducts)
                    item.PimCategoryProductId = _pimCategoryProductRepository.Table.Where(x => x.PimCategoryId == categoryId && x.PimProductId == item.PimProductId).Select(g => g.PimCategoryProductId).FirstOrDefault();
            
            categoryProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return categoryProductListModel;
        }

        //Get associated Product By Category 
        public virtual CategoryProductListModel GetAssociatedCategoryProducts(int categoryId, bool associatedProducts, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            //Get locale id from filter otherwise set default
            int localeId = GetLocaleId(filters);
            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated  :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClause);

            string attributeCode = GetAttributeCodes(filters);
            
            IZnodeViewRepository<CategoryProductModel> objStoredProc = new ZnodeViewRepository<CategoryProductModel>();
            objStoredProc.SetParameter("@WhereClause", whereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@IsAssociated", associatedProducts, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCategoryEnum.PimCategoryId.ToString(), categoryId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@AttributeCode", attributeCode, ParameterDirection.Input, DbType.String);
            //List of all products associated to category.
            IList<CategoryProductModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetPimCategoryProductList @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId,@PimCategoryId,@IsAssociated,@AttributeCode", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("List of all products associated to category count :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, list.Count);
            CategoryProductListModel categoryProductListModel = new CategoryProductListModel { CategoryProducts = list?.Count > 0 ? list.ToList() : null };
           
            //Get PimCategoryProductId 
            if (categoryProductListModel.CategoryProducts?.Count > 0)
                foreach (var item in categoryProductListModel.CategoryProducts)
                    item.PimCategoryProductId = _pimCategoryProductRepository.Table.Where(x => x.PimCategoryId == categoryId && x.PimProductId == item.PimProductId).Select(g => g.PimCategoryProductId).FirstOrDefault();
            
            categoryProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return categoryProductListModel;
        }

        //Get the list of associated categories to product. 
        public virtual CategoryProductListModel GetAssociatedCategoriesToProducts(int productId, bool associatedProducts, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters productId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, productId);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            //Get locale id from filter otherwise set default
            int localeId = GetLocaleId(filters);
            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated  :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClause);

            string attributeCode = GetAttributeCodes(filters);
            IZnodeViewRepository<CategoryProductModel> objStoredProc = new ZnodeViewRepository<CategoryProductModel>();
            objStoredProc.SetParameter("@WhereClause", whereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PimProductIdInput", productId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsAssociated", associatedProducts, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@AttributeCode", attributeCode, ParameterDirection.Input, DbType.String);

            //List of all categories associated to products.
            IList<CategoryProductModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetPimProductCategoryList @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId,@PimProductIdInput,@IsAssociated,@AttributeCode", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("List of all categories associated to products count :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, list.Count);

            CategoryProductListModel categoryProductListModel = new CategoryProductListModel { CategoryProducts = list?.Count > 0 ? list.ToList() : null };
           
            categoryProductListModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return categoryProductListModel;
        }

        //Get locale id from filter otherwise set default
        public static int GetLocaleId(FilterCollection filters)
        {
            int localeId = Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale);
            //Checking For LocaleId exists in Filters Or Not
            if (filters.Exists(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower()))
            {
                localeId = Convert.ToInt32(filters.Where(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower()).Select(x => x.FilterValue).FirstOrDefault());
                filters.RemoveAll(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower());
            }

            return localeId;
        }

        //Publish Category 
        public virtual PublishedModel Publish(ParameterModel parameterModel)
        {

            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            int status;
            bool isCatalogCategoryPublishInProgress = new ZnodeRepository<ZnodePublishCatalogLog>().Table.Any(x => x.IsCatalogPublished == null || x.IsCategoryPublished == null);
            ZnodeLogging.LogMessage("isCatalogCategoryPublishInProgress status", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, isCatalogCategoryPublishInProgress);

            if (isCatalogCategoryPublishInProgress)
                throw new ZnodeException(ErrorCodes.CategoryPublishError, PIM_Resources.ErrorPublishCatalogCategory);


            DataSet resultDataSet = GetCategoryPublishDataset(parameterModel, out status);

            if (resultDataSet?.Tables?.Count > 1 && resultDataSet?.Tables[0].Rows.Count > 0)
            {
                List<int> catalogIds = resultDataSet.Tables[0].Rows[0]["PimCatalogId"].ToString().Split(',').Select(s => int.Parse(s)).ToList();
                ZnodeLogging.LogMessage("catalogIds :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, catalogIds.Count);
                List<string> categoryXmlList = resultDataSet.Tables[1].AsEnumerable().Select(dataRow => dataRow.Field<string>("CategoryXml")).ToList();

                if (parameterModel.RevisionType == ZnodePublishStatesEnum.PRODUCTION.ToString())

                    SaveCategoryInMongo(categoryXmlList, ZnodePublishStatesEnum.PREVIEW.ToString());

                SaveCategoryInMongo(categoryXmlList, parameterModel.RevisionType);

                //Clear cache call               
                var clearCacheInitializer = new ZnodeEventNotifier<List<int>>(catalogIds);
                foreach (int catalogId in catalogIds)
                {
                    ClearCacheAfterPublish(catalogId);
                }
            }
            else
            {
                if (status == 0)
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublished };
                else if (status == 1)
                    throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.CategoryPublishError);
            }
            ZnodeLogging.LogMessage("Publish method execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return new PublishedModel { IsPublished = true, ErrorMessage = Admin_Resources.SuccessPublish };
        }

        //Get all groups associated with default family and familyId
        public static IList<View_PimAttributeGroupbyFamily> GetGroupsAssociatedWithFamily(int familyId, bool IsCategory, int localeId = 0)
        {
            ZnodeLogging.LogMessage("Input parameters :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { familyId, localeId });

            localeId = localeId == 0 ? Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale) : localeId;
            IZnodeViewRepository<View_PimAttributeGroupbyFamily> pimAttributeGroupbyFamily = new ZnodeViewRepository<View_PimAttributeGroupbyFamily>();
            pimAttributeGroupbyFamily.SetParameter(ZnodePimAttributeFamilyEnum.PimAttributeFamilyId.ToString(), familyId, ParameterDirection.Input, DbType.Int32);
            pimAttributeGroupbyFamily.SetParameter(ZnodePimAttributeGroupLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            pimAttributeGroupbyFamily.SetParameter(ZnodePimAttributeGroupEnum.IsCategory.ToString(), IsCategory, ParameterDirection.Input, DbType.Boolean);
            return pimAttributeGroupbyFamily.ExecuteStoredProcedureList("Znode_GetPimAttributeGroupbyFamily @PimAttributeFamilyId,@IsCategory,@LocaleId");
        }

        //Save category in mongo db.
        public void SaveCategoryInMongo(IEnumerable<string> categoryXmlList, string revisionType)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, null);

            IMongoRepository<CategoryEntity> _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            List<int> publishCatalogIds = new List<int>();
            ZnodeLogging.LogMessage("publishCatalogIds :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, publishCatalogIds);

            if (string.IsNullOrEmpty(revisionType) || revisionType.Equals("NONE",StringComparison.OrdinalIgnoreCase))
                revisionType = ZnodePublishStatesEnum.PRODUCTION.ToString();

            foreach (string categoryXml in categoryXmlList)
                PublishPreviewCategory(revisionType, _categoryMongoRepository, publishCatalogIds, categoryXml);
            
        }


        #endregion

        #region Private Methods

        private void PublishPreviewCategory(string revisionType, IMongoRepository<CategoryEntity> _categoryMongoRepository, List<int> publishCatalogIds, string categoryXml)
        {
            CategoryEntity categoryEntity = ConvertXMLStringToModel<CategoryEntity>(categoryXml);
            
            publishCatalogIds.Add(categoryEntity.ZnodeCatalogId);

            if (IsNotNull(categoryEntity))
            {
                int versionId = PreviewHelper.GetVersionId(categoryEntity.ZnodeCatalogId, revisionType, categoryEntity.LocaleId);
                ZnodeLogging.LogMessage("versionId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, versionId);

                categoryEntity.ZnodeParentCategoryIds = !string.IsNullOrEmpty(categoryEntity.TempZnodeParentCategoryIds) ? ((categoryEntity.TempZnodeParentCategoryIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                categoryEntity.ProductIds = !string.IsNullOrEmpty(categoryEntity.TempProductIds) ? ((categoryEntity.TempProductIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                categoryEntity.ProfileIds = !string.IsNullOrEmpty(categoryEntity.TempProfileIds) ? ((categoryEntity.TempProfileIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                categoryEntity.VersionId = versionId;
                
                if (IsWebstorePreviewEnabled() ? !string.Equals(revisionType, ZnodePublishStatesEnum.PRODUCTION.ToString()) : true)
                {
                    //Delete Publish category associated with given catalog
                    PreviewHelper.DeleteRecords(new MongoRepository<CategoryEntity>(), new MongoRepository<_LogCategoryEntity>(), versionId, Query.And(
                            Query<CategoryEntity>.EQ(x => x.ZnodeCatalogId, categoryEntity.ZnodeCatalogId),
                            Query<CategoryEntity>.EQ(x => x.ZnodeCategoryId, categoryEntity.ZnodeCategoryId),
                            Query<CategoryEntity>.EQ(x => x.VersionId, versionId)
                            ));

                    _categoryMongoRepository.Create(categoryEntity);
                    
                    SaveSEOInMongo(categoryEntity.ZnodeCatalogId, categoryEntity.LocaleId, categoryEntity.VersionId, versionId, revisionType);
                    
                }
                else
                    PublishCategory(categoryEntity, versionId);
               
                PreviewHelper.SetPimCategoryByCatalogIdAndSetStatus(categoryEntity.ZnodeCategoryId, revisionType, 0);
                //Update SEO publish status.
                PreviewHelper.UpdateSeoPublishStatusBySEOCode(categoryEntity.CategoryCode, revisionType);
                PreviewHelper.CreatePreviewLog(revisionType, categoryEntity.VersionId, categoryEntity.VersionId, categoryEntity.ZnodeCatalogId, "category", categoryEntity.LocaleId, GetLocaleName(categoryEntity.LocaleId));
            }
            else
            {
                string logMessage = $"One of the categories failed to publish to Mongo due to blank XML. Method name - PublishPreviewCategory. Class Name - CategoryService. Parameters: revisionType:{revisionType}, publishCatalogIds:{publishCatalogIds}. Object - categoryXml:{categoryXml}.";
                ZnodeLogging.LogMessage(logMessage, "Publish", TraceLevel.Warning);
            }
        }

        //Publish SEO configuration
        private void SaveSEOInMongo(int publishCatalogId, int localeId, int currentlyPreviewedVersionId, int previousPreviewVersionId, string revisionType)
        {

            SEODetailsEnum[] seoTypes = new SEODetailsEnum[] { SEODetailsEnum.Category };

            ZnodePublishStatesEnum znodePublishStateEnum = (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), revisionType);
            
            List<int> portalIds = _portalCatalogRepository.Table.Where(x => x.PublishCatalogId == publishCatalogId)?.Select(x => x.PortalId)?.ToList();
            ZnodeLogging.LogMessage("portalIds count :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, portalIds);
            if(IsNotNull(portalIds))
                foreach (int portalId in portalIds)
                    webSiteService.SaveSeoToMongo(portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId, znodePublishStateEnum, seoTypes);
           
        }

        //Publish Category 
        private static void PublishCategory(CategoryEntity categoryEntity, int previousVersionId)
        {
            ZnodeLogging.LogMessage("PublishCategory method started", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, null);

            int previewVersionId = PreviewHelper.GetVersionId(categoryEntity.ZnodeCatalogId, ZnodePublishStatesEnum.PREVIEW.ToString(), categoryEntity.LocaleId);
            ZnodeLogging.LogMessage("previewVersionId", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, previewVersionId);
            //Delete Publish category associated with given catalog
            PreviewHelper.DeleteRecords(new MongoRepository<SeoEntity>(), new MongoRepository<_LogSeoEntity>(), previousVersionId, Query.And(
                    Query<SeoEntity>.EQ(x => x.VersionId, previousVersionId),
                    Query<SeoEntity>.EQ(x => x.LocaleId, categoryEntity.LocaleId),
                    Query<SeoEntity>.EQ(x => x.SEOCode, categoryEntity.CategoryCode)
                    ));

            //Delete Publish category associated with given catalog
            PreviewHelper.DeleteRecords(new MongoRepository<CategoryEntity>(), new MongoRepository<_LogCategoryEntity>(), previousVersionId, Query.And(
                    Query<CategoryEntity>.EQ(x => x.ZnodeCatalogId, categoryEntity.ZnodeCatalogId),
                    Query<CategoryEntity>.EQ(x => x.ZnodeCategoryId, categoryEntity.ZnodeCategoryId),
                    Query<CategoryEntity>.EQ(x => x.VersionId, previousVersionId),
                    Query<CategoryEntity>.EQ(x => x.LocaleId, categoryEntity.LocaleId)
                    ));

            PreviewHelper.CopyEntitiesForPreview<CategoryEntity, _LogCategoryEntity>(categoryEntity.VersionId, Query.And(
                    Query<CategoryEntity>.EQ(x => x.ZnodeCatalogId, categoryEntity.ZnodeCatalogId),
                    Query<CategoryEntity>.EQ(x => x.ZnodeCategoryId, categoryEntity.ZnodeCategoryId),
                    Query<CategoryEntity>.EQ(x => x.VersionId, previewVersionId),
                    Query<CategoryEntity>.EQ(x => x.LocaleId, categoryEntity.LocaleId)
                    ), new MongoRepository<CategoryEntity>());

            PreviewHelper.CopyEntitiesForPreview<SeoEntity, _LogSeoEntity>(categoryEntity.VersionId, Query.And(
                    Query<SeoEntity>.EQ(x => x.VersionId, previewVersionId),
                    Query<SeoEntity>.EQ(x => x.LocaleId, categoryEntity.LocaleId),
                    Query<SeoEntity>.EQ(x => x.SEOCode, categoryEntity.CategoryCode)
                    ), new MongoRepository<SeoEntity>());

            ZnodeLogging.LogMessage("PublishCategory method completed", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, null);

        }

        //Gets the attribute code form filters.
        protected virtual string GetAttributeCodes(FilterCollection filters)
        {
            if (filters?.Count > 0)
            {
                string attributeCode = string.Join(",", filters.Select(x => x.FilterName).ToArray());
                
                if (!string.IsNullOrEmpty(attributeCode) && attributeCode.Contains("|"))
                    attributeCode = attributeCode.Replace('|', ',');
                return attributeCode;
            }
            return string.Empty;
        }

        //Return List of categories on basis of where clause
        private CategoryListModel GetXmlCategory(FilterCollection filters, PageListModel pageListModel, string orderBy)
        {
            //Get locale Id.
            int localeId = GetLocaleId(filters);

            bool isCatalogFilter= false;
            //Get PIM Catalog Id from filters.
            int pimCatalogId = ServiceHelper.GetCatalogFilterValues(filters, ref isCatalogFilter);

            //gets the where clause with filter Values.              
            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClause);

            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Order_By", orderBy, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PimCatalogId", pimCatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@IsCatalogFilter", isCatalogFilter, ParameterDirection.Input, SqlDbType.Bit);

            var ds = objStoredProc.GetSPResultInDataSet("Znode_ManageCategoryList_XML");
            
            return Xmldataset(ds, out pageListModel.TotalRowCount);
        }

        //Convert dataset to dymanic list
        private CategoryListModel Xmldataset(DataSet ds, out int recoredCount)
        {
            // out pageListModel.TotalRowCount
            recoredCount = 0;
            if (!Equals(ds, null) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var xml = Convert.ToString(ds.Tables[0]?.Rows[0]["CategoryXML"]);
               
                if (!string.IsNullOrEmpty(xml))
                {
                    var columns = ds.Tables[1];

                    var _columnlist = GetDictionary(columns);

                    if (!string.IsNullOrEmpty(ds.Tables[2].Rows[0]["RowsCount"].ToString()))
                        recoredCount = Convert.ToInt32(ds.Tables[2].Rows[0]["RowsCount"].ToString());

                    XDocument xmlDoc = XDocument.Parse(xml);

                    dynamic root = new ExpandoObject();

                    XmlToDynamic.Parse(root, xmlDoc.Elements().First());

                    var _list = new List<dynamic>();
                    if (!(root.MainCategory.Category is List<dynamic>))
                    {
                        _list.Add(root.MainCategory.Category);
                    }
                    else
                    {
                        _list = (List<dynamic>)root.MainCategory.Category;
                    }

                    return new CategoryListModel { AttrubuteColumnName = _columnlist, XmlDataList = _list };
                }
            }
            return new CategoryListModel();
        }

        private Dictionary<string, object> GetDictionary(DataTable dt)
        {
            return dt.AsEnumerable()
              .ToDictionary<DataRow, string, object>(row => row.Field<string>(0),
                                        row => row.Field<object>(1));
        }

        //return List of product on basis of where clause
        protected virtual IList<CategoryProductModel> ProductList(FilterCollection filters, PageListModel pageListModel, int categoryId, bool associatedProduts, int catalogId)
        {
            int localeId = GetLocaleId(filters);

            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClause);

            string attributeCode = GetAttributeCodes(filters);
            int portalId = GetPortalId(filters);
            ZnodeLogging.LogMessage("portalId :", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, portalId);

            IZnodeViewRepository<CategoryProductModel> objStoredProc = new ZnodeViewRepository<CategoryProductModel>();
            objStoredProc.SetParameter("@WhereClause", whereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCategoryEnum.PimCategoryId.ToString(), categoryId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCatalogEnum.PimCatalogId.ToString(), catalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsAssociated", associatedProduts, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@AttributeCode", attributeCode, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@PortalId", portalId.ToString(), ParameterDirection.Input, DbType.Int32);
            //List of all products associated to category.
            return objStoredProc.ExecuteStoredProcedureList("ZNode_GetCatalogCategoryProducts @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId,@PimCategoryId,@PimCatalogId,@IsAssociated,0,@AttributeCode,@PortalId", 4, out pageListModel.TotalRowCount)?.ToList();

        }

        //Get Portal id from filter otherwise set default
        protected virtual int GetPortalId(FilterCollection filters)
        {
            int PortalId = 0;
            if (filters.Exists(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower()))
            {
                PortalId = Convert.ToInt32(filters.Where(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower()).Select(x => x.FilterValue).FirstOrDefault());
                filters.RemoveAll(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower());
            }
            return PortalId;
        }

        //Insert/Update category.
        private IList<View_ReturnBoolean> SaveUpdateAttributeValues(List<PIMCategoryValuesListModel> attributeValues)
        {
            string xmlData = ToXML<List<PIMCategoryValuesListModel>>(attributeValues);
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("CategoryXML", xmlData, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            return objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdatePimCategory @CategoryXML,@Status OUT, @UserId ", 1, out status);
        }

        //Get All Pim category Families except the default family
        private IList<PIMAttributeFamilyModel> GetCategoryFamilies(int localeId = 0)
        {
            //Get all family with familyId
            IZnodeViewRepository<PIMAttributeFamilyModel> pimAttributeValues = new ZnodeViewRepository<PIMAttributeFamilyModel>();
            pimAttributeValues.SetParameter(ZnodePimAttributeEnum.IsCategory.ToString(), true, ParameterDirection.Input, DbType.Boolean);
            pimAttributeValues.SetParameter(ZnodePimAttributeLocaleEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            var pimFamilies = pimAttributeValues.ExecuteStoredProcedureList("Znode_GetPimAttributeFamilyList @IsCategory, @LocaleId");
            return pimFamilies;
        }

        private PIMFamilyDetailsModel MapToPIMFamilyDetailsModel(int familyId, int categoryId, IList<View_PimCategoryAttributeValues> pimAttributes, IList<View_PimAttributeGroupbyFamily> pimAttributeGroups, IList<PIMAttributeFamilyModel> pimAttributeFamilies)
        => new PIMFamilyDetailsModel
        { 

            PimAttributeFamilyId = familyId,
            Attributes = pimAttributes.ToModel<PIMProductAttributeValuesModel>().ToList(),
            Groups = pimAttributeGroups.ToModel<PIMAttributeGroupModel>().ToList(),
            Family = pimAttributeFamilies.ToList(),
            Name = pimAttributes.FirstOrDefault(x => x.AttributeCode == "CategoryName")?.AttributeValue,
            Id = categoryId,
            Locale = _localeRepository.Table.Where(x => x.IsActive).ToModel<LocaleModel>().ToList(),
        };

        //Insert into catalog mapping table.
        private void InsertCatalogData(CategoryValuesListModel model)
        {
            
            //Insert into mapping table.
            if (model?.PimCatalogId > 0)
            {
                IZnodeRepository<ZnodePimCategoryHierarchy> _categoryHierarchyRepository = new ZnodeRepository<ZnodePimCategoryHierarchy>();
                model.PimParentCategoryId = model.PimParentCategoryId > 0 ? model.PimParentCategoryId : null;
                int maxDisplayOrder = GetMaxDisplayOrder(model.PimCatalogId.GetValueOrDefault(), model.PimParentCategoryId) + 75000;
                if (_categoryHierarchyRepository.Insert(new ZnodePimCategoryHierarchy { IsActive = true, PimCatalogId = model.PimCatalogId, PimCategoryId = model.PimCategoryId, ParentPimCategoryHierarchyId = model.PimParentCategoryId, DisplayOrder = maxDisplayOrder })?.PimCategoryHierarchyId > 0)
                {
                    ZnodeLogging.LogMessage(PIM_Resources.SuccessInsertInCategoryHierarchy, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    IZnodeRepository<ZnodePimCatalogCategory> _catalogCategoryRepository = new ZnodeRepository<ZnodePimCatalogCategory>();
                    ZnodeLogging.LogMessage(_catalogCategoryRepository.Insert(new ZnodePimCatalogCategory { PimCatalogId = model.PimCatalogId, PimCategoryId = model.PimCategoryId })?.PimCatalogCategoryId > 0 ? "Inserted successfull in catalog category." : "Fail to insert in catalog category.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                }
                ZnodeLogging.LogMessage(PIM_Resources.ErrorInsertInCategoryHierarchy, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            }
        }

        //Get the maximum display order of category.
        private int GetMaxDisplayOrder(int catalogId, int? parentCategory)
            => _pimCategoryHierarchyRepository.Table.Where(x => x.PimCatalogId == catalogId && x.ParentPimCategoryHierarchyId == parentCategory)?.Max(x => x.DisplayOrder) ?? 0;

        //Clear Cache of portal after catalog publish.
        private void ClearCacheAfterPublish(int publishCatalogId)
        {
            List<ZnodePortalCatalog> associatedPortalId = _portalCatalogRepository.Table.Where(portalCatalog => portalCatalog.PublishCatalogId == publishCatalogId)?.ToList();
            List<int> ids = associatedPortalId.Select(x => x.PortalId).ToList();
            string portalIds = string.Join(",", ids);
            ClearCacheHelper.ClearCacheAfterPublish(portalIds);
        }
        #endregion

        #region Publish Category

        private DataSet GetCategoryPublishDataset(ParameterModel parameterModel, out int status)
    {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            int categoryId = 0;
            
            parameterModel.LocaleIds = GetActiveLocaleList().Select(x => x.LocaleId).ToList();
            
            //Currently only single category Id be received for publish Parameter model is used for future purpose
            Int32.TryParse(parameterModel.Ids, out categoryId);

            status = 0;
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PimCategoryId", categoryId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", GetLoginUserId(), ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.SetTableValueParameter("@LocaleIds", PublishHelper.ConvertKeywordListToDataTable(parameterModel.LocaleIds), ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");
            executeSpHelper.GetParameter("@Status", status, ParameterDirection.Output, SqlDbType.Int);
            // If CatalogId is supplied then Znode give only data specific to that catalog 
            if (IsNotNull(parameterModel.publishCataLogId) && parameterModel.publishCataLogId > 0)
                executeSpHelper.GetParameter("@PimCatalogId", parameterModel.publishCataLogId, ParameterDirection.Input, SqlDbType.Int);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return executeSpHelper.GetSPResultInDataSet("Znode_GetPublishSingleCategory",3, out status);

        }

        [Obsolete]
        //Update category name in mongo product entity and also update in elastic search.
        protected void UpdateCategoryNameInMongoAndElatic(List<int> publishCategoryIds, List<int> localeIds, List<CategoryEntity> categoryEntities)
        {
            foreach (int publishCategoryId in publishCategoryIds)
            {
                List<ProductEntity> products = _productMongoRepository.GetEntityList(Query<ProductEntity>.EQ(pr => pr.ZnodeCategoryIds, publishCategoryId));
                
                if (products?.Count > 0)
                {
                    foreach (int localeId in localeIds)
                    {
                        string localeWiseCatName = categoryEntities.FirstOrDefault(l => l.LocaleId == localeId).Name.ToString();
                        
                        products.Where(s => s.LocaleId == localeId).ToList().ForEach(p => p.CategoryName = localeWiseCatName);
                    }
                    //Deleting Products in Mongo
                    _productMongoRepository.DeleteByQuery(Query<ProductEntity>.EQ(c => c.ZnodeCategoryIds, publishCategoryId));
                    //Save Products in Mongo
                    _productMongoRepository.Create(products);
                    //Save Products in Search (Elastic)
                    PublishHelper.SaveInSearchForCategoryName(products);
                }
            }
        }

        [Obsolete]
        //Publish Product SEO Details by SEOCode.
        public void PublishCategorySEOBySEOCode(List<string> SEOCodes)
        {
            //Publish SEO Locale Detail to mongo
            string seoCodes = string.Join(", ", SEOCodes);
            
            IZnodeViewRepository<SeoEntity> objStoredProc = new ZnodeViewRepository<SeoEntity>();
            objStoredProc.SetParameter("@PortalId", 0, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsBrand", 0, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@SeoCode", seoCodes, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SeoType", ZnodeConstant.Category, ParameterDirection.Input, DbType.String);
            
            List<SeoEntity> getCategorySeoLocaleDetailList;//code review point
            getCategorySeoLocaleDetailList = objStoredProc.ExecuteStoredProcedureList("Znode_GetSeoDetailsForPublish @PortalId,@IsBrand, @SeoCode, @SeoType").ToList();
            
            if (getCategorySeoLocaleDetailList?.Count > 0)
            {
                _seoProductMongoRepository.DeleteByQuery(Query<SeoEntity>.In(pr => pr.SEOCode, SEOCodes));
                _seoProductMongoRepository.Create(getCategorySeoLocaleDetailList);
                UpdateSeoPublishStatusBySEOCode(SEOCodes);
            }
        }
        //Update SEO product publish status
        private void UpdateSeoPublishStatusBySEOCode(List<string> seoCodes)
        {
            ZnodeLogging.LogMessage("Input parameters seoCodes:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, seoCodes);
            List<ZnodeCMSSEODetail> cMSSEODetailList = _seoDetailRepository.Table.Where(x => seoCodes.Contains(x.SEOCode)).ToList();
            ZnodeLogging.LogMessage("cMSSEODetailList :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, cMSSEODetailList);
            foreach (var cMSSEODetail in cMSSEODetailList)
            {
                cMSSEODetail.IsPublish = true;
                _seoDetailRepository.Update(cMSSEODetail);
            }
        }

        #endregion

    }
}
