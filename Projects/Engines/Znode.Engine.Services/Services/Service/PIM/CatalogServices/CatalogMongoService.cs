﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public class CatalogMongoService: ICatalogMongo
    {
        private readonly IMongoRepository<CatalogEntity> _catalogMongoRepository;

        public CatalogMongoService()
        {
            _catalogMongoRepository = new MongoRepository<CatalogEntity>();
        }

        /// <summary>
        ///  Method Gets Publish catalog by publish catalog Id
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Catalog</param>
        /// <returns>CatalogEntity</returns>
        public virtual CatalogEntity GetCatalogByPublishId(int catalogId)
            => _catalogMongoRepository.Table.MongoCollection.FindOne(Query<CatalogEntity>.EQ(e => e.ZnodeCatalogId, catalogId));
    }

}
