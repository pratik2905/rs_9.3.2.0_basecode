﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Utilities = Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public class PublishProductService : BaseService, IPublishProductService
    {
        #region Private Variables
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _userProfile;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IMongoRepository<ConfigurableProductEntity> _configurableproductRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IZnodeRepository<ZnodeAccount> _userAccount;
        private readonly IZnodeRepository<ZnodeUser> _user;

        public static string SKU { get; } = "sku";
        public static string Width { get; } = "width";
        public static string Height { get; } = "height";

        #endregion

        #region Constructor
        public PublishProductService()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
            _userProfile = new ZnodeRepository<ZnodeUserProfile>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>(GetCatalogVersionId());
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _userAccount = new ZnodeRepository<ZnodeAccount>();
            _user = new ZnodeRepository<ZnodeUser>();
        }
        #endregion

        #region Public Methods      

        public virtual PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, publishProductId);

            bool isChildPersonalizableAttribute = false;
            List<PublishAttributeModel> parentPersonalizableAttributes = null;
            int portalId, localeId;
            int? catalogVersionId;
            PublishProductModel publishProduct=null;
            List<ProductEntity> products;
            string parentProductImageName = null;

            //Get publish product from mongo
            products = GetPublishProductFromMongo(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            List<int> associatedCategoryIds = new List<int>();

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
               IEnumerable<int> categoryIds=products.Select(x=>x.ZnodeCategoryIds);

                List<IMongoQuery> query = new List<IMongoQuery>();
                query.Add(Query<CategoryEntity>.In(d => d.ZnodeCategoryId, categoryIds));
                query.Add(Query<CategoryEntity>.EQ(d => d.IsActive, true));
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));
                
                List<CategoryEntity> categoryEntities=_categoryMongoRepository.GetEntityList(Query.And(query), true);
                 
                associatedCategoryIds.AddRange(categoryEntities.Select(x=>x.ZnodeCategoryId));
              
                publishProduct= products.FirstOrDefault(x=>associatedCategoryIds.Contains(x.ZnodeCategoryIds)).ToModel<PublishProductModel>();

                parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                ConfigurableProductEntity configEntiy = GetConfigurableProductEntity(publishProductId, catalogVersionId);

                if (HelperUtility.IsNotNull(configEntiy))
                {
                    //Selecting child SKU
                    IEnumerable<string> childSKU = publishProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();
                    //Get Associated product by child SKUs
                    List<ProductEntity> associatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, childSKU);

                    parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();

                    publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.ConfigurableAttributeCodes, catalogVersionId.GetValueOrDefault());
                }
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault(), null, GetProfileId());

                isChildPersonalizableAttribute = publishProduct.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                GetProductImagePath(portalId, publishProduct, true, parentProductImageName);
                
                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);

                publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;
            }
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
        }        

        /// <summary>
        /// This method only returns the details of a parent published product found in mongo.
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        public virtual PublishProductModel GetpublishParentProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            PublishProductModel publishProduct = GetPublishedProductFromMongo(publishProductId, filters);
            return publishProduct;

        }

        /// <summary>
        /// This method only returns the brief details of a published product found in mongo.
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        public virtual PublishProductModel GetPublishProductBrief(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            //Get parameter values from filters.
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId);

            PublishProductModel publishProduct = GetPublishedProductFromMongo(publishProductId, filters);

            if (HelperUtility.IsNotNull(publishProduct))
            {
                List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(publishProductId, catalogVersionId);
                //Get associated configurable product list.
                List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(publishProductId, localeId, catalogVersionId, configEntiy);

                if (associatedProducts?.Count > 0)
                    publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy?.SelectMany(x => x.ConfigurableAttributeCodes).Distinct()?.ToList(), catalogVersionId.GetValueOrDefault());
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault());

                GetProductImagePath(portalId, publishProduct, false);

                SetPortalBasedDetails(portalId, publishProduct);
            }
            return publishProduct;
        }

        /// <summary>
        /// This method only returns the extended details of a published product found in mongo based on the supplied expands.
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands">Pass appropriate expands to get the corresponding detail in response.</param>
        /// <returns></returns>
        public virtual PublishProductModel GetExtendedPublishProductDetails(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            //Get parameter values from filters.
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId);

            PublishProductModel publishProduct = GetPublishedProductFromMongo(publishProductId, filters);

            if (HelperUtility.IsNotNull(publishProduct))
            {
                if (ContainsExpandables(expands))
                {
                    List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(publishProduct.PublishProductId, catalogVersionId);
                    //Get associated configurable product list.
                    List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(publishProduct.PublishProductId, localeId, catalogVersionId, configEntiy);

                    if (associatedProducts?.Count > 0)
                        publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy?.SelectMany(x => x.ConfigurableAttributeCodes).Distinct()?.ToList(), catalogVersionId.GetValueOrDefault());
                    else
                        //Get expands associated to Product
                        publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault());
                }

                if (expands.AllKeys.Contains(ZnodeConstant.ProductImage.ToLowerInvariant()))
                    GetProductImagePath(portalId, publishProduct, true);
            }

            return publishProduct;
        }

        //set stored based In Stock, Out Of Stock, Back Order Message.
        protected void SetPortalBasedDetails(int portalId, PublishProductModel publishProduct)
        {
            ZnodePortal portalDetails = GetPortalDetailsById(portalId);
            if (HelperUtility.IsNotNull(portalDetails) && HelperUtility.IsNotNull(publishProduct))
            {
                publishProduct.InStockMessage = portalDetails.InStockMsg;
                publishProduct.OutOfStockMessage = portalDetails.OutOfStockMsg;
                publishProduct.BackOrderMessage = portalDetails.BackOrderMsg;
            }
        }

        //Get product by product sku.
        public virtual PublishProductModel GetPublishProductBySKU(ParameterProductModel model, NameValueCollection expands, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId;

            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            string configurableProductSKU = model.ParentProductSKU;
            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId, localeId);
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { configurableProductSKU = configurableProductSKU, catalogVersionId = catalogVersionId });
            //Get publish product from mongo
            PublishProductModel publishProduct = publishProductHelper.GetPublishProductBySKU(model.SKU, catalogId, localeId, catalogVersionId)?.ToModel<PublishProductModel>();

            if (model.ParentProductId > 0 && !string.IsNullOrEmpty(configurableProductSKU) && HelperUtility.IsNotNull(publishProduct))
            {
                publishProduct.ConfigurableProductSKU = model.SKU;
                publishProduct.ConfigurableProductId = model.ParentProductId;
                publishProduct.SKU = configurableProductSKU;
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(publishProduct.PublishProductId, catalogVersionId);
                //Get associated configurable product list.
                List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(publishProduct.PublishProductId, localeId, catalogVersionId, configEntiy);
                ZnodeLogging.LogMessage("List counts:", string.Empty, TraceLevel.Verbose, new { configEntiyCount = configEntiy?.Count(), associatedProductsCount = associatedProducts?.Count() });
                //If associatedProducts count is greater, get default configurable product.
                if (associatedProducts?.Count > 0)
                    publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.FirstOrDefault().ConfigurableAttributeCodes);
            }
            ZnodeLogging.LogMessage("Parameter for GetProductImagePath:", string.Empty, TraceLevel.Verbose, new object[] { portalId, publishProduct });
            GetProductImagePath(portalId, publishProduct);
            //set stored based In Stock, Out Of Stock, Back Order Message.
            SetPortalBasedDetails(portalId, publishProduct);

            //Get expands associated to Product
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, "", GetLoginUserId(), null, null, GetProfileId());
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProduct;
        }


        public virtual PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            string productType;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);
            productType = filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.ProductType, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
            ZnodeLogging.LogMessage("productType:", string.Empty, TraceLevel.Verbose, new object[] { productType });
            if (!string.IsNullOrEmpty(productType))
            {
                filters.RemoveAll(x => x.Item1 == FilterKeys.ProductType);
                filters.Add(FilterKeys.AttributeValueForProductType, FilterOperators.Contains, productType);
            }

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

            int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new object[] { versionId = versionId, catalogId = catalogId });
            //get catalog current version id by catalog id.
            if (catalogId > 0)
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(versionId));
            else
            {
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, filters.Exists(x => x.Item1 == Utilities.FilterKeys.RevisionType) ? GetCatalogAllVersionIds(localeId) : GetCatalogAllVersionIds());

                //Get comma separated ids of the catalogs which are associated to the store(s).
                string activeCatalogIds = string.Join(",", (from portalCatalog in _portalCatalogRepository.Table
                                                            join publishCatalog in _publishCatalogRepository.Table on portalCatalog.PublishCatalogId equals publishCatalog.PublishCatalogId
                                                            select portalCatalog.PublishCatalogId).Distinct().ToArray());
                ZnodeLogging.LogMessage("activeCatalogIds:", string.Empty, TraceLevel.Verbose, new object[] { activeCatalogIds });
                filters.Add(FilterKeys.CatalogId, FilterOperators.In, activeCatalogIds);
            }
            if (filters.Exists(x => x.Item1 == FilterKeys.RevisionType))
                filters.RemoveAll(x => x.Item1 == FilterKeys.RevisionType);
            if (userId > 0 && EnableProfileBasedSearch(portalId))
            {
                int? userProfileId = GetUserProfileId(userId);
                ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { userProfileId = userProfileId, userId = userId });
                if (!Equals(userProfileId, null))
                    filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
            }
            //Get filter value
            string filterValue = filters.FirstOrDefault(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In)?.FilterValue;
            ZnodeLogging.LogMessage("filterValue:", string.Empty, TraceLevel.Verbose, new object[] { filterValue });
            if (!string.IsNullOrEmpty(filterValue))
            {
                //Remove Payment Setting Filters with IN operator from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In);

                //Add Payment Setting Filters
                filters.Add(FilterKeys.AttributeValuesForPromotion, FilterOperators.In, filterValue.Replace('_', ','));
            }

            if (filters.Any(w => w.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower()))
            {
                //Remove ZnodeCategoryId Filters from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower());
                filters.Add(FilterKeys.ZnodeCategoryId, FilterOperators.In, GetActiveCategoryIds(catalogId));
            }

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            //Check if products are taken for some specific category.
            SetProductIndexFilter(filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to publishProducts list :", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
            //get publish products from mongo
            List<ProductEntity> publishProducts = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProducts list count:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, publishProducts?.Count());
            PublishProductListModel publishProductListModel = new PublishProductListModel() { PublishProducts = publishProducts.ToModel<PublishProductModel>().ToList() };

            GetExpands(portalId, localeId, expands, publishProductListModel, versionId);

            //Map pagination parameters
            publishProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }

        //
        public virtual PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page, ParameterKeyModel parameters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            PublishProductListModel publishProductListModel = new PublishProductListModel();

            switch (parameters.ParameterKey)
            {
                case ZnodeConstant.MongoId:
                    int catalogId, portalId, localeId;
                    GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

                    ObjectId[] objectIDs = parameters.Ids.Split(',').Select(objectId => ObjectId.Parse(objectId)).ToArray();
                    ZnodeLogging.LogMessage("objectIDs count:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, objectIDs?.Count());
                    //Mongo product query by Object Id.
                    IMongoRepository<ProductEntity> _productMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
                    publishProductListModel.PublishProducts = new List<PublishProductModel>();
                    publishProductListModel.PublishProducts = _productMongoRepository.Table.MongoCollection.Find(Query<ProductEntity>.In(pr => pr.Id, objectIDs))?.ToModel<PublishProductModel>()?.ToList();

                    GetExpands(portalId, localeId, expands, publishProductListModel);
                    //get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());
                    if (portalId > 0)
                    {
                        ZnodeLogging.LogMessage("portalId:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, portalId);
                        SetProductDetailsForList(portalId, publishProductListModel);
                    }
                    break;
                case ZnodeConstant.ZnodeCategoryIds:
                    filters.Add(new FilterTuple(FilterKeys.ZnodeCategoryIds, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
                case ZnodeConstant.ProductSKU:
                    filters.Add(new FilterTuple(FilterKeys.SKU, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
                default:
                    filters.Add(new FilterTuple(FilterKeys.ZnodeProductId, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }


        /// <summary>
        /// To get active products for recent viewed products
        /// </summary>
        /// <param name="parentIds"></param>
        /// <param name="catalogId"></param>
        /// <param name="localeId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public List<RecentViewProductModel> GetActiveProducts(List<int> parentIds, int catalogId, int localeId, int versionId)
        {         
            try
            {
                var query = new List<IMongoQuery>
                {
                    Query.And(Query<ProductEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                    Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId),
                                    Query<ProductEntity>.EQ(pr => pr.VersionId, GetCatalogVersionId()), Query<ProductEntity>.EQ(pr => pr.ProductIndex, 1)),

                    Query<ProductEntity>.EQ(pr => pr.IsActive, true),
                    Query<ProductEntity>.In(pr => pr.ZnodeProductId, parentIds)
                };

                List<ProductEntity> productList = _ProductMongoRepository.GetEntityList(Query.And(query));
                if (HelperUtility.IsNotNull(productList) && productList.Count > 0)
                {
                   return (from p in productList
                            where p.ZnodeCatalogId!=0
                            select new RecentViewProductModel
                            {
                                IsActive = p.IsActive,
                                ZnodeProductId = p.ZnodeProductId,
                                ZnodeCatalogId = p.ZnodeCatalogId
                            }).ToList();
                }

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"An exception occurred while fetching recent viewed products for catalogId: {catalogId}", ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error, ex);
            }
            return null;
        }


        //Get product price and inventory.
        public virtual ProductInventoryPriceListModel GetProductPriceAndInventory(ParameterInventoryPriceModel parameters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            List<InventorySKUModel> inventory = publishProductHelper.GetInventoryBySKUs(parameters.Parameter?.Split(',')?.ToList(), parameters.PortalId);
            List<PriceSKUModel> priceSKU = publishProductHelper.GetPricingBySKUs(parameters.Parameter?.Split(',')?.ToList(), parameters.PortalId, GetLoginUserId(), GetProfileId());
            ZnodeLogging.LogMessage("list count:", string.Empty, TraceLevel.Verbose, new { priceSKUCount = priceSKU?.Count(), inventoryCount = inventory?.Count() });
            ProductInventoryPriceListModel list = new ProductInventoryPriceListModel { ProductList = new List<ProductInventoryPriceModel>() };

            if (inventory?.Count > 0 && priceSKU?.Count > 0)
            {
                foreach (var sku in parameters.Parameter.Split(',')?.ToList())
                    list.ProductList.Add(GetProductInventoryModel(inventory, priceSKU, sku));
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return list;
        }

        //get assocaited products.
        public virtual WebStoreBundleProductListModel GetBundleProducts(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            IMongoRepository<BundleProductEntity> _bundleProductMongoRepository = new MongoRepository<BundleProductEntity>();
            WebStoreBundleProductListModel model = new WebStoreBundleProductListModel();
            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            string localeId = filters.Find(x => x.FilterName == ZnodeLocaleEnum.LocaleId.ToString())?.Item3;
            ZnodeLogging.LogMessage("localeId:", string.Empty, TraceLevel.Info, localeId);
            filters.RemoveAll(x => x.FilterName == ZnodeLocaleEnum.LocaleId.ToString());

            //get bundle product entity.
            List<BundleProductEntity> associatedZnodeProductId = _bundleProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));
            ZnodeLogging.LogMessage("associatedZnodeProductId list count:", string.Empty, TraceLevel.Verbose, associatedZnodeProductId?.Count());
            if (HelperUtility.IsNotNull(associatedZnodeProductId))
            {
                filters.Clear();

                //Associated product ids.
                int[] associatedProducts = associatedZnodeProductId.Select(x => x.AssociatedZnodeProductId).ToArray();
                filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, string.Join(",", associatedProducts));
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId);
                filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);

                //get associated product list.
                List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));
                ZnodeLogging.LogMessage("products list count:", string.Empty, TraceLevel.Verbose, products?.Count());
                products = products.GroupBy(x => x.ZnodeProductId).Select(grp => grp.First()).ToList();
                products?.ForEach(x => x.AssociatedProductDisplayOrder = associatedZnodeProductId.FirstOrDefault(y => y.AssociatedZnodeProductId == x.ZnodeProductId).AssociatedProductDisplayOrder);
                model.BundleProducts = products?.ToModel<WebStoreBundleProductModel>().ToList();
                if (PortalId > 0)
                {
                    string ImageName = string.Empty;
                    IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", PortalId));
                    //Get image path for associated products.
                    model.BundleProducts.ForEach(
                        x =>
                        {
                            ImageName = x.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                            x.ImageSmallPath = image.GetImageHttpPathSmall(ImageName);
                            x.ImageMediumPath = image.GetImageHttpPathMedium(ImageName);
                            x.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ImageName);
                            x.ImageSmallThumbnailPath = image.GetImageHttpPathSmallThumbnail(ImageName);
                        });
                }
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return model;
        }

        //get assocaited products.
        public virtual WebStoreGroupProductListModel GetGroupProducts(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            WebStoreGroupProductListModel model = new WebStoreGroupProductListModel();
            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            int portalId = Convert.ToInt32(filters.Find(x => string.Equals(x.FilterName, FilterKeys.PortalId, StringComparison.CurrentCultureIgnoreCase))?.Item3);

            string localeId = filters.Find(x => string.Equals(x.FilterName, ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3;

            string publishCatalogId = filters.Find(x => string.Equals(x.FilterName, FilterKeys.PublishCatalogId, StringComparison.CurrentCultureIgnoreCase))?.Item3;
            filters.RemoveAll(x => string.Equals(x.FilterName, ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.CurrentCultureIgnoreCase));
            filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.PortalId, StringComparison.CurrentCultureIgnoreCase));
            filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.VersionId, StringComparison.CurrentCultureIgnoreCase));
            filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.PublishCatalogId, StringComparison.CurrentCultureIgnoreCase));
            int? versionIds = GetCatalogVersionId(Convert.ToInt32(publishCatalogId), Convert.ToInt32(localeId));
            filters.Add(FilterKeys.VersionId, FilterOperators.Equals, versionIds.HasValue ? versionIds.Value.ToString() : "0");

            //get group product entity.
            model.GroupProducts = GetGroupProductList(filters, portalId, localeId, true);
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Info, new { publishCatalogId = publishCatalogId, localeId = localeId, portalId = portalId, versionIds = versionIds, GroupProducts = model.GroupProducts });

            if (portalId > 0)
            {
                string ImageName = string.Empty;
                IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", PortalId));
                ZnodePortal portalDetails = GetPortalDetailsById(portalId);

                //Get image path for associated products.
                model.GroupProducts.ForEach(
                    x =>
                    {
                        ImageName = x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                        x.ImageSmallPath = image.GetImageHttpPathSmall(ImageName);
                        x.ImageMediumPath = image.GetImageHttpPathMedium(ImageName);
                        x.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ImageName);
                        x.ImageSmallThumbnailPath = image.GetImageHttpPathSmallThumbnail(ImageName);
                        x.InStockMessage = portalDetails?.InStockMsg;
                        x.OutOfStockMessage = portalDetails?.OutOfStockMsg;
                        x.BackOrderMessage = portalDetails?.BackOrderMsg;
                    });
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return model;
        }

        //Get Product Attribute
        public virtual ConfigurableAttributeListModel GetProductAttribute(int productId, ParameterProductModel model)
        {
            if (productId > 0)
            {
                int? versionId = GetCatalogVersionId(model.PublishCatalogId);
                List<IMongoQuery> query = new List<IMongoQuery>();
                query.Add(Query<ProductEntity>.EQ(d => d.ZnodeProductId, model.ParentProductId));
                query.Add(Query<CategoryEntity>.EQ(d => d.IsActive, true));
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, versionId));

                ProductEntity parentProduct = _ProductMongoRepository.GetEntity(Query.And(query));

                //Selecting child SKU
                IEnumerable<string> childSKU = parentProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();

                //Creating new query
                List<ProductEntity> products = GetAssociatedConfigurableProducts(model.LocaleId, versionId, childSKU);

                ConfigurableProductEntity configEntiy = GetConfigurableProductEntity(model.ParentProductId, versionId);

                return new ConfigurableAttributeListModel()
                {
                    //Get associated configurable product Attribute list.
                    Attributes = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(products, configEntiy?.ConfigurableAttributeCodes), model.SelectedCode, model.SelectedValue, model.SelectedAttributes, products, configEntiy?.ConfigurableAttributeCodes, model.PortalId),
                };
            }
            return new ConfigurableAttributeListModel();
        }

        //Get Configurable Product
        public virtual PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            PublishProductModel product = null;
            //new implementaion
            int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ProductEntity>.EQ(d => d.ZnodeProductId, productAttributes.ParentProductId));
            query.Add(Query<CategoryEntity>.EQ(d => d.IsActive, true));
            query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, versionId));

            ProductEntity parentProduct = _ProductMongoRepository.GetEntity(Query.And(query));

            //Selecting child SKU
            IEnumerable<string> childSKU = parentProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();

            //Creating new query
            List<ProductEntity> productList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);

            foreach (var item in productAttributes.SelectedAttributes)
            {

                productList = (from productentity in productList
                               from attribute in productentity.Attributes
                               where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                               select productentity).ToList();
            }

            ProductEntity productEntity = productList.FirstOrDefault();

            //If Combination does not exist.
            if (HelperUtility.IsNull(productEntity))
            {
                //Creating new query
                List<ProductEntity> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
                product = newproductList?.FirstOrDefault().ToModel<PublishProductModel>();
                product.IsDefaultConfigurableProduct = true;
            }
            else
                product = productEntity?.ToModel<PublishProductModel>();


            if (HelperUtility.IsNotNull(product))
            {
                bool isChildPersonalizableAttribute = product.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                var parentPersonalizableAttributes = parentProduct.Attributes?.Where(x => x.IsPersonalizable);
                product.AssociatedGroupProducts = productList.ToModel<WebStoreGroupProductModel>().ToList();

                product.ConfigurableProductId = productAttributes.ParentProductId;
                product.IsConfigurableProduct = true;

                product.ProductType = ZnodeConstant.ConfigurableProduct;
                product.ConfigurableProductSKU = product.SKU;
                product.SKU = productAttributes.ParentProductSKU;

                publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());

                GetProductImagePath(productAttributes.PortalId, product);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(productAttributes.PortalId, product);

                if (!isChildPersonalizableAttribute && parentPersonalizableAttributes?.Count() > 0)
                    product.Attributes.AddRange(parentPersonalizableAttributes.ToModel<PublishAttributeModel>().ToList());
            }
            return product;
        }

        //Get publish Product from mongo excluding assigned Ids.
        public virtual PublishProductListModel GetUnAssignedPublishProductList(ParameterModel assignedIds, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            filters.Add(Utilities.FilterKeys.ZnodeProductId, FilterOperators.NotIn, assignedIds.Ids);
            return GetPublishProductList(expands, filters, sorts, page);
        }

        //Bind Associated products.
        public virtual void BindAssociatedProductDetails(ProductInventoryPriceModel productModel, int productId, int publishedCatalogId, int portalId, int localeId, string productType)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter productId, portalId,publishedCatalogId,localeId:", string.Empty, TraceLevel.Info, new object[] { productId, portalId, publishedCatalogId, localeId });
            //If main product type is GroupedProduct then get its associated products.            
            if (Equals(productType, ZnodeConstant.GroupedProduct))
                GetAssociatedGroupProductPrice(productModel, productId, portalId, localeId);
            //If main product type is ConfigurableProduct then get first product form associated products and assign price.
            else if (Equals(productType, ZnodeConstant.ConfigurableProduct) && (HelperUtility.IsNull(productModel.SalesPrice) && HelperUtility.IsNull(productModel.RetailPrice)))
            {
                PublishProductModel product = GetAssociatedConfigurableProduct(productId, localeId, GetCatalogVersionId(publishedCatalogId), portalId);
                if (HelperUtility.IsNotNull(product))
                {
                    productModel.SalesPrice = product.SalesPrice;
                    productModel.RetailPrice = product.RetailPrice;
                    productModel.CurrencyCode = product.CurrencyCode;
                    productModel.CultureCode = product.CultureCode;
                }
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
        }

        //Get price for products through ajax async call. 
        public virtual ProductInventoryPriceListModel GetProductPrice(ParameterInventoryPriceModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            if (HelperUtility.IsNotNull(parameter))
            {
                switch (parameter.ProductType)
                {
                    //Get price for group products.
                    case ZnodeConstant.GroupedProduct:
                        PageListModel pageListModel = GetPageModel(parameter.Parameter, parameter.CatalogId);
                        ZnodeLogging.LogMessage("pageListModel to product list: ", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
                        int total = 0;
                        List<ProductEntity> productList = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, 1, 10, out total);
                        ZnodeLogging.LogMessage("productList list count:", string.Empty, TraceLevel.Verbose, productList?.Count());
                        if (productList?.Count > 0)
                            return GetAsyncGroupProductPrice(productList, parameter.LocaleId, parameter.PortalId);
                        break;
                    //Get price for configurable products.
                    case ZnodeConstant.ConfigurableProduct:
                        PageListModel pageModel = GetPageModel(parameter.Parameter, parameter.CatalogId);
                        ZnodeLogging.LogMessage("pageListModel to product list :", string.Empty, TraceLevel.Verbose, pageModel.ToDebugString());
                        int totalRow = 0;
                        List<ProductEntity> products = publishProductHelper.GetPublishProductList(pageModel.MongoWhereClause, pageModel.MongoOrderBy, 0, 0, out totalRow);
                        ZnodeLogging.LogMessage("products list count:", string.Empty, TraceLevel.Verbose, products?.Count());
                        if (products?.Count > 0)
                            return GetAsyncConfigurableProductPrice(products, parameter.CatalogId, parameter.LocaleId, parameter.PortalId);
                        break;
                    //Get price for Simple and Bundle products.
                    default:
                        return new ProductInventoryPriceListModel { ProductList = MapProductPrice(publishProductHelper.GetPricingBySKUs(parameter.Parameter.Split(','), PortalId, GetLoginUserId(), GetProfileId())) };
                }
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return new ProductInventoryPriceListModel();
        }

        //Get message for group product.
        public virtual string GetGroupProductMessage(List<WebStoreGroupProductModel> list, ProductInventoryPriceModel product)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            List<decimal?> priceList = new List<decimal?>();
            //Add sales price and retail price in list.
            priceList.AddRange(list.Where(x => x.SalesPrice != null)?.Select(y => y.SalesPrice));
            priceList.AddRange(list.Where(x => x.RetailPrice != null)?.Select(y => y.RetailPrice));
            //Order list in asending order.
            decimal? price = priceList.OrderBy(x => x.Value).FirstOrDefault();
            //Currency code for price format.
            string cultureCode = list.FirstOrDefault(x => x.SalesPrice == price || x.RetailPrice == price)?.CultureCode;
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { price = price, cultureCode = cultureCode });
            product.ProductPrice = price;
            if (price > 0)
                return string.Format(WebStore_Resources.GroupProductMessage, ServiceHelper.FormatPriceWithCurrency(price, cultureCode));
            else
                return string.Empty;
        }

        //Get publish products from mongo
        public virtual PublishProductListModel GetPublishProductForSiteMap(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);
            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);

            sorts.Add(ZnodeConstant.ZnodeCategoryIds, "asc");

            //added expand for SEO URL
            expands.Add(ZnodeConstant.SEO.ToString().ToLower(), ZnodeConstant.SEO.ToString().ToLower());

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to publishProducts list :", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
            string ZnodeCategoryIds = GetActiveCategoryIdsForSiteMap(catalogId, pageListModel.PagingStart);
            ZnodeLogging.LogMessage("ZnodeCategoryIds:", string.Empty, TraceLevel.Verbose, ZnodeCategoryIds);
            filters.Add(ZnodeConstant.ZnodeCategoryIds, FilterOperators.In, ZnodeCategoryIds);

            //get publish products from mongo
            List<ProductEntity> publishProducts = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, 1, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProducts list count:", string.Empty, TraceLevel.Verbose, publishProducts?.Count());
            PublishProductListModel publishProductListModel = new PublishProductListModel() { PublishProducts = new List<PublishProductModel>() };

            if (publishProducts.Count > 0)
            {
                foreach (ProductEntity publishProductModel in publishProducts)
                    //Gets the produsts which having attributecode as hidden false.
                    if (publishProductModel?.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.HiddenAttribute)?.AttributeValues != ZnodeConstant.TrueValue)
                        publishProductListModel.PublishProducts.Add(publishProductModel.ToModel<PublishProductModel>());

                if (publishProductListModel?.PublishProducts?.Count > 0)
                    //get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());
            }
            publishProductListModel.PublishProducts = publishProductListModel.PublishProducts.Where(x => (x.SEOTitle != null) || (x.SEOUrl != null) || (x.SEOKeywords != null) || (x.SEODescription != null)).ToList();           
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }

        //Get Publish Product Count.
        public virtual int GetPublishProductCount(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            PageListModel pageListModel = new PageListModel(filters, new NameValueCollection(), new NameValueCollection());
            ZnodeLogging.LogMessage("pageListModel to publishProducts list: ", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
            //get publish products from mongo
            List<ProductEntity> publishProducts = _ProductMongoRepository.GetPagedList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, 1, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProducts list count:", string.Empty, TraceLevel.Verbose, publishProducts?.Count());
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return pageListModel.TotalRowCount;
        }

        public virtual void GetExpands(int portalId, int localeId, NameValueCollection expands, PublishProductListModel publishProductListModel, int cataLogVersionId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", string.Empty, TraceLevel.Info, new { portalId = portalId, localeId = localeId });
            //Get required input parameters to get the data of products.
            DataTable productDetails = GetProductFiltersForSP(publishProductListModel.PublishProducts);
            //get expands associated to Product
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), cataLogVersionId, GetProfileId());
            GetRequiredProductDetails(publishProductListModel, productDetails, localeId, GetLoginUserId(), portalId);
        }


        public string ValueFromSelectValue(List<PublishAttributeModel> attributes, string attributeCode)
  => attributes?.FirstOrDefault(x => x.AttributeCode == attributeCode)?.SelectValues?.FirstOrDefault()?.Code;

        public DataTable GetProductFiltersForSP(List<PublishProductModel> products)
        {
            ZnodeLogging.LogMessage("Execution Started:", string.Empty, TraceLevel.Info);
            DataTable table = new DataTable("ProductTable");
            DataColumn productId = new DataColumn("Id");
            productId.DataType = typeof(int);
            productId.AllowDBNull = false;
            table.Columns.Add(productId);
            table.Columns.Add("ProductType", typeof(string));
            table.Columns.Add("OutOfStockOptions", typeof(string));
            table.Columns.Add("SKU", typeof(string));

            foreach (PublishProductModel item in products)
                table.Rows.Add(item.PublishProductId, ValueFromSelectValue(item.Attributes, ZnodeConstant.ProductType), ValueFromSelectValue(item.Attributes, ZnodeConstant.OutOfStockOptions), item.SKU);

            return table;
        }

        //Get Mongo product list.
        public virtual PublishProductListModel GetMongoProductsList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            int versionId = GetCatalogVersionId(catalogId).GetValueOrDefault();
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Info, new { versionId = versionId });
            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(versionId));

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to publishProducts list :", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
            //get publish products from mongo
            List<ProductEntity> publishProducts = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProducts list count:", string.Empty, TraceLevel.Verbose, publishProducts?.Count());
            PublishProductListModel publishProductListModel = new PublishProductListModel() { PublishProducts = publishProducts.ToModel<PublishProductModel>().ToList() };
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }

        //Get details of category products.
        protected void GetRequiredProductDetails(PublishProductListModel publishProductListModel, DataTable tableDetails, int localeId, int userId = 0, int portalId = 0)
        {

            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter localeId:", string.Empty, TraceLevel.Info, new object[] { localeId });
            IZnodeViewRepository<PublishCategoryProductDetailModel> objStoredProc = new ZnodeViewRepository<PublishCategoryProductDetailModel>();
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@currentUtcDate", HelperUtility.GetDateTime().Date, ParameterDirection.Input, DbType.String);
            IList<PublishCategoryProductDetailModel> productDetails;

            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
            {
                objStoredProc.SetParameter("@ProductDetailsFromWebStore", tableDetails?.ToJson(), ParameterDirection.Input, DbType.String);
                //Gets the entity list according to where clause, order by clause and pagination
                productDetails = objStoredProc.ExecuteStoredProcedureList("Znode_GetProductInfoForWebStoreWithJSON @PortalId,@LocaleId,@UserId,@ProductDetailsFromWebStore,@currentUtcDate");
            }
            else
            {
                objStoredProc.SetTableValueParameter("@ProductDetailsFromWebStore", tableDetails, ParameterDirection.Input, SqlDbType.Structured, "dbo.ProductDetailsFromWebStore");
                //Gets the entity list according to where clause, order by clause and pagination
                productDetails = objStoredProc.ExecuteStoredProcedureList("Znode_GetProductInfoForWebStore @PortalId,@LocaleId,@UserId,@currentUtcDate,@ProductDetailsFromWebStore");
            }
            //Bind product details.
            BindProductDetails(publishProductListModel, portalId, productDetails);
        }

        //Bind product details.
        protected void BindProductDetails(PublishProductListModel publishProductListModel, int portalId, IList<PublishCategoryProductDetailModel> productDetails)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId:", string.Empty, TraceLevel.Info, new { portalId = portalId });
            IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            ZnodePortal portalDetails = GetPortalDetailsById(portalId);

            publishProductListModel?.PublishProducts?.ForEach(product =>
            {
                PublishCategoryProductDetailModel productSKU = productDetails?
                            .FirstOrDefault(productdata => productdata.SKU == product.SKU);

                if (HelperUtility.IsNotNull(productSKU))
                {
                    product.SalesPrice = productSKU.SalesPrice;
                    product.RetailPrice = productSKU.RetailPrice;
                    product.CurrencyCode = productSKU.CurrencyCode;
                    product.CultureCode = productSKU.CultureCode;
                    product.CurrencySuffix = productSKU.CurrencySuffix;
                    product.Quantity = productSKU.Quantity;
                    product.ReOrderLevel = productSKU.ReOrderLevel;
                    product.Rating = productSKU.Rating;
                    product.TotalReviews = productSKU.TotalReviews;
                    string ImageName = product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.ImageMediumPath = imageHelper.GetImageHttpPathMedium(ImageName);
                    product.ImageThumbNailPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.ImageSmallThumbnailPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.InStockMessage = portalDetails?.InStockMsg;
                    product.OutOfStockMessage = portalDetails?.OutOfStockMsg;
                    product.BackOrderMessage = portalDetails?.BackOrderMsg;
                    product.ProductType = product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Value;
                    product.IsActive = Convert.ToBoolean(product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.IsActive)?.AttributeValues);
                }
            });
        }

        /// <summary>
        /// This method only returns the details of a parent published product found in mongo.
        /// </summary>
        /// <param name="parentProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        public virtual PublishProductModel GetParentProduct(int parentProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, parentProductId);
            PublishProductModel publishProduct = null;
            int portalId, localeId;
            int? catalogVersionId;

            //Get publish product from mongo
            List<ProductEntity> products = GetPublishProductFromMongo(parentProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            ZnodeLogging.LogMessage("Get publish product from mongo:", string.Empty, TraceLevel.Verbose, products?.Count());

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
                foreach (ProductEntity product in products)
                {
                    publishProduct = product.ToModel<PublishProductModel>();
                }
            }
            ZnodeLogging.LogMessage("GetProductImagePath execution done.", string.Empty, TraceLevel.Info);
            return publishProduct;
        }
        #endregion

        #region protected Methods

        //Replace Filter Keys
        protected void ReplaceFilterKeys(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (string.Equals(tuple.Item1, FilterKeys.LocaleId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.LocaleId, FilterKeys.MongoLocaleId); }
                if (string.Equals(tuple.Item1, FilterKeys.ZnodeCatalogId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ZnodeCatalogId.ToLower(), FilterKeys.ZnodeCatalogId); }
                if (string.Equals(tuple.Item1, FilterKeys.Name, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.Name.ToLower(), FilterKeys.Name); }
                if (string.Equals(tuple.Item1, FilterKeys.ItemName, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ItemName.ToLower(), FilterKeys.Name); }
                if (string.Equals(tuple.Item1, FilterKeys.Sku, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.Sku, FilterKeys.SKU); }
                if (string.Equals(tuple.Item1, FilterKeys.CatalogId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.CatalogId.ToLower(), FilterKeys.ZnodeCatalogId); }
                if (string.Equals(tuple.Item1, FilterKeys.ZnodeCategoryId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ZnodeCategoryId.ToLower(), FilterKeys.ZnodeCategoryIds); }
                if (string.Equals(tuple.Item1, FilterKeys.ZnodeCategoryIds, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ZnodeCategoryIds.ToLower(), FilterKeys.ZnodeCategoryIds); }
                if (string.Equals(tuple.Item1, FilterKeys.ZnodeProductId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ZnodeProductId.ToLower(), FilterKeys.ZnodeProductId); }
                if (string.Equals(tuple.Item1, WebStoreEnum.ProfileIds.ToString(), StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, WebStoreEnum.ProfileIds.ToString().ToLower(), WebStoreEnum.ProfileIds.ToString()); }
                if (string.Equals(tuple.Item1, WebStoreEnum.IsActive.ToString(), StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, WebStoreEnum.IsActive.ToString().ToLower(), WebStoreEnum.IsActive.ToString()); }
                if (string.Equals(tuple.Item1, WebStoreEnum.VersionId.ToString(), StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, WebStoreEnum.VersionId.ToString().ToLower(), WebStoreEnum.VersionId.ToString()); }
                if (string.Equals(tuple.Item1, FilterKeys.AttributeCodeForPromotion, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.AttributeCodeForPromotion.ToLower(), FilterKeys.AttributeCodeForPromotion); }
                if (string.Equals(tuple.Item1, FilterKeys.AttributeValuesForPromotion, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.AttributeValuesForPromotion.ToLower(), FilterKeys.AttributeValuesForPromotion); }
                if (string.Equals(tuple.Item1, FilterKeys.ProductIndex, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ProductIndex.ToLower(), FilterKeys.ProductIndex); }
                if (string.Equals(tuple.Item1, FilterKeys.PublishProductAttributeValueForSelect, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.PublishProductAttributeValueForSelect.ToLower(), FilterKeys.PublishProductAttributeValueForSelect); }
                if (string.Equals(tuple.Item1, FilterKeys.MongoCatalogName, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.MongoCatalogName.ToLower(), FilterKeys.MongoCatalogName); }
                if (string.Equals(tuple.Item1, FilterKeys.AttributeValueForProductType, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.AttributeValueForProductType.ToLower(), FilterKeys.AttributeValueForProductType); }

            }
            ReplaceFilterKeysForOr(ref filters);
        }

        //Replace Filter Keys
        protected void ReplaceFilterKeysForOr(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (tuple.Item1.Contains("|"))
                {
                    List<string> newValues = new List<string>();
                    foreach (var item in tuple.Item1.Split('|'))
                    {
                        if (string.Equals(item, FilterKeys.ItemName, StringComparison.OrdinalIgnoreCase)) { newValues.Add(FilterKeys.Name); }
                        else if (string.Equals(item, Utilities.FilterKeys.MongoCatalogName, StringComparison.OrdinalIgnoreCase)) { newValues.Add(Utilities.FilterKeys.MongoCatalogName); }
                        else if (string.Equals(item, FilterKeys.Sku, StringComparison.OrdinalIgnoreCase)) { newValues.Add(FilterKeys.SKU); }
                        else if (string.Equals(item, FilterKeys.Name, StringComparison.OrdinalIgnoreCase)) { newValues.Add(FilterKeys.Name); }
                        else if (string.Equals(item, FilterKeys.ProductType, StringComparison.OrdinalIgnoreCase)) { newValues.Add(FilterKeys.AttributeValueForProductType); }
                        else newValues.Add(item);
                    }
                    ReplaceFilterKeyName(ref filters, tuple.Item1, string.Join("|", newValues));
                }
            }
        }

        //Replace sort Keys
        protected void ReplaceSortKeys(ref NameValueCollection sorts)
        {
            foreach (string key in sorts.Keys)
            {
                if (string.Equals(key, FilterKeys.PublishProductId, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.PublishProductId.ToLower(), FilterKeys.ZnodeProductId); }
                if (string.Equals(key, FilterKeys.Name.ToLower(), StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.Name.ToLower(), FilterKeys.Name); }
                if (string.Equals(key, FilterKeys.Sku, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.Sku, FilterKeys.SKU); }
                if (string.Equals(key, FilterKeys.ItemName, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.ItemName, FilterKeys.Name); }
                if (string.Equals(key, FilterKeys.ItemId, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.ItemId, FilterKeys.ZnodeProductId); }
                if (string.Equals(key, FilterKeys.MongoCatalogName.ToLower(), StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.MongoCatalogName.ToLower(), FilterKeys.MongoCatalogName); }
            }
        }

        //Get expands and add them to navigation properties
        public virtual List<string> GetExpands(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (HelperUtility.IsNotNull(expands) && expands.HasKeys())
            {
                foreach (string key in expands.Keys)
                {
                    //check if expand key is present or not and add it to navigation properties.
                    switch (key)
                    {
                        case ZnodeConstant.Promotions:
                            SetExpands(ZnodeConstant.Promotions, navigationProperties);
                            break;
                        case ZnodeConstant.Inventory:
                            SetExpands(ZnodeConstant.Inventory, navigationProperties);
                            break;
                        case ZnodeConstant.ProductTemplate:
                            SetExpands(ZnodeConstant.ProductTemplate, navigationProperties);
                            break;
                        case ZnodeConstant.ProductReviews:
                            SetExpands(ZnodeConstant.ProductReviews, navigationProperties);
                            break;
                        case ZnodeConstant.Pricing:
                            SetExpands(ZnodeConstant.Pricing, navigationProperties);
                            break;
                        case ZnodeConstant.SEO:
                            SetExpands(ZnodeConstant.SEO, navigationProperties);
                            break;
                        case ZnodeConstant.AdminSEO:
                            SetExpands(ZnodeConstant.AdminSEO, navigationProperties);
                            break;
                        case ZnodeConstant.AddOns:
                            SetExpands(ZnodeConstant.AddOns, navigationProperties);
                            break;
                        case ZnodeConstant.ConfigurableAttribute:
                            SetExpands(ZnodeConstant.ConfigurableAttribute, navigationProperties);
                            break;
                        case ZnodeConstant.AssociatedProducts:
                            SetExpands(ZnodeConstant.AssociatedProducts, navigationProperties);
                            break;
                        case ZnodeConstant.WishlistAddOns:
                            SetExpands(ZnodeConstant.WishlistAddOns, navigationProperties);
                            break;
                        default:
                            if (Equals(key, ZnodeConstant.Brand.ToLower())) SetExpands(ZnodeConstant.Brand, navigationProperties);
                            break;
                    }
                }
            }
            return navigationProperties;
        }

        //get product invenoty model.
        protected virtual ProductInventoryPriceModel GetProductInventoryModel(List<InventorySKUModel> inventoryList, List<PriceSKUModel> priceSKU, string sku)
        => (from inventory in inventoryList
            join price in priceSKU on inventory.SKU equals price.SKU
            select new ProductInventoryPriceModel
            {
                SalesPrice = price?.SalesPrice,
                RetailPrice = price?.RetailPrice,
                ReOrderLevel = inventory?.ReOrderLevel,
                Quantity = inventory?.Quantity,
                CurrencyCode = price.CurrencyCode,
                CultureCode = price.CultureCode,
                SKU = sku
            }).FirstOrDefault();

        //Map Attributes values.        
        protected virtual List<PublishAttributeModel> MapWebStoreConfigurableAttributeData(List<List<AttributeEntity>> attributeList, string selectedCode, string selectedValue, Dictionary<string, string> SelectedAttributes, List<ProductEntity> products, List<string> ConfigurableAttributeCodes, int portalId)
        {
            List<PublishAttributeModel> configurableAttributeList = new List<PublishAttributeModel>();
            List<ConfigurableAttributeModel> attributesList = new List<ConfigurableAttributeModel>();
            IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            if (SelectedAttributes.Count <= 0 && !string.IsNullOrEmpty(attributeList.FirstOrDefault().FirstOrDefault().AttributeCode) && !string.IsNullOrEmpty(attributeList.FirstOrDefault().FirstOrDefault().AttributeValues))
                SelectedAttributes.Add(attributeList.FirstOrDefault().FirstOrDefault().AttributeCode, attributeList.FirstOrDefault().FirstOrDefault().AttributeValues);

            IEnumerable<AttributeEntity> attributes = ExitingAttributeList(selectedCode, selectedValue, products, SelectedAttributes);

            var matchAttributeList = attributes.GroupBy(w => w.AttributeCode).Select(g => new
            {
                AttributeCode = g.Key,
                AttributeValues = g.Select(c => c.AttributeValues)
            });

            foreach (List<AttributeEntity> attributeEntityList in attributeList)
            {
                attributesList.Clear();
                PublishAttributeModel attributesModel = new PublishAttributeModel();
                foreach (AttributeEntity attributeValue in attributeEntityList)
                {
                    //Check if attribute already exist in list.
                    if (!AlreadyExist(attributesList, attributeValue.AttributeValues).GetValueOrDefault())
                    {
                        IEnumerable<string> matchAttribute = matchAttributeList?.FirstOrDefault(x => x.AttributeCode == attributeValue.AttributeCode)?.AttributeValues;

                        ConfigurableAttributeModel attribute = new ConfigurableAttributeModel();

                        if (HelperUtility.IsNotNull(matchAttribute) && !matchAttribute.Contains(attributeValue.AttributeValues) && ConfigurableAttributeCodes?.Count != 1)
                            attribute.IsDisabled = true;

                        attribute.AttributeValue = attributeValue.AttributeValues;

                        //Assign the display order value of configurable type attributes.
                        attribute.SelectValues = attributeValue.SelectValues?.ToModel<AttributesSelectValuesModel>().ToList();

                        if (attribute.SelectValues?.Count > 0)
                        {
                            AttributesSelectValuesModel selectValues = attribute.SelectValues.FirstOrDefault();

                            if (string.Equals(attributeValue.IsSwatch, ZnodeConstant.TrueValue, StringComparison.InvariantCultureIgnoreCase))
                                attribute.ImagePath = image.GetImageHttpPathSmallThumbnail(selectValues.Path);

                            attribute.SwatchText = selectValues.SwatchText;
                            attribute.DisplayOrder = selectValues.DisplayOrder;
                        }
                        attributesList.Add(attribute);
                    }
                }

                AttributeEntity attributeEntity = attributeEntityList.FirstOrDefault();
                attributesModel.AttributeName = attributeEntity.AttributeName;
                attributesModel.AttributeCode = attributeEntity.AttributeCode;
                attributesModel.AttributeValues = attributeEntity.AttributeValues;
                attributesModel.IsConfigurable = attributeEntity.IsConfigurable;
                attributesModel.IsSwatch = attributeEntity.IsSwatch;
                attributesModel.SelectValues = attributeEntity.SelectValues?.ToModel<AttributesSelectValuesModel>().ToList();
                attributesModel.ConfigurableAttribute.AddRange(attributesList);
                configurableAttributeList.Add(attributesModel);
            }
            return configurableAttributeList;
        }

        //
        protected virtual IEnumerable<AttributeEntity> ExitingAttributeList(string selectedCode, string selectedValue, List<ProductEntity> products, Dictionary<string, string> SelectedAttributes)
        {
            List<ProductEntity> matchProductList = new List<ProductEntity>();

            foreach (ProductEntity product in products)
            {
                foreach (AttributeEntity attribute in product.Attributes)
                {
                    if (SelectedAttributes.Keys.Contains(attribute.AttributeCode) && SelectedAttributes.Values.Contains(attribute.AttributeValues))
                        matchProductList.Add(product);
                }
            }
            IEnumerable<AttributeEntity> Attributes = matchProductList.SelectMany(x => x.Attributes?.Where(y => y.IsConfigurable));
            return Attributes;
        }

        //Get attribute values and code.
        protected virtual Dictionary<string, string> GetAttributeValues(string codes, string values)
        {
            //Attribute Code And Value 
            string[] Codes = codes.Split(',');
            string[] Values = values.Split(',');
            Dictionary<string, string> SelectedAttributes = new Dictionary<string, string>();

            //Add code and value pair
            for (int i = 0; i < Codes.Length; i++)
                SelectedAttributes.Add(Codes[i], Values[i]);
            return SelectedAttributes;
        }

        //Get Attribute Vlaue alreaduy exist
        public virtual bool? AlreadyExist(List<ConfigurableAttributeModel> ConfigurableAttributeList, string value) =>
         ConfigurableAttributeList?.Any(x => x.AttributeValue == value);

        //Get Mongo Query For Configurable Product
        protected virtual List<IMongoQuery> GetMongoQueryForConfigurableProduct(ParameterProductModel productAttributes, FilterCollection filters)
        {
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            //Set query according code and value
            foreach (var attribute in productAttributes?.SelectedAttributes)
            {
                mongoQuery.Add(Query.EQ("Attributes.AttributeCode", BsonRegularExpression.Create(new Regex($"^{attribute.Key}$", RegexOptions.IgnoreCase))));
                mongoQuery.Add(Query.EQ("Attributes.AttributeValues", BsonRegularExpression.Create(new Regex($"^{attribute.Value}$", RegexOptions.IgnoreCase))));
                break;
            }
            mongoQuery.Add(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));

            ZnodeLogging.LogMessage("mongoQuery list count:", string.Empty, TraceLevel.Verbose, mongoQuery?.Count());
            return mongoQuery;
        }

        //Get configurable product filter.
        protected virtual FilterCollection GetConfigurableProductFilter(int localeId, List<ConfigurableProductEntity> configEntity)
        {
            ZnodeLogging.LogMessage("Input Parameter localeId:", string.Empty, TraceLevel.Info, new object[] { localeId });
            FilterCollection filters = new FilterCollection();
            //Associated product ids.
            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, string.Join(",", (configEntity?.Select(x => x.AssociatedZnodeProductId)?.ToArray())));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId.ToString());
            return filters;
        }

        //Get Product Image Path
        protected virtual void GetProductImagePath(int portalId, PublishProductModel publishProduct, bool includeAlternateImages = true, string parentProductImageName = "")
        {
            //Get Product Image Path
            if (portalId > 0 && HelperUtility.IsNotNull(publishProduct))
            {
                IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));
                string ProductImageName = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;

                publishProduct.ImageLargePath = image.GetImageHttpPathLarge(ProductImageName);
                publishProduct.ImageMediumPath = image.GetImageHttpPathMedium(ProductImageName);
                publishProduct.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ProductImageName);
                publishProduct.ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
                publishProduct.OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
                if (string.IsNullOrEmpty(publishProduct.ParentProductImageSmallPath) && !string.IsNullOrEmpty(parentProductImageName) && publishProduct.IsConfigurableProduct)
                    publishProduct.ParentProductImageSmallPath = image.GetImageHttpPathSmall(parentProductImageName);
                if (includeAlternateImages)
                    GetProductAlterNateImages(publishProduct, portalId, image);
            }
        }

        //Get Product AlterNateImages.
        protected virtual void GetProductAlterNateImages(PublishProductModel product, int portalId, IImageHelper imageHelper)
        {
            string alternateImages = product.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.AttributeValues;
            if (!string.IsNullOrEmpty(alternateImages))
            {
                product.AlternateImages = new List<ProductAlterNateImageModel>();
                var images = alternateImages.Split(',');
                foreach (string image in images)
                    product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image, ImageSmallPath = imageHelper.GetImageHttpPathSmall(image), ImageThumbNailPath = imageHelper.GetImageHttpPathThumbnail(image), OriginalImagePath = imageHelper.GetOriginalImagepath(image), ImageLargePath = imageHelper.GetImageHttpPathLarge(image) });
            }
        }

        //Get Portal and locale id from filter.
        protected virtual void GetLocaleAndPortalId(FilterCollection filters, out int localeId, out int portalId)
        {
            int.TryParse(filters.FirstOrDefault(x => x.FilterName == FilterKeys.LocaleId)?.FilterValue, out localeId);
            int.TryParse(filters.FirstOrDefault(x => x.FilterName == FilterKeys.PortalId)?.FilterValue, out portalId);
        }

        //Get Image Path For Category and set stored based In Stock, Out Of Stock, Back Order Message for List.
        protected virtual void SetProductDetailsForList(int portalId, PublishProductListModel publishProductListModel)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId:", string.Empty, TraceLevel.Info, new object[] { portalId });
            string ImageName = string.Empty;
            IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            if (portalId > 0)
            {
                ZnodePortal portalDetails = GetPortalDetailsById(portalId);

                //Get image path for products.
                publishProductListModel?.PublishProducts.ForEach(
                    x =>
                    {
                        ImageName = x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                        x.ImageSmallPath = image.GetImageHttpPathSmall(ImageName);
                        x.ImageMediumPath = image.GetImageHttpPathMedium(ImageName);
                        x.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ImageName);
                        x.ImageSmallThumbnailPath = image.GetImageHttpPathSmallThumbnail(ImageName);
                        x.InStockMessage = portalDetails?.InStockMsg;
                        x.OutOfStockMessage = portalDetails?.OutOfStockMsg;
                        x.BackOrderMessage = portalDetails?.BackOrderMsg;
                        x.PortalId = portalId;
                        x.ProductType = x.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Value;
                        x.IsActive = Convert.ToBoolean(x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.IsActive)?.FirstOrDefault()?.AttributeValues);
                    });
            }
            else
            {
                //Get image path for products.
                publishProductListModel?.PublishProducts.ForEach(
                    x =>
                    {
                        ImageName = x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                        x.ImageSmallPath = image.GetImageHttpPathSmall(ImageName);
                        x.ImageMediumPath = image.GetImageHttpPathMedium(ImageName);
                        x.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ImageName);
                        x.ImageSmallThumbnailPath = image.GetImageHttpPathSmallThumbnail(ImageName);
                        x.ProductType = x.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Value;
                        x.IsActive = Convert.ToBoolean(x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.IsActive)?.FirstOrDefault()?.AttributeValues);
                    });
            }
        }
        //Where Clause For PortalId.
        public virtual string WhereClauseForPortalId(int portalId)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId.ToString());
            return DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause;
        }

        //Get parameter values from filters.
        protected virtual void GetParametersValueForFilters(FilterCollection filters, out int catalogId, out int portalId, out int localeId)
        {
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out catalogId);
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);
        }

        //Get Default configurable product.
        protected virtual PublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<ProductEntity> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            //Parent Product Properties
            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;
            string parentProductImageSmallPath = publishProduct.ParentProductImageSmallPath;
            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;

            //assign values from select values into attribute values
            associatedProducts.ForEach(x => x.Attributes.Where(y => y.IsConfigurable).ToList()
            .ForEach(z => {
                z.AttributeValues = z.SelectValues.FirstOrDefault()?.Value;
            }));

            //Get first product from list of associated products 
            publishProduct = associatedProducts.FirstOrDefault().ToModel<PublishProductModel>();

            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;
            publishProduct.ParentProductImageSmallPath = parentProductImageSmallPath;
            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = catalogVersionId > 0 ? catalogVersionId : GetCatalogVersionId();

            //Get expands associated to Product.
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes.FirstOrDefault(x => x.IsConfigurable);

            List<PublishAttributeModel> variants = publishProduct.Attributes?.Where(x => x.IsConfigurable).ToList();

            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(associatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, associatedProducts, ConfigurableAttributeCodes, portalId);

            //foreach (PublishAttributeModel item in attributeList)
            publishProduct.Attributes.RemoveAll(x => attributeList.Select(y => y.AttributeCode).Contains(x.AttributeCode));

            publishProduct.Attributes.AddRange(attributeList);

            return publishProduct;
        }

        protected virtual Dictionary<string, string> GetSelectedAttributes(List<PublishAttributeModel> variants)
        {
            Dictionary<string, string> selectedAttribute = new Dictionary<string, string>();

            variants.ForEach(m =>
            {
                if (HelperUtility.IsNotNull(m) && !selectedAttribute.ContainsKey(m.AttributeCode))
                    selectedAttribute.Add(m.AttributeCode, m.AttributeValues);
            });
            return selectedAttribute;
        }

        //Gets Category hierarchy
        protected virtual List<PublishCategoryModel> GetProductCategory(string categoryIds, int localeId, int catalogId, List<SeoEntity> categorySeoDetails)
        {
            ZnodeLogging.LogMessage("Input Parameter categoryIds,localeId and catalogId:", string.Empty, TraceLevel.Info, new object[] { categoryIds, localeId, catalogId });
            FilterCollection categoryFilter = new FilterCollection() { new FilterTuple(WebStoreEnum.ZnodeCategoryId.ToString(), FilterOperators.In, categoryIds),
                                                                               new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId.ToString()),
                                                                               new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, catalogId.ToString())};

            List<CategoryEntity> categoryList = _categoryMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(categoryFilter.ToFilterMongoCollection())).ToList();
            ZnodeLogging.LogMessage("categoryList list count:", string.Empty, TraceLevel.Verbose, categoryList?.Count());
            return categoryList?.Select(x => GetElasticCategory(categorySeoDetails, x)).ToList();
        }

        //Maps category entity to Elatic category.
        protected virtual PublishCategoryModel GetElasticCategory(List<SeoEntity> categorySeoDetails, CategoryEntity parentCategory)
        {
            PublishCategoryModel category = new PublishCategoryModel();
            category.SEODetails = new SEODetailsModel();
            category.Name = parentCategory.Name;
            category.PublishCategoryId = parentCategory.ZnodeCategoryId;
            category.SEODetails.SEOUrl = categorySeoDetails.Where(seoDetail => seoDetail.SEOId == parentCategory.ZnodeCategoryId && seoDetail.LocaleId == parentCategory.LocaleId)?.FirstOrDefault()?.SEOUrl;
            category.ParentCategory = (parentCategory.ZnodeParentCategoryIds?.Count() > 0) ? GetParentCategories(parentCategory.ZnodeParentCategoryIds, categorySeoDetails) : null;

            return category;
        }

        //Gets parent category hierarchy.
        protected virtual List<PublishCategoryModel> GetParentCategories(int[] parentCategoryIds, List<SeoEntity> categorySeoDetails)
        {
            List<PublishCategoryModel> parentCategories = new List<PublishCategoryModel>();
            foreach (int categoryId in parentCategoryIds)
            {
                FilterCollection categoryFilter = new FilterCollection() { new FilterTuple(WebStoreEnum.ZnodeCategoryId.ToString(), FilterOperators.In, string.Join(",", categoryId)) };

                CategoryEntity parentCategory = _categoryMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(categoryFilter.ToFilterMongoCollection()));
                if (HelperUtility.IsNotNull(parentCategory))
                {
                    PublishCategoryModel elasticParentCategory = GetElasticCategory(categorySeoDetails, parentCategory);

                    parentCategories.Add(elasticParentCategory);
                }
            }
            ZnodeLogging.LogMessage("parentCategories list count:", string.Empty, TraceLevel.Verbose, parentCategories?.Count());
            return parentCategories;
        }

        //to check EnableProfileBasedSearch is true or false
        protected bool EnableProfileBasedSearch(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId:", string.Empty, TraceLevel.Info, new object[] { portalId });
            Dictionary<string, bool> portalFeatureValues = ZnodeConfigManager.GetSiteConfigFeatureValueList(portalId);
            return portalFeatureValues.ContainsKey(HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()) && portalFeatureValues[HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()];
        }

        //to get user profileid by userId
        protected int? GetUserProfileId(int userId)
        {
            ZnodeLogging.LogMessage("Input Parameter userId:", string.Empty, TraceLevel.Info, new object[] { userId });
            int? profileId = null;
            int? userCatalogId = (from acc in _userAccount.Table
                                  join user in _user.Table on acc.AccountId equals user.AccountId
                                  where user.UserId == userId
                                  select acc)?.FirstOrDefault()?.PublishCatalogId;
            if (Equals(userCatalogId, null))
                profileId = (_userProfile.Table.Where(x => x.UserId == userId && x.IsDefault == true)?.FirstOrDefault()?.ProfileId) ?? null;
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Info, new { profileId = profileId, userCatalogId = userCatalogId });
            return profileId;
        }

        //Get associated simple product price of group product.
        public virtual void GetAssociatedGroupProductPrice(ProductInventoryPriceModel productModel, int productId, int portalId, int localeId)
        {
            //Get associated simple products.
            List<WebStoreGroupProductModel> groupProductList = GetAssociatedGroupProducts(productId, portalId, localeId);
            ZnodeLogging.LogMessage("groupProductList list count:", string.Empty, TraceLevel.Verbose, groupProductList?.Count());
            productModel.GroupProductPriceMessage = GetGroupProductMessage(groupProductList, productModel);
        }

        //Get associated group products.
        protected virtual List<WebStoreGroupProductModel> GetAssociatedGroupProducts(int productId, int portalId, int localeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, productId.ToString());

            //get group product entity.
            return GetGroupProductList(filters, portalId, Convert.ToString(localeId));
        }

        //Get associated product list.
        public virtual PublishProductModel GetAssociatedConfigurableProduct(int productId, int localeId, int? catalogVersionId, int portalId)
        {

            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter localeId,productId and portalId:", string.Empty, TraceLevel.Info, new object[] { localeId, productId, portalId });
            //Get configurable product entity.
            List<ConfigurableProductEntity> configEntity = publishProductHelper.GetConfigurableProductEntity(productId, catalogVersionId);
            //Get associated product list.
            List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(productId, localeId, catalogVersionId, configEntity);
            ZnodeLogging.LogMessage("List count:", string.Empty, TraceLevel.Verbose, new { associatedProductsCount = associatedProducts?.Count(), configEntityCount = configEntity?.Count() });
            if (associatedProducts?.Count > 0)
            {
                //Get first product from list of associated products 
                PublishProductModel publishProduct = associatedProducts.FirstOrDefault().ToModel<PublishProductModel>();
                //Get expands associated to Product.
                publishProductHelper.GetProductPriceData(publishProduct, portalId, GetLoginUserId(), GetProfileId());
                return publishProduct;
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return new PublishProductModel();
        }

        public virtual List<WebStoreGroupProductModel> GetGroupProductList(FilterCollection filters, int portalId, string localeId, bool isInventory = false)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter localeId and portalId:", string.Empty, TraceLevel.Info, new object[] { localeId, portalId });
            List<WebStoreGroupProductModel> productsList = new List<WebStoreGroupProductModel>();

            IMongoRepository<GroupProductEntity> _groupProductMongoRepository = new MongoRepository<GroupProductEntity>(GetCatalogVersionId());

            List<GroupProductEntity> groupProducts = _groupProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
            ZnodeLogging.LogMessage("groupProducts list count:", string.Empty, TraceLevel.Verbose, groupProducts?.Count());
            if (HelperUtility.IsNotNull(groupProducts))
            {
                filters.Clear();

                //Associated product ids.
                filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, string.Join(",", groupProducts?.Select(x => x.AssociatedZnodeProductId)?.ToArray()));
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId);
                filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(groupProducts?.FirstOrDefault()?.VersionId));

                SetProductIndexFilter(filters);

                //get associated product list.
                List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
                ZnodeLogging.LogMessage("products list count:", string.Empty, TraceLevel.Verbose, products?.Count());
                products?.ForEach(x => x.AssociatedProductDisplayOrder = groupProducts.FirstOrDefault(y => y.AssociatedZnodeProductId == x.ZnodeProductId).AssociatedProductDisplayOrder);
                products = products?.GroupBy(x => x.ZnodeProductId).Select(y => y.First()).ToList();
                productsList = products?.ToModel<WebStoreGroupProductModel>().ToList();

                //Map price of simple products associated to group product.
                if (isInventory)
                    MapGroupProductPriceAndInventory(productsList, publishProductHelper.GetPricingBySKUs(productsList.Select(x => x.SKU), portalId, GetLoginUserId(), GetProfileId()), publishProductHelper.GetInventoryBySKUs(productsList.Select(x => x.SKU), portalId));
                else
                    MapGroupProductPrice(productsList, publishProductHelper.GetPricingBySKUs(productsList.Select(x => x.SKU), portalId, GetLoginUserId(), GetProfileId()));
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return productsList;
        }

        //Get Clipart Value from Xml.
        public string GetClipartValue(string decorationOption, string descendants, string element)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter decorationOption,descendants and element:", string.Empty, TraceLevel.Info, new object[] { decorationOption, descendants, element });
            if (!string.IsNullOrEmpty(decorationOption))
            {
                XDocument xml = XDocument.Parse($"<Root>{decorationOption}</Root>");
                var values = (from el in xml.Descendants(descendants) select new { el.Element(element)?.Value })?.FirstOrDefault();
                return (values != null) ? values.Value : string.Empty;
            }
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return decorationOption;
        }

        //Map Price and inventory of group products.
        protected virtual void MapGroupProductPriceAndInventory(List<WebStoreGroupProductModel> productsList, List<PriceSKUModel> priceList, List<InventorySKUModel> inventoryList)
        {
            if (priceList?.Count > 0)
            {
                ZnodeLogging.LogMessage("productsList list count:", string.Empty, TraceLevel.Verbose, productsList?.Count());
                MapGroupProductPrice(productsList, priceList);
                productsList.ForEach(product =>
                {
                    InventorySKUModel inventorySKU = inventoryList
                                 .Where(productdata => productdata.SKU == product.SKU)
                                 ?.FirstOrDefault();

                    if (HelperUtility.IsNotNull(inventorySKU))
                    {
                        product.Quantity = inventorySKU.Quantity;
                        product.ReOrderLevel = inventorySKU.ReOrderLevel;
                    }
                });
            }
        }

        //Map Price of group products.
        protected virtual void MapGroupProductPrice(List<WebStoreGroupProductModel> productsList, List<PriceSKUModel> priceList)
        {
            if (priceList?.Count > 0)
            {
                ZnodeLogging.LogMessage("productsList list count:", string.Empty, TraceLevel.Verbose, productsList?.Count());
                productsList.ForEach(product =>
                {
                    PriceSKUModel priceSKU = priceList
                                 .Where(productdata => productdata.SKU == product.SKU)
                                 ?.FirstOrDefault();


                    if (HelperUtility.IsNotNull(priceSKU))
                    {
                        product.SalesPrice = priceSKU.SalesPrice;
                        product.RetailPrice = priceSKU.RetailPrice;
                        product.CurrencyCode = priceSKU.CurrencyCode;
                        product.CultureCode = priceSKU.CultureCode;
                        product.CurrencySuffix = priceSKU.CurrencySuffix;
                    }

                });
            }
        }



        //Map Price of products.
        protected virtual List<ProductInventoryPriceModel> MapProductPrice(List<PriceSKUModel> productPriceList)
        {
            if (productPriceList?.Count > 0)
            {
                ZnodeLogging.LogMessage("productsList list count:", string.Empty, TraceLevel.Verbose, productPriceList?.Count());
                List<ProductInventoryPriceModel> productList = new List<ProductInventoryPriceModel>();

                foreach (PriceSKUModel productPrice in productPriceList)
                {
                    ProductInventoryPriceModel product = new ProductInventoryPriceModel();
                    product.SalesPrice = productPrice.SalesPrice;
                    product.RetailPrice = productPrice.RetailPrice;
                    product.SKU = productPrice.SKU;
                    product.CurrencyCode = productPrice.CurrencyCode;
                    product.CultureCode = productPrice.CultureCode;
                    productList.Add(product);
                }
                return productList;
            }
            return new List<ProductInventoryPriceModel>();
        }

        //Get paging model with filters.
        protected virtual PageListModel GetPageModel(string skus, int catalogId)
        {
            ZnodeLogging.LogMessage("Input Parameter skus and catalogId:", string.Empty, TraceLevel.Info, new object[] { skus, catalogId });
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeTaxClassSKUEnum.SKU.ToString(), FilterOperators.In, skus));
            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));
            return new PageListModel(filters, null, null);
        }

        //Get group product price.
        protected virtual ProductInventoryPriceListModel GetAsyncGroupProductPrice(List<ProductEntity> productList, int localeId, int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId and localeId:", string.Empty, TraceLevel.Info, new object[] { portalId, localeId });
            ProductInventoryPriceListModel groupProductList = new ProductInventoryPriceListModel { ProductList = new List<ProductInventoryPriceModel>() };
            foreach (ProductEntity product in productList)
            {
                ProductInventoryPriceModel productPrice = new ProductInventoryPriceModel();
                GetAssociatedGroupProductPrice(productPrice, product.ZnodeProductId, portalId, localeId);
                productPrice.SKU = product.SKU;
                groupProductList.ProductList.Add(productPrice);
            }

            return groupProductList;
        }

        //Get configurable product price.
        protected virtual ProductInventoryPriceListModel GetAsyncConfigurableProductPrice(List<ProductEntity> productList, int catalogId, int localeId, int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId,catalogId and localeId:", string.Empty, TraceLevel.Info, new object[] { portalId, catalogId, localeId });
            ProductInventoryPriceListModel configurableProductList = new ProductInventoryPriceListModel { ProductList = new List<ProductInventoryPriceModel>() };
            foreach (ProductEntity product in productList)
            {
                PublishProductModel productModel = product?.ToModel<PublishProductModel>();

                //Get price of configurable product.
                publishProductHelper.GetDataFromExpands(portalId, new List<string> { ZnodeConstant.Promotions, ZnodeConstant.Pricing }, productModel, localeId, "", GetLoginUserId(), GetCatalogVersionId(catalogId).GetValueOrDefault(), null, GetProfileId());

                //get associated products price if main configurable product don't have price  associated.
                if (HelperUtility.IsNull(productModel.SalesPrice) && HelperUtility.IsNull(productModel.RetailPrice))
                {
                    productModel = GetAssociatedConfigurableProduct(product.ZnodeProductId, localeId, GetCatalogVersionId(catalogId).GetValueOrDefault(), portalId);
                    productModel.SKU = product.SKU;
                    configurableProductList.ProductList.Add(productModel);
                }
                configurableProductList.ProductList.Add(productModel);
            }
            return configurableProductList;
        }

        //Get Portal Details By Id.
        public ZnodePortal GetPortalDetailsById(int portalId)
        {
            string cacheKey = $"PortalDeatails_{portalId}";
            ZnodePortal portalDeatails = Equals(HttpRuntime.Cache[cacheKey], null)
               ? GetPortalDetailsByIdFromDB(portalId, cacheKey)
               : ((ZnodePortal)HttpRuntime.Cache.Get(cacheKey));
            ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return portalDeatails;
        }

        protected virtual ZnodePortal GetPortalDetailsByIdFromDB(int portalId, string cacheKey)
        {
            ZnodePortal znodePortal = _portalRepository.GetById(portalId);
            if (HelperUtility.IsNotNull(znodePortal))
                HttpRuntime.Cache.Insert(cacheKey, znodePortal);
            return znodePortal;
        }

        //Get active category ids.
        protected string GetActiveCategoryIds(int catalogId)
        {
            ZnodeLogging.LogMessage("Input Parameter catalogId:", string.Empty, TraceLevel.Info, new object[] { catalogId });
            int verstionId = GetCatalogVersionId(catalogId).GetValueOrDefault();
            ZnodeLogging.LogMessage("verstionId:", string.Empty, TraceLevel.Info, new object[] { verstionId });
            return string.Join(",", _categoryMongoRepository.Table.MongoCollection.Find((Query.And(
                                                         Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                                         Query<CategoryEntity>.EQ(pr => pr.VersionId, verstionId),
                                                         Query<CategoryEntity>.EQ(pr => pr.IsActive, true))))?.Select(s => s.ZnodeCategoryId)?.Distinct()?.ToArray());
        }

        //Get active category ids.
        protected virtual string GetActiveCategoryIdsForSiteMap(int catalogId, int pageNo)
        {
            ZnodeLogging.LogMessage("Input Parameter catalogId and pageNo:", string.Empty, TraceLevel.Info, new object[] { catalogId, pageNo });
            string catgoryId = string.Join(",", _categoryMongoRepository.Table.MongoCollection.Find((Query.And(
                                                         Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                                         Query<CategoryEntity>.EQ(pr => pr.ZnodeParentCategoryIds, null),
                                                         Query<CategoryEntity>.EQ(pr => pr.VersionId, GetCatalogVersionId(catalogId).GetValueOrDefault()),
                                                         Query<CategoryEntity>.EQ(pr => pr.IsActive, true))))?.OrderBy(s => s.DisplayOrder).Select(s => s.ZnodeCategoryId)?.Distinct()?.Skip(pageNo - 1).Take(1));
            ZnodeLogging.LogMessage("catgoryId:", string.Empty, TraceLevel.Info, new object[] { catgoryId });
            return !string.IsNullOrEmpty(catgoryId) ? string.Join(",", catgoryId, GetActiveSubCategoryIdsForSiteMap(catalogId, Convert.ToInt32(catgoryId))) : string.Empty;
        }

        //Get active subcategory id.
        protected virtual string GetActiveSubCategoryIdsForSiteMap(int catalogId, int categoryId)
            => string.Join(",", _categoryMongoRepository.Table.MongoCollection.Find((Query.And(
                                                         Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                                         Query<CategoryEntity>.EQ(pr => pr.ZnodeParentCategoryIds, Convert.ToInt32(categoryId)),
                                                         Query<CategoryEntity>.EQ(pr => pr.VersionId, GetCatalogVersionId(catalogId).GetValueOrDefault()),
                                                         Query<CategoryEntity>.EQ(pr => pr.IsActive, true))))?.OrderBy(s => s.DisplayOrder).Select(s => s.ZnodeCategoryId)?.Distinct()?.ToArray());

        #endregion

        #region Private Methods

        /// <summary>
        /// This method returns product entity for supplied publishProductId from mongo db.
        /// Additionally, it also returns the category hierarchy for the returned product.
        /// Category hierarchy retrieval is merged with product retrieval since they both require traversing through the associated categories at least once. 
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        private PublishProductModel GetPublishedProductFromMongo(int publishProductId, FilterCollection filters)
        {
            ISEOService _seoService = ZnodeDependencyResolver.GetService<ISEOService>();
            //Get parameter values from filters.
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            PublishProductModel publishProduct = null;
            CategoryEntity associatedCategory = null;
            //Get publish product from mongo
            List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), false);

            if (HelperUtility.IsNotNull(products))
            {
                associatedCategory = GetActiveAssociatedCategory(products);
                if (HelperUtility.IsNotNull(associatedCategory))
                {
                    publishProduct = products?.FirstOrDefault(x => x.ZnodeCategoryIds == associatedCategory.ZnodeCategoryId).ToModel<PublishProductModel>();
                    publishProduct.CategoryHierarchy = GetProductCategory(associatedCategory.ZnodeCategoryId.ToString(), localeId, catalogId, _seoService.GetPublishSEOSettingList(ZnodeConstant.Category, portalId, localeId));
                }
            }

            return publishProduct;
        }

        /// <summary>
        /// This method returns an associated category from mongo db on the basis of following criteria:
        /// 1. A category has been associated to any of the products found in supplied product list.
        /// 2. A category has its flag "IsActive" set to "true".
        /// Single category selection is done on "First to appear" basis.
        /// </summary>
        /// <param name="products">Collection of products which have one category associated with each.</param>
        /// <returns></returns>
        private CategoryEntity GetActiveAssociatedCategory(List<ProductEntity> products)
        {
            List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds).ToList();
            IMongoQuery categoryQuery = Query.And(
                Query<CategoryEntity>.In(x => x.ZnodeCategoryId, categoryIds),
                Query<CategoryEntity>.EQ(x => x.IsActive, true));

            return _categoryMongoRepository.GetEntity(categoryQuery);
        }

        /// <summary>
        /// This method checks if the supplied collection of expands has one of the keys which can be expanded as a navigational property within the entity framework context.
        /// </summary>
        /// <param name="expands"></param>
        /// <returns>Returns true if the supplied collection has an expandable key.</returns>
        private bool ContainsExpandables(NameValueCollection expands)
        {
            string[] expandKeys = expands.AllKeys;
            string[] expandableKeys = new string[]
            {
                ZnodeConstant.Promotions.ToLowerInvariant(),
                ZnodeConstant.SEO.ToLowerInvariant(),
                ZnodeConstant.Inventory.ToLowerInvariant(),
                ZnodeConstant.Pricing.ToLowerInvariant(),
                ZnodeConstant.AddOns.ToLowerInvariant(),
                ZnodeConstant.ProductBrand.ToLowerInvariant(),
                ZnodeConstant.ProductReviews.ToLowerInvariant(),
                ZnodeConstant.AssociatedProducts.ToLowerInvariant(),
                ZnodeConstant.ProductTemplate.ToLowerInvariant()
            };

            return expandKeys.Intersect(expandableKeys).Count() > 0;
        }

        /// <summary>
        /// This method checks if child product has personalized attribute or not
        /// if not it will check parent product's personalized attribute and will add in child products attribute model.
        /// </summary>
        /// <param name="publishProduct"></param>
        /// <param name="childPersonalizableAttribute"></param>
        /// <param name="parentPersonalizableAttribute"></param>
        /// <returns>Returns true if the supplied collection has an expandable key.</returns>
        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return publishProduct;           
        }

        /// <summary>
        /// This method only returns the details of a parent published product found in mongo.
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        private List<ProductEntity> GetPublishProductFromMongo(int publishProductId, FilterCollection filters, out int portalId, out int localeId, out int? catalogVersionId, PublishProductModel publishProduct, out List<ProductEntity> products)
        {
            //Get parameter values from filters.
            int catalogId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);
           
            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            publishProduct = null;
            //Get publish product from mongo
            products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);

            return products;
        }

        public virtual List<ProductEntity> GetAssociatedConfigurableProducts(int localeId, int? catalogVersionId, IEnumerable<string> childSKU)
        {
            List<IMongoQuery> mongoQuery = QueryForAssociatedProduct(localeId, catalogVersionId, childSKU);

            List<ProductEntity> associatedProducts = _ProductMongoRepository.GetEntityList(Query.And(mongoQuery),true);

            associatedProducts.ForEach(x => x.Attributes.Where(y => y.IsConfigurable).ToList()
           .ForEach(z => {
               z.AttributeValues = z.SelectValues.FirstOrDefault()?.Value;
           }));

            associatedProducts = associatedProducts.OrderBy(d => childSKU.ToList().IndexOf(d.SKU)).ToList();

            return associatedProducts;
        }

        private List<IMongoQuery> QueryForAssociatedProduct(int localeId, int? catalogVersionId, IEnumerable<string> childSKU)
        {
            return new List<IMongoQuery>
            {
                Query<ProductEntity>.In(pr => pr.SKU, childSKU),
                Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId),
                Query<ProductEntity>.EQ(pr => pr.VersionId, catalogVersionId)
            };
        }

        public virtual ConfigurableProductEntity GetConfigurableProductEntity(int productId, int? catalogVersionId)
        {
            IMongoRepository<ConfigurableProductEntity> _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ConfigurableProductEntity>.EQ(d => d.ZnodeProductId, productId));
            if (catalogVersionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, catalogVersionId));

            //Get Configurable Product
            ConfigurableProductEntity configEntity = _configurableproductRepository.GetEntity(Query.And(query), true);

            return configEntity;
        }
        #endregion
    }
}
