﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class DashboardService : BaseService, IDashboardService
    {
        #region Private Variables
        private readonly IZnodeViewRepository<DashboardTopItemsModel> objStoredProc;
        #endregion

        #region Constructor
        public DashboardService()
        {
            objStoredProc = new ZnodeViewRepository<DashboardTopItemsModel>();
        }

        #endregion

        #region Public Methods
        //Gets total sales, total orders, total new customers and avg orders
        public virtual DashboardTopItemsListModel GetDashboardSalesDetails(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            int portalId = GetPortalIdFromFilters(filters);
            ZnodeLogging.LogMessage("portalId:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info, portalId);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to generate dashboardTopItemsList list ", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<DashboardTopItemsModel> dashboardTopItemsList = new List<DashboardTopItemsModel>();

            GetDashBoardSalesList(portalId, ref dashboardTopItemsList);
            GetDashboardBrandList(portalId, ref dashboardTopItemsList);
            GetDashboardProductList(portalId, ref dashboardTopItemsList);
            GetDashboardSearchList(portalId, ref dashboardTopItemsList);

            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopItemsList?.ToList() };

            dashboardTopItemsListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }

        //Gets the list of top brands used.
        public virtual DashboardTopItemsListModel GetDashboardTopBrands(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            int portalId = GetPortalIdFromFilters(filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to generate dashboardTopBrandsList list ", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<DashboardTopItemsModel> dashboardTopBrandsList = new List<DashboardTopItemsModel>();
           
            GetDashboardBrandList(portalId, ref dashboardTopBrandsList);

            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopBrandsList?.ToList() };

            dashboardTopItemsListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }

        //Gets the list of top products used.
        public virtual DashboardTopItemsListModel GetDashboardTopProducts(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            int portalId = GetPortalIdFromFilters(filters);

            //Bind the filters, sorts and paging details
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to generate dashboardTopProductsList list ", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<DashboardTopItemsModel> dashboardTopProductsList = new List<DashboardTopItemsModel>();
            GetDashboardProductList(portalId, ref dashboardTopProductsList);

            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopProductsList?.ToList() };
            dashboardTopItemsListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }

        //Gets the list of top items searched with their number
        public virtual DashboardTopItemsListModel GetDashboardTopSearches(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            int portalId = GetPortalIdFromFilters(filters);
            //Bind the filters, sorts and paging details
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to generate dashboardTopSearchesList list ", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<DashboardTopItemsModel> dashboardTopSearchesList = new List<DashboardTopItemsModel>();

            GetDashboardSearchList(portalId, ref dashboardTopSearchesList);

            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopSearchesList?.ToList() };

            dashboardTopItemsListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }

        //Gets the count of low inventory product
        public virtual DashboardTopItemsListModel GetDashboardLowInventoryProductCount(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            int portalId = GetPortalIdFromFilters(filters);            
            //fetches the count of low inventory product

            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.String);

            IList<DashboardTopItemsModel> dashboardTopItemsList = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_DashboardLowInventoryProductCount @PortalId");
            ZnodeLogging.LogMessage("dashboardTopItemsList list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, dashboardTopItemsList?.Count());
            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopItemsList?.ToList() };
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }

        //Gets the list of top categories used.
        public virtual DashboardTopItemsListModel GetDashboardTopCategories(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            //Bind the filters, sorts and paging details
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to generate dashboardTopItemsList list ", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            //fetches the top categories list from database
            IList<DashboardTopItemsModel> dashboardTopItemsList = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_DashboardTopCategory");
            ZnodeLogging.LogMessage("dashboardTopItemsList list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, dashboardTopItemsList?.Count());
            DashboardTopItemsListModel dashboardTopItemsListModel = new DashboardTopItemsListModel { TopItemsList = dashboardTopItemsList?.ToList() };

            dashboardTopItemsListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            return dashboardTopItemsListModel;
        }
        #endregion

        #region Private Methods
        //Get Dashboard Search List
        private void GetDashboardSearchList(int portalId, ref List<DashboardTopItemsModel> dashboardTopItemsList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter portalId:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, portalId);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.String);

            //fetches the top searched items list from database
            IList<DashboardTopItemsModel> list = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_DashboardTopSearches @PortalId");
            ZnodeLogging.LogMessage("DashboardTopItemsModel list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, list?.Count());
            list.Select(x => { x.Type = ZnodeConstant.Search; return x; }).ToList();
            dashboardTopItemsList.AddRange(list);
        }

        //Get Dashboard Product List
        private void GetDashboardProductList(int portalId, ref List<DashboardTopItemsModel> dashboardTopItemsList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter portalId:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, portalId);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.String);

            //fetches the top products list from database
            IList<DashboardTopItemsModel> list = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_DashboardTopProducts @PortalId");
            ZnodeLogging.LogMessage("DashboardTopItemsModel list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, list?.Count());
            list.Select(x => { x.Type = ZnodeConstant.Product; return x; }).ToList();
            dashboardTopItemsList.AddRange(list);
        }

        //Get Dashboard Brand List
        private void GetDashboardBrandList(int portalId, ref List<DashboardTopItemsModel> dashboardTopItemsList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter portalId:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, portalId);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.String);

            //fetches the top brands list from database
            IList<DashboardTopItemsModel> list = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_DashboardTopBrands @PortalId");
            ZnodeLogging.LogMessage("DashboardTopItemsModel list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, list?.Count());
            list.Select(x => { x.Type = ZnodeConstant.Brand; return x; }).ToList();
            dashboardTopItemsList.AddRange(list);
        }

        //Get Dashboard Sales List
        private void GetDashBoardSalesList(int portalId, ref List<DashboardTopItemsModel> dashboardTopItemsList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter portalId:", ZnodeLogging.Components.GlobalSettings.ToString(),TraceLevel.Verbose, portalId);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.String);

            //fetches the total sales, total orders, total new customers and avg orders list from database
            IList<DashboardTopItemsModel> list = objStoredProc.ExecuteStoredProcedureList("ZnodeReport_Dashboard  @PortalId");
            ZnodeLogging.LogMessage("DashboardTopItemsModel list count:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, list?.Count());
            list.Select(x => { x.Type = ZnodeConstant.Sales; return x; }).ToList();
            dashboardTopItemsList.AddRange(list);
        }

        //gets portalId from filters
        private static int GetPortalIdFromFilters(FilterCollection filters)
        {
            int portalId = 0;
            if (HelperUtility.IsNotNull(filters))
                portalId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower())?.FirstOrDefault()?.Item3);
            ZnodeLogging.LogMessage("portalId:", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Verbose, portalId);
            return portalId;
        }
        #endregion
    }
}
