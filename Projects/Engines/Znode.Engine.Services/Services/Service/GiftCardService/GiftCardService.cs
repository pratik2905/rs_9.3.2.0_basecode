﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class GiftCardService : BaseService, IGiftCardService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeGiftCard> _giftCardRepository;
        private readonly IZnodeRepository<ZnodeRmaConfiguration> _rmaConfigurationeRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        #endregion

        #region Constructor
        public GiftCardService()
        {
            _giftCardRepository = new ZnodeRepository<ZnodeGiftCard>();
            _rmaConfigurationeRepository = new ZnodeRepository<ZnodeRmaConfiguration>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
        }
        #endregion

        #region Public Methods
        //Create new GiftCard.
        public virtual GiftCardModel CreateGiftCard(GiftCardModel giftCardModel)
        {
            ZnodeLogging.LogMessage("CreateGiftCard method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(giftCardModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.GiftCardModelNotNull);

            //Check gift card to be created is for Referral Commission Payment or not.
            if (giftCardModel.IsReferralCommission)
                //Check if amount paying to customer is greater than amount to be paid throws an exception.
                CheckAmount(giftCardModel);
            else if (giftCardModel.UserId > 0 && giftCardModel.SendMail)
            {
                RMAConfigurationModel rmaConfigurationModel = _rmaConfigurationeRepository.GetEntity(string.Empty)?.ToModel<RMAConfigurationModel>();
                giftCardModel.NotificationSentToCustomer = rmaConfigurationModel?.GcNotification;
                //If gift card is created for customer and is not referral, then mail will be sent to customer.
                SendMailToCustomer(giftCardModel);
            }
            ZnodeGiftCard giftCard = _giftCardRepository.Insert(giftCardModel.ToEntity<ZnodeGiftCard>());
            ZnodeLogging.LogMessage("Inserted giftCard with id ", ZnodeLogging.Components.Warehouse.ToString(), TraceLevel.Verbose, giftCard?.GiftCardId);

            ZnodeLogging.LogMessage(IsNotNull(giftCard) ? String.Format(Admin_Resources.SuccessCreateGiftCard, giftCardModel.CardNumber): Admin_Resources.ErrorCreateGiftCard, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNotNull(giftCard))
                return giftCard.ToModel<GiftCardModel>();
            ZnodeLogging.LogMessage("CreateGiftCard method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return giftCardModel;
        }

        //Update Gift Card.
        public virtual bool UpdateGiftCard(GiftCardModel giftCardModel)
        {
            ZnodeLogging.LogMessage("UpdateGiftCard method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(giftCardModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.GiftCardModelNotNull);

            if (giftCardModel.GiftCardId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne,Admin_Resources.IdCanNotBeLessThanOne);

            //Update gift card.
            bool isGiftCardUpdated = _giftCardRepository.Update(giftCardModel.ToEntity<ZnodeGiftCard>());

            if (isGiftCardUpdated && giftCardModel.UserId > 0)
            {
                RMAConfigurationModel rmaConfigurationModel = _rmaConfigurationeRepository.GetEntity(string.Empty)?.ToModel<RMAConfigurationModel>();
                giftCardModel.NotificationSentToCustomer = rmaConfigurationModel?.GcNotification;

                if (giftCardModel.SendMail)
                    //If gift card is created for customer and is not referral, then mail will be sent to customer.
                    SendMailToCustomer(giftCardModel);
            }

            ZnodeLogging.LogMessage(isGiftCardUpdated ? string.Format(Admin_Resources.SuccessUpdateGiftCard,giftCardModel.CardNumber) : string.Format(Admin_Resources.ErrorUpdateWarehouse,giftCardModel.CardNumber),ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("UpdateGiftCard method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return isGiftCardUpdated;
        }

        //Get GiftCard by gift card id.
        public virtual GiftCardModel GetGiftCard(int giftCardId)
        {
            ZnodeLogging.LogMessage("GetGiftCard method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (giftCardId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.IdCanNotBeLessThanOne);

            ZnodeGiftCard giftCardEntity = _giftCardRepository.GetEntity(GetWhereClause(giftCardId), GetNavigationProperties());
            GiftCardModel giftCard = giftCardEntity.ToModel<GiftCardModel>();
            if (IsNotNull(giftCard))
            {
                if (IsNotNull(giftCardEntity.ZnodeUser))
                    giftCard.CustomerName = GetCustomerName(giftCardEntity.ZnodeUser.FirstName, giftCardEntity.ZnodeUser.LastName, giftCardEntity.ZnodeUser.Email);
                giftCard.StoreName = _portalRepository.Table.FirstOrDefault(x => x.PortalId == giftCard.PortalId)?.StoreName;
            }
            ZnodeLogging.LogMessage("GetGiftCard method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return giftCard;
        }

        //Get Customer Name
        protected virtual string GetCustomerName(string firstName, string lastName, string username)
        {
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                return $"{username} | {firstName} {lastName}";
            else
                return username;
        }

        //Build WhereClause with giftCardId
        protected virtual string GetWhereClause(int giftCardId)
        {
            FilterCollection filters = new FilterCollection() { new FilterTuple(ZnodeGiftCardEnum.GiftCardId.ToString(), ProcedureFilterOperators.Equals, giftCardId.ToString()) };
            return DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause;
        }

        //Set navigation properties
        protected virtual List<string> GetNavigationProperties()
        {
            List<string> navigationProperty = new List<string>();
            SetExpands(ZnodeGiftCardEnum.ZnodeUser.ToString(), navigationProperty);
            return navigationProperty;
        }

        //Get paged Gift Card list.
        public virtual GiftCardListModel GetGiftCardList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("GetGiftCardList method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Get Authorized Portal Access for login User.
            string portalAccess = GetAvailablePortals();
            string expirationDate = filters?.Find(x => string.Equals(x.FilterName, ZnodeGiftCardEnum.ExpirationDate.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3;
            filters?.RemoveAll(x => string.Equals(x.FilterName, ZnodeGiftCardEnum.ExpirationDate.ToString(), StringComparison.CurrentCultureIgnoreCase));

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("SP parameter values: ", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            GiftCardListModel listModel = new GiftCardListModel();
            IZnodeViewRepository<GiftCardModel> objStoredProc = new ZnodeViewRepository<GiftCardModel>();

            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowsCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalAccess, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@ExpirationDate", string.IsNullOrEmpty(expirationDate) ? string.Empty : expirationDate, ParameterDirection.Input, DbType.String);

            IList<GiftCardModel> giftCardList = objStoredProc.ExecuteStoredProcedureList("Znode_GetGiftCardList  @WhereClause,@Rows,@PageNo,@Order_By,@RowsCount OUT,@PortalId,@ExpirationDate", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("Gift card list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, giftCardList?.Count());
            listModel.GiftCardList = giftCardList?.Count > 0 ? giftCardList.ToList() : new List<GiftCardModel>();

            //If filter contains IsReferralCommission, Set true flag for ReferralCommissionCount, if referral commission count is greater than zero. 
            if (filters.Any(x => string.Equals(x.FilterName, ZnodeGiftCardEnum.IsReferralCommission.ToString(), StringComparison.CurrentCultureIgnoreCase)))
                SetReferralCommissionCountFlag(listModel, filters);

            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("GetGiftCardList method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Delete GiftCard  by giftCardId.
        public virtual bool DeleteGiftCard(ParameterModel giftCardId)
        {
            ZnodeLogging.LogMessage("DeleteGiftCard method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool status = false;
            if (giftCardId.Ids.Count() < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.IdCanNotBeLessThanOne);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeGiftCardEnum.GiftCardId.ToString(), ProcedureFilterOperators.In, giftCardId.Ids.ToString()));

            IZnodeRepository<ZnodeRmaRequestItem> _rmaRequestItem = new ZnodeRepository<ZnodeRmaRequestItem>();
            _rmaRequestItem.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
            ZnodeLogging.LogMessage("GenerateDynamicWhereClause method call with parameter : filters.ToFilterDataCollection() ", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, filters.ToFilterDataCollection());
            status = _giftCardRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
            ZnodeLogging.LogMessage(status ? String.Format(Admin_Resources.SuccessDeleteGiftCard,giftCardId.Ids) : String.Format(Admin_Resources.ErrorDeleteWarehouse,giftCardId.Ids), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("DeleteGiftCard method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return status;
        }

        //Create new random card number.
        public virtual string GetRandomCardNumber() => GenerateRandomNumber.GetNextGiftCardNumber();

        //Get paged Gift Card history list for a user.
        public virtual GiftCardHistoryListModel GetGiftCardHistoryList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("GetGiftCardHistoryList method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (filters?.Count() < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorFiltersEmpty);
            int userId = Convert.ToInt32(filters?.Find(x => x.FilterName == "userid")?.Item3);
            int portalId = Convert.ToInt32(filters?.Find(x => x.FilterName == "portalid")?.Item3);

            if (userId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.IdCanNotBeLessThanOne);
            GiftCardHistoryListModel model = new GiftCardHistoryListModel();

            //Gift card history list of a user.
            model.GiftCardHistoryList = (from gc in new ZnodeRepository<ZnodeGiftCard>().GetEntityList(string.Empty).ToList()
                                         join gch in new ZnodeRepository<ZnodeGiftCardHistory>().GetEntityList(string.Empty).ToList() on gc.GiftCardId equals gch.GiftCardId
                                         join odr in new ZnodeRepository<ZnodeOmsOrderDetail>().Table on gch.OmsOrderDetailsId equals odr.OmsOrderDetailsId
                                         join od in new ZnodeRepository<ZnodeOmsOrder>().Table on odr.OmsOrderId equals od.OmsOrderId
                                         where gc.UserId == userId && gc.PortalId == portalId
                                         select new GiftCardHistoryModel
                                         {
                                             OrderId = odr.OmsOrderId,
                                             OrderNumber = od.OrderNumber,
                                             TransactionAmount = gch.TransactionAmount,
                                             TransactionDate = gch.TransactionDate,
                                             GiftCardNumber = gc.CardNumber,
                                         }).ToList();
            ZnodeLogging.LogMessage("GiftCardHistoryList list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, model.GiftCardHistoryList?.Count());
            ZnodeLogging.LogMessage("GetGiftCardHistoryList method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return model;
        }
        #endregion

        #region Private Method
        //Set true flag for ReferralCommissionCount, if referral commission count is greater than zero. 
        private void SetReferralCommissionCountFlag(GiftCardListModel listModel, FilterCollection filters)
        {
            string userId = filters.Find(x => string.Equals(x.FilterName, ZnodeUserEnum.UserId.ToString(), StringComparison.CurrentCultureIgnoreCase))?.FilterValue;

            FilterCollection referralCommissionFilter = new FilterCollection();
            referralCommissionFilter.Add(ZnodeUserEnum.UserId.ToString(), FilterOperators.Equals, userId);

            IZnodeRepository<ZnodeOmsReferralCommission> _referralCommission = new ZnodeRepository<ZnodeOmsReferralCommission>();
            listModel.ReferralCommissionCount = _referralCommission.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(referralCommissionFilter.ToFilterDataCollection()).WhereClause).Any();
        }

        //Check if amount paying to customer is greater than amount to be paid throws an exception.
        private void CheckAmount(GiftCardModel giftCardModel)
        {
            IZnodeRepository<View_CustomerReferralCommissionDetail> _viewCustomerReferralCommissionDetail = new ZnodeRepository<View_CustomerReferralCommissionDetail>();

            //Get list of owed amount and calculate its total.
            giftCardModel.OwedAmount = _giftCardRepository.Table.Where(x => x.UserId == giftCardModel.UserId && x.IsReferralCommission == true)?.Select(x => x.Amount)?.ToList()?.Sum();
            ZnodeLogging.LogMessage("Gift Card Owed Amount:", ZnodeLogging.Components.OMS.ToString(),TraceLevel.Verbose, giftCardModel.OwedAmount);
            //Get list of order commission amount and calculate its total.
            decimal? totalCommission = _viewCustomerReferralCommissionDetail.Table.Where(x => x.UserId == giftCardModel.UserId)?.Select(x => x.OrderCommission)?.ToList()?.Sum();
            ZnodeLogging.LogMessage("Gift Card Total Commission:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,totalCommission);
            //Check if sum of owned amount and newly entered amount is greater than total commission. 
            if ((giftCardModel.OwedAmount + giftCardModel.Amount) > totalCommission)
            {
                //Left amount is amount left to pay to customer.
                giftCardModel.LeftAmount = totalCommission - giftCardModel.OwedAmount;

                //If sum of owned amount and newly entered amount is greater than amount to be paid,throws an exception.
                throw new ZnodeException(ErrorCodes.InvalidData, $"{Admin_Resources.PaymentGreaterErrorMessage} { Math.Round(giftCardModel.LeftAmount.GetValueOrDefault(), Convert.ToInt32(DefaultGlobalConfigSettingHelper.GetDefaultGlobalConfigSetings()?.DefaultGlobalConfigs.Where(x => x.FeatureName == GlobalSettingEnum.PriceRoundOff.ToString())?.Select(x => x.FeatureValues)?.FirstOrDefault()))}");
            }
        }

        //This method will send gift card mail to customer.        
        private void SendMailToCustomer(GiftCardModel giftCardModel)
        {
            if (IsNotNull(giftCardModel))
            {
                EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.GiftCardIssue, giftCardModel.PortalId);

                if (IsNotNull(emailTemplateMapperModel))
                {
                    //Get default global config list.
                    List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();

                    IUserService userService = GetService<IUserService>();
                    string storeName = string.Empty;
                    //Get user details.
                    UserModel userDetails = userService.GetUserById(Convert.ToInt32(giftCardModel?.UserId), null);

                    if (giftCardModel.PortalId > 0)
                        storeName = _portalRepository.Table.Where(w => w.PortalId == giftCardModel.PortalId)?.FirstOrDefault()?.StoreName;
                    ZnodeLogging.LogMessage("Store Name:", ZnodeLogging.Components.OMS.ToString(),TraceLevel.Verbose, storeName);
                    string messageText = emailTemplateMapperModel.Descriptions;
                    string subject = $"{emailTemplateMapperModel?.Subject} - {storeName}";

                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.GCNotification, giftCardModel.NotificationSentToCustomer, messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.CustomerName, $" {userDetails?.FirstName} {userDetails?.LastName}", messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.GiftCardAmount, $"{FormatPriceWithCurrency(giftCardModel?.Amount, string.IsNullOrEmpty(giftCardModel.CultureCode) ? GetDefaultCulture(defaultGlobalSettingData) : giftCardModel.CultureCode, GetDefaultPriceRoundOff(defaultGlobalSettingData))}", messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.GiftCardNumber, giftCardModel?.CardNumber.ToString(), messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.StoreLogo, GetCustomPortalDetails(giftCardModel.PortalId)?.StoreLogo, messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.FirstName, $" {userDetails?.FirstName}", messageText);
                    messageText = ReplaceTokenWithMessageText(ZnodeConstant.StoreName, $" {storeName}", messageText);

                    if (IsNotNull(giftCardModel?.ExpirationDate))
                        messageText = ReplaceTokenWithMessageText(ZnodeConstant.ExpirationDate, giftCardModel?.ExpirationDate?.ToString(GetDefaultDateFormat(defaultGlobalSettingData)), messageText);
                    else
                        messageText = ReplaceTokenWithMessageText(ZnodeConstant.ExpirationDateMessage, string.Empty, messageText);

                    ZnodeLogging.LogMessage("Parameter for SendEmail", ZnodeLogging.Components.OMS.ToString(),
                    TraceLevel.Verbose, new object[] { userDetails?.Email, subject, messageText, giftCardModel.PortalId });
                    //Send mail to customer.
                    SendEmail(userDetails?.Email, subject, messageText, giftCardModel.PortalId);
                }
            }
        }

        //Send Email.
        private void SendEmail(string email, string subject, string messageText, int portalId = 0)
        {
          ZnodeEmail.SendEmail(portalId, email, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, subject, messageText, true);
        }

        //TO DO - in progress

        //Get default global config list.
        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }

        //Get default currency code.
        private string GetDefaultCulture(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //Get default price round off.
        private string GetDefaultPriceRoundOff(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
            => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.PriceRoundOff.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //Get default date format.
        private string GetDefaultDateFormat(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
            => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.DateFormat.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //For Price according to currency.
        private static string FormatPriceWithCurrency(decimal? price, string CultureName, string defaultPriceRoundOff)
        {
            string currencyValue;
            if (IsNotNull(CultureName))
            {
                CultureInfo info = new CultureInfo(CultureName);
                info.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(defaultPriceRoundOff);
                currencyValue = $"{price.GetValueOrDefault().ToString("c", info.NumberFormat)}";
            }
            else
                currencyValue = Convert.ToString(price);

            return currencyValue;
        }
        #endregion
    }
}
