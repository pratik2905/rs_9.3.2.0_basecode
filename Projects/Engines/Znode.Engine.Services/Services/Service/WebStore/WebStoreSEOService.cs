﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public partial class SEOService
    {
        #region Public Method
        //Get SEO setting by seo type.
        public virtual List<SEODetailsModel> GetSeoSettingList(string seoTypeName, int portalId, int localeId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters seoTypeName, portalId and localeId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { seoTypeName, portalId, localeId });

            IZnodeViewRepository<SEODetailsModel> objStoredProc = new ZnodeViewRepository<SEODetailsModel>();
            objStoredProc.SetParameter("@SeoId", string.Empty, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SeoType", seoTypeName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            List<SEODetailsModel> SeoSettings = objStoredProc.ExecuteStoredProcedureList("Znode_GetSeoDetails  @SeoId,@SeoType,@LocaleId,@PortalId")?.ToList();
            ZnodeLogging.LogMessage("SeoSettings list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, SeoSettings?.Count);
            SeoSettings?.ForEach(x => x.LocaleId = localeId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return SeoSettings;
        }

        public virtual List<SeoEntity> GetPublishSEOSettingList(string seoTypeName, int? portalId, int localeId = 0, int? versionId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters seoTypeName, portalId, localeId and versionId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { seoTypeName, portalId, localeId, versionId });
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            if (HelperUtility.IsNotNull(portalId))
                mongoQuery.Add(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId));

            mongoQuery.Add(Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoTypeName));
            mongoQuery.Add(Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId));
            if (!Equals(versionId, 0) && HelperUtility.IsNotNull(versionId))
                mongoQuery.Add(Query<ProductEntity>.EQ(pr => pr.VersionId, versionId));

            IMongoRepository<SeoEntity> seoEntityRepository = null;
            if (seoTypeName == ZnodeConstant.Category || seoTypeName == ZnodeConstant.Product || seoTypeName == ZnodeConstant.Brand)
            {
                seoEntityRepository = new MongoRepository<SeoEntity>(!Equals(versionId, 0) && HelperUtility.IsNotNull(versionId) ? versionId : GetCatalogVersionId());
            }
            else if (seoTypeName == ZnodeConstant.ContentPage || seoTypeName == ZnodeConstant.BlogNews)
            {
                seoEntityRepository = new MongoRepository<SeoEntity>(WebstoreVersionId);
            }
            ZnodeLogging.LogMessage("Mongo query to get publishSEOList: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);
            List<SeoEntity> publishSEOList = seoEntityRepository?.GetEntityList(Query.And(mongoQuery), !Equals(versionId, 0));
            ZnodeLogging.LogMessage("publishSEOList count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, publishSEOList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return publishSEOList;
        }

        public List<ZnodeCMSSEODetail> GetSEODetailsList(int portalId, string seoType)
        {
            List<ZnodeCMSSEODetail> znodeCMSSEODetails = (from seoDetails in _seoDetailRepository.Table
                                                          join seotype in _seoTypeRepository.Table on seoDetails.CMSSEOTypeId equals seotype.CMSSEOTypeId
                                                          where seoDetails.PortalId == portalId && seotype.Name == seoType
                                                          select seoDetails).ToList();
            ZnodeLogging.LogMessage("znodeCMSSEODetails count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, znodeCMSSEODetails?.Count);

            return znodeCMSSEODetails;
        }

        //Get SEO Setting
        public SEODetailsModel GetSeoSetting(int seoId, string seoTypeName, int portalId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters seoId, seoTypeName, portalId and localeId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { seoId, seoTypeName, portalId, localeId });
            IZnodeViewRepository<SEODetailsModel> objStoredProc = new ZnodeViewRepository<SEODetailsModel>();
            objStoredProc.SetParameter("@SeoId", seoId.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SeoType", seoTypeName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            SEODetailsModel seoDetails = objStoredProc.ExecuteStoredProcedureList("Znode_GetSeoDetails  @SeoId,@SeoType,@LocaleId,@PortalId")?.FirstOrDefault();
            ZnodeLogging.LogMessage("ItemName, CMSSEODetailId, SEOId and SEOCode properties of SEODetailsModel to be returned: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { ItemName = seoDetails?.ItemName, CMSSEODetailId = seoDetails?.CMSSEODetailId, SEOId = seoDetails?.SEOId, SEOCode = seoDetails?.SEOCode });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return seoDetails;
        }

        public SeoEntity GetPublishSeoSetting(string seoCode, string seoTypeName, int portalId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.And(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId),
                               Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoTypeName),
                               Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId),
                               Query<SeoEntity>.EQ(pr => pr.SEOCode, seoCode)));
            ZnodeLogging.LogMessage("Mongo query to get publishSEOList: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);
            SeoEntity publishSEOList = _seoMongoRepository.GetEntity(Query.And(mongoQuery));
            ZnodeLogging.LogMessage("ItemName, CMSSEODetailId, SEOId and SEOCode properties of publishSEOList to be returned: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { ItemName = publishSEOList?.ItemName, CMSSEODetailId = publishSEOList?.CMSSEODetailId, SEOId = publishSEOList?.SEOId, SEOCode = publishSEOList?.SEOCode });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return publishSEOList;
        }


        //Get SEO default setting by portal id.
        public virtual ZnodeCMSPortalSEOSetting GetPortalSeoDefaultSetting(string portalId)
             => _portalSEOSettingRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(PortalFilter(portalId).ToFilterDataCollection()).WhereClause).FirstOrDefault();
        #endregion

        #region Private Method
        //Generate filters for Portal Id.
        private static FilterCollection PortalFilter(string portalId)
            => new FilterCollection() { new FilterTuple(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId) };
        #endregion
    }
}
