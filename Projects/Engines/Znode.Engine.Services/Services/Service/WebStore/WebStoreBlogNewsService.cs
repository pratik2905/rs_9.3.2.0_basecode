﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public partial class BlogNewsService
    {
        #region Public Region
        //Get list of blogs or news to display them on webstore.
        public virtual WebStoreBlogNewsListModel GetBlogNewsListForWebstore(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            string localeId = filters.Find(x => x.FilterName == FilterKeys.LocaleId)?.Item3;
            string blogNewsType = filters.Find(x => x.FilterName == FilterKeys.BlogNewsType)?.Item3;
            string portalId = filters.Find(x => x.FilterName == FilterKeys.PortalId)?.Item3;
            ZnodeLogging.LogMessage("localeId, blogNewsType and portalId generated from filters: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { localeId, blogNewsType, portalId });

            WebStoreBlogNewsListModel listModel = new WebStoreBlogNewsListModel();
            List<BlogNewsEntity> blogNewsList = GetBlogNewsData(filters, blogNewsType);
            ZnodeLogging.LogMessage("blogNewsList count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, blogNewsList?.Count);

            listModel.BlogNewsList = blogNewsList?.ToModel<WebStoreBlogNewsModel>().ToList();

            ISEOService seoService = GetService<ISEOService>();

            List<SeoEntity> seosettings = seoService.GetPublishSEOSettingList(ZnodeConstant.BlogNews, Convert.ToInt32(portalId), Convert.ToInt32(localeId));
            MapBlogNewsWithSEO(listModel.BlogNewsList, seosettings);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get published blog/news for displaying on webstore along with commwnts against it.
        public virtual WebStoreBlogNewsModel GetBlogNewsForWebstore(int blogNewsId, int localeId, int portalId, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters blogNewsId, localeId and portalId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { blogNewsId, localeId, portalId });
            //Mongo query.
            IMongoQuery query = MongoQueryForBlogNews(blogNewsId, localeId, portalId);
            ZnodeLogging.LogMessage("Mongo query returned from method MongoQueryForBlogNews: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query);

            BlogNewsEntity blogNews = _blogNewsMongoRepository.GetEntity(query);

            WebStoreBlogNewsModel model = blogNews?.ToModel<WebStoreBlogNewsModel>();
            if (HelperUtility.IsNotNull(model))
            {
                //If content page id exist, then get the content text from mongo. Else get the new blog/news content added.
                if (model?.CMSContentPagesId > 0)
                {
                    List<IMongoQuery> textWidgetQuery = new List<IMongoQuery>();
                    textWidgetQuery.Add(Query<TextWidgetEntity>.EQ(d => d.LocaleId, localeId));
                    textWidgetQuery.Add(Query<TextWidgetEntity>.EQ(d => d.MappingId, model.CMSContentPagesId));

                    model.BlogNewsContent = _textWidegetMongoRepository.GetEntity(Query.And(textWidgetQuery))?.Text;
                    model.BlogNewsContent = !string.IsNullOrEmpty(model.BlogNewsContent) ? Regex.Replace(model.BlogNewsContent, "<.*?>", string.Empty) : string.Empty;
                }

                //Get the list of user comments against a blog/news.
                FilterCollection filters = new FilterCollection();
                filters.Add(ZnodeBlogNewsCommentEnum.BlogNewsId.ToString(), FilterOperators.Equals, blogNewsId.ToString());
                model.Comments = GetUserCommentList(filters, null, null, null);

                MapSEODetails(localeId, model, blogNews);
                ZnodeLogging.LogMessage("WebStoreBlogNewsModel to be returned having id: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model.BlogNewsId);
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
                return model;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return new WebStoreBlogNewsModel();
        }

        //Create new blog or news comments.
        public virtual WebStoreBlogNewsCommentModel SaveComments(WebStoreBlogNewsCommentModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorBlogNewsCommentModelNull);

            //Get userid of logged in user.
            int? userId = GetLoginUserId();
            ZnodeLogging.LogMessage("userId returned from method GetLoginUserId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, userId);

            model.UserId = userId > 0 ? userId : null;

            model.IsApproved = true;
            //Save data into blog/news comment table.
            model.BlogNewsCommentId = _blogNewsCommentRepository.Insert(model.ToEntity<ZnodeBlogNewsComment>()).ToModel<WebStoreBlogNewsCommentModel>().BlogNewsCommentId;

            //Save data into blog/news comment locale table.
            if (model?.BlogNewsCommentId > 0)
            {
                ZnodeLogging.LogMessage(String.Format(Admin_Resources.SuccessBlogNewsCreate,model.BlogNewsCommentId), ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

                //Insert data into blog/news comment locale entity.
                SaveBlogNewsCommentLocale(model);
                ZnodeLogging.LogMessage("WebStoreBlogNewsModel to be returned having id: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model.BlogNewsId);
                return model;
            }

            ZnodeLogging.LogMessage(String.Format(Admin_Resources.ErrorBlogNewsCreate,model.BlogNewsCommentId), ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("WebStoreBlogNewsModel to be returned: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model);
            return model;
        }

        //Get the list of user comments against a blog/news.
        public virtual List<WebStoreBlogNewsCommentModel> GetUserCommentList(FilterCollection filters, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page)
        {
            List<WebStoreBlogNewsCommentModel> listModel = BlogNewCommentData(filters);
            if (listModel?.Count > 0)
            {
                List<int?> userIds = listModel.Where(x => x.UserId != null).Select(y => y.UserId).ToList();
                IList<ZnodeUser> users = null;

                if (userIds.Count > 0)
                {
                    FilterCollection usersFilter = new FilterCollection();
                    usersFilter.Add(ZnodeUserEnum.UserId.ToString(), FilterOperators.In, string.Join(",", userIds));
                    users = _userRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(usersFilter.ToFilterDataCollection()).WhereClause);
                }

                foreach (WebStoreBlogNewsCommentModel item in listModel)
                {
                    ZnodeUser userData = users?.FirstOrDefault(x => x.UserId == item.UserId);
                    if (HelperUtility.IsNull(userData))
                        item.UserName = "Guest";
                    else
                        item.UserName = $"{userData.FirstName}{userData.LastName}";
                }
                ZnodeLogging.LogMessage("WebStoreBlogNewsCommentModel list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.Count);
                return listModel;
            }
            return new List<WebStoreBlogNewsCommentModel>();
        }
        #endregion

        #region Private Region
        //Get blog news data .
        private List<BlogNewsEntity> GetBlogNewsData(FilterCollection filters, string type)
        {
            type = (type == "blog") ? "Blog" : "News";

            //Get Portal and locale id from filter.
            int portalId, localeId;

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<BlogNewsEntity>.EQ(d => d.PortalId, portalId));
            query.Add(Query<BlogNewsEntity>.EQ(d => d.LocaleId, localeId));
            query.Add(Query<BlogNewsEntity>.EQ(d => d.BlogNewsType, type));

            IMongoQuery query1 = Query.And(query);
            ZnodeLogging.LogMessage("Mongo query to generate whereClause get blog news data: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query1);
            IMongoQuery whereClause = PublishHelper.AddFilterForDate(query1);
            ZnodeLogging.LogMessage("whereClause get blog news data: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, whereClause);
            return _blogNewsMongoRepository.GetEntityList(whereClause);
        }

        //Map seo data with its blog or news.
        private void MapBlogNewsWithSEO(List<WebStoreBlogNewsModel> blogNewsList, List<SeoEntity> seoData)
        {
            foreach (var blogNews in blogNewsList)
            {
                //Get blog news seo.
                SeoEntity seo = seoData
                            .FirstOrDefault(seoDetail => seoDetail.SEOCode == blogNews.BlogNewsCode);

                if (HelperUtility.IsNotNull(seo))
                {
                    blogNews.SEOUrl = seo.SEOUrl;
                    blogNews.SEODescription = seo.SEODescription;
                    blogNews.SEOTitle = seo.SEOTitle;
                    blogNews.SEOKeywords = seo.SEOKeywords;

                }
            }
        }

        //Create a mongo query for blog/news.
        private IMongoQuery MongoQueryForBlogNews(int blogNewsId, int localeId, int portalId)
        {
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<BlogNewsEntity>.EQ(d => d.PortalId, portalId));
            query.Add(Query<BlogNewsEntity>.EQ(d => d.LocaleId, localeId));
            query.Add(Query<BlogNewsEntity>.EQ(d => d.BlogNewsId, blogNewsId));
            IMongoQuery query1 = Query.And(query);
            return query1;
        }

        //Insert data into blog/news comment locale table.
        private void SaveBlogNewsCommentLocale(WebStoreBlogNewsCommentModel model)
        {
            ZnodeBlogNewsCommentLocale blogNewsCommentLocale = _blogNewsCommentLocaleRepository.Insert(model?.ToEntity<ZnodeBlogNewsCommentLocale>());

            ZnodeLogging.LogMessage(blogNewsCommentLocale?.BlogNewsCommentLocaleId > 0
             ? Admin_Resources.SuccessBlogNewsCommentLocaleInsert : Admin_Resources.ErrorBlogNewsCommentLocaleInsert, ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
        }

        //Get blog news comment data.
        private List<WebStoreBlogNewsCommentModel> BlogNewCommentData(FilterCollection filters)
        {
            //Get Portal Locale data.
            return (from asl in _blogNewsCommentRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause)
                    join locale in _blogNewsCommentLocaleRepository.GetEntityList(string.Empty) on asl.BlogNewsCommentId equals locale.BlogNewsCommentId
                    where asl.IsApproved == true
                    select new WebStoreBlogNewsCommentModel
                    {
                        BlogNewsCommentId = locale.BlogNewsCommentId.Value,
                        BlogNewsComment = locale.BlogComment,
                        UserId = asl.UserId,
                        IsApproved = asl.IsApproved.GetValueOrDefault(),
                        CreatedDate = asl.CreatedDate,
                    }).ToList();


        }

        //Map SEO details for a particular blog/news.
        private void MapSEODetails(int localeId, WebStoreBlogNewsModel model, BlogNewsEntity blogNews)
        {
            ISEOService seoService = GetService<ISEOService>();
            List<SeoEntity> seoData = seoService.GetPublishSEOSettingList(ZnodeConstant.BlogNews, Convert.ToInt32(PortalId), Convert.ToInt32(localeId));
            //Get blog news seo.
            SeoEntity seo = seoData
                        .FirstOrDefault(seoDetail => seoDetail.SEOCode == blogNews.BlogNewsCode);

            if (HelperUtility.IsNotNull(seo))
            {
                model.SEOUrl = seo.SEOUrl;
                model.SEODescription = seo.SEODescription;
                model.SEOTitle = seo.SEOTitle;
                model.SEOKeywords = seo.SEOKeywords;

            }
        }
        #endregion
    }
}
