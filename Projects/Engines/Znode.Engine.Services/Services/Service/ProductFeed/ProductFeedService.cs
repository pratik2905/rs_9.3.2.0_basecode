﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Resources;

namespace Znode.Engine.Services
{
    public class ProductFeedService : BaseService, IProductFeedService
    {
        #region Private Variables
        private readonly string strNamespace = ZnodeApiSettings.SiteMapNameSpace;
        private readonly string strGoogleFeedNamespace = ZnodeApiSettings.GoogleProductFeedNameSpace;
        private const string ItemCategory = "category";
        private const string ItemContentPages = "content";
        private const string ItemProduct = "product";
        private readonly string ItemUrlset = "urlset";
        private const string ItemAll = "all";
        private readonly IZnodeRepository<ZnodeProductFeed> _productFeedRepository;
        private readonly IZnodeRepository<ZnodeProductFeedPriority> _productFeedPriorityRepository;
        private readonly IZnodeRepository<ZnodeProductFeedSiteMapType> _productFeedSiteMapTypeRepository;
        private readonly IZnodeRepository<ZnodeProductFeedTimeStamp> _productFeedTimeStampRepository;
        private readonly IZnodeRepository<ZnodeProductFeedType> _productFeedTypeRepository;
        #endregion

        #region Constructor
        public ProductFeedService()
        {
            _productFeedRepository = new ZnodeRepository<ZnodeProductFeed>();
            _productFeedPriorityRepository = new ZnodeRepository<ZnodeProductFeedPriority>();
            _productFeedSiteMapTypeRepository = new ZnodeRepository<ZnodeProductFeedSiteMapType>();
            _productFeedTimeStampRepository = new ZnodeRepository<ZnodeProductFeedTimeStamp>();
            _productFeedTypeRepository = new ZnodeRepository<ZnodeProductFeedType>();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// This method will create the Product Feed or SiteMap
        /// </summary>
        /// <param name="model">Product Feed Model</param>
        /// <returns>Product Feed Model</returns>
        public virtual ProductFeedModel CreateGoogleSiteMap(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            string domainName = model.SuccessXMLGenerationMessage;
            ProductFeedModel generatedXml = new ProductFeedModel();
            string feedType = string.Empty;
            bool isFeed = false;

            //check if Feed is for Site map or Product
            CheckIsSiteMapOrProduct(model, out feedType, out isFeed);

            int fileNameCount = 0;
            ZnodeProductFeedHelper znodeProductFeed = new ZnodeProductFeedHelper();
            string portalidSrt = string.Join(",", Array.ConvertAll(model.PortalId, x => x.ToString()));
            ZnodeLogging.LogMessage("portalidSrt:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalidSrt);
            if (isFeed)
                fileNameCount = znodeProductFeed.GetProductList(portalidSrt, this.strGoogleFeedNamespace, model.FileName, model.Title, model.Link, model.Description, feedType, model.LocaleId, model.ProductFeedTimeStampName, model.Date);
            else
            {
                //Call if Site map to be generated for Category/Content Page
                ZnodeLogging.LogMessage("Parameter for getting fileNameCount", ZnodeLogging.Components.Admin.ToString(),TraceLevel.Verbose, new object[] { "ProductFeedModel model", fileNameCount, znodeProductFeed, portalidSrt, feedType });
                fileNameCount = GetFileNameCount(model, fileNameCount, znodeProductFeed, portalidSrt, feedType);

                //Show error if XML for feed or site map is not generated.
                if (Equals(fileNameCount, 0))
                {
                    generatedXml.ErrorMessage = Admin_Resources.ErrorGeneratingFeed;
                    return generatedXml;
                }     
            }

            string siteMapFile = string.Empty;
            //Create XML file if count more than 0.  FileCount will only be more than 0 if XML is generated for Feed or Site Map.
            if (fileNameCount > 0)
                siteMapFile = znodeProductFeed.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.FileName, Convert.ToString(model.ProductFeedPriority));

            //File is saved properly in folder location then send that file name along with domain name to be downloaded by Admin user.
            if (!string.IsNullOrEmpty(siteMapFile))
            {
                generatedXml.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = $"{domainName}{model.SuccessXMLGenerationMessage.Replace("~", string.Empty)}";
                model.Stores = portalidSrt;

                //Set product feed master details.
                SetProductFeedMasterDetails(model);

                if (!model.IsFromScheduler)
                    _productFeedRepository.Insert(model.ToEntity<ZnodeProductFeed>());
            }
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }

        //Get list of product feed.
        public virtual ProductFeedListModel GetProductFeedList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("pageListModel to generate ProductFeedModel list ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            IZnodeViewRepository<ProductFeedModel> objStoredProc = new ZnodeViewRepository<ProductFeedModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            IList<ProductFeedModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetProductFeedDetailsList @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("ProductFeedModel list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, list?.Count());
            ProductFeedListModel listModel = new ProductFeedListModel { ProductFeeds = list?.ToList() };

            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get ProductFeed by ProductFeed id.
        public virtual ProductFeedModel GetProductFeed(int productFeedId, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (productFeedId > 0)
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeProductFeedEnum.ProductFeedId.ToString(), FilterOperators.Equals, productFeedId.ToString()));

                return _productFeedRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClause(filters.ToFilterDataCollection()), GetExpands(expands))?.ToModel<ProductFeedModel>();
            }
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return null;
        }

        //Get product feed master table details.
        public ProductFeedModel GetProductFeedMasterDetails()
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new ProductFeedModel()
            {
                ProductFeedPriorityList = _productFeedPriorityRepository.GetEntityList(string.Empty)?.ToModel<ProductFeedPriorityModel>().ToList(),
                ProductFeedSiteMapTypeList = _productFeedSiteMapTypeRepository.GetEntityList(string.Empty)?.ToModel<ProductFeedSiteMapTypeModel>().ToList(),
                ProductFeedTypeList = _productFeedTypeRepository.GetEntityList(string.Empty)?.ToModel<ProductFeedTypeModel>().ToList(),
                ProductFeedTimeStampList = _productFeedTimeStampRepository.GetEntityList(string.Empty)?.ToModel<ProductFeedTimeStampModel>().ToList()
            };
        }

        //Update  Product Feed.
        public virtual bool UpdateProductFeed(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel,Admin_Resources.ModelNotNull);
            if (model.ProductFeedId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne,Admin_Resources.IdCanNotBeLessThanOne);

            ProductFeedModel generatedXml = new ProductFeedModel();
            string domainName = model.SuccessXMLGenerationMessage;
            string feedType = string.Empty;
            bool isFeed = false;

            //check if Feed is for Site map or Product
            CheckIsSiteMapOrProduct(model, out feedType, out isFeed);

            int fileNameCount = 0;
            ZnodeProductFeedHelper znodeProductFeed = new ZnodeProductFeedHelper();
            string portalidSrt = string.IsNullOrEmpty(model.Stores) ? string.Join(",", Array.ConvertAll(model.PortalId, x => x.ToString())) : model.Stores;
            ZnodeLogging.LogMessage("portalidSrt: ", ZnodeLogging.Components.Admin.ToString(),TraceLevel.Verbose, portalidSrt);
            if (isFeed)
                fileNameCount = znodeProductFeed.GetProductList(portalidSrt, this.strGoogleFeedNamespace, model.FileName, model.Title, model.Link, model.Description, feedType, model.LocaleId, model.ProductFeedTimeStampName, model.Date);
            else
            {
                //Call if Site map to be generated for Category/Content Page
                ZnodeLogging.LogMessage("Parameter for getting fileNameCount", ZnodeLogging.Components.Admin.ToString(),TraceLevel.Verbose, new object[] {" model", fileNameCount, znodeProductFeed, portalidSrt });
                fileNameCount = GetFileNameCount(model, fileNameCount, znodeProductFeed, portalidSrt);

                //Show error if XML for feed or site map is not generated.
                if (Equals(fileNameCount, 0))
                {
                    ZnodeLogging.LogMessage(Admin_Resources.ErrorGeneratingXml, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    return false;
                }
            }

            string siteMapFile = string.Empty;
            //Create XML file if count more than 0.  FileCount will only be more than 0 if XML is generated for Feed or Site Map.
            if (fileNameCount > 0)
                siteMapFile = znodeProductFeed.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.FileName, Convert.ToString(model.ProductFeedPriority));

            //File is saved properly in folder location then send that file name along with domain name to be downloaded by Admin user.
            if (!string.IsNullOrEmpty(siteMapFile))
            {
                generatedXml.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = $"{domainName}{model.SuccessXMLGenerationMessage.Replace("~", string.Empty)}";
                model.Stores = portalidSrt;

            }

            //Set product feed master details.
            SetProductFeedMasterDetails(model);
            bool isProductFeedUpdated = false;
            if (!model.IsFromScheduler)
                isProductFeedUpdated = _productFeedRepository.Update(model.ToEntity<ZnodeProductFeed>());

            ZnodeLogging.LogMessage(isProductFeedUpdated ? Admin_Resources.SuccessProductFeedUpdation : Admin_Resources.ErrorProductFeedUpdation, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return isProductFeedUpdated;
        }

        //Delete product feed  by product feed ids.
        public virtual bool DeleteProductFeed(ParameterModel productFeedIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(productFeedIds?.Ids))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.IdCanNotBeLessThanOne);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeProductFeedEnum.ProductFeedId.ToString(), ProcedureFilterOperators.In, productFeedIds?.Ids?.ToString()));
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return _productFeedRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
        }
        #endregion

        #region Private Methods
        //Get expands and add them to navigation properties
        private List<string> GetExpands(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (IsNotNull(expands) && expands.HasKeys())
            {
                foreach (string key in expands.Keys)
                {
                    if (Equals(key, ZnodeProductFeedEnum.ZnodeProductFeedType.ToString().ToLower())) SetExpands(ZnodeProductFeedEnum.ZnodeProductFeedType.ToString(), navigationProperties);
                    if (Equals(key, ZnodeProductFeedEnum.ZnodeProductFeedPriority.ToString().ToLower())) SetExpands(ZnodeProductFeedEnum.ZnodeProductFeedPriority.ToString(), navigationProperties);
                    if (Equals(key, ZnodeProductFeedEnum.ZnodeProductFeedSiteMapType.ToString().ToLower())) SetExpands(ZnodeProductFeedEnum.ZnodeProductFeedSiteMapType.ToString(), navigationProperties);
                    if (Equals(key, ZnodeProductFeedEnum.ZnodeProductFeedType.ToString().ToLower())) SetExpands(ZnodeProductFeedEnum.ZnodeProductFeedType.ToString(), navigationProperties);
                    if (Equals(key, ZnodeProductFeedEnum.ZnodeProductFeedTimeStamp.ToString().ToLower())) SetExpands(ZnodeProductFeedEnum.ZnodeProductFeedTimeStamp.ToString(), navigationProperties);
                }
            }
            return navigationProperties;
        }

        //Set Product Feed Type Code Details.
        private void GetProductFeedTypeCodeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTypeCode", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedTypeCode);
            if (IsNotNull(model))
            {
                ZnodeProductFeedType productFeedType = _productFeedTypeRepository.Table.Where(w => w.ProductFeedTypeCode == model.ProductFeedTypeCode)?.FirstOrDefault();
                model.ProductFeedTypeId = IsNotNull(productFeedType) ? productFeedType.ProductFeedTypeId : 0;
            }
        }

        //Set Product Feed Time Stamp Code Details.
        private void GetProductFeedTimeStampCodeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTimeStampName", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedTimeStampName);
            if (IsNotNull(model))
            {
                ZnodeProductFeedTimeStamp productFeedTimeStamp = _productFeedTimeStampRepository.Table.Where(w => w.ProductFeedTimeStampName == model.ProductFeedTimeStampName)?.FirstOrDefault();
                model.ProductFeedTimeStampId = IsNotNull(productFeedTimeStamp) ? productFeedTimeStamp.ProductFeedTimeStampId : 0;
            }
        }

        //Set Product Feed Site Map Type Details.
        private void GetProductFeedSiteMapTypeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedSiteMapTypeCode", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedSiteMapTypeCode);
            if (IsNotNull(model))
            {
                ZnodeProductFeedSiteMapType productFeedSiteMapType = _productFeedSiteMapTypeRepository.Table.Where(w => w.ProductFeedSiteMapTypeCode == model.ProductFeedSiteMapTypeCode)?.FirstOrDefault();
                model.ProductFeedSiteMapTypeId = IsNotNull(productFeedSiteMapType) ? productFeedSiteMapType.ProductFeedSiteMapTypeId : 0;
            }
        }

        //Set Product Feed Priority Details.
        private void GetProductFeedPriorityDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedPriority", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedPriority);
            if (IsNotNull(model))
            {
                ZnodeProductFeedPriority productFeedPriority = _productFeedPriorityRepository.Table.Where(w => w.ProductFeedPriority == model.ProductFeedPriority)?.FirstOrDefault();
                model.ProductFeedPriorityId = IsNotNull(productFeedPriority) ? productFeedPriority.ProductFeedPriorityId : 0;
            }
        }

        //Set product feed master details.
        private void SetProductFeedMasterDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("SetProductFeedMasterDetails method execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            GetProductFeedPriorityDetails(model);
            GetProductFeedSiteMapTypeDetails(model);
            GetProductFeedTimeStampCodeDetails(model);
            GetProductFeedTypeCodeDetails(model);
        }

        //Create Sitemap files and return count of files created.
        private int GetFileNameCount(ProductFeedModel model, int fileNameCount, ZnodeProductFeedHelper znodeProductFeed, string portalidSrt, string feedType = "")
        {
            switch (model.ProductFeedSiteMapTypeCode.ToLower())
            {
                case ItemCategory:
                    fileNameCount = znodeProductFeed.GetCategoryList(portalidSrt, model, ItemUrlset, this.strNamespace);
                    break;
                case ItemContentPages:
                    fileNameCount = znodeProductFeed.GetContentPagesList(portalidSrt, ItemUrlset, this.strNamespace, model);
                    break;
                case ItemProduct:
                    fileNameCount = znodeProductFeed.GetProductXMLList(portalidSrt, this.strNamespace, feedType, ItemUrlset, model);
                    break;
                case ItemAll:
                    fileNameCount = znodeProductFeed.GetAllList(model, portalidSrt, ItemUrlset, this.strNamespace, feedType);
                    break;
                default:
                    break;
            }
            ZnodeLogging.LogMessage("fileNameCount:", ZnodeLogging.Components.Admin.ToString(),TraceLevel.Verbose, fileNameCount);
            return fileNameCount;
        }

        //
        private void CheckIsSiteMapOrProduct(ProductFeedModel model, out string feedType, out bool isFeed)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTypeCode", ZnodeLogging.Components.Admin.ToString(),TraceLevel.Verbose, model.ProductFeedTypeCode);
            feedType = string.Empty;
            isFeed = false;
            if (!model.ProductFeedTypeCode.ToLower().Equals("xmlsitemap"))
            {
                feedType = model.ProductFeedTypeCode;
                isFeed = true;
            }
        }
        #endregion
    }
}
