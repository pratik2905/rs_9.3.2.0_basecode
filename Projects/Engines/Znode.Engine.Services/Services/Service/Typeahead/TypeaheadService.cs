﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;
using System.Collections.Generic;
namespace Znode.Engine.Services
{
    public class TypeaheadService : BaseService, ITypeaheadService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodePimCatalog> _pimCatalogRepository;

        #endregion

        #region Constructor
        public TypeaheadService()
        {
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _pimCatalogRepository = new ZnodeRepository<ZnodePimCatalog>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the suggestions of typeahead.
        /// </summary>
        /// <param name="typeaheadreqModel">Typeahead Request model.</param>
        /// <returns>Returns suggestions list.</returns>
        public virtual TypeaheadResponselistModel GetTypeaheadList(TypeaheadRequestModel typeaheadreqModel)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            switch (typeaheadreqModel.Type)
            {
                case ZnodeTypeAheadEnum.StoreList:
                    return GetStoreList();
                case ZnodeTypeAheadEnum.CatalogList:
                    return GetCatologList(typeaheadreqModel);
                case ZnodeTypeAheadEnum.PIMCatalogList:
                    return GetPIMCatologList();
            }
            return null;
        }

        //Get the store list according to logged in user id.
        private TypeaheadResponselistModel GetStoreList()
        {
            TypeaheadResponselistModel typeaheadResponselistModel = new TypeaheadResponselistModel();
            int userId = GetLoginUserId();
            List<ZnodeUserPortal> userPortals = _userPortalRepository.Table.Where(x => x.UserId == userId).ToList();
            if (HelperUtility.IsNotNull(userPortals) && userPortals.Count > 0)
            {
                if (userPortals.Any(x => x.PortalId == null))
                {
                    typeaheadResponselistModel.Typeaheadlist = (from n in _portalRepository.Table
                                                                select new TypeaheadResponseModel
                                                                {
                                                                    Id = n.PortalId,
                                                                    Name = n.StoreName,
                                                                    DisplayText = n.StoreName
                                                                }).OrderBy(s => s.Name).ToList();
                }
                else
                {
                    typeaheadResponselistModel.Typeaheadlist = (from n in _portalRepository.Table
                                                                join portalId in userPortals.Select(z => z.PortalId).ToList() on n.PortalId equals portalId
                                                                select new TypeaheadResponseModel
                                                                {
                                                                    Id = n.PortalId,
                                                                    Name = n.StoreName,
                                                                    DisplayText = n.StoreName
                                                                }).OrderBy(s => s.Name).ToList();
                }
            }
            return typeaheadResponselistModel;
        }

        //Get published catalog list
        private TypeaheadResponselistModel GetCatologList(TypeaheadRequestModel typeaheadreqModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            FilterCollection filters = null;

            ZnodeLogging.LogMessage("TypeaheadRequestModel to get TypeaheadResponselistModel: ", string.Empty, TraceLevel.Verbose, typeaheadreqModel);
            //to bind filter by fieldname in where clause
            if (!string.IsNullOrEmpty(typeaheadreqModel.FieldName))
            {
                filters = new FilterCollection { new FilterTuple(typeaheadreqModel.FieldName, FilterOperators.Is, typeaheadreqModel.FieldName) };
            }
            //for getting the published category list
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return new TypeaheadResponselistModel
            {
                Typeaheadlist = (from n in _publishCatalogRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter
                                 (filters.ToFilterDataCollection())?.WhereClause)
                                 select new TypeaheadResponseModel
                                 {
                                     Id = n.PublishCatalogId,
                                     Name = n.CatalogName,
                                     DisplayText = n.CatalogName
                                 }).OrderBy(p => p.Name).ToList()
            };
        }

        //Get PIM catalog list
        private TypeaheadResponselistModel GetPIMCatologList() => new TypeaheadResponselistModel
        {
            Typeaheadlist = (from n in _pimCatalogRepository.Table
                             select new TypeaheadResponseModel
                             {
                                 Id = n.PimCatalogId,
                                 Name = n.CatalogName,
                                 DisplayText = n.CatalogName
                             }).OrderBy(p => p.Name).ToList()
        };
        #endregion
    }
}
