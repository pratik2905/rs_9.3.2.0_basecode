﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.V2;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;

namespace Znode.Engine.Services
{
    public class PublishProductServiceV2 : PublishProductService, IPublishProductServiceV2
    {
        #region Private Variables
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private readonly IPublishProductHelper _publishProductHelper;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        #endregion

        #region Constructor
        public PublishProductServiceV2()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            _publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
        }
        #endregion

        #region Public Methods
        public virtual PublishProductListModel GetPublishProductsByAttribute(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);
            ZnodeLogging.LogMessage("catalogId, portalId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, portalId, localeId });

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

            //get catalog current version id by catalog id.
            if (catalogId > 0)
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));
            else
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, GetCatalogAllVersionIds());

            if (userId > 0 && EnableProfileBasedSearch(portalId))
            {
                int? userProfileId = GetUserProfileId(userId);
                filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
            }

            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            //Check if products are taken for some specific category.
            SetProductIndexFilter(filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            //get publish products from mongo
            List<ProductEntity> publishProducts = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicElemMatchWhereClause(filters.ToFilterMongoCollection(), ZnodeConstant.MongoAttribute));
            
            PublishProductListModel publishProductListModel = new PublishProductListModel
            {
                PublishProducts = FilterProductsByAttribute(publishProducts, filters.Where(x => x.FilterName.Contains(ZnodeConstant.MongoAttribute)).Select(y => y).ToList())
            };

            publishProductListModel.PublishProducts = Equals(pageListModel.PagingLength, int.MaxValue) ? publishProductListModel.PublishProducts.ToList()
                                                                                                       : publishProductListModel.PublishProducts.Take(pageListModel.PagingLength).ToList();

            if (publishProductListModel?.PublishProducts?.Count > 0)
                _publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());

            if (HelperUtility.IsNotNull(expands[ZnodeConstant.Pricing]))
            {
                foreach (PublishProductModel product in publishProductListModel.PublishProducts)
                {
                    string productType = product.Attributes.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
                    BindAssociatedProductDetails(product, product.PublishProductId, product.PublishedCatalogId, portalId, localeId, productType);
                }
            }

            ZnodeLogging.LogMessage("PublishProducts and locale list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { publishProductListModel?.PublishProducts, publishProductListModel?.Locale });
            publishProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishProductListModel;
        }

        //Get price for products through ajax async call. 
        public virtual ProductInventoryPriceListModel GetProductPriceV2(ParameterInventoryPriceModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNotNull(parameter))
            {
                switch (parameter.ProductType)
                {
                    //Get price for group products.
                    case ZnodeConstant.GroupedProduct:
                        PageListModel pageListModel = GetPageModel(parameter.Parameter, parameter.CatalogId);
                        int total = 0;
                        ZnodeLogging.LogMessage("MongoWhereClause and MongoOrderBy to get products list: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { pageListModel?.MongoWhereClause, pageListModel?.MongoOrderBy });
                        List<ProductEntity> productList = _publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, 1, 10, out total);
                        ZnodeLogging.LogMessage("Products list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, productList?.Count);
                        if (productList?.Count > 0)
                            return GetAsyncGroupProductPrice(productList, parameter.LocaleId, parameter.PortalId);
                        break;
                    //Get price for configurable products.
                    case ZnodeConstant.ConfigurableProduct:
                        PageListModel pageModel = GetPageModel(parameter.Parameter, parameter.CatalogId);
                        int totalRow = 0;
                        ZnodeLogging.LogMessage("MongoWhereClause and MongoOrderBy to get products list: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { pageModel?.MongoWhereClause, pageModel?.MongoOrderBy });
                        List<ProductEntity> products = _publishProductHelper.GetPublishProductList(pageModel.MongoWhereClause, pageModel.MongoOrderBy, 0, 0, out totalRow);
                        ZnodeLogging.LogMessage("Products list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, products?.Count);

                        if (products?.Count > 0)
                            return GetAsyncConfigurableProductPrice(products, parameter.CatalogId, parameter.LocaleId, parameter.PortalId);
                        break;
                    //Get price for Simple and Bundle products.
                    default:
                        return new ProductInventoryPriceListModel { ProductList = MapProductPrice(_publishProductHelper.GetPricingBySKUs(parameter.Parameter.Split(','), PortalId, GetLoginUserId(), GetProfileId())) };
                }
            }

            return new ProductInventoryPriceListModel();
        }

        public virtual PublishProductModelV2 GetPublishProductV2(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int catalogId, portalId, localeId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, publishProductId.ToString());

            PublishProductModel publishProduct = null;
            //Get publish product from mongo
            ZnodeLogging.LogMessage("publishProductId to get published product list from mongo: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, publishProductId.ToString());
            List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));
            ZnodeLogging.LogMessage("Published products list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, products?.Count);
            foreach (ProductEntity product in products ?? new List<ProductEntity>())
            {
                CategoryEntity category = _categoryMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(new FilterMongoCollection() { new FilterMongoTuple(FilterKeys.ZnodeCategoryId, FilterOperators.Equals, product.ZnodeCategoryIds.ToString()) }));
                
                publishProduct = product.ToModel<PublishProductModel>();
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                ZnodeLogging.LogMessage("catalogId, portalId, localeId to get data from expands: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, portalId, localeId });
                _publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), GetCatalogVersionId(catalogId).GetValueOrDefault(), null, GetProfileId());
                GetProductImagePath(portalId, publishProduct);
                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishProduct.ToEntity<PublishProductModelV2>();
        }

        //Get the list of published products for V2 Apis
        public virtual PublishProductListModel GetPublishProductListV2(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page, string requiredAttrFilter = "")
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);
            ZnodeLogging.LogMessage("catalogId, portalId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, portalId, localeId });

            int.TryParse(filters.Where(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out userId);

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

            //get catalog current version id by catalog id.
            if (catalogId > 0)
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));
            else
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, GetCatalogAllVersionIds());

            if (userId > 0 && EnableProfileBasedSearch(portalId))
            {
                int? userProfileId = GetUserProfileId(userId);
                filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
            }
            //Get filter value
            string filterValue = filters.FirstOrDefault(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In)?.FilterValue;

            if (!string.IsNullOrEmpty(filterValue))
            {
                //Remove Payment Setting Filters with IN operator from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In);

                //Add Payment Setting Filters
                filters.Add(FilterKeys.AttributeValuesForPromotion, FilterOperators.In, filterValue.Replace('_', ','));
            }

            if (filters.Any(w => w.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower()))
            {
                //Remove ZnodeCategoryId Filters from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower());
                filters.Add(FilterKeys.ZnodeCategoryId, FilterOperators.In, GetActiveCategoryIds(catalogId));
            }
            //Replace filter keys with mongo filter keys
            ReplaceFilterKeys(ref filters);

            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            //Check if products are taken for some specific category.
            SetProductIndexFilter(filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            //get publish products from mongo
            ZnodeLogging.LogMessage("pageListModel to get publish products list from mongo: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<ProductEntity> publishProducts = _publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
           
            PublishProductListModel publishProductListModel = new PublishProductListModel()
            {
                PublishProducts = Equals(pageListModel.PagingLength, int.MaxValue) ? publishProducts.ToModel<PublishProductModel>().ToList()
                                                                                   : publishProducts.Take(pageListModel.PagingLength).ToModel<PublishProductModel>().ToList()
            };

            if (!string.IsNullOrEmpty(requiredAttrFilter))
            {
                string[] requiredAttributesArr = requiredAttrFilter.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (PublishProductModel item in publishProductListModel.PublishProducts)
                {
                    item.Attributes = item.Attributes.Where(x => requiredAttributesArr.Any(y => y == x.AttributeCode.ToLower())).ToList();
                }
            }

            if (publishProductListModel?.PublishProducts?.Count > 0)
                _publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());

            if (!Equals(expands[ZnodeConstant.Pricing], null))
            {
                foreach (PublishProductModel product in publishProductListModel.PublishProducts)
                {
                    string productType = product.Attributes.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
                    BindAssociatedProductDetails(product, product.PublishProductId, product.PublishedCatalogId, portalId, localeId, productType);
                }
            }
            publishProductListModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("PublishProducts and locale list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { publishProductListModel?.PublishProducts, publishProductListModel?.Locale });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishProductListModel;
        }

        public virtual PublishProductModel GetPublishProductBySkuV2(ParameterProductModel model, NameValueCollection expands, FilterCollection filters)
        {
            int catalogId, portalId, localeId;

            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            string configurableProductSKU = model.ParentProductSKU;

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId, localeId);

            //Get publish product from mongo
            PublishProductModel publishProduct = _publishProductHelper.GetPublishProductBySKU(model.SKU, catalogId, localeId, catalogVersionId)?.ToModel<PublishProductModel>();

            if (model.ParentProductId > 0 && !string.IsNullOrEmpty(configurableProductSKU) && HelperUtility.IsNotNull(publishProduct))
            {
                publishProduct.ConfigurableProductSKU = model.SKU;
                publishProduct.ConfigurableProductId = model.ParentProductId;
                publishProduct.SKU = configurableProductSKU;
            }
            else if (HelperUtility.IsNull(publishProduct))
            {
                return null;
            }

            //Get expands associated to Product
            _publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, "", GetLoginUserId());

            publishProduct.AssociatedProducts = publishProduct.AssociatedProducts.DistinctBy(x => x.SKU).ToList();

            return publishProduct;
        }

        #endregion

        #region Private Methods
        private List<PublishProductModel> FilterProductsByAttribute(List<ProductEntity> publishedProducts, IList<FilterTuple> attribFilters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if ((attribFilters.Count % 2).Equals(0))
            {
                for (int i = 0, j = 1; i < attribFilters.Count(); i += 2, j += 2)
                {
                    if (publishedProducts?.Count > 0)
                    {
                        publishedProducts = publishedProducts.Where(x => x.Attributes
                                                                          .Any(y => y.AttributeCode.IndexOf(attribFilters[i].FilterValue, StringComparison.InvariantCultureIgnoreCase) >= 0
                                                                                    && ValueFilter(y, attribFilters[j].FilterValue, attribFilters[j].FilterOperator)))
                                                                          .Select(z => z).ToList();
                    }
                }
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishedProducts.ToModel<PublishProductModel>().ToList();
        }

        private bool ValueFilter(AttributeEntity model, string filterValue, string filterOperator)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            bool result = false;

            if (model.SelectValues.Count > 0)
            {
                switch (model.AttributeTypeName)
                {
                    case "Simple Select":
                        result = model.SelectValues.Any(x => string.Equals(x.Code, filterValue, StringComparison.InvariantCultureIgnoreCase));
                        break;
                    case "Multi Select":
                        string[] filterValues = filterValue.Split(new char[] { ';' });
                        foreach (string filter in filterValues)
                        {
                            if ((filterOperator == FilterOperators.Contains && model.SelectValues.Any(x => string.Equals(x.Code, filter, StringComparison.InvariantCultureIgnoreCase))
                                 || (filterOperator == FilterOperators.NotContains && !model.SelectValues.Any(x => string.Equals(x.Code, filter, StringComparison.InvariantCultureIgnoreCase)))))
                                return true;
                        }

                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
            }
            else
                result = model.AttributeValues.IndexOf(filterValue, StringComparison.InvariantCultureIgnoreCase) >= 0;

            return result;
        }
        #endregion
    }
}