﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public static class MongoHelper
    {
        #region Public Methods
        //Create Mongo indexes on Entity Type. 
        public static void EnsureMongoIndex(MongoEntityTypeModel mongoEntityTypeModel)
        {
            if (mongoEntityTypeModel != null)
            {
                ZnodeLogging.LogMessage($"Execution started for {mongoEntityTypeModel.EntityType}", ZnodeLogging.Components.API.ToString(), TraceLevel.Info, mongoEntityTypeModel.EntityType, "EnsureMongoIndex");
                List<MongoIndexModel> mongoIndexList = GetMongoIndexList(mongoEntityTypeModel.EntityType);
                mongoIndexList?.ForEach(indexDetail =>
                {
                    if (!string.IsNullOrEmpty(indexDetail.CollectionName) || !string.IsNullOrEmpty(indexDetail.DelimitedColumns))
                    {
                        MongoIndexHelper.EnsureIndex(indexDetail.CollectionName, indexDetail.DelimitedColumns, indexDetail.IsAscending, indexDetail.IsCompoundIndex);
                    }
                });

                ZnodeLogging.LogMessage($"Execution done for {mongoEntityTypeModel.EntityType}", ZnodeLogging.Components.API.ToString(), TraceLevel.Info, mongoEntityTypeModel.EntityType, "EnsureMongoIndex");
            }
        }
        #endregion

        #region Private Methods
        //Get Mongo Indexes Defination.
        private static List<MongoIndexModel> GetMongoIndexList(string entityType)
        {
            List<MongoIndexModel> mongoIndexList = null;
            if (!string.IsNullOrEmpty(entityType))
            {
                IZnodeRepository<ZnodeMongoIndex> _mongoIndex = new ZnodeRepository<ZnodeMongoIndex>();
                mongoIndexList = (from mongoIndex in _mongoIndex.Table
                                  where mongoIndex.EntityType == entityType
                                  select mongoIndex).ToModel<MongoIndexModel>()?.ToList();
            }
            return mongoIndexList;
        }
        #endregion

    }
}
