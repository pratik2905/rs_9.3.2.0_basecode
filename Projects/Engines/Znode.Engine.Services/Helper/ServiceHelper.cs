﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Znode.Engine.Api.Models;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using System.Reflection;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public static class ServiceHelper
    {
        #region Constants
        public const string DateTimeRange = "DateTimeRange";
        public const string IsCatalogFilter = "IsCatalogFilter";
        #endregion

        public static void BindPageListModel(this BaseListModel baseListModel, PageListModel pageListModel)
        {
            if (IsNotNull(pageListModel))
            {
                baseListModel.TotalResults = pageListModel.TotalRowCount;
                baseListModel.PageIndex = pageListModel.PagingStart;
                baseListModel.PageSize = pageListModel.PagingLength;
            }
        }

        //Format price according to global setting.
        public static string FormatPriceWithCurrency(decimal? price, string CultureName)
        {
            string currencyValue;
            if (IsNotNull(CultureName))
            {
                CultureInfo info = new CultureInfo(CultureName);
                info.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(DefaultGlobalConfigSettingHelper.DefaultPriceRoundOff);
                currencyValue = $"{price.GetValueOrDefault().ToString("c", info.NumberFormat)}";
            }
            else
                currencyValue = Convert.ToString(price);

            return currencyValue;
        }
        /// <summary>
        /// This method will invoke IsCodeExists method based on service name
        /// </summary>
        /// <param name="parameterModel">Parameter Model</param>
        /// <param name="className">Service name</param>
        /// <returns>boolean value</returns>
        public static bool ExecuteFunctionByName(HelperParameterModel parameterModel, string className, string methodName, string namespaceName = "", string projectName = "")
        {
            namespaceName = string.IsNullOrEmpty(namespaceName) ? "Znode.Engine.Services" : namespaceName;
            projectName = string.IsNullOrEmpty(projectName) ? "Znode.Engine.Services" : projectName;
            //Get Class Reference
            Type connector = Type.GetType(string.Concat(namespaceName, ".", className, ",", projectName));

            //Get Method of that class
            MethodInfo methodInfo = connector.GetMethod(methodName);
            object result = null;
            if (methodInfo != null)
            {
                //Create instance of that class
                object classInstance = Activator.CreateInstance(connector, null);
                //set parameter for method
                object[] parametersArray = new object[] { parameterModel };
                //Invoke method of that class
                result = methodInfo.Invoke(classInstance, parametersArray);

            }
            return (bool)result;
        }
        /// <summary>
        /// This method will round off inventory quantity based on Global Setting
        /// </summary>
        /// <param name="Quantity">Quantity</param>
        /// <returns></returns>
        public static string ToInventoryRoundOff(decimal Quantity)
          => GetInventoryRoundOff(Convert.ToString(Quantity));
        /// <summary>
        /// Get Inventory Round off by quantity
        /// </summary>
        /// <param name="Quantity"></param>
        /// <returns></returns>
        public static string GetInventoryRoundOff(string Quantity)
        {
            string roundOff = DefaultGlobalConfigSettingHelper.DefaultInventoryRoundOff;
            decimal size = Convert.ToDecimal(Quantity);
            return Convert.ToString(Math.Round((size), Convert.ToInt32(roundOff), MidpointRounding.AwayFromZero));
        }

        /// <summary>
        /// Adds date time value in filter collection against created date column.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <returns>returns filters</returns>
        public static FilterCollection AddDateTimeValueInFilter(FilterCollection filters)
        {           
            if(filters?.Count > 0)
            {
                string _dateTimeRange = filters.FirstOrDefault(x => string.Equals(x.FilterName, DateTimeRange, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
                //Update filters if _dateTimeRange is not null.
                if (_dateTimeRange != null && _dateTimeRange != string.Empty)
                {
                    filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.CreatedDate, StringComparison.InvariantCultureIgnoreCase));
                    filters.Add(new FilterTuple(FilterKeys.CreatedDate, FilterOperators.Between, _dateTimeRange));
                    filters.RemoveAll(x => string.Equals(x.FilterName, DateTimeRange, StringComparison.InvariantCultureIgnoreCase));
                }
            }           
            return filters;
        }

        //Get catalog filter values from FilterCollection.
        public static int GetCatalogFilterValues(FilterCollection filters, ref bool isCatalogFilter)
        {
            int pimCatalogId = 0;
            if (filters.Any(x => string.Equals(x.FilterName, FilterKeys.CatalogId, StringComparison.CurrentCultureIgnoreCase)))
            {
                Int32.TryParse(filters.FirstOrDefault(x => string.Equals(x.FilterName, FilterKeys.CatalogId, StringComparison.CurrentCultureIgnoreCase))?.FilterValue, out pimCatalogId);
                filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.CatalogId, StringComparison.InvariantCultureIgnoreCase));
            }
            if (filters.Any(x => string.Equals(x.FilterName, IsCatalogFilter, StringComparison.CurrentCultureIgnoreCase)))
            {
                isCatalogFilter = Convert.ToBoolean(filters.FirstOrDefault(x => string.Equals(x.FilterName, IsCatalogFilter, StringComparison.InvariantCultureIgnoreCase))?.FilterValue);
                filters.RemoveAll(x => x.FilterName.Equals(IsCatalogFilter, StringComparison.InvariantCultureIgnoreCase));
            }
            return pimCatalogId;
        }
    }
}
