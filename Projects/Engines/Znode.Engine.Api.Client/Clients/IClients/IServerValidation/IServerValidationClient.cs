﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
   public interface IServerValidationClient : IBaseClient
    {
        Dictionary<string, string> validateControl(ValidateServerModel model);
    }
}
