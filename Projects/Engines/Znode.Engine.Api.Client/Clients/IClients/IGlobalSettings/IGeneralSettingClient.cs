﻿using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public interface IGeneralSettingClient : IBaseClient
    {
        /// <summary>
        /// Method to get list of all GeneralSettings.
        /// </summary>
        /// <returns>model contains list of general settings</returns>
        GeneralSettingModel list();

        /// <summary>
        /// Method to ppdate existing GeneralSettings.
        /// </summary>
        /// <param name="generalSettingModel"></param>
        /// <returns>true/false</returns>
        bool Update(GeneralSettingModel generalSettingModel);

        /// <summary>
        /// Get filtered list for PublishState - ApplicationType mapping.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PublishStateMappingListModel GetPublishStateMappingList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method to get Cache Management data 
        /// </summary>
        /// <returns>model containing cache management data</returns>
        CacheListModel GetCacheData();

        /// <summary>
        /// Method to Create/Update cache data.
        /// </summary>
        /// <param name="cacheListModel">model to be updated</param>
        /// <returns>return true if updated or created successfully else return false.</returns>
        bool CreateUpdateCacheData(CacheListModel cacheListModel);

        /// <summary>
        /// Method to refresh cache data.
        /// </summary>
        /// <param name="cacheModel">model to be updated</param>
        /// <returns>refresh cachemodel</returns>
        CacheModel RefreshCacheData(CacheModel cacheModel);

        /// <summary>
        /// Method to Enable or Disable publish state to application type mapping.
        /// </summary>
        /// <param name="publishStateMappingId"></param>
        /// <param name="isEnabled"></param>
        /// <returns></returns>
        bool EnableDisablePublishStateMapping(int publishStateMappingId, bool isEnabled);
        /// <summary>
        /// Get the Load balace details
        /// </summary>
        /// <returns></returns>
        LBDetailsModel GetLBDetails();

        /// <summary>
        /// Update the Load Balance environment details
        /// </summary>
        /// <param name="lbDetailstModel"></param>
        /// <returns></returns>
        bool UpdateLBDetails(LBDetailsModel lbDetailstModel);
    }
}
