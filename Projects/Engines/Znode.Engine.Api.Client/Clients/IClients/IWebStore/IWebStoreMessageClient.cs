﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public interface IWebStoreMessageClient : IBaseClient
    {
        /// <summary>
        /// Get message by Message Key, Area and Portal Id passed in filters.
        /// </summary>
        /// <param name="expands">Expand Collection if any.</param>
        /// <param name="filters">Filter containing Message Key, Area and Portal Id.</param>
        /// <returns>Returns ManageMessageModel returning required message.</returns>
        ManageMessageModel GetMessage(ExpandCollection expands, FilterCollection filters);

        /// <summary>
        /// Get message by Area and Portal Id passed in filters.
        /// </summary>
        /// <param name="expands">Expand Collection if any.</param>
        /// <param name="filters">Filter containing Area and Portal Id.</param>
        /// <param name="localeId">Current Locale Id.</param>
        /// <returns>Returns ManageMessageListModel returning required messages.</returns>
        ManageMessageListModel GetMessages(ExpandCollection expands, FilterCollection filters, int localeId);
    }
}
