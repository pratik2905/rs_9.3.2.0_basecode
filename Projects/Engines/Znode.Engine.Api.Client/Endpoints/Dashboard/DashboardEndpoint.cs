﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class DashboardEndpoint : BaseEndpoint
    {
        //Gets the list of top products
        public static string GetDashboardTopProductList() => $"{ApiRoot}/dashboard/getdashboardtopproductlist";

        //Gets the list of top categories
        public static string GetDashboardTopCategoriesList() => $"{ApiRoot}/dashboard/getdashboardtopcategorieslist";

        //Gets the list of top brands
        public static string GetDashboardTopBrandsList() => $"{ApiRoot}/dashboard/getdashboardtopbrandslist";

        //Gets the list of top searches
        public static string GetDashboardTopSearchesList() => $"{ApiRoot}/dashboard/getdashboardtopsearcheslist";

        //Gets the list of total orders, total sales, total new customers, total average orders
        public static string GetDashboardSalesDetails() => $"{ApiRoot}/dashboard/getdashboardsalesdetails";

        //Gets the count of low inventory products
        public static string GetDashboardLowInventoryProductsCount() => $"{ApiRoot}/dashboard/getdashboardlowinventoryproductcount";
    }
}
