﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class TokenEndpoint : BaseEndpoint
    {
        public static string GenearteToken(string key) => $"{ApiRoot}/token/genaratetoken/{key}";

        public static string GenerateToken() => $"{ApiRoot}/token/genaratetoken";
    }
}
