﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class CatalogEndpoint : BaseEndpoint
    {
        //Get category list endpoint
        public static string GetCatalogList() => $"{ApiRoot}/catalogs";

        //Get category endpoint
        public static string Get(int pimCatalogId) => $"{ApiRoot}/catalog/{pimCatalogId}";

        //Create category endpoint
        public static string Create() => $"{ApiRoot}/catalog";

        //Update category endpoint
        public static string Update() => $"{ApiRoot}/catalog/update";

        //Copy category endpoint
        public static string CopyCatalog() => $"{ApiRoot}/catalog/copycatalog";

        //Delete category endpoint
        public static string Delete() => $"{ApiRoot}/catalog/delete";

        //created endpoint for getting category structure for tree.
        public static string GetCategoryTree() => $"{ApiRoot}/catalog/getcategorytree";

        //Get Assoicated Catalog Tree
        public static string GetCatalogCategoryTree(int pimProductId) => $"{ApiRoot}/catalog/getAssociatedCatalogTree/{pimProductId}";

        //created endpoint associte categories to catalog.
        public static string AssociateCategory() => $"{ApiRoot}/catalog/associatecategory";

        //Get category list endpoint
        public static string GetAssociatedCategoryList() => $"{ApiRoot}/catalogs/getassociatedcategorylist";

        //created endpoint unassocite categories to catalog.
        public static string UnAssociateCategory() => $"{ApiRoot}/catalog/unassociatecategory";

        //created endpoint unassocite products to catalog.
        public static string UnAssociateProduct() => $"{ApiRoot}/catalog/unassociateproduct";

        //Get product list endpoint
        public static string GetAssociatedProductList(int catalogId) => $"{ApiRoot}/catalogs/getassociatedproductlist/{catalogId}";

        //created endpoint associte products to catalog.
        public static string AssociateProducts() => $"{ApiRoot}/catalog/associateproducts";

        //created endpoint unassocite products to catalog.
        public static string UnAssociateProducts() => $"{ApiRoot}/catalog/unassociateproducts";

        //Get product list associated to category endpoint
        public static string GetCategoryAssociatedProducts() => $"{ApiRoot}/catalogs/getcategoryassociatedproducts";

        //Publish Catalog
        public static string Publish(int pimCatalogId, string revisionType) => $"{ApiRoot}/catalog/publish/{pimCatalogId}/{revisionType}";

        //Publish catalog category associated products.
        public static string PublishCategoryProducts(int pimCatalogId, int pimCategoryHierarchyId, string revisionType) => $"{ApiRoot}/catalog/publishcatalogcategoryproducts/{pimCatalogId}/{pimCategoryHierarchyId}/{revisionType}";

        //Get Associate Category endpoint
        public static string GetAssociateCategoryDetails() => $"{ApiRoot}/catalog/getassociatecategorydetails";

        //Update Associate Category endpoint
        public static string UpdateAssociateCategoryDetails() => $"{ApiRoot}/catalog/updateassociatecategorydetails";

        //Endpoint for moving folder to another folder.
        public static string MoveFolder() => $"{ApiRoot}/catalog/movecategory";

        //Get Catalog Publish Status list endpoint
        public static string GetCatalogPublishStatus() => $"{ApiRoot}/catalogs/getcatalogpublishstatus";

        //Update product associated to the catalog.
        public static string UpdateCatalogCategoryProduct() => $"{ApiRoot}/catalog/updatecatalogcategoryproduct";

    }
}
