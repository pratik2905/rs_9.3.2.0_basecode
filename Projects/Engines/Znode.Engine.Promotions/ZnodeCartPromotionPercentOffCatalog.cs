using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffCatalog : ZnodeCartPromotionType
    {
        #region Private Variables
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        #endregion

        #region Constructor
        public ZnodeCartPromotionPercentOffCatalog()
        {
            Name = "Percent Off Catalog";
            Description = "Applies a percent off products for a particular catalog; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.RequiredCatalog);
            Controls.Add(ZnodePromotionControl.RequiredCatalogMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off products for a particular catalog in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy);
            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all catalog of promotion by PromotionId
            List<CatalogModel> promotionsCatalog = promotionHelper.GetPromotionCatalogs(PromotionBag.PromotionId);

            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                bool isPromotionApplied = false;
                // Get the catalogs by product
                List<CatalogModel> productCatalogs = promotionHelper.GetCatalogByProduct(cartItem.Product.ProductID);

                foreach (CatalogModel promotion in promotionsCatalog)
                {
                    foreach (CatalogModel product in productCatalogs)
                    {
                        if (promotion.PimCatalogId == product.PimCatalogId)
                        {
                            ApplyDiscount(out isPromotionApplied, isCouponValid, couponIndex, cartItem);
                            // Break out of the catalogs loop
                            break;
                        }
                    }
                    if (isPromotionApplied)
                        // Break out of the category loop
                        break;
                }
            }
            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method
        private void ApplyDiscount(out bool isPromotionApplied, bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {
            decimal subTotal = GetCartSubTotal(ShoppingCart);
            decimal qtyOrdered = GetCartQuantity();
            bool discountApplied = false;

            ZnodePricePromotionManager pricePromoManager = new ZnodePricePromotionManager();
            decimal basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, false))
                {
                    isPromotionApplied = false;
                    return;

                }
                discountApplied = ApplyProductDiscount(couponIndex, subTotal, qtyOrdered, cartItem, basePrice);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, qtyOrdered, ShoppingCart.SubTotal, true))
                {
                    isPromotionApplied = false;
                    return;
                }

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        discountApplied = ApplyProductDiscount(couponIndex, subTotal, qtyOrdered, cartItem, basePrice, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }
            isPromotionApplied = discountApplied;
        }

        //to apply product discount
        private bool ApplyProductDiscount(int? couponIndex, decimal subTotal, decimal qtyOrdered, ZnodeShoppingCartItem cartItem, decimal basePrice, string couponCode = "")
        {
            bool discountApplied = false;
            if (PromotionBag.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal
                && cartItem.Product?.ZNodeConfigurableProductCollection.Count == 0
                && cartItem.Product?.ZNodeGroupProductCollection.Count == 0)
            {
                decimal discount = cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                cartItem.Product.DiscountAmount += discount;
                cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                discountApplied = true;
                SetPromotionalPriceAndDiscount(cartItem, discount);
            }
            else if (string.IsNullOrEmpty(couponCode) && cartItem.Product?.ZNodeConfigurableProductCollection.Count == 0
                && cartItem.Product?.ZNodeGroupProductCollection.Count == 0)
            {
                if (cartItem.Product.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                {
                    cartItem.Product.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                }
                cartItem.Product.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                discountApplied = true;
            }

            if (cartItem.Product?.ZNodeConfigurableProductCollection.Count > 0)
            {
                ApplyConfigurableProductDiscount(cartItem, qtyOrdered, subTotal, out discountApplied);
            }

            if (cartItem.Product?.ZNodeGroupProductCollection.Count > 0)
            {
                ApplyGroupProductDiscount(cartItem, qtyOrdered, subTotal, out discountApplied);
            }

            if (!string.IsNullOrEmpty(couponCode) && discountApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = true;
            }

            return discountApplied;
        }

        // to apply configurable product Discount
        private void ApplyConfigurableProductDiscount(ZnodeShoppingCartItem cartItem, decimal qtyOrdered, decimal subTotal, out bool discountApplied, string couponCode = "")
        {
            bool isDiscountApplied = false;
            decimal basePrice = cartItem.PromotionalPrice;
            foreach (ZnodeProductBaseEntity config in cartItem.Product.ZNodeConfigurableProductCollection)
            {
                if (PromotionBag.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    cartItem.Product.DiscountAmount += discount;
                    config.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), config.OrdersDiscount);
                    isDiscountApplied = true;
                    SetPromotionalPriceAndDiscount(cartItem, discount);
                }
                else
                {
                    if (config.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                    {
                        config.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    }
                    config.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    config.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), config.OrdersDiscount);
                    isDiscountApplied = true;
                }
            }
            discountApplied = isDiscountApplied;
        }

        // to apply group product Discount
        private void ApplyGroupProductDiscount(ZnodeShoppingCartItem cartItem, decimal qtyOrdered, decimal subTotal, out bool discountApplied, string couponCode = "")
        {
            bool isDiscountApplied = false;
            foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
            {
                decimal basePrice = group.FinalPrice;
                if (PromotionBag.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    group.DiscountAmount += discount;
                    group.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), group.OrdersDiscount);
                    isDiscountApplied = true;
                }
                else
                {
                    if (group.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                    {
                        group.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    }
                    group.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    group.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), group.OrdersDiscount);
                    isDiscountApplied = true;
                }
            }
            discountApplied = isDiscountApplied;
        }

        #endregion
    }
}
