﻿using System;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api.Models
{
    public class ERPTaskSchedulerModel : BaseModel
    {
        public int ERPTaskSchedulerId { get; set; }

        public string SchedulerName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.RequiredTouchPointName)]
        public string TouchPointName { get; set; }

        public string SchedulerFrequency { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? ExpireDate { get; set; }
        public int RepeatTaskEvery { get; set; }
        public string RepeatTaskForDuration { get; set; }
        public int RecurEvery { get; set; }
        public string WeekDays { get; set; }
        public string Months { get; set; }
        public string Days { get; set; }
        public string OnDays { get; set; }
        public bool IsRepeatTaskEvery { get; set; }
        public bool IsEnabled { get; set; }
        public int ERPConfiguratorId { get; set; }
        public int TaskSchedulerSettingId { get; set; }
        public bool IsMonthlyDays { get; set; }
        public string ExePath { get; set; }
        public string ExeParameters { get; set; }
        public string SchedulerParameters { get; set; }
        public int PortalId { get; set; }
        public string IndexName { get; set; }
        public int CatalogId { get; set; }

        public int CatalogIndexId { get; set; }
        public bool IsAssignTouchPoint { get; set; }
        public string SchedulerType { get; set; }
        public string SchedulerCallFor { get; set; }
        public string DomainName { get; set; }
    }
}
