﻿using System;

namespace Znode.Engine.Api.Models
{
    public class PromotionModel : BaseModel
    {
        public int PromotionId { get; set; }
        public string PromoCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? PromotionTypeId { get; set; }
        public string PromotionTypeName { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? OrderMinimum { get; set; }
        public decimal? QuantityMinimum { get; set; }
        public bool? IsCouponRequired { get; set; }
        public int? DisplayOrder { get; set; }
        public bool IsUnique { get; set; }
        public int? PortalId { get; set; }
        public int? ProfileId { get; set; }
        public decimal? PromotionProductQuantity { get; set; }
        public int? ReferralPublishProductId { get; set; }
        public string ProductName { get; set; }
        public string ReferralProductName { get; set; }
        public string Code { get; set; }
        public CouponListModel CouponList { get; set; }
        public string PromotionMessage { get; set; }
        public PromotionTypeModel PromotionType { get; set; }
        public bool PortalAllowsMultipleCoupon { get; set; }
        public bool IsAllowedWithOtherCoupons { get; set; }
        public string BrandCode { get; set; }

        public string AssociatedCatelogIds { get; set; }
        public string AssociatedCategoryIds { get; set; }
        public string AssociatedProductIds { get; set; }
        public string AssociatedBrandIds { get; set; }
        public string StoreName { get; set; }
        public string AssociatedShippingIds { get; set; }
        public bool IsAllowWithOtherPromotionsAndCoupons { get; set; } 

        public string GetClassName()
        {
            return PromotionType?.ClassName;
        }
    }
}
