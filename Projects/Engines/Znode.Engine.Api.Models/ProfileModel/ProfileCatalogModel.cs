﻿namespace Znode.Engine.Api.Models
{
    public class ProfileCatalogModel : BaseModel
    {
        public int ProfileCatalogId { get; set; }
        public int ProfileId { get; set; }
        public int PimCatalogId { get; set; }
        public string ProfileName { get; set; }
        public string CatalogName { get; set; }
        public string PimCatalogIds { get; set; }
        public string ProductIds { get; set; }
        public int PimCategoryId { get; set; }
        public int PimCategoryHierarchyId { get; set; }
    }
}
