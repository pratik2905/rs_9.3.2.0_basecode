﻿namespace Znode.Engine.Api.Models
{
    public class BlogNewsParameterModel
    {
        public string BlogNewsId { get; set; }

        public bool IsTrueOrFalse { get; set; }

        public string Activity { get; set; }
    }
}
