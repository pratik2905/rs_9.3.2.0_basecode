﻿namespace Znode.Engine.Api.Models
{
    public class PortalProfileShippingModel : BaseModel
    {
        public int ProfileId { get; set; }
        public int ProfileShippingId { get; set; }
        public int PortalShippingId { get; set; }
        public int PortalId { get; set; }
        public string ShippingIds { get; set; }
        public int DisplayOrder { get; set; }
        public string PublishState { get; set; }
    }
}
