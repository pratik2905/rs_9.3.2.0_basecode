﻿namespace Znode.Engine.Api.Models.Responses
{
    public class PortalDisplaySettingResponse : BaseResponse
    {
        public PortalDisplaySettingModel DisplaySetting { get; set; }
    }
}
