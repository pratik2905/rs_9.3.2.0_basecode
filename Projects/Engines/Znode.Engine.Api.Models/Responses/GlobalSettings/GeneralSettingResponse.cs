﻿namespace Znode.Engine.Api.Models.Responses
{
    public class GeneralSettingResponse : BaseResponse
    {
        public GeneralSettingModel GeneralSetting { get; set; }
        public CacheListModel CacheData { get; set; }
        public CacheModel Cache { get; set; }
        public LBDetailsModel LoadBalanceDetails { get; set; }
    }
}
