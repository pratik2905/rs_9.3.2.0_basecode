﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    public class PublishMongoProductModel
    {
        public int VersionId { get; set; }
        public long? PublishStartTime { get; set; }
        public string IndexId { get; set; }
        public int ZnodeProductId { get; set; }
        public int ZnodeCatalogId { get; set; }
        public string SKU { get; set; }
        public int LocaleId { get; set; }
        public string Name { get; set; }
        public int ZnodeCategoryIds { get; set; }
        public int[] ProfileIds { get; set; }
        public bool IsActive { get; set; }
        public List<PublishAttributeModel> Attributes { get; set; }
        public List<PublishMongoBrandModel> Brands { get; set; }
        public string CategoryName { get; set; }
        public string CatalogName { get; set; }
        public int DisplayOrder { get; set; }
        public string revisionType { get; set; }
        public int AssociatedProductDisplayOrder { get; set; }
        public int ProductIndex { get; set; }
        public string SalesPrice { get; set; }
        public string RetailPrice { get; set; }
        public string CultureCode { get; set; }
        public string CurrencySuffix { get; set; }
        public string CurrencyCode { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }
        public string ImageSmallPath { get; set; }
        public string SKULower { get; set; }
        public string TempProfileIds { get; set; }
    }
}
