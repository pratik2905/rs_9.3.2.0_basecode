﻿namespace Znode.Engine.Api.Models
{
    public class PublishMongoBrandModel
    {
        public int BrandId { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public int VersionId { get; set; }
        public long? PublishStartTime { get; set; }
    }
}
