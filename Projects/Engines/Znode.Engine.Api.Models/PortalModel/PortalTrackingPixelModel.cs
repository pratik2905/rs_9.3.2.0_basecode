﻿namespace Znode.Engine.Api.Models
{
    public class PortalTrackingPixelModel : BaseModel
    {
        public int PortalId { get; set; }
        public int PortalPixelTrackingId { get; set; }
        public string StoreName { get; set; }
        public string PixelId1 { get; set; }
        public string PixelId2 { get; set; }
        public string PixelId3 { get; set; }
        public string PixelId4 { get; set; }
        public string PixelId5 { get; set; }
        public string CodePixel1 { get; set; }
        public string CodePixel2 { get; set; }
        public string CodePixel3 { get; set; }
        public string CodePixel4 { get; set; }
        public string CodePixel5 { get; set; }
        public string HelpTextPixel1 { get; set; }
        public string HelpTextPixel2 { get; set; }
        public string HelpTextPixel3 { get; set; }
        public string HelpTextPixel4 { get; set; }
        public string HelpTextPixel5 { get; set; }
        public string DisplayName1 { get; set; }
        public string DisplayName2 { get; set; }
        public string DisplayName3 { get; set; }
        public string DisplayName4 { get; set; }
        public string DisplayName5 { get; set; }
    }
}
