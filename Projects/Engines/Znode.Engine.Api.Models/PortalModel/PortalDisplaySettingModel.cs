﻿using System.ComponentModel.DataAnnotations;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Models
{
    public class PortalDisplaySettingModel : BaseModel
    {
        public int PortalDisplaySettingsId { get; set; }
        public int? MediaId { get; set; }
        public int PortalId { get; set; }
        [Required]
        public int MaxDisplayItems { get; set; }
        [Required]
        public int MaxSmallThumbnailWidth { get; set; }
        [Required]
        public int MaxSmallWidth { get; set; }
        [Required]
        public int MaxMediumWidth { get; set; }
        [Required]
        public int MaxThumbnailWidth { get; set; }
        [Required]
        public int MaxLargeWidth { get; set; }
        [Required]
        public int MaxCrossSellWidth { get; set; }

        public string PortalName { get; set; }
        public string MediaPath { get; set; }
        public string PortalDefaultImageName { get; set; }

        public static PortalDisplaySettingModel GetDefaultDisplaySetting()
        {
            return new PortalDisplaySettingModel
            {
                MaxSmallThumbnailWidth = ZnodeConstant.MaxSmallThumbnailWidth,
                MaxLargeWidth = ZnodeConstant.MaxLargeWidth,
                MaxSmallWidth = ZnodeConstant.MaxSmallWidth,
                MaxThumbnailWidth = ZnodeConstant.MaxThumbnailWidth,
                MaxMediumWidth = ZnodeConstant.MaxMediumWidth,
                MaxCrossSellWidth = ZnodeConstant.MaxCrossSellWidth,
            };
        }
    }
}