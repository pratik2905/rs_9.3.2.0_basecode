﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class RecentViewProductModel : BrandModel
    {
        public int ZnodeProductId { get; set; }
        public int ZnodeCatalogId { get; set; }
        public int ZnodeCategoryIds { get; set; }
    }
}
