﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Api.Models
{
    public class ParameterInventoryPriceModel : BaseModel
    {
        /// <summary>
        /// This helps to pass in query parameter (Comma seperated string)
        /// </summary>
        [Required]
        public string Parameter { get; set; }
        public string ProductType { get; set; }
        public int PortalId { get; set; }
        public int LocaleId { get; set; }
        public int CatalogId { get; set; }

    }
}
