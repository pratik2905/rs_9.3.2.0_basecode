﻿

namespace Znode.Engine.Api.Models
{
   public class MongoEntityTypeModel : BaseModel
    {
        public string EntityType { get; set; }
        public int PortalId { get; set; }
        public int CatalogId { get; set; }
    }
}
