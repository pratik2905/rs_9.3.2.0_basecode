﻿using System;

namespace Znode.Engine.Api.Models
{
    public class GiftCardHistoryModel : BaseModel
    {
        public int GiftCardHistoryId { get; set; }
        public string GiftCardNumber { get; set; }
        public int GiftCardId { get; set; }
        public int OrderId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public string OrderNumber { get; set; }
    }
}
