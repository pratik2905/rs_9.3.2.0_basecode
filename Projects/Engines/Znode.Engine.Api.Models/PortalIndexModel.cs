﻿namespace Znode.Engine.Api.Models
{
    public class PortalIndexModel : BaseModel
    {
        public int PortalIndexId { get; set; }
        public int CatalogIndexId { get; set; }
        public int PortalId { get; set; }
        public string IndexName { get; set; }
        public string StoreName { get; set; }
        public int SearchCreateIndexMonitorId { get; set; }

        public int PublishCatalogId { get; set; }

        public string CatalogName { get; set; }

        public PortalModel ZnodePortal { get; set; }
        public string RevisionType { get; set; }
    }
}
