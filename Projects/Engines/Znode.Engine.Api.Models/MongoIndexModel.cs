﻿
namespace Znode.Engine.Api.Models
{
    public class MongoIndexModel : BaseModel
    {
        public int MongoIndexId { get; set; }
        public string CollectionName { get; set; }
        public string DelimitedColumns { get; set; }
        public bool IsAscending { get; set; }
        public bool IsCompoundIndex { get; set; }
        public string EntityType { get; set; }
    }
}
