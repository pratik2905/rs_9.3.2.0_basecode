﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReportCategoryListModel : BaseListModel
    {
        public List<ReportCategoryModel> ReportCategoryList { get; set; }
    }
}
