﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReportDiscountTypeModel
    {
        public int OmsDiscountTypeId{ get; set; }
        public string Name { get; set; }
    }
}
