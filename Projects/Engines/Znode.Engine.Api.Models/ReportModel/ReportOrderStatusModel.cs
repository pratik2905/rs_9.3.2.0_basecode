﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReportOrderStatusModel
    {
        public int OmsOrderStateId { get; set; }
        public string OrderStateName { get; set; }
    }
}
