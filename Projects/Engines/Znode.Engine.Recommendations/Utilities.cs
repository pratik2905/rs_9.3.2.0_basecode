﻿using System.Collections.Generic;

namespace Znode.Engine.Recommendations
{
    public static class Utilities
    {
        //To merge the dictionary.
        public static Dictionary<string, decimal?> Merge(params Dictionary<string, decimal?>[] dictionaries)
        {
            var mergedDict = new Dictionary<string, decimal?>();

            foreach (var dictionary in dictionaries)
                foreach (var pair in dictionary)
                {
                    if (!mergedDict.ContainsKey(pair.Key)) mergedDict[pair.Key] = 0;
                    mergedDict[pair.Key] += pair.Value;
                }

            return mergedDict;
        }
    }
}