﻿using System;
using System.Collections.Generic;
using Znode.Engine.Recommendations.Models;

namespace Znode.Engine.Recommendations
{
    public static class Mappers
    {
        /// <summary>
        /// Map list of orders to HistoricalContext model.
        /// </summary>
        /// <param name="orders">List of orders with SKU and Quantity of products.</param>
        /// <returns>HistoricalContext</returns>    
        public static HistoricalContext MapToHistoricalContext(List<List<Product>> orders)
        {
            if (orders == null)
                throw new ArgumentNullException("Argument 'orders' cannot be null.");

            var orderObjects = new List<Order>();

            var orderId = 1;
            //Map the order details to List of Order model of recommendation engine.
            foreach (var order in orders)
            {
                var orderObject = new Order { OrderId = orderId.ToString() };
                foreach (var item in order)
                {
                    //Set quantity for SKU.
                    orderObject.Products[item.SKU] = item.Quantity;
                }

                orderObjects.Add(orderObject);
                orderId++;
            }

            var context = new HistoricalContext
            {
                Customers = new List<Customer> { new Customer { Orders = orderObjects } }
            };

            return context;
        }
    }
}