﻿using System.Collections.Generic;
using Znode.Engine.Recommendations.Models;

namespace Znode.Engine.Recommendations
{
    public interface IRecommendationEngine
    {
        /// <summary>
        /// To get the recommended products for recommendation context passed.
        /// </summary>
        /// <param name="context">Recommendation context</param>
        /// <returns>Recommended products</returns>
        Recommendation GetProductRecommendations(RecommendationContext context);
    }
}
