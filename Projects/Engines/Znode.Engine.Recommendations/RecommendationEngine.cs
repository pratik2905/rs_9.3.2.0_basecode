﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Recommendations.Models;

namespace Znode.Engine.Recommendations
{
    public class RecommendationEngine : IRecommendationEngine
    {
        private readonly Dictionary<string, Dictionary<string, decimal?>> internalModel = new Dictionary<string, Dictionary<string, decimal?>>();

        public RecommendationEngine(HistoricalContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            if (context.Customers == null)
                throw new ArgumentNullException("context.Customers");                      

            var orders = new List<Order>();
            context.Customers.ForEach(customer => orders.AddRange(customer.Orders));
            internalModel = BuildInternalModel(orders.Select(order => order.Products.Keys.ToList()).ToList(), orders.Select(order => order.Products).ToList());
        }

        //To get the recommendations.
        public virtual Recommendation GetProductRecommendations(RecommendationContext recommendationContext)
        {
            if (recommendationContext == null)
            {
                throw new ArgumentNullException(nameof(recommendationContext));
            }                

            if (recommendationContext.RecentlyViewedProductSkus?.Count > 0) // To get recommendations for home page.
            {
                return _getIds(recommendationContext.RecentlyViewedProductSkus);
            }                

            if (!string.IsNullOrEmpty(recommendationContext.ProductSkuCurrentlyBeingViewed)) // To get recommendations for PDP page.
            {
                return _getIds(recommendationContext.ProductSkuCurrentlyBeingViewed);
            }                

            if (recommendationContext.ProductSkusInCart != null && recommendationContext.ProductSkusInCart.Count > 0) // To get recommendations for cart page.
            {
                return _getIds(recommendationContext.ProductSkusInCart);
            }               

            return new Recommendation();
        }       

        //To build the engine's internal m
        protected virtual Dictionary<string, Dictionary<string, decimal?>> BuildInternalModel(List<List<string>> orders, List<Dictionary<string, decimal?>> orderListWithQuantity)
        {
            //To store the unique SKU of product as key and the list of products(SKU and Quantity) ordered with the product stored as key.
            var model = new Dictionary<string, Dictionary<string, decimal?>>();

            // Build list of unique IDs:
            var ids = new List<string>();
            foreach (var order in orders) ids = ids.Union(order).ToList();

            // Build model:
            //TODO: Replace nested looping with Linq.
            foreach (var id in ids)
            {
                if (!model.ContainsKey(id)) model.Add(id, new Dictionary<string, decimal?>());
                var dict = model[id];

                foreach (var order in orderListWithQuantity)
                {
                    if (order.Keys.Contains(id))
                    {
                        foreach (var siblingId in order)
                        {
                            if (!siblingId.Key.Equals(id))
                            {
                                if (!dict.ContainsKey(siblingId.Key)) dict.Add(siblingId.Key, 0);
                                dict[siblingId.Key] = dict[siblingId.Key] + siblingId.Value;
                            }
                        }                            
                    }                        
                }                    
            }                     
            
            //Only the specific orders(having sibling SKU) are considered while calculating quantity.
            return model;
        }

        //To get the list of SKUs of recommended products in descending order of quantity
        protected virtual List<KeyValuePair<string, decimal?>> OrderPairs(Dictionary<string, decimal?> dictionary)
        => dictionary.OrderByDescending(kv => kv.Value).ToList();

        //To get the merged dictionary.
        protected virtual Dictionary<string, decimal?> GetMergedDictionary(List<string> ids)
        {
            var dictionaries = new List<Dictionary<string, decimal?>>();
            foreach (var id in ids)
            {
                var dictionary = internalModel.ContainsKey(id) ? internalModel[id] : null;
                if (dictionary != null) dictionaries.Add(dictionary);
            }

            return Utilities.Merge(dictionaries.ToArray());
        }

        //To return the ordered pairs for list of SKUs.
        protected virtual List<KeyValuePair<string, decimal?>> GetOrderedPairs(List<string> ids)
        {
            var mergedDictionary = GetMergedDictionary(ids);
            return OrderPairs(mergedDictionary);
        }

        //To return the ordered pairs for SKU.
        protected virtual List<KeyValuePair<string, decimal?>> GetOrderedPairs(string id)
        {
            var dictionary = internalModel.ContainsKey(id) ? internalModel[id] : null;
            if (dictionary != null) return OrderPairs(dictionary);
            return new List<KeyValuePair<string, decimal?>>();
        }

        //Will get the list of recommended products SKUs.
        protected virtual Recommendation _getIds(string id)
        =>  new Recommendation { ProductSkus = GetOrderedPairs(id).Select(kv => kv.Key).ToList() };

        //Will get the list of recommended products SKUs.
        protected virtual Recommendation _getIds(List<string> ids)
        {
            var recommendedProductIds = GetOrderedPairs(ids).Select(kv => kv.Key).ToList();
            recommendedProductIds = recommendedProductIds.Except(ids).ToList(); // remove the items for which the recommendations are derived.
            return new Recommendation { ProductSkus = recommendedProductIds };
        }
    }
}