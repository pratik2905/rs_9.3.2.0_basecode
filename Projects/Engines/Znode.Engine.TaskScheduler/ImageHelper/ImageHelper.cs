﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Libraries.MediaStorage;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.TaskScheduler
{
    public class ImageHelper : BaseScheduler, ISchdulerProviders
    {
        #region Private Variables
        private MediaConfigurationModel Configuration;
        private string ApiURL = string.Empty;
        private readonly string UriItemSeparator = ",";
        private readonly string UriKeyValueSeparator = "~";
        private readonly string LogFileName = $"ImageUtilityLogFile_{DateTime.Now.ToString("MMddyyyy")}.txt";
        private int TotalFilesPublished = 0;
        private const string SchedulerName = "ImageScheduler";
        private long QualityFactor = 90;
        private const string AuthorizationHeader = "Authorization";
        private string AuthorizationHeaderValue = string.Empty;
        private const string TokenHeader = "Token";
        private string TokenValue = string.Empty;
        #endregion

        #region Public Properties
        public MediaConfigurationModel DefaultMediaConfiguration
        {
            get
            {
                return Configuration;
            }
        }
        #endregion

        #region Public Methods
        public void InvokeMethod(string[] args)
        {
            try
            {
                string type = Convert.ToString(args[0]);
                int portalId = int.Parse(args[1].ToString());
                int chunkSize = int.Parse(args[2].ToString());
                int userId = int.Parse(args[3].ToString());
                int catalogId = int.Parse(args[4].ToString());
                int localeId = int.Parse(args[5].ToString());
                ApiURL = Convert.ToString(args[6]);
                int qualityFactor = Convert.ToInt32(args[7]);
                AuthorizationHeaderValue = Convert.ToString(args[8]);
                if (args.Length > 9)
                    TokenValue = Convert.ToString(args[9]);
                // args[10] contains request timeout value.
                if (args.Length > 10 && !string.IsNullOrEmpty(args[10]))
                    base.requesttimeout = int.Parse(args[10]);

                if (qualityFactor > 0)
                    QualityFactor = qualityFactor;

                if (type.ToLower().Equals("imagehelper"))
                {

                    //Get all display settings
                    PortalDisplaySettingModel displaySettings = GetDisplaySettings(portalId);

                    GenerateCategoryImages(catalogId, localeId, portalId, displaySettings);

                    GerateProductImages(portalId, catalogId, localeId, chunkSize, displaySettings);
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error occurred while image publishing. Error - " + ex.Message.ToString(), SchedulerName);
                LogMessage("Stack Trace - " + ex.StackTrace, SchedulerName);
            }
        }

        //Generate Product Images
        private void GerateProductImages(int portalId, int catalogId, int localeId, int pageLength, PortalDisplaySettingModel displaySettings)
        {
            int start = 1;
            decimal totalPages;
            bool continueIndex = true;

            int totalCount = GetPublishProductCount(catalogId, localeId, portalId);

            if (totalCount < pageLength)
                totalPages = 1;
            else
                totalPages = Math.Ceiling((decimal)totalCount / pageLength);

            LogMessage($"Total Pages:{totalPages}", SchedulerName);

            //Call the API to get default Media Configuration
            Configuration = GetMediaConfiguration();


            string orgPath = Configuration.Server == ZnodeConstant.NetworkDrive ? Path.Combine(Configuration?.NetworkUrl + Configuration.BucketName) : GetMediaServerUrl(Configuration);

            string destinationPath = $"{orgPath}/Catalog/{portalId}/";

            ImageHelperModel imghelperModel = GetImageHelperModel(destinationPath, orgPath, displaySettings, portalId);

            do
            {
                //Get all images chunkwise
                PublishProductListModel publishList = GetPublishProductData(catalogId, localeId, start, pageLength);

                LogMessage($"Total Product Receive :{publishList?.PublishProducts?.Count}", SchedulerName);

                if (start <= totalPages)
                {
                    if (publishList?.PublishProducts?.Count > 0)
                    {
                        //Get all images to resize
                        List<string> publishProdImgList = GetImageList(publishList, displaySettings);

                        if (publishProdImgList?.Count > 0)
                        {
                            //Log the start of process
                            LogMessage($"Image publishing of product started for {publishProdImgList.Count} images.", SchedulerName);

                            //Generate images
                            GenerateResizedImages(publishProdImgList, imghelperModel);

                            int failedFiles = publishProdImgList.Count - TotalFilesPublished;

                            LogMessage($"Image creation completed.  {TotalFilesPublished} images published successfully. {failedFiles} images failed to published", SchedulerName);
                        }
                        else
                            LogMessage("No images available. Please check publish product list.", SchedulerName);
                    }
                    start++;
                }
                else
                    continueIndex = false;

            } while (continueIndex);
        }

        //Generate Category Images
        private void GenerateCategoryImages(int catalogId, int localeId, int portalId, PortalDisplaySettingModel displaySettings)
        {
            //Get category images
            PublishCategoryListModel categoryList = GetPublishCategoryData(catalogId, localeId);

            List<string> imageList = new List<string>();

            //get category image list
            if (categoryList?.PublishCategories?.Count > 0)
                imageList.AddRange(categoryList.PublishCategories.SelectMany(x => x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.CategoryImage)?.Select(z => z.AttributeValues)).ToList());

            if (imageList?.Count > 0)
            {
                //Log the start of process
                LogMessage($"Image publishing started for {imageList.Count} images.", SchedulerName);

                //Call the API to get default Media Configuration
                Configuration = GetMediaConfiguration();

                string orgPath = Configuration.Server == ZnodeConstant.NetworkDrive ? Path.Combine(Configuration?.NetworkUrl + Configuration.BucketName) : GetMediaServerUrl(Configuration);

                string destinationPath = $"{orgPath}/Catalog/{portalId}/";

                ImageHelperModel imghelperModel = GetImageHelperModel(destinationPath, orgPath, displaySettings, portalId);

                //Generate images
                GenerateResizedImages(imageList, imghelperModel);

                int failedFiles = imageList.Count - TotalFilesPublished;

                LogMessage($"Image creation completed.  {TotalFilesPublished} images published successfully. {failedFiles} images failed to published", SchedulerName);
            }
            else
                LogMessage("No images available. Please check publish catgeory list.", SchedulerName);
        }

        //Get total product count.
        private int GetPublishProductCount(int catalogId, int localeId, int portalId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(catalogId));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, Convert.ToString(localeId));
            filters.Add(ZnodeConstant.IsActive.ToString(), FilterOperators.Equals, "True");

            StringResponse model = new StringResponse();

            string jsonString = string.Empty;

            string requestPath = $"{ApiURL}/publishproduct/getpublishproductcount";
            requestPath += BuildEndpointQueryString(null, filters, null, null, null);

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Timeout = base.requesttimeout;
                request.Headers.Add($"{ AuthorizationHeader }: Basic { AuthorizationHeaderValue }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    model = JsonConvert.DeserializeObject<StringResponse>(jsonString);
                    reader.Close();
                    datastream.Close();
                    LogMessage("getpublishproductcount - API Call Success.", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(ApiURL, AuthorizationHeaderValue);
                    GetPublishProductCount(catalogId, localeId, portalId);
                }
                else
                    LogMessage($"getpublishproductcount - Failed: {webex.Message}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"getpublishproductcount - Failed: {ex.Message}", SchedulerName);
            }
            LogMessage($"TotalProductOunt - Failed: {Convert.ToInt32(model.Response)}", SchedulerName);

            return Convert.ToInt32(model.Response);
        }
        #endregion

        #region Private Methods

        //Get the product main image, gallery images and portal default image and returns the list of it
        private List<string> GetImageList(PublishProductListModel publishList, PortalDisplaySettingModel displaySettings)
        {
            List<string> imageList = new List<string>();
            //Add default portal image
            if (!string.IsNullOrEmpty(displaySettings.PortalDefaultImageName))
                imageList.Add(displaySettings.PortalDefaultImageName);

            //get products main images
            if (!Equals(publishList, null) && publishList?.PublishProducts?.Count > 0)
                imageList.AddRange(publishList.PublishProducts.SelectMany(x => x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.Select(z => z.AttributeValues)).ToList());

            //Get the products gallery images and add it in image list
            List<string[]> galleryImagesList = publishList?.PublishProducts?.SelectMany(x => x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.GalleryImages)?.Select(z => z.AttributeValues.Split(','))).ToList();
            
            if (galleryImagesList?.Count > 0)
                imageList.AddRange(galleryImagesList.SelectMany(x => x));

            //Get the products gallery images and add it in image list
            List<string[]> swatchImagesList = publishList?.PublishProducts?.SelectMany(x => x.Attributes.Where(y => y.IsSwatch == "true" && y.SelectValues != null).SelectMany(z => z.SelectValues).Where(w => !string.IsNullOrEmpty(w.Path)).Select(z => z.Path.Split(','))).ToList();
            if (swatchImagesList?.Count > 0)
                imageList.AddRange(swatchImagesList.SelectMany(x => x).Distinct());
            return imageList;
        }

        //get the category images
        private PublishCategoryListModel GetPublishCategoryData(int catalogId, int localeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(catalogId));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, Convert.ToString(localeId));
            filters.Add(ZnodeConstant.IsActive.ToString(), FilterOperators.Equals, "True");
            filters.Add(WebStoreEnum.IsCallFromWebstore.ToString(), FilterOperators.Equals, "true");

            PublishCategoryListModel model = new PublishCategoryListModel();
            string jsonString = string.Empty;

            string requestPath = $"{ApiURL}/publishcategory/list";
            requestPath += BuildEndpointQueryString(null, filters, null, null, null);

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Timeout = base.requesttimeout;
                request.Headers.Add($"{ AuthorizationHeader }: Basic { AuthorizationHeaderValue }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    model = JsonConvert.DeserializeObject<PublishCategoryListModel>(jsonString);
                    reader.Close();
                    datastream.Close();
                    LogMessage("GetPublishCategoryData - API Call Success.", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(ApiURL, AuthorizationHeaderValue);
                    GetPublishCategoryData(catalogId, localeId);
                }
                else
                    LogMessage($"GetPublishCategoryData - Failed: {webex.Message}, stack: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"GetPublishCategoryData - Failed: {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }

            return model;
        }

        //Create helper for the image generation operation
        private ImageHelperModel GetImageHelperModel(string destinationPath, string orgPath, PortalDisplaySettingModel displaySettings, int portalId)
        {
            return new ImageHelperModel
            {
                SourcePath = orgPath,
                DestinationPath = destinationPath,
                LargeImgWidth = displaySettings.MaxLargeWidth,
                MediumImgWidth = displaySettings.MaxMediumWidth,
                SmallImgWidth = displaySettings.MaxSmallWidth,
                CrossImgWidth = displaySettings.MaxCrossSellWidth,
                ThumbImgWidth = displaySettings.MaxThumbnailWidth,
                SmallThumbImgWidth = displaySettings.MaxSmallThumbnailWidth,
                BucketName = Configuration.BucketName,
                PortalId = portalId
            };
        }

        //Delete all the media files from server
        private void DeleteMediaImages(string portalId)
        {
            try
            {
                if (!DefaultMediaConfiguration.MediaServer.ClassName.Equals("LocalAgent"))
                {
                    string folderName = $"Catalog/{portalId}/";
                    ServerConnector mediaServerConnector = new ServerConnector(new FileUploadPolicyModel(DefaultMediaConfiguration.AccessKey, DefaultMediaConfiguration.SecretKey, DefaultMediaConfiguration.BucketName, DefaultMediaConfiguration.ThumbnailFolderName, DefaultMediaConfiguration.URL, DefaultMediaConfiguration.NetworkUrl));
                    mediaServerConnector.CallConnector(DefaultMediaConfiguration.MediaServer.ClassName, "DeleteFolder", null, portalId, folderName);
                    mediaServerConnector = null;
                }
                else
                {
                    DirectoryInfo dir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory);
                    string path = dir.Parent.FullName;
                    path = $"{path}\\Media\\Catalog\\{portalId}";
                    if (Directory.Exists(path))
                        Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                LogMessage("DeleteMediaImages - Error - " + ex.Message.ToString(), SchedulerName);
                LogMessage("Stack Trace - " + ex.StackTrace, SchedulerName);
            }
        }

        //Get the published products data
        private PublishProductListModel GetPublishProductData(int catalogId, int localeId, int pageNo, int pageSize)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(catalogId));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, Convert.ToString(localeId));
            filters.Add(ZnodeConstant.IsActive.ToString(), FilterOperators.Equals, "True");

            PublishProductListModel model = new PublishProductListModel();
            string jsonString = string.Empty;

            string requestPath = $"{ApiURL}/publishproduct/getmongoproductslist";
            requestPath += BuildEndpointQueryString(null, filters, null, pageNo, pageSize);

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Timeout = base.requesttimeout;
                request.Headers.Add($"{ AuthorizationHeader }: Basic { AuthorizationHeaderValue }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    model = JsonConvert.DeserializeObject<PublishProductListModel>(jsonString);
                    reader.Close();
                    datastream.Close();
                    LogMessage("getmongoproductslist - API Call Success.", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(ApiURL, AuthorizationHeaderValue);
                    GetPublishProductData(catalogId, localeId, pageNo, pageSize);
                }
                else
                    LogMessage($"getmongoproductslist - Failed: {webex.Message}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"getmongoproductslist - Failed: {ex.Message}", SchedulerName);
            }

            return model;
        }

        //Get media configuration data
        private MediaConfigurationModel GetMediaConfiguration()
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, Convert.ToString(true));

            ExpandCollection expands = new ExpandCollection();
            expands.Add("mediaserver");

            MediaConfigurationResponse result = new MediaConfigurationResponse();
            MediaConfigurationModel model = new MediaConfigurationModel();
            string jsonString = string.Empty;

            string requestPath = $"{ApiURL}/mediaconfiguration";
            requestPath += BuildEndpointQueryString(expands, filters, null, null, null);

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Timeout = base.requesttimeout;
                request.Headers.Add($"{ AuthorizationHeader }: Basic { AuthorizationHeaderValue }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<MediaConfigurationResponse>(jsonString);
                    model = result.MediaConfiguration;
                    reader.Close();
                    datastream.Close();
                    LogMessage("GetMediaConfiguration - API Call Success.", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(ApiURL, AuthorizationHeaderValue);
                    GetMediaConfiguration();
                }
                else
                    LogMessage($"GetMediaConfiguration - Failed: {webex.Message}, stack: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"GetMediaConfiguration - Failed: {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }

            return model;
        }

        //Get Display settings
        private PortalDisplaySettingModel GetDisplaySettings(int portalId)
        {
            PortalDisplaySettingResponse result = new PortalDisplaySettingResponse();
            PortalDisplaySettingModel model = new PortalDisplaySettingModel();
            string jsonString = string.Empty;
            string message = string.Empty;

            string requestPath = $"{ApiURL}/portal/getdisplaysetting/{portalId}";

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                request.Timeout = base.requesttimeout;
                request.Headers.Add($"{ AuthorizationHeader }: Basic { AuthorizationHeaderValue }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<PortalDisplaySettingResponse>(jsonString);
                    model = result.DisplaySetting;
                    reader.Close();
                    datastream.Close();
                    LogMessage("GetDisplaySettings - API Call Success.", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(ApiURL, AuthorizationHeaderValue);
                    GetDisplaySettings(portalId);
                }
                else
                    LogMessage($"GetDisplaySettings - API Call Failed: {webex.Message}, stack: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"GetDisplaySettings - API Call Failed: {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }
            return model;
        }

        //Build the query string for request
        private string BuildEndpointQueryString(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            // IMPORTANT: Expand always starts with ? while all the others start with &, which
            // means the expands must be added first when building the querystring parameters.

            var queryString = BuildExpandQueryString(expands);
            queryString += BuildFilterQueryString(filters);
            queryString += BuildSortQueryString(sorts);
            queryString += BuildPageQueryString(pageIndex, pageSize);

            return queryString;
        }

        //Genreate query string for Expand
        private string BuildExpandQueryString(ExpandCollection expands)
        {
            var queryString = "?expand=";

            if (expands != null)
            {
                foreach (var e in expands)
                    queryString += e + UriItemSeparator;

                if (!string.IsNullOrEmpty(UriItemSeparator))
                    queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        //Genreate query string for Filter
        private string BuildFilterQueryString(FilterCollection filters)
        {
            var queryString = "&filter=";

            if (filters != null)
            {
                foreach (var f in filters)
                    queryString += $"{f.FilterName}{UriKeyValueSeparator}{f.FilterOperator }{UriKeyValueSeparator }{HttpUtility.UrlEncode(f.FilterValue)}{UriItemSeparator }";

                if (!string.IsNullOrEmpty(UriItemSeparator))
                    queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        //Genreate query string for Sort
        private string BuildSortQueryString(SortCollection sorts)
        {
            var queryString = "&sort=";

            if (sorts != null)
            {
                foreach (var s in sorts)
                    queryString += $"{ s.Key}{UriKeyValueSeparator}{s.Value}{UriItemSeparator}";

                if (!string.IsNullOrEmpty(UriItemSeparator))
                    queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        //Genreate query string for Pagination
        private string BuildPageQueryString(int? pageIndex, int? pageSize)
        {
            var queryString = "&page=";

            if (pageIndex.HasValue && pageSize.HasValue)
            {
                queryString += $"index{UriKeyValueSeparator}{pageIndex.Value}";
                queryString += UriItemSeparator;
                queryString += $"size{ UriKeyValueSeparator} { pageSize.Value}";
            }

            return queryString;
        }

        //generate resize images
        private void GenerateResizedImages(List<string> publishProdImgList, ImageHelperModel imageHelperModel)
        {
            TotalFilesPublished = 0;
            foreach (string imgs in publishProdImgList)
            {
                try
                {
                    foreach (string img in imgs.Split(','))
                    {
                        string imageNamewithPath = GetImageName(imageHelperModel.SourcePath, img);
                        LogMessage($"GenerateResizedImages - Started for - {img} - ImagePath - {imageNamewithPath}", SchedulerName);

                        ResizeAndSaveImage(imageNamewithPath, imageHelperModel, img);

                        LogMessage($"GenerateResizedImages - Completed for - {img} - ImagePath - {imageNamewithPath}", SchedulerName);

                        TotalFilesPublished++;
                    }
                }
                catch (Exception ex)
                {
                    LogMessage($"GenerateResizedImages - Error - {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
                    continue;
                }
            }
        }

        //Resize the image and save it for all Sizes
        private void ResizeAndSaveImage(string imageNamewithPath, ImageHelperModel imageHelperModel, string imageName)
        {
            SaveLargeImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.LargeImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
            SaveMediumImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.MediumImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
            SaveSmallImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.SmallImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
            SaveCrossSellImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.CrossImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
            SaveThumbnailImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.ThumbImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
            SaveSmallThumbanailImage(imageNamewithPath, imageName, imageHelperModel.DestinationPath, imageHelperModel.SmallThumbImgWidth, imageHelperModel.BucketName, imageHelperModel.PortalId);
        }

        //Save the small thumbnail images
        private void SaveSmallThumbanailImage(string imageNamewithPath, string imageName, string destinationPath, int smallThumbImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, smallThumbImgWidth, smallThumbImgWidth);
            SaveResizedImage(imageToSave, smallThumbImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the thumbnail images
        private void SaveThumbnailImage(string imageNamewithPath, string imageName, string destinationPath, int thumbImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, thumbImgWidth, thumbImgWidth);
            SaveResizedImage(imageToSave, thumbImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the cross-sell images
        private void SaveCrossSellImage(string imageNamewithPath, string imageName, string destinationPath, int crossImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, crossImgWidth, crossImgWidth);
            SaveResizedImage(imageToSave, crossImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the small images
        private void SaveSmallImage(string imageNamewithPath, string imageName, string destinationPath, int smallImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, smallImgWidth, smallImgWidth);
            SaveResizedImage(imageToSave, smallImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the medium images
        private void SaveMediumImage(string imageNamewithPath, string imageName, string destinationPath, int mediumImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, mediumImgWidth, mediumImgWidth);
            SaveResizedImage(imageToSave, mediumImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the large images
        private void SaveLargeImage(string imageNamewithPath, string imageName, string destinationPath, int width, string bucketName, int portalId)
        {
            //Generate image using image details
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, width, width);
            SaveResizedImage(imageToSave, width, destinationPath, imageName, bucketName, portalId);
        }

        //Get the image to use
        private Image GetImageToUse(string imageNamewithPath)
        {
            Image img = null;
            byte[] stream = GetImageStream(imageNamewithPath);
            using (MemoryStream mStream = new MemoryStream(stream))
                img = Image.FromStream(mStream);
            return img;
        }

        //Get image in stream
        private byte[] GetImageStream(string url)
        {
            byte[] imageData = null;

            using (var wc = new System.Net.WebClient())
                imageData = wc.DownloadData(url);

            return imageData;
        }

        //Save the resized image
        private void SaveResizedImage(Image imageToSave, int folderName, string destinationPath, string imageName, string bucketName, int portalId)
        {
            try
            {
                if (!Equals(imageToSave, null))
                {
                    string localHostPath = $"~/{bucketName}/Catalog/{portalId}";
                    string destPath = $"{destinationPath}/{folderName}/{imageName}";

                    ImageFormat imageFormat = GetImageFormat(Path.GetExtension(imageName));

                    ImageCodecInfo jpgEncoder = GetEncoder(imageFormat);
                    Encoder myEncoder = Encoder.Quality;
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);
                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, QualityFactor);
                    myEncoderParameters.Param[0] = myEncoderParameter;

                    using (MemoryStream stream = new MemoryStream())
                    {

                        imageToSave.Save(stream, jpgEncoder, myEncoderParameters);

                        if (DefaultMediaConfiguration.MediaServer.ClassName.Equals("LocalAgent"))
                            SaveLocalImage(portalId.ToString(), folderName.ToString(), imageName, stream);
                        else if (DefaultMediaConfiguration.MediaServer.ClassName.Equals("NetworkDriveAgent"))
                            SaveLocalImageForNetworkDrive(portalId.ToString(), folderName.ToString(), imageName, stream);
                        else
                        {
                            string destFolderName = $"Catalog/{portalId}/{folderName}";
                            ServerConnector mediaServerConnector = new ServerConnector(new FileUploadPolicyModel(DefaultMediaConfiguration.AccessKey, DefaultMediaConfiguration.SecretKey, DefaultMediaConfiguration.BucketName, DefaultMediaConfiguration.ThumbnailFolderName, DefaultMediaConfiguration.URL, DefaultMediaConfiguration.NetworkUrl));
                            mediaServerConnector.CallConnector(DefaultMediaConfiguration.MediaServer.ClassName, MediaStorageAction.Upload, stream, imageName, destFolderName);
                            mediaServerConnector = null;
                        }

                        imageToSave = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error occurred in SaveResizedImage function. Error - " + ex.Message.ToString(), SchedulerName);
                LogMessage("Stack Trace - " + ex.StackTrace, SchedulerName);
            }
        }

        public ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        //Save image for local server
        private void SaveLocalImage(string portalId, string folderName, string imageName, MemoryStream stream)
        {
            DirectoryInfo dir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory);
            dir = dir.Parent;

            string path = $"{dir.FullName}\\Media";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\Catalog";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + portalId.ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + folderName.ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + imageName;

            //write to file
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                stream.WriteTo(file);
            }
        }

        private void SaveLocalImageForNetworkDrive(string portalId, string folderName, string imageName, MemoryStream stream)
        {
            string path = Path.Combine(Configuration.NetworkUrl + $"/{Configuration.BucketName}");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\Catalog";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + portalId.ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + folderName.ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path += "\\" + imageName;

            //write to file
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                stream.WriteTo(file);
            }
        }

        //Get image format
        private ImageFormat GetImageFormat(string extension)
        {
            switch (extension.ToLower())
            {
                case @".bmp":
                    return ImageFormat.Bmp;
                case @".gif":
                    return ImageFormat.Gif;
                case @".ico":
                    return ImageFormat.Icon;
                case @".jpg":
                case @".jpeg":
                    return ImageFormat.Jpeg;
                case @".png":
                    return ImageFormat.Png;
                case @".tif":
                case @".tiff":
                    return ImageFormat.Tiff;
                case @".wmf":
                    return ImageFormat.Wmf;
                default:
                    return ImageFormat.Png;
            }
        }

        //Create the rezised image
        private Image ResizeImage(Image sourceImage, int maxHeight, int maxWidth)
        {
            try
            {
                int originalWidth = sourceImage.Width;
                int originalHeight = sourceImage.Height;

                float percentWidth = (float)maxWidth / originalWidth;
                float percentHeight = (float)maxHeight / originalHeight;

                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

                int newWidth = (int)(originalWidth * percent);
                int newHeight = (int)(originalHeight * percent);

                Bitmap thumbnailBitmap = new Bitmap(sourceImage, newWidth, newHeight);

                Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);

                thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbnailGraph.SmoothingMode = SmoothingMode.AntiAlias;
                thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                thumbnailGraph.Clear(Color.White);

                Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbnailGraph.DrawImage(sourceImage, imageRectangle);

                Image imageToSave = thumbnailBitmap;

                return imageToSave;
            }
            catch (Exception ex)
            {
                LogMessage("Error occurred in ResizeImage function. Error - " + ex.Message.ToString(), SchedulerName);
                LogMessage("Stack Trace - " + ex.StackTrace, SchedulerName);
                return null;
            }
        }

        //Get complete image name
        private string GetImageName(string sourcePath, string imageName)
            => $"{sourcePath}/{imageName}";

        //Get Media Server Url
        protected string GetMediaServerUrl(MediaConfigurationModel configuration)
        {
            if (HelperUtility.IsNotNull(configuration))
            {
                return string.IsNullOrWhiteSpace(configuration.CDNUrl) ? configuration.URL
                           : configuration.CDNUrl.EndsWith("/") ? configuration.CDNUrl : $"{configuration.CDNUrl}/";
            }
            return string.Empty;
        }

        #endregion
    }
}
