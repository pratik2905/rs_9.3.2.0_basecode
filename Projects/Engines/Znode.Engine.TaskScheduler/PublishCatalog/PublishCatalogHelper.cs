﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.TaskScheduler.PublishCatalog
{
    public class PublishCatalogHelper : BaseScheduler, ISchdulerProviders
    {
        #region MyRegion
        private const string SchedulerName = "PublishCatalogHelper";
        private const string AuthorizationHeader = "Authorization";
        private const string UserHeader = "Znode-UserId";
        private const string TokenHeader = "Token";
        private string TokenValue = string.Empty;
        #endregion

        public void InvokeMethod(string[] args)
        {
            if (args.Length > 6)
                TokenValue = args[6];
            // args[7] contains request timeout value.
            if (args.Length > 7 && !string.IsNullOrEmpty(args[7]))
                base.requesttimeout = int.Parse(args[7]);

            CallPublishCatalogAPI(Convert.ToInt32(args[1]), args[2], args[3], args[4], "PRODUCTION", args[5]);
        }

        private void CallPublishCatalogAPI(int catalogId, string catalogName, string apiDomainURL, string userId, string revisionType, string token)
        {
            string requestPath = $"{apiDomainURL}/catalog/publish/{catalogId}/{revisionType}";
            LogMessage(requestPath, SchedulerName);
            string jsonString = string.Empty;
            string message = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";

                request.Headers.Add($"{ UserHeader }: { userId }");
                request.Headers.Add($"{ AuthorizationHeader }: Basic { token }");
                if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                    request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                request.Timeout = base.requesttimeout;
                LogMessage("SyncCallApi method Called and Set request parameter.", SchedulerName);
                using (HttpWebResponse responce = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = responce.GetResponseStream();
                    LogMessage("Got Response Stream.", SchedulerName);
                    StreamReader reader = new StreamReader(datastream);
                    LogMessage("read Response Stream.", SchedulerName);
                    jsonString = reader.ReadToEnd();
                    reader.Close();
                    datastream.Close();
                    PublishedResponse result = new PublishedResponse();
                    result = JsonConvert.DeserializeObject<PublishedResponse>(jsonString);
                    PublishedModel model = result.PublishedModel;
                    LogMessage("API Call Successfully.", SchedulerName);
                    //Call Send Scheduler Activity Log Method with "No Error" and scheduler status as true
                    LogMessage("Sending Scheduler Activity Log.", SchedulerName);
                    if (model.IsPublished)
                        LogMessage($"Scheduler for catalog {catalogName} started sucessfully at {DateTime.Now} ", SchedulerName);
                    else
                        LogMessage($"Failed to publish catalog {catalogName} because { model.ErrorMessage}", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenValue = GetToken(apiDomainURL, token);
                    CallPublishCatalogAPI(catalogId, catalogName, apiDomainURL, userId, revisionType, token);
                }
                else
                    LogMessage($"GetPublishProductData - Failed: {webex.Message} , StackTrace: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"GetPublishProductData - Failed: {ex.Message} , StackTrace: {ex.StackTrace}", SchedulerName);
            }
        }

    }
}
