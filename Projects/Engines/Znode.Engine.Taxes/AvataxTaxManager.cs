﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Znode.Engine.Taxes
{
    class AvataxTaxManager
    {
    }

    [XmlRootAttribute("AvataxSettings", Namespace = "", IsNullable = false)]
    public class AvataxSettings
    {
        #region Private members
        private string _avalaraAccount;
        private string _avalaraCompanyCode;
        private string _avalaraLicense;
        private string _avalaraServiceURL;
        private string _avalaraFreightIdentifier;
        #endregion

        #region Constructor

        #endregion

        #region Properties

        public string AvalaraAccount
        {
            get
            {
                return _avalaraAccount;
            }
            set
            {
                _avalaraAccount = value;
            }
        }
        public string AvalaraCompanyCode
        {
            get
            {
                return _avalaraCompanyCode;
            }
            set
            {
                _avalaraCompanyCode = value;
            }
        }
        public string AvalaraLicense
        {
            get
            {
                return _avalaraLicense;
            }
            set
            {
                _avalaraLicense = value;
            }
        }
        public string AvalaraServiceURL
        {
            get
            {
                return _avalaraServiceURL;
            }
            set
            {
                _avalaraServiceURL = value;
            }
        }
        public string AvalaraFreightIdentifier
        {
            get
            {
                return _avalaraFreightIdentifier;
            }
            set
            {
                _avalaraFreightIdentifier = value;
            }
        }


        #endregion
    } //end class AvataxSettings

    [XmlRootAttribute("AvataxClassIdentifierCollection", Namespace = "", IsNullable = false)]
    public class AvataxClassIdentifierCollection : IList<AvataxClassIdentifier>
    {
        #region private members
        private readonly List<AvataxClassIdentifier> _innerList;
        #endregion

        #region constructor
        public AvataxClassIdentifierCollection()
        {
            _innerList = new List<AvataxClassIdentifier>();
        }
        #endregion

        #region properties
        public List<AvataxClassIdentifier> InnerList
        {
            get { return _innerList; }
        }
        #endregion

        #region helper methods

        public void Add(AvataxClassIdentifier item)
        {
            if (item == null)
                throw new ArgumentException("Identifier cannot be nothing.");
            if (this.Contains(item))
                throw new ArgumentException("Identifier has already been added to the collection.");
            _innerList.Add(item);
        }
        public bool Remove(AvataxClassIdentifier item)
        {
            AvataxClassIdentifier i = this.FirstOrDefault(c => c.ClassIdentifier == item.ClassIdentifier);
            if (i == null)
                throw new ArgumentException("Item does not exist in collection.");
            return _innerList.Remove(item);
        }
        public void RemoveAt(int index)
        {
            _innerList.RemoveAt(index);
        }
        public bool Contains(AvataxClassIdentifier item)
        {
            return _innerList.FirstOrDefault(i => i.ClassIdentifier == item.ClassIdentifier) != null;
        }
        public int IndexOf(AvataxClassIdentifier item)
        {
            return _innerList.TakeWhile(id => item.ClassIdentifier != id.ClassIdentifier).Count();
        }
        public void Insert(int index, AvataxClassIdentifier item)
        {
            if (item == null)
                throw new ArgumentException("Identifier cannot be nothing.");
            if (this.Contains(item))
                throw new ArgumentException("Identifier has already been added to the collection.");
            _innerList.Insert(index, item);
        }
        public void Clear()
        {
            _innerList.Clear();
        }
        public int Count
        {
            get { return _innerList.Count; }
        }
        public void CopyTo(AvataxClassIdentifier[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }
        public bool IsReadOnly
        {
            get { return false; }
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        public System.Collections.Generic.IEnumerator<AvataxClassIdentifier> GetEnumerator()
        {
            foreach (AvataxClassIdentifier i in _innerList)
                yield return i;
        }
        public AvataxClassIdentifier this[int index]
        {
            get { return _innerList[index]; }
            set
            {
                if (!this.Contains(value))
                {
                    AvataxClassIdentifier i = (AvataxClassIdentifier)value;
                    _innerList[index] = i;
                }
            }
        }

        #endregion
    }

    [XmlRootAttribute("AvataxClassIdentifier", Namespace = "", IsNullable = false)]
    public class AvataxClassIdentifier
    {
        #region Private members
        private string _classIdentifier;
        private string _className;
        #endregion

        #region Properties

        public string ClassIdentifier
        {
            get
            {
                return _classIdentifier;
            }
            set
            {
                _classIdentifier = value;
            }
        }
        public string ClassName
        {
            get
            {
                return _className;
            }
            set
            {
                _className = value;
            }
        }

        #endregion
    }//end class AvataxClassIdentifier
}
