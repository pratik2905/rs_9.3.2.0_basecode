﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Taxes.Helper;
using Znode.Engine.Taxes.STOCCHWebservice;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Taxes
{
    //This is the class created to calculate the CCH tax for the specific order
    public class STOCCHTax : ZnodeTaxesType
    {
        #region Private Variables

        private readonly string SourceSystem = ZnodeApiSettings.CCHSourceSystem;
        private readonly string TransactionDescription = ZnodeApiSettings.CCHTransactionDescription;
        private readonly string TransactionType = ZnodeApiSettings.CCHTransactionType;// sale
        private readonly string CustomerType = ZnodeApiSettings.CCHCustomerType;//retail
        private readonly string ProviderType = ZnodeApiSettings.CCHProviderType;//Retail

        private readonly string CCHTaxGroup = ZnodeApiSettings.CCHTaxGroup;
        private readonly string CCHTaxItem = ZnodeApiSettings.CCHTaxItem;

        private STOWebServices WebService { get { return new STOWebServices() { Url = ZnodeApiSettings.CCHWebServiceURL }; } }

        #endregion Private Variables

        #region Constructors

        public STOCCHTax()
        {
            Name = "STO CCH Tax";
            Description = "Applies STO CCH tax to the shopping cart.";
        }

        #endregion Constructors

        #region Public Method

        // Calculates the sales tax and updates the shopping cart.
        public override void Calculate()
        {
            try
            {
                if (ShoppingCart.IsCchCalculate)
                    PartialReturnOrderLineItem();

                //create cch order object
                Order cchOrder = new Order();
                cchOrder.InvoiceDate = DateTime.Now;
                cchOrder.InvoiceID = $"cch_{ShoppingCart.UserId}";
                cchOrder.SourceSystem = SourceSystem;
                cchOrder.TestTransaction = Convert.ToBoolean(ZnodeApiSettings.CCHTestTransactionMode);
                cchOrder.TransactionDescription = TransactionDescription;
                cchOrder.TransactionType = TransactionType;
                cchOrder.CustomerType = CustomerType;
                cchOrder.ProviderType = ProviderType;
                cchOrder.finalize = false;
                cchOrder.TransactionID = 0;

                cchOrder.NexusInfo = new NexusInfo();
                cchOrder.NexusInfo.ShipToAddress = GetShipToAddress();
                cchOrder.NexusInfo.ShipFromAddress = GetShipFromAddress();

                //Get tax data for a portal.
                TaxPortalModel taxPortalModel = GetTaxPortalData();
                string entityId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.EntityId : string.Empty;
                string divisionId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.DivisionId : string.Empty;

                string freightTaxGroupCode = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.FreightTaxGroupCode : string.Empty;
                string freightTaxItemCode = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.FreightTaxItemCode : string.Empty;

                List<LineItem> cchLineItems = GetCCHLineItems(freightTaxGroupCode, freightTaxItemCode);
                cchOrder.LineItems = cchLineItems.ToArray();

                TaxResponse cchResponse = WebService.CalculateRequest(entityId, divisionId, cchOrder);

                if (cchResponse.TransactionStatus.Equals(1)) //failed transaction
                    CCHLogResponse(cchResponse);
                else  // either In progress or successful transaction
                    SetTaxDetails(cchResponse);
            }
            catch (Exception ex)
            {
                ShoppingCart.TaxRate = 0;
                ZnodeLogging.LogMessage($"Tax Error: {ex.Message.ToString()} - {ex.StackTrace}",string.Empty,TraceLevel.Error,ex);
            }
        }

        //CCH full order return request ,compensates the transaction for returns or losses.
        public override void CCHFullReturnRequest(ShoppingCartModel shoppingCartModel)
        {
            //Get entity and division id.
            string entityId, divisionId;
            GetEntityAndDivisionId(shoppingCartModel.PortalId, out entityId, out divisionId);

            TaxResponse cchReturnResponse = new STOWebServices().AttributedFullReturnRequest(entityId, divisionId, Convert.ToUInt64(new ZnodeTaxHelper().GetTaxTransactionNumber(shoppingCartModel.OmsOrderId)),
                                            SourceSystem, $"cch_{shoppingCartModel.UserId}", shoppingCartModel.OrderDate.GetValueOrDefault(), TransactionDescription);

            if (cchReturnResponse.TransactionStatus.Equals(1)) //failed transaction
                CCHLogResponse(cchReturnResponse);
        }

        // Process anything that must be done before the order is submitted.
        public override bool PreSubmitOrderProcess()
        {
            AddressModel destinationaddress = ShoppingCart?.Payment?.BillingAddress ?? new AddressModel();

            if (string.IsNullOrEmpty(destinationaddress.CountryName) || string.IsNullOrEmpty(destinationaddress.StateName))
            {
                // set tax rate and error message
                ShoppingCart.TaxRate = 0;
                ShoppingCart.AddErrorMessage = "tax error: invalid destination country or state code.";
                return false;
            }

            return true;
        }

        // Process anything that must be done after the order is submitted.
        public override void PostSubmitOrderProcess()
        {
            ZnodeTaxHelper helper = new ZnodeTaxHelper();
            string taxTransactionNumber = helper.GetTaxTransactionNumber(ShoppingCart?.OrderId);
            if (!string.IsNullOrEmpty(taxTransactionNumber))
                TaxFulfillment(taxTransactionNumber);
        }

        //Cancels the tax request based on the tax transaction number
        public override bool CancelTaxRequest(string taxTransactionNumber, int? portalId)
        {
            //Get tax data for a portal.
            TaxPortalModel taxPortalModel = GetTaxPortalData(portalId);
            string entityId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.EntityId : string.Empty;
            string divisionId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.DivisionId : string.Empty;

            TransactionDetail cchResponse = WebService.CancelRequest(entityId, divisionId, Convert.ToUInt64(taxTransactionNumber));

            if (cchResponse.transactionStatus != 2)
            {
                CCHLogRequestResponse(cchResponse, "Cancel Tax");
                return false;
            }
            else
                return true;
        }

        //Fulfils the tax in the CCH account
        public override bool TaxFulfillment(string taxTransactionNumber)
        {
            //Get tax data for a portal.
            TaxPortalModel taxPortalModel = GetTaxPortalData();
            string entityId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.EntityId : string.Empty;
            string divisionId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.DivisionId : string.Empty;

            TransactionDetail cchResponse = WebService.FinalizeRequest(entityId, divisionId, Convert.ToUInt64(taxTransactionNumber));

            if (cchResponse.transactionStatus != 2)
            {
                CCHLogRequestResponse(cchResponse, "Fulfilment Tax");
                return false;
            }
            else
                return true;
        }

        #endregion Public Method

        #region Private Methods

        //Get the CCH order line item details
        private List<LineItem> GetCCHLineItems(string freightTaxGroupCode, string freightTaxItemCode)
        {
            List<LineItem> cchLineItems = new List<LineItem>();
            int lineItemId = 1;
            // Go through each item in the cart
            ProductInfo productInfo = null;
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                decimal cartQuantity = cartItem.Product.ZNodeGroupProductCollection.Count > 0 ? cartItem.Product.ZNodeGroupProductCollection[0].SelectedQuantity : cartItem.Quantity;

                LineItem cchItem = new LineItem();
                cchItem.ID = $"{lineItemId}";
                cchItem.Amount = cartItem.UnitPrice * cartQuantity;
                cchItem.Quantity = cartQuantity;
                productInfo = new ProductInfo();
                productInfo.ProductGroup = GetProductAttributeValueCode("CCHTaxGroup", cartItem) ?? CCHTaxGroup;
                productInfo.ProductItem = GetProductAttributeValueCode("CCHTaxItem", cartItem) ?? CCHTaxItem;
                cchItem.ProductInfo = productInfo;
                cchLineItems.Add(cchItem);
                lineItemId++;
            }

            //Add shipping amount in tax list
            if (ShoppingCart?.ShippingCost > 0 && this.TaxBag?.ShippingTaxInd == true)
            {
                LineItem cchItem = new LineItem();
                cchItem.ID = $"{lineItemId}";
                cchItem.ProductInfo = new ProductInfo();
                cchItem.Amount = ShoppingCart.ShippingCost;
                cchItem.Quantity = 1;
                cchItem.ProductInfo.ProductGroup = freightTaxGroupCode;
                cchItem.ProductInfo.ProductItem = freightTaxItemCode;
                cchLineItems.Add(cchItem);

                lineItemId++;
            }

            if (!string.IsNullOrEmpty(freightTaxGroupCode))
            {
                LineItem cchItem = new LineItem();
                cchItem.ID = $"{lineItemId}";
                cchItem.ProductInfo = new ProductInfo();
                cchItem.Quantity = 1;
                cchItem.ProductInfo.ProductGroup = freightTaxGroupCode;
                cchItem.ProductInfo.ProductItem = freightTaxItemCode;

                cchLineItems.Add(cchItem);
            }

            return cchLineItems;
        }

        //Get te Ship to address for CCH tax calculation
        private Address GetShipToAddress()
        {
            Address address = new Address();
            address.Line1 = ShoppingCart?.Payment?.ShippingAddress?.Address1;
            address.Line2 = ShoppingCart?.Payment?.ShippingAddress?.Address2;
            address.City = ShoppingCart?.Payment?.ShippingAddress?.CityName;
            address.StateOrProvince = ShoppingCart?.Payment?.ShippingAddress?.StateCode;
            address.PostalCode = ShoppingCart?.Payment?.ShippingAddress?.PostalCode;
            address.CountryCode = ShoppingCart?.Payment?.ShippingAddress?.CountryName;

            return address;
        }

        //Get te Ship from address for CCH tax calculation
        private Address GetShipFromAddress()
        {
            Address address = new Address();
            ZnodeTaxHelper taxHelper = new ZnodeTaxHelper();
            AddressModel portalWareHouseAddressModel = taxHelper.GetPortalShippingAddress(ShoppingCart.PortalId.GetValueOrDefault());
            if(HelperUtility.IsNotNull(portalWareHouseAddressModel))
            {
                address.Line1 = portalWareHouseAddressModel.Address1;
                address.Line2 = portalWareHouseAddressModel.Address2;
                address.City = portalWareHouseAddressModel.CityName;
                address.StateOrProvince = portalWareHouseAddressModel.StateCode;
                address.PostalCode = portalWareHouseAddressModel.PostalCode;
                address.CountryCode = portalWareHouseAddressModel.CountryName;
            }
            return address;
        }

        //Set the tax details in shopping cart for each product
        private void SetTaxDetails(TaxResponse cchResponse)
        {
            int iIndex = 0;
            ShoppingCart.SalesTax += cchResponse.TotalTaxApplied;

            decimal lineItemTax = 0;

            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                cartItem.IsTaxCalculated = true;
                cartItem.Product.SalesTax = cchResponse.LineItemTaxes[iIndex].TotalTaxApplied;
                cartItem.TaxTransactionNumber = Convert.ToString(cchResponse.TransactionID);
                cartItem.TaxRuleId = this.TaxBag.TaxRuleId;
                lineItemTax += cchResponse.LineItemTaxes[iIndex].TotalTaxApplied;
                iIndex++;
            }
            ShoppingCart.TaxOnShipping = cchResponse.TotalTaxApplied - lineItemTax;
        }

        //Save the error details in log file
        private void CCHLogResponse(TaxResponse cchResponse)
        {
            ZnodeLogging.LogMessage("Tax Errors:");
            if (cchResponse?.Messages?.Length > 0)
            {
                foreach (Message message in cchResponse.Messages)
                    ZnodeLogging.LogMessage($"{message.Code} - {message.Severity} - {message.Source} - {message.Reference} - {message.Info}");
            }
        }

        //Log the cacel request errors
        private void CCHLogRequestResponse(TransactionDetail cchResponse, string type)
        {
            ZnodeLogging.LogMessage($"Tax Errors: {type} request");
            if (cchResponse?.messagesField?.Length > 0)
            {
                foreach (Message message in cchResponse.messagesField)
                    ZnodeLogging.LogMessage($"{message.Code} - {message.Severity} - {message.Source} - {message.Reference} - {message.Info}");
            }
        }

        //Get Tax Portal Data.
        private TaxPortalModel GetTaxPortalData(int? portalId = 0)
        {
            int? taxPortalId = (HelperUtility.IsNotNull(ShoppingCart)) ? ShoppingCart?.PortalId.Value : portalId.Value;
            return (taxPortalId > 0) ? new ZnodeTaxHelper().GetTaxPortalData(taxPortalId.Value) : null;
        }

        //Partial return order line item.
        public override void PartialReturnOrderLineItem()
        {
            List<PartialLineItem> partialLineItems = GetCCHPartialLineItems();
            if (partialLineItems?.Count > 0)
            {
                PartialReturnOrder partialReturnOrder = new PartialReturnOrder();
                partialReturnOrder.OriginalTransactionID = Convert.ToUInt64(new ZnodeTaxHelper().GetTaxTransactionNumber(ShoppingCart?.OrderId));
                partialReturnOrder.InvoiceID = $"cch_{ShoppingCart.UserId}";
                partialReturnOrder.InvoiceDate = ShoppingCart.OrderDate.GetValueOrDefault();
                partialReturnOrder.SourceSystem = SourceSystem;
                partialReturnOrder.TransactionDescription = TransactionDescription;
                partialReturnOrder.LineItems = partialLineItems.ToArray();

                //Get entity and division id.
                string entityId, divisionId;
                GetEntityAndDivisionId(out entityId, out divisionId);

                TaxResponse cchReturnResponse = WebService.PartialReturnRequest(entityId, divisionId, partialReturnOrder);

                if (cchReturnResponse.TransactionStatus.Equals(1)) //failed transaction
                    CCHLogResponse(cchReturnResponse);
                else  // either In progress or successful transaction
                    SetTaxDetails(cchReturnResponse);
            }
        }

        //Get entity and division id.
        private void GetEntityAndDivisionId(out string entityId, out string divisionId)
        {
            TaxPortalModel taxPortalModel = GetTaxPortalData();
            entityId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.EntityId : string.Empty;
            divisionId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.DivisionId : string.Empty;
        }

        //Get entity and division id.
        private void GetEntityAndDivisionId(int portalId, out string entityId, out string divisionId)
        {
            TaxPortalModel taxPortalModel = GetTaxPortalData(portalId);
            entityId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.EntityId : string.Empty;
            divisionId = HelperUtility.IsNotNull(taxPortalModel) ? taxPortalModel.DivisionId : string.Empty;
        }

        //Get the CCH order line item details
        private List<PartialLineItem> GetCCHPartialLineItems()
        {
            List<PartialLineItem> cchPartialLineItems = new List<PartialLineItem>();
            int lineItemId = 1;
            // Go through each item in the cart
            foreach (ReturnOrderLineItemModel cartItem in ShoppingCart.ReturnItemList)
            {
                PartialLineItem cchPartialItem = new PartialLineItem();
                cchPartialItem.ID = $"{lineItemId}";
                cchPartialItem.Amount = cartItem.UnitPrice * cartItem.Quantity;
                cchPartialItem.Quantity = cartItem.Quantity;
                cchPartialItem.AvgUnitPrice = cartItem.UnitPrice;
                cchPartialLineItems.Add(cchPartialItem);
                lineItemId++;
            }

            return cchPartialLineItems;
        }

        //Get Attribute value
        private string GetProductAttributeValueCode(string attributeCode, ZnodeShoppingCartItem cartItem)
        {
            if (HelperUtility.IsNotNull(cartItem))
            {
                if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                    return cartItem?.Product?.ZNodeGroupProductCollection[0]?.Attributes?.Where(x => x.AttributeCode == attributeCode)?.FirstOrDefault()?.AttributeValueCode;
                else
                    return cartItem?.Product?.Attributes?.Where(x => x.AttributeCode == attributeCode)?.FirstOrDefault()?.AttributeValueCode;
            }
            return string.Empty;
        }

        #endregion Private Methods
    }
}