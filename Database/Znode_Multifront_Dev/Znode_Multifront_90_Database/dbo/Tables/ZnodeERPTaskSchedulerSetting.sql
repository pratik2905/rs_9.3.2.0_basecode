﻿CREATE TABLE [dbo].[ZnodeERPTaskSchedulerSetting] (
    [TaskSchedulerSettingId] INT            IDENTITY (1, 1) NOT NULL,
    [WeekDays]               VARCHAR (4000) NULL,
    [Months]                 VARCHAR (4000) NULL,
    [Days]                   VARCHAR (4000) NULL,
    [OnDays]                 VARCHAR (4000) NULL,
    [OnMonths]               VARCHAR (4000) NULL,
    [RecurEvery]             INT            NULL,
    [IsMonthlyDays]          BIT            NULL,
    [CreatedBy]              INT            NOT NULL,
    [CreatedDate]            DATETIME       NOT NULL,
    [ModifiedBy]             INT            NOT NULL,
    [ModifiedDate]           DATETIME       NOT NULL,
    CONSTRAINT [PK_ZnodeERPTaskSchedulerSetting] PRIMARY KEY CLUSTERED ([TaskSchedulerSettingId] ASC)
);







