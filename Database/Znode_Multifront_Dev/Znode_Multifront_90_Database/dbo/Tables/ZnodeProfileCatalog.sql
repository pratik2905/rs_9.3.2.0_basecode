﻿CREATE TABLE [dbo].[ZnodeProfileCatalog] (
    [ProfileCatalogId] INT      IDENTITY (1, 1) NOT NULL,
    [ProfileId]        INT      NULL,
    [PimCatalogId]     INT      NULL,
    [CreatedBy]        INT      NOT NULL,
    [CreatedDate]      DATETIME NOT NULL,
    [ModifiedBy]       INT      NOT NULL,
    [ModifiedDate]     DATETIME NOT NULL,
    CONSTRAINT [PK_ZnodeProfileCatalog] PRIMARY KEY CLUSTERED ([ProfileCatalogId] ASC),
    CONSTRAINT [FK_ZnodeProfileCatalog_ZnodePImCatalogId] FOREIGN KEY ([PimCatalogId]) REFERENCES [dbo].[ZnodePimCatalog] ([PimCatalogId]),
    CONSTRAINT [FK_ZnodeProfileCatalog_ZnodeProfile] FOREIGN KEY ([ProfileId]) REFERENCES [dbo].[ZnodeProfile] ([ProfileId])
);

