﻿
----dt- 10/03/2019 ZPD-7415 and ZPD-7141
if not exists(select * from sys.indexes where name = 'ZnodeUserPortal_UserId')
begin 
CREATE NONCLUSTERED INDEX [ZnodeUserPortal_UserId] ON [dbo].[ZnodeUserPortal] ([UserId])
end