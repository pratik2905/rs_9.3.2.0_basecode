﻿
--dt\09\10\2019 ZBT-369

INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'IsObsolete'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'IsObsolete')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0
go

--dt \14\10\2019 ZBT-358

DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,AttributeTypeId int,AttributeCode nvarchar(300))
INSERT INTO ZnodePimAttribute (AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsFilterable,IsSystemDefined
,IsConfigurable,IsPersonalizable,IsShowOnGrid,DisplayOrder,HelpDescription,IsCategory,IsHidden,IsSwatch,
CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
OUTPUT Inserted.PimAttributeId,Inserted.AttributeTypeId,Inserted.AttributeCode INTO @InsertedPimAttributeIds  		
SELECT (SELECT AttributeTypeId FROM ZnodeAttributeType WHERE AttributeTypeName = 'Text')
,'UPC',0,1,1,0,0,0,0,10,null,0,0,null,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimAttribute ZPA WHERE ZPA.AttributeCode = 'UPC')
		
INSERT INTO ZnodePimAttributeLocale (LocaleId,PimAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1 ,IPAS.PimAttributeId, 'UPC Code', null, 2,GETDATE(),2,GETDATE()   
FROM @InsertedPimAttributeIds IPAS 
		
insert into ZnodePimFrontendProperties 
(PimAttributeId,IsComparable,IsUseInSearch,IsHtmlTags,IsFacets,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select IPA.PimAttributeId,1 IsComparable, 1 IsUseInSearch, 0 IsHtmlTags,0 IsFacets,2,getdate(),2,getdate()
from @InsertedPimAttributeIds IPA
		
INSERT INTO ZnodePimAttributeGroupMapper
(PimAttributeGroupId,PimAttributeId,AttributeDisplayOrder,IsSystemDefined,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductDetails'),(select PimAttributeId from znodePimattribute where AttributeCode = 'UPC'),null,1,2,getdate(),2,getdate()
WHERE NOT EXISTS (select * from ZnodePimAttributeGroupMapper where PimAttributeGroupId =(select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductDetails') AND
PimAttributeId = (select PimAttributeId from znodePimattribute where AttributeCode = 'UPC') )
GO
--dt 16/10/2019 ZPD-7674
update ZnodePimAttribute set IsSystemDefined = 1 where AttributeCode='UPC'

go
--dt 16/10/2019 ZPD-7673
INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductDetails'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'UPC'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'UPC')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductDetails') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0

--dt 17/10/2019 ZPD-7674
update ZnodePimAttributeGroupMapper set IsSystemDefined = 1
where PimAttributeId = (select Top 1 PimAttributeId from ZnodePimAttribute where AttributeCode  = 'upc')

update  ZnodePimFamilyGroupMapper set IsSystemDefined = 1  
where PimAttributeId = (select Top 1 PimAttributeId from ZnodePimAttribute where AttributeCode  = 'upc')

--dt 08/11/2019 ZPD-7815 --> ZPD-7800
update ZnodePimAttribute set HelpDescription= 'Enter the minimum quantity that can be selected when adding an item to the cart.' where AttributeCode = 'MinimumQuantity'
GO
update ZnodePimAttribute set HelpDescription= 'Enter the maximum quantity that can be selected when adding an item to the cart.' where AttributeCode = 'MaximumQuantity'
