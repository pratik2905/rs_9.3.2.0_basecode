﻿
--dt\16\10\2019 ZPD-7656

Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'GeneralSetting','GetLBData',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName = 'GetLBData')


insert into ZnodeActionMenu ( MenuId,	ActionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
select 
 (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting')	
    ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'GetLBData') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId = 
     (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting') and ActionId = 
     (select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'GetLBData'))

insert into ZnodeMenuActionsPermission ( MenuId,	ActionId, AccessPermissionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
select 
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'GetLBData')	
,1,2,Getdate(),2,Getdate() where not exists 
(select * from ZnodeMenuActionsPermission where MenuId = 
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting') and ActionId = 
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'GetLBData'))

GO

--dt\30\10\2019 ZPD-7764

Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'GeneralSetting','UpdateLBData',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName = 'UpdateLBData')


insert into ZnodeActionMenu ( MenuId,	ActionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
select 
 (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting')	
    ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'UpdateLBData') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId = 
     (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting') and ActionId = 
     (select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'UpdateLBData'))

insert into ZnodeMenuActionsPermission ( MenuId,	ActionId, AccessPermissionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
select 
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'UpdateLBData')	
,2,2,Getdate(),2,Getdate() where not exists 
(select * from ZnodeMenuActionsPermission where MenuId = 
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Global Settings' AND ControllerName = 'GeneralSetting') and ActionId = 
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'GeneralSetting' and ActionName= 'UpdateLBData'))

GO

--dt\13\11\2019 ZPD-7269

INSERT  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT NULL ,'Recommendation','GetRecommendationSetting',0,2,Getdate(),2,Getdate() WHERE NOT EXISTS
(SELECT * FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName = 'GetRecommendationSetting')
UNION ALL 
SELECT NULL ,'Recommendation','SaveRecommendationSetting',0,2,Getdate(),2,Getdate() WHERE NOT EXISTS
(SELECT * FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName = 'SaveRecommendationSetting')

INSERT INTO ZnodeActionMenu ( MenuId,	ActionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
SELECT 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience')	
,(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'GetRecommendationSetting') ,2,Getdate(),2,Getdate()
WHERE NOT EXISTS (SELECT * FROM ZnodeActionMenu WHERE MenuId = 
(SELECT TOP 1 MenuId FROM ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience') and ActionId = 
(SELECT TOP 1 ActionId from ZnodeActions where ControllerName = 'Recommendation' and ActionName= 'GetRecommendationSetting'))
UNION ALL 
SELECT 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience')	
,(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'SaveRecommendationSetting') ,2,Getdate(),2,Getdate()
WHERE NOT EXISTS (SELECT * FROM ZnodeActionMenu WHERE MenuId = 
(SELECT TOP 1 MenuId FROM ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience') and ActionId = 
(SELECT TOP 1 ActionId from ZnodeActions where ControllerName = 'Recommendation' and ActionName= 'SaveRecommendationSetting'))




insert into ZnodeMenuActionsPermission ( MenuId,	ActionId, AccessPermissionId,	CreatedBy ,CreatedDate,	ModifiedBy, ModifiedDate )
select 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience')
,(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'GetRecommendationSetting')	
,1,2,Getdate(),2,Getdate() where not exists 
(SELECT * FROM ZnodeMenuActionsPermission where MenuId = 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience') and ActionId = 
(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'GetRecommendationSetting'))
UNION ALL 
select 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience')
,(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'SaveRecommendationSetting')	
,1,2,Getdate(),2,Getdate() where not exists 
(SELECT * FROM ZnodeMenuActionsPermission where MenuId = 
(SELECT TOP 1 MenuId from ZnodeMenu WHERE MenuName = 'Store Experience' AND ControllerName = 'StoreExperience') and ActionId = 
(SELECT TOP 1 ActionId FROM ZnodeActions WHERE ControllerName = 'Recommendation' and ActionName= 'SaveRecommendationSetting'))

GO
--dt\06\12\2019 ZPD-8230

Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'Customer','GetSalesRepList',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'Customer' and ActionName = 'GetSalesRepList')

insert into ZnodeActionMenu ( MenuId, ActionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Users' AND ControllerName = 'Customer')
   ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Customer' and ActionName= 'GetSalesRepList') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId =
    (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Users' AND ControllerName = 'Customer') and ActionId =
    (select TOP 1 ActionId from ZnodeActions where ControllerName = 'Customer' and ActionName= 'GetSalesRepList'))

insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Users' AND ControllerName = 'Customer'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Customer' and ActionName= 'GetSalesRepList')
,1,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeMenuActionsPermission where MenuId =
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Users' AND ControllerName = 'Customer') and ActionId =
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Customer' and ActionName= 'GetSalesRepList'))


--dt\06\12\2019 ZPD-8238

Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'WebSite','ManageMediaWidgetConfiguration',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'WebSite' and ActionName = 'ManageMediaWidgetConfiguration')

insert into ZnodeActionMenu ( MenuId, ActionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Pages' AND ControllerName = 'Content')
   ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'WebSite' and ActionName= 'ManageMediaWidgetConfiguration') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId =
    (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Pages' AND ControllerName = 'Content') and ActionId =
    (select TOP 1 ActionId from ZnodeActions where ControllerName = 'WebSite' and ActionName= 'ManageMediaWidgetConfiguration'))

insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Pages' AND ControllerName = 'Content'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'WebSite' and ActionName= 'ManageMediaWidgetConfiguration')
,1,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeMenuActionsPermission where MenuId =
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Pages' AND ControllerName = 'Content') and ActionId =
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'WebSite' and ActionName= 'ManageMediaWidgetConfiguration'))
go
--dt 13-12-2019 ZPD-8307


Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'Template','DownloadTemplate',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'Template' and ActionName = 'DownloadTemplate')

insert into ZnodeActionMenu ( MenuId, ActionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Page Templates' AND ControllerName = 'Template')
   ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Template' and ActionName= 'DownloadTemplate') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId =
    (select TOP 1 MenuId from ZnodeMenu where MenuName = 'Page Templates' AND ControllerName = 'Template') and ActionId =
    (select TOP 1 ActionId from ZnodeActions where ControllerName = 'Template' and ActionName= 'DownloadTemplate') )

insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Page Templates' AND ControllerName = 'Template'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Template' and ActionName= 'DownloadTemplate')
,1,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeMenuActionsPermission where MenuId =
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'Page Templates' AND ControllerName = 'Template') and ActionId =
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'Template' and ActionName= 'DownloadTemplate'))
go
Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'TouchPointConfiguration','GetUnAssignedTouchPointsList',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName = 'GetUnAssignedTouchPointsList')

insert into ZnodeActionMenu ( MenuId, ActionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration')
   ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'GetUnAssignedTouchPointsList') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId =
    (select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration') and ActionId =
    (select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'GetUnAssignedTouchPointsList') )

insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'GetUnAssignedTouchPointsList')
,1,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeMenuActionsPermission where MenuId =
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration') and ActionId =
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'GetUnAssignedTouchPointsList'))
go
Insert  INTO ZnodeActions (AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select NULL ,'TouchPointConfiguration','AssignTouchPointToActiveERP',0,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName = 'AssignTouchPointToActiveERP')

insert into ZnodeActionMenu ( MenuId, ActionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration')
   ,(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'AssignTouchPointToActiveERP') ,2,Getdate(),2,Getdate()
where not exists (select * from ZnodeActionMenu where MenuId =
    (select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration') and ActionId =
    (select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'AssignTouchPointToActiveERP') )

insert into ZnodeMenuActionsPermission ( MenuId, ActionId, AccessPermissionId, CreatedBy ,CreatedDate, ModifiedBy, ModifiedDate )
select
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration'),
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'AssignTouchPointToActiveERP')
,1,2,Getdate(),2,Getdate() where not exists
(select * from ZnodeMenuActionsPermission where MenuId =
(select TOP 1 MenuId from ZnodeMenu where MenuName = 'ERP Configuration' AND ControllerName = 'TouchPointConfiguration') and ActionId =
(select TOP 1 ActionId from ZnodeActions where ControllerName = 'TouchPointConfiguration' and ActionName= 'AssignTouchPointToActiveERP'))
