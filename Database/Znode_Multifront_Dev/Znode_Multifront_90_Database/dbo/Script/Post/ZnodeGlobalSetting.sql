﻿
BEGIN
UPDATE ZnodeGlobalSetting set FeatureValues='False',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledWarning' 
UPDATE ZnodeGlobalSetting set FeatureValues='False',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledInfo' 
UPDATE ZnodeGlobalSetting set FeatureValues='False',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledDebug' 
UPDATE ZnodeGlobalSetting set FeatureValues='True',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledError' 
UPDATE ZnodeGlobalSetting set FeatureValues='False',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledAll' 
UPDATE ZnodeGlobalSetting set FeatureValues='False',ModifiedDate=GETDATE(),ModifiedBy=2 where FeatureName='IsLoggingLevelsEnabledFatal' 
END

GO

--dt\13\08\2019 ZPD-7000

INSERT INTO znodeglobalsetting (FeatureName,FeatureValues,FeatureSubValues,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'ClearLoadBalancerAPICacheIPs','False',NULL,2, GETDATE(),2 , GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM znodeglobalsetting WHERE FeatureName = 'ClearLoadBalancerAPICacheIPs')
GO


INSERT INTO znodeglobalsetting (FeatureName,FeatureValues,FeatureSubValues,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'ClearLoadBalancerWebStoreCacheIPs','False',NULL,2, GETDATE(),2 , GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM znodeglobalsetting WHERE FeatureName = 'ClearLoadBalancerWebStoreCacheIPs')
GO

--dt 20-11-2019 ZPD-8051
INSERT INTO znodeglobalsetting (FeatureName,FeatureValues,FeatureSubValues,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'DefaultProductLimitForRecommendations','12',NULL,2, GETDATE(),2 , GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM znodeglobalsetting WHERE FeatureName = 'DefaultProductLimitForRecommendations')
GO