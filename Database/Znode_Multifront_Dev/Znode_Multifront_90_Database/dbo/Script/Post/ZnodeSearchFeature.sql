﻿
-- dt-10-12-2019 ZPD-7021 --> ZPD-8262 
insert into ZnodeSearchFeature(ParentSearchFeatureId,FeatureCode,FeatureName,IsAdvanceFeature,ControlType,HelpDescription
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select null,'DfsQueryThenFetch','Enable Accurate Scoring',0,'Yes/No','Find all the matching documents from all the shards from the index and apply the relevance score to get a more relevant result. Note: Enabling this feature may hamper the performance of the query by 20%',
2,getdate(),2,getdate()
where not exists(select * from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch' )

insert into ZnodeSearchQueryTypeFeature (SearchFeatureId,SearchQueryTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch'),
(select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match'),
2,getdate(),2,GETDATE()
where not exists(select * from ZnodeSearchQueryTypeFeature
where SearchFeatureId = (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch')
and SearchQueryTypeId = (select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match')
)

insert into ZnodeSearchQueryTypeFeature (SearchFeatureId,SearchQueryTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch'),
(select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match Phrase'),
2,getdate(),2,GETDATE()
where not exists(select * from ZnodeSearchQueryTypeFeature
where SearchFeatureId = (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch')
and SearchQueryTypeId = (select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match Phrase')
)

insert into ZnodeSearchQueryTypeFeature (SearchFeatureId,SearchQueryTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch'),
(select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match Phrase Prefix'),
2,getdate(),2,GETDATE()
where not exists(select * from ZnodeSearchQueryTypeFeature
where SearchFeatureId = (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch')
and SearchQueryTypeId = (select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Match Phrase Prefix')
)

insert into ZnodeSearchQueryTypeFeature (SearchFeatureId,SearchQueryTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch'),
(select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Multi Match'),
2,getdate(),2,GETDATE()
where not exists(select * from ZnodeSearchQueryTypeFeature
where SearchFeatureId = (select top 1 SearchFeatureId from ZnodeSearchFeature where FeatureCode = 'DfsQueryThenFetch')
and SearchQueryTypeId = (select top 1 SearchQueryTypeId from ZnodeSearchQueryType where QueryTypeName = 'Multi Match')
)