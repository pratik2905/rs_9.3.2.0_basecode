﻿
---dt\14\10\2019  ZPD-7646
INSERT INTO ZnodeActionMenu (MenuId,ActionId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs'), 
(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList'),2,getdate(),2,getdate()
where exists(select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs') and
exists(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList')
and not exists(select * from ZnodeActionMenu where MenuId = (select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs')
and ActionId =(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList'))

INSERT INTO ZnodeMenuActionsPermission (MenuId,ActionId,AccessPermissionId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT (select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs'), 
(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList'),1,2,getdate(),2,getdate()
where exists(select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs') and
exists(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList')
and not exists(select * from ZnodeMenuActionsPermission where MenuId = (select TOP 1 MenuId from ZnodeMenu where MenuName='Application Logs')
and ActionId =(select TOP 1 ActionId from ZnodeActions where ActionName = 'ImpersonationLogList'))