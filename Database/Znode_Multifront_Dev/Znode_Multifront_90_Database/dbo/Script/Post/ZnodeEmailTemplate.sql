﻿
--dt\17\10\2019 ZPD-7588

IF EXISTS (SELECT TOP 1 1 FROM ZnodeEmailTemplate WHERE TemplateName = 'ResetPasswordLink')
BEGIN
update ZnodeEmailTemplateLocale
set [Subject] = 'Create Your Password'
WHERE [Subject] = 'Reset Password'
END
GO


IF EXISTS (SELECT TOP 1 1 FROM ZnodeEmailTemplate WHERE TemplateName = 'ResetPasswordLink')
BEGIN
update ZnodeEmailTemplateLocale
set [Subject] = 'Create Your Password Link French'
WHERE [Subject] = 'Reset Password Link French'
END
GO

IF EXISTS (SELECT TOP 1 1 FROM ZnodeEmailTemplate WHERE TemplateName = 'ResetPasswordLink')
BEGIN
update ZnodeEmailTemplateLocale
set [Subject] = 'Create Your Password Link Arabic'
WHERE [Subject] = 'Reset Password Link Arabic'
END
GO
