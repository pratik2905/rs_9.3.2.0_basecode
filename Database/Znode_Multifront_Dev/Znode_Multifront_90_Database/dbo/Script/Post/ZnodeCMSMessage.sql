﻿--dt\14\11\2019 ZPD-7906

INSERT INTO ZnodeCMSMessageKey(MessageKey,MessageTag,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'RecommendedProductsTitle',NULL,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSMessageKey WHERE MessageKey='RecommendedProductsTitle')

INSERT INTO ZnodeCMSMessage(LocaleId,Message,IsPublished,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,PublishStateId)
SELECT 1,'<p>Recommended Products</p>',NULL,2,GETDATE(),2,GETDATE(),3
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSMessage WHERE Message='<p>Recommended Products</p>')

INSERT INTO ZnodeCMSPortalMessage(PortalId,CMSMessageKeyId,CMSMessageId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT NULL ,(SELECT TOP 1 CMSMessageKeyId FROM ZnodeCMSMessageKey WHERE MessageKey='RecommendedProductsTitle'),
(SELECT TOP 1 CMSMessageId  FROM ZnodeCMSMessage WHERE Message='<p>Recommended Products</p>'),2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT * FROM ZnodeCMSPortalMessage WHERE CMSMessageKeyId IN (SELECT CMSMessageKeyId FROM ZnodeCMSMessageKey WHERE MessageKey='RecommendedProductsTitle'))
AND NOT EXISTS 
(SELECT * FROM ZnodeCMSPortalMessage WHERE CMSMessageId IN (SELECT CMSMessageId  FROM ZnodeCMSMessage WHERE Message='<p>Recommended Products</p>') and PortalId is null)
