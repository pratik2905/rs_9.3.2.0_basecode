﻿

--SET IDENTITY_INSERT [dbo].[ZnodeMedia] ON 
--INSERT [dbo].[ZnodeMedia] ([MediaId], [MediaConfigurationId], [Path], [FileName], [Size], [Height], [Width], [Length], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 5, NULL, N'Media.Jpg', N'5', NULL, NULL, NULL, N'.Jpg', 2, CAST(N'2016-04-05 00:00:00.000' AS DateTime), 2, CAST(N'2016-04-05 00:00:00.000' AS DateTime))
--SET IDENTITY_INSERT [dbo].[ZnodeMedia] OFF

--delete from ZnodeApplicationSetting 
--GO 

--Set identity_insert ZnodeApplicationSetting  on 
--GO 
--insert into ZnodeApplicationSetting (ApplicationSettingId,
--GroupName,
--ItemName,
--Setting,
--ViewOptions,
--FrontPageName,
--FrontObjectName,
--IsCompressed,
--OrderByFields,
--ItemNameWithoutCurrency,
--CreatedByName,
--ModifiedByName,
--CreatedBy,
--CreatedDate,
--ModifiedBy,
--ModifiedDate)
--select ApplicationSettingId,
--GroupName,
--ItemName,
--Setting,
--ViewOptions,
--FrontPageName,
--FrontObjectName,
--IsCompressed,
--OrderByFields,
--ItemNameWithoutCurrency,
--CreatedByName,
--ModifiedByName,
--CreatedBy,
--CreatedDate,
--ModifiedBy,
--ModifiedDate
--from Znode_Multifront_90_Dev..ZnodeApplicationSetting

--Set identity_insert ZnodeApplicationSetting  Off


--Delete  from ZnodePimAttributeValidation  where PimAttributeId in  (275,94)
--Delete from ZnodeAttributeInputValidation where InputValidationId =  16
--Delete  from [ZnodePimAttributeDefaultValueLocale] where PimAttributeDefaultValueId = 15 
--Delete  from [ZnodePimAttributeDefaultValue] where PimAttributeDefaultValueId = 15 
 

-- delete from ZnodeMediaFamilyLocale where MediaFamilyLocaleId in (
--select max(MediaFamilyLocaleId) from ZnodeMediaFamilyLocale 
--group by LocaleId,MediaAttributeFamilyId,AttributeFamilyName having count(*) >1  
--)


--update [dbo].[ZnodePimAttributeGroup] set [IsNonEditable] = 0 


--06-Jun-2016

update ZnodeMenu set ControllerName = 'Store' , ActionName = 'Index' where   Menuid in (2) 
update ZnodeMenu set ControllerName = 'PIM/Products' , ActionName = 'Index' where   Menuid in (6) 
update ZnodeMenu set ControllerName = 'OMS/OMS' , ActionName = 'Index' where   Menuid in (7) 
update ZnodeMenu set ControllerName = 'Reports' , ActionName = 'Index' where   Menuid in (8) 
update ZnodeMenu set ControllerName = 'MediaManager/MediaManager' , ActionName = 'Index' where   Menuid in (9) 
update ZnodeMenu set Isactive= 0 where   Menuid in (10) 
update ZnodeMenu set ControllerName = 'Account' , ActionName = 'Index' where   Menuid in (11) 
update ZnodeMenu set MenuSequence = 2  where   Menuid in (1040) 
update ZnodeMenu set MenuSequence = 2  where   Menuid in (1039) 


set identity_insert ZnodeMenu on 
 
insert into ZnodeMenu 
(MenuId,ParentMenuId,MenuName,MenuSequence,ControllerName,ActionName,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select MenuId,ParentMenuId,MenuName,MenuSequence,ControllerName,ActionName,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate from 
[Znode_Multifront_90_QA]..ZnodeMenu where Menuid in (1041)
set identity_insert ZnodeMenu Off 


----------------26-APR-2017 

SET IDENTITY_INSERT ZnodeActions  ON 
insert into ZnodeActions (ActionId,AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select ActionId,AreaName,ControllerName,ActionName,IsGlobalAccess,CreatedBy,CreatedDate,
ModifiedBy,ModifiedDate FROM Znode_Multifront_90_DEV..ZnodeActions where ActionId 
 in (1849,1850,1851,1852)
SET IDENTITY_INSERT ZnodeActions  OFF 

SET IDENTITY_INSERT ZnodeMenuActionsPermission ON 

 Insert into ZnodeMenuActionsPermission
 (MenuActionsPermissionId,MenuId,ActionId,AccessPermissionId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
 select MenuActionsPermissionId,MenuId,ActionId,AccessPermissionId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate FROM Znode_Multifront_90_DEV..ZnodeMenuActionsPermission where
  ActionId 
 in (1849,1850,1851,1852)
 SET IDENTITY_INSERT ZnodeMenuActionsPermission OFF 

 
SET Identity_insert ZnodeActionMenu  on 
insert into ZnodeActionMenu (ActionMenuId
,MenuId
,ActionId
,CreatedBy
,CreatedDate
,ModifiedBy
,ModifiedDate)
Select ActionMenuId
,MenuId
,ActionId
,CreatedBy
,CreatedDate
,ModifiedBy
,ModifiedDate
from  Znode_Multifront_90_DEv..ZnodeActionMenu  where  ActionId 
 in (1849,1850,1851,1852)
 SET Identity_insert ZnodeActionMenu  Off 

--Select * from ZnodeApplicationSetting
--Except 
--Select * from Znode_Multifront_90_Database..ZnodeApplicationSetting

--Select * into _ZnodeListView from ZnodeListView
--delete from ZnodeListView
--delete from ZnodeListViewFilter

--Begin transaction 
--delete from ZnodeApplicationSetting 
--SET Identity_insert ZnodeApplicationSetting on 
--insert into ZnodeApplicationSetting 
--(ApplicationSettingId,GroupName,ItemName,Setting,ViewOptions,FrontPageName,FrontObjectName,IsCompressed,OrderByFields,ItemNameWithoutCurrency,CreatedByName
--,ModifiedByName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
--Select ApplicationSettingId,GroupName,ItemName,Setting,ViewOptions,FrontPageName,FrontObjectName,IsCompressed,OrderByFields,ItemNameWithoutCurrency,CreatedByName
--,ModifiedByName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate from Znode_Multifront_90_Dev..ZnodeApplicationSetting 
--SET Identity_insert ZnodeApplicationSetting Off 
--Rollback transaction 


--sp_tables  '%review%'

--Select * From ZnodeCMSCustomerReview
--ZnodeProductReviewState

update ZnodePimFrontendProperties  set IsUseInSearch=0 where PimAttributeId in (select zpa.PimAttributeId from znodepimAttribute zpa where AttributeCode not in ('ShortDescription','ProductName','SKU','FeatureDescription','ProductSpecification'))




UPDATE a  
SET GroupDisplayOrder = b.GroupDisplayOrder
FROM ZnodePimFamilyGroupMapper a 
INNER JOIN Znode_Multifront_90_QA..ZnodePimFamilyGroupMapper b ON (b.PimFamilyGroupMapperId = a.PimFamilyGroupMapperId)
WHERE b.PimAttributeFamilyId =1 

UPDATE a  
SET GroupDisplayOrder = b.GroupDisplayOrder
FROM ZnodePimFamilyGroupMapper a 
INNER JOIN Znode_Multifront_90_QA..ZnodePimFamilyGroupMapper b ON (b.PimFamilyGroupMapperId = a.PimFamilyGroupMapperId)
WHERE b.PimAttributeFamilyId =2 

SELECT * FROM ZnodePimFamilyGroupMapper


UPDATE ZnodePimFamilyGroupMapper 
SET GroupDisplayOrder = 0 
WHERE GroupDisplayOrder IS NULL 





update ZnodePaymenttype set Name  = 'CREDIT_CARD' where  PaymentTypeId =1 
update ZnodePaymenttype set Name  = 'PURCHASE_ORDER' where  PaymentTypeId =2 
update ZnodePaymenttype set Name  = 'PAYPAL_EXPRESS' where  PaymentTypeId =3 


GO 
UPDATE ZnodeDomain 
SET DomainName = 'admin.multifront900.localhost.com'
WHERE DomainId = 1 

UPDATE ZnodeDomain 
SET DomainName = 'webstore.multifront900.localhost.com'
WHERE DomainId = 2


UPDATE ZnodeDomain 
SET DomainName = 'api.multifront900.localhost.com'
WHERE DomainId = 3


update ZnodeDomain SET DomainName = 'admin9x.mrrsoft.com' , ApiKey = '60c20be1-c907-42b7-9ca0-2067e32362c0' where DomainId =1 
update ZnodeDomain SET DomainName = 'webstore9x.mrrsoft.com' , ApiKey = '9fae41c7-9645-4374-867c-8111c38c5723' where DomainId =2 
update ZnodeDomain SET DomainName = 'api9x.mrrsoft.com' , ApiKey = '621b32d5-17ee-401d-ba41-61be3e94a935' where DomainId =3 