﻿
CREATE FUNCTION [dbo].[Fn_GetDate]
(

)
RETURNS DATETIME
AS
/*
This function is used to Get the current date
*/
     BEGIN
         -- Declare the return variable here
         DECLARE @Date DATETIME;
         SET @Date = GETDATE();
         
         RETURN @Date;
     END;