﻿
-- SELECT * FROM View_ZnodePimAttributeValue
-- SELECT * FROM ZnodePimAttribute  WHERE AttributeCode = 'ProductName'
	CREATE VIEW [dbo].[View_GetCatalogCategoryProduts]
	 As
	SELECT DISTINCT ISNULL(Null,1) RowId,  a.PimProductId , b.ProductName ,a.PimCatalogId,a.PimCategoryId,b.Localeid
	FROM ZnodePimCatalogCategory a 
	INNER JOIN View_ZnodePimAttributeValue b ON (a.PimProductId = b.PimProductId )
	inner JOIN ZnodePimAttribute c ON (b.PimAttributeId = c.PimAttributeId AND c.AttributeCode = 'ProductName' AND c.IsCategory = 0)