﻿CREATE VIEW [dbo].[View_GetCatalogProduct]
AS
SELECT DISTINCT ZPCC.PimCatalogId, ZPP.PimProductId, ZPP.PublishStateId AS ProductPublishStateId, ZPP.PimAttributeFamilyId, ZPP.ExternalId
FROM            dbo.ZnodePimCatalogCategory AS ZPCC INNER JOIN
                         dbo.ZnodePimProduct AS ZPP ON ZPP.PimProductId = ZPCC.PimProductId

GO