﻿CREATE PROCEDURE [dbo].[Znode_InsertUpdateERPScheduler]
(   @SchedulerXML XML,
    @UserId       INT,
    @Status       BIT OUT)
AS
   /* 
    Summary: This procedure is used insert and update ERP scheduler data 
    		   Get average rating of products 
    		   Get Price / Inventory / SEO details .
    Unit Testing
	BEGIN TRAN   
     EXEC Znode_InsertUpdateERPScheduler 
	ROLLBACK TRAN

	*/
     BEGIN
         BEGIN TRY
             SET NOCOUNT ON;
             DECLARE @TBL_ERPTaskScheduler TABLE
             (ERPTaskSchedulerId    INT,
              SchedulerName         NVARCHAR(200),
			  SchedulerType			VARCHAR(20),	
              TouchPointName        NVARCHAR(200),
              SchedulerFrequency    NVARCHAR(100),
              StartDate             DATETIME,
              [ExpireDate]          DATETIME,
              RepeatTaskEvery       VARCHAR(200),
              RepeatTaskForDuration VARCHAR(200),
              RecurEvery            VARCHAR(200),
              WeekDays              VARCHAR(4000),
              Months                VARCHAR(4000),
              Days                  VARCHAR(4000),
              OnDays                VARCHAR(4000),
              IsEnabled             BIT,
              IsMonthlyDays         BIT,
			  SchedulerCallFor      VARCHAR(200)
             );
             DECLARE @TaskSchedulerSettingId INT;
			 DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
             DECLARE @ERPConfiguratorId INT=
             (
                 SELECT TOP 1 ERPConfiguratorId
                 FROM ZnodeERPConfigurator
                 WHERE IsActive = 1
             );
             DECLARE @ERPTaskShedulerId INT;
             INSERT INTO @TBL_ERPTaskScheduler
                    SELECT Tbl.Col.value('ERPTaskSchedulerId[1]', 'NVARCHAR(max)') AS ERPTaskSchedulerId,
                           Tbl.Col.value('SchedulerName[1]', 'VARCHAR(max)') AS SchedulerName,
						   Tbl.Col.value('SchedulerType[1]', 'NVARCHAR(max)') AS SchedulerType,
                           Tbl.Col.value('TouchPointName[1]', 'NVARCHAR(max)') AS TouchPointName,
                           Tbl.Col.value('SchedulerFrequency[1]', 'NVARCHAR(max)') AS SchedulerFrequency,
                           Tbl.Col.value('StartDate[1]', 'NVARCHAR(max)') AS StartDate,
                           Tbl.Col.value('ExpireDate[1]', 'NVARCHAR(max)') AS [ExpireDate],
                           Tbl.Col.value('RepeatTaskEvery[1]', 'NVARCHAR(max)') AS RepeatTaskEvery,
                           Tbl.Col.value('RepeatTaskForDuration[1]', 'NVARCHAR(max)') AS RepeatTaskForDuration,
                           Tbl.Col.value('RecurEvery[1]', 'NVARCHAR(max)') AS RecurEvery,
                           Tbl.Col.value('WeekDays[1]', 'NVARCHAR(max)') AS WeekDays,
                           Tbl.Col.value('Months[1]', 'NVARCHAR(max)') AS Months,
                           Tbl.Col.value('Days[1]', 'NVARCHAR(max)') AS Days,
                           Tbl.Col.value('OnDays[1]', 'NVARCHAR(max)') AS OnDays,
                           Tbl.Col.value('IsEnabled[1]', 'NVARCHAR(max)') AS IsEnabled,
                           Tbl.Col.value('IsMonthlyDays[1]', 'NVARCHAR(max)') AS IsMonthlyDays,
						   Tbl.Col.value('SchedulerCallFor[1]', 'NVARCHAR(max)') AS SchedulerCallFor
                    FROM @SchedulerXML.nodes('//ERPTaskSchedulerModel') AS Tbl(Col);
             SELECT @TaskSchedulerSettingId = TaskSchedulerSettingId
             FROM ZnodeERPTaskScheduler AS a
                  INNER JOIN @TBL_ERPTaskScheduler AS b ON(a.ERPTaskSchedulerId = b.ERPTaskSchedulerId);
             UPDATE a
               SET
                   SchedulerName = b.SchedulerName,
				   SchedulerType = b.SchedulerType,
                   TouchPointName = b.TouchPointName,
                   SchedulerFrequency = b.SchedulerFrequency,
                   RepeatTaskEvery = b.RepeatTaskEvery,
                   RepeatTaskForDuration = b.RepeatTaskForDuration,
                   StartDate = b.StartDate,
                   ExpireDate = b.ExpireDate,
                   IsEnabled = b.IsEnabled,
                   ModifiedBy = @UserId,
                   ModifiedDate = @GetDate
             FROM ZnodeERPTaskScheduler a
                  INNER JOIN @TBL_ERPTaskScheduler b ON(a.ERPTaskSchedulerId = b.ERPTaskSchedulerId);
             UPDATE a
               SET
                   WeekDays = B.WeekDays,
                   Months = B.Months,
                   Days = B.Days,
                   OnDays = B.OnDays,
                   RecurEvery = B.RecurEvery,
                   ModifiedBy = @UserId,
                   IsMonthlyDays = b.IsMonthlyDays,
                   ModifiedDate = @GetDate
             FROM ZnodeERPTaskSchedulerSetting a
                  INNER JOIN @TBL_ERPTaskScheduler b ON(a.TaskSchedulerSettingId = @TaskSchedulerSettingId);
             INSERT INTO ZnodeERPTaskSchedulerSetting
             (WeekDays,
              Months,
              Days,
              OnDays,
              RecurEvery,
              IsMonthlyDays,
              CreatedBy,
              CreatedDate,
              ModifiedBy,
              ModifiedDate
             )
                    SELECT WeekDays,
                           Months,
                           Days,
                           OnDays,
                           RecurEvery,
                           IsMonthlyDays,
                           @UserId,
                           @GetDate,
                           @UserId,
                           @GetDate
                    FROM @TBL_ERPTaskScheduler
                    WHERE ERPTaskSchedulerId = 0;
             SET @TaskSchedulerSettingId = SCOPE_IDENTITY();
             INSERT INTO ZnodeERPTaskScheduler
             (TaskSchedulerSettingId,
              ERPConfiguratorId,
              SchedulerName,
			  SchedulerType,
              TouchPointName,
              SchedulerFrequency,
              RepeatTaskEvery,
              RepeatTaskForDuration,
              StartDate,
              ExpireDate,
              IsEnabled,
			  SchedulerCallFor,
              CreatedBy,
              CreatedDate,
              ModifiedBy,
              ModifiedDate
             )
                    SELECT @TaskSchedulerSettingId,
                           @ERPConfiguratorId,
                           SchedulerName,
						   SchedulerType,
                           TouchPointName,
                           SchedulerFrequency,
                           RepeatTaskEvery,
                           RepeatTaskForDuration,
                           StartDate,
                           ExpireDate,
                           IsEnabled,
						   SchedulerCallFor,
                           @UserId,
                           @GetDate,
                           @UserId,
                           @GetDate
                    FROM @TBL_ERPTaskScheduler
                    WHERE ERPTaskSchedulerId = 0;
             SET @ERPTaskShedulerId = SCOPE_IDENTITY();
             SET @Status = CAST(1 AS BIT);
             SELECT
             (
                 SELECT TOP 1 CASE
                                  WHEN ERPTaskSchedulerId = 0
                                  THEN @ERPTaskShedulerId
                                  ELSE ERPTaskSchedulerId
                              END
                 FROM @TBL_ERPTaskScheduler
             ) AS Id,
             CAST(1 AS BIT) AS Status;
         END TRY
         BEGIN CATCH
            
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_InsertUpdateERPScheduler @SchedulerXML = '+CAST(@SchedulerXML AS VARCHAR(max))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  select ERROR_MESSAGE();
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_InsertUpdateERPScheduler',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
     END;