﻿CREATE PROCEDURE [dbo].[Znode_DeleteProfileCatalogCategory]
(
	  @ProfileCatalogId INT 
	 ,@PimCatalogId    INT  
	 ,@PimCategoryHierarchyId INT = 0  
	 ,@Status		   BIT  = 0  OUT 
)
AS 
	/* Summary :- This Procedure is used to remove the portal catalog category ids 
	 Unit Testing 
	 EXEC Znode_DeleteProfileCatalogCategory @ProfileCatalogId= ,@PimCatalogId =,@PimCategoryId=
	*/ 
  BEGIN 
   BEGIN TRAN DeleteProfileCategory
	 BEGIN TRY 
     
	 DECLARE @TBL_PimCategoryHierarchyId TABLE (PimCategoryHierarchyId INT)
	 DECLARE @TBL_PimCatalogCategoryId TABLE (PimCatalogCategoryId INT )
	 	 
		 ;with Cet_Data AS 
		  (
		   SELECT PimCategoryHierarchyId ,ParentPimCategoryHierarchyId
		   FROM ZnodePimCategoryHierarchy yrt 
		   WHERE yrt.PimCategoryHierarchyId = @PimCategoryHierarchyId 
		   AND yrt.PimCatalogId = @PimCatalogId
		   UNION ALL 
		   SELECT yrtr.PimCategoryHierarchyId ,yrtr.ParentPimCategoryHierarchyId
		   FROM ZnodePimCategoryHierarchy yrtr 
		   INNER JOIN Cet_Data thr ON ( yrtr.ParentPimCategoryHierarchyId = thr.PimCategoryHierarchyId )  
		    WHERE yrtr.PimCatalogId = @PimCatalogId
		 )

     SELECT * 
	 INTO #Cet_Data
	 FROM Cet_Data a 

	 INSERT INTO @TBL_PimCategoryHierarchyId (PimCategoryHierarchyId)
	 SELECT PimCategoryHierarchyId  
	 FROM #Cet_Data
	
	 INSERT INTO @TBL_PimCatalogCategoryId(PimCatalogCategoryId)
	 SELECT PimCatalogCategoryId
	 FROM ZnodePimCatalogCategory ZPCC 
	 WHERE ZPCC.PimCatalogId = @PimCatalogId 
	 AND  EXISTS ( SELECT TOP 1  1 FROM #Cet_Data FN WHERE ZPCC.PimCategoryHierarchyId = FN.PimCategoryHierarchyId)
	 
	 DELETE FROM ZnodeProfileCatalogCategory 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_PimCatalogCategoryId TBPCC WHERE TBPCC.PimCatalogCategoryId = ZnodeProfileCatalogCategory.PimCatalogCategoryId)
			AND ProfileCatalogId = @ProfileCatalogId
	 
	 DELETE FROM ZnodeProfileCategoryHierarchy 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_PimCategoryHierarchyId TBPH WHERE TBPH.PimCategoryHierarchyId = ZnodeProfileCategoryHierarchy.PimCategoryHierarchyId)
			AND ProfileCatalogId = @ProfileCatalogId

	 SET @Status = 1 
	 SELECT 1 AS ID , CAST(1 AS BIT) AS [Status];
	COMMIT TRAN DeleteProfileCategory 
   END TRY 
   BEGIN CATCH 
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_DeleteProfileCatalogCategory @ProfileCatalogId = '+CAST(@ProfileCatalogId AS VARCHAR(200))+',@PimCatalogId='+CAST(@PimCatalogId AS VARCHAR(200))+',@Status='+CAST(@Status AS VARCHAR(200));
             SET @Status = 0;
             SELECT 0 AS ID,
                    CAST(0 AS BIT) AS Status;
			 ROLLBACK TRAN DeleteProfileCategory;
             EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_DeleteProfileCatalogCategory',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
   END CATCH 
 END