﻿
CREATE PROCEDURE [dbo].[Znode_MergeOmsSavedCartLineItems] 
(
 @OmsSavedCartId INT  
,@OldOmsSavedCartId INT 
,@UserId  INT 
,@Status  BIT = 0  OUT
)
AS 
BEGIN 
 BEGIN TRY 
 SET NOCOUNT ON 

     DECLARE @AddOnOrderLineItemRelationshipTypeId INT = (SELECT TOP 1 OrderLineItemRelationshipTypeId 
															FROM ZnodeOmsOrderLineItemRelationshipType
															WHERE Name = 'Addons')
	 DECLARE @TBL_OmsSavedCartOld TABLE(SKU NVARCHAR(2000), OmsSavedCartLineItemId INT ,ParentSKU NVARCHAR(2000) , ParentOmsSavedCartLineItemId INT ,AddOnSKU NVARCHAR(2000), OmsSavedCartLineItemIdAddOn NVARCHAR(2000) ,PersonalizeCode NVARCHAR(1200), PersonalizeValue  NVARCHAr(max)  )

	 DECLARE @TBL_OmsSavedCartNew TABLE(SKU NVARCHAR(2000), OmsSavedCartLineItemId INT ,ParentSKU NVARCHAR(2000) , ParentOmsSavedCartLineItemId INT ,AddOnSKU NVARCHAR(2000), OmsSavedCartLineItemIdAddOn NVARCHAR(2000) ,PersonalizeCode NVARCHAR(1200), PersonalizeValue  NVARCHAr(max)  )

	 
	 SELECT * 
	 INTO #ZnodeOmsSavedCartLineItemOld
	 FROM ZnodeOmsSavedCartLineItem a 
	 WHERE OmsSavedCartId = @OldOmsSavedCartId 


	 SELECT * 
	 INTO #ZnodeOmsPersonalizeCartItemOld
	 FROM ZnodeOmsPersonalizeCartItem a 
	 WHERE EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsSavedCartLineItemOld t WHERE t.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)

	  SELECT * 
	 INTO #ZnodeOmsSavedCartLineItemNew
	 FROM ZnodeOmsSavedCartLineItem a 
	 WHERE OmsSavedCartId = @OmsSavedCartId 


	 SELECT * 
	 INTO #ZnodeOmsPersonalizeCartItemNew
	 FROM ZnodeOmsPersonalizeCartItem a 
	 WHERE EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)

	 

	 INSERT INTO @TBL_OmsSavedCartOld (SKU,OmsSavedCartLineItemId,ParentSKU,ParentOmsSavedCartLineItemId)
	    SELECT SKU , OmsSavedCartLineItemId,(SELECT TOP 1 SKU FROM #ZnodeOmsSavedCartLineItemOld TBL_B WHERE TBL_B.OmsSavedCartLineItemId = ISNULL(TBL_A.ParentOmsSavedCartLineItemId,0)  ) ParentSKU
					, ParentOmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemOld TBL_A
		WHERE OrderLineItemRelationshipTypeId IS NOT NULL AND OrderLineItemRelationshipTypeId <> @AddOnOrderLineItemRelationshipTypeId
	 
	

	 ;With Cte_UpdateOld AS 
	 (
		SELECT ParentOmsSavedCartLineItemId , SUBSTRING((SELECT ','+SKU FROM #ZnodeOmsSavedCartLineItemOld t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  SKU
		     , SUBSTRING((SELECT ','+CAST(OmsSavedCartLineItemId AS NVARCHAR(max)) FROM #ZnodeOmsSavedCartLineItemOld t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  OmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemOld a 
		WHERE a.OrderLineItemRelationshipTypeId = @AddOnOrderLineItemRelationshipTypeId
	 )
	 
	 UPDATE TBL_O
	 SET TBL_O.AddOnSKU =  TBL_ON.SKU
		,TBL_O.OmsSavedCartLineItemIdAddOn =  TBL_ON.OmsSavedCartLineItemId
	 FROM @TBL_OmsSavedCartOld TBL_O 
	 INNER JOIN Cte_UpdateOld TBL_ON ON (TBL_ON.ParentOmsSavedCartLineItemId  = TBL_O.OmsSavedCartLineItemId )
	   
     UPDATE TBL_O
	 SET TBL_O.PersonalizeCode = TBL_ON.PersonalizeCode
		,TBL_O.PersonalizeValue =  TBL_ON.PersonalizeValue
	 FROM @TBL_OmsSavedCartOld TBL_O 
	 INNER JOIN #ZnodeOmsPersonalizeCartItemOld TBL_ON ON (TBL_ON.OmsSavedCartLineItemId  = TBL_O.OmsSavedCartLineItemId )

	  INSERT INTO @TBL_OmsSavedCartNew (SKU,OmsSavedCartLineItemId,ParentSKU,ParentOmsSavedCartLineItemId)
	    SELECT SKU , OmsSavedCartLineItemId,(SELECT TOP 1 SKU FROM #ZnodeOmsSavedCartLineItemNew TBL_B WHERE TBL_B.OmsSavedCartLineItemId = ISNULL( TBL_A.ParentOmsSavedCartLineItemId,0)   ) ParentSKU
					, ParentOmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemNew TBL_A
		WHERE OrderLineItemRelationshipTypeId IS NOT NULL AND OrderLineItemRelationshipTypeId <> @AddOnOrderLineItemRelationshipTypeId
	 
	 ;With Cte_UpdateNew AS 
	 (
		SELECT ParentOmsSavedCartLineItemId , SUBSTRING((SELECT ','+SKU FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  SKU
		     , SUBSTRING((SELECT ','+CAST(OmsSavedCartLineItemId AS NVARCHAR(max)) FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  OmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemNew a 
		WHERE a.OrderLineItemRelationshipTypeId = @AddOnOrderLineItemRelationshipTypeId
	 )
	 
	 UPDATE TBL_O
	 SET TBL_O.AddOnSKU =  TBL_ON.SKU
		,TBL_O.OmsSavedCartLineItemIdAddOn =  TBL_ON.OmsSavedCartLineItemId
	 FROM @TBL_OmsSavedCartNew TBL_O 
	 INNER JOIN Cte_UpdateNew TBL_ON ON (TBL_ON.ParentOmsSavedCartLineItemId  = TBL_O.OmsSavedCartLineItemId )
	   

	    
    
	 UPDATE TBL_O
	 SET TBL_O.PersonalizeCode = TBL_ON.PersonalizeCode
		,TBL_O.PersonalizeValue =  TBL_ON.PersonalizeValue
	 FROM @TBL_OmsSavedCartNew TBL_O 
	 INNER JOIN #ZnodeOmsPersonalizeCartItemNew TBL_ON ON (TBL_ON.OmsSavedCartLineItemId  = TBL_O.OmsSavedCartLineItemId )



	 UPDATE a 
	 SET  a.Quantity =  d.Quantity 
	 FROM ZnodeOmsSavedCartLineItem a 
	 INNER JOIN @TBL_OmsSavedCartNew b ON (b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	 INNER JOIN @TBL_OmsSavedCartOld c ON (c.SKU = b.SKU AND c.ParentSKU = b.ParentSKU AND ISNULL(c.AddOnSKU,'-1') = ISNULL(b.AddOnSKU,'-1') AND ISNULL(c.PersonalizeCode,'-1') = ISNULL(b.PersonalizeCode,'-1') AND ISNULL(c.PersonalizeValue,'-1') = ISNULL(b.PersonalizeValue,'-1')) 
	 INNER JOIN #ZnodeOmsSavedCartLineItemOld d ON (d.OmsSavedCartLineItemId = c.OmsSavedCartLineItemId)
	 
	 UPDATE  a
	 SET a.OmsSavedCartId = @OmsSavedCartId
	 FROM ZnodeOmsSavedCartLineItem a 
	 WHERE NOT EXISTS (SELECT TOP 1 1 FROM @TBL_OmsSavedCartOld b 
	 INNER JOIN @TBL_OmsSavedCartNew c ON (c.SKU = b.SKU AND c.ParentSKU = b.ParentSKU AND ISNULL(c.AddOnSKU,'-1') = ISNULL(b.AddOnSKU,'-1') AND ISNULL(c.PersonalizeCode,'-1') = ISNULL(b.PersonalizeCode,'-1') AND ISNULL(c.PersonalizeValue,'-1') = ISNULL(b.PersonalizeValue,'-1'))
	 WHERE b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
	 AND NOT EXISTS (SELECT TOP 1 1 FROM @TBL_OmsSavedCartOld b 
	 INNER JOIN @TBL_OmsSavedCartNew c ON (c.SKU = b.SKU AND c.ParentSKU = b.ParentSKU AND ISNULL(c.AddOnSKU,'-1') = ISNULL(b.AddOnSKU,'-1') AND ISNULL(c.PersonalizeCode,'-1') = ISNULL(b.PersonalizeCode,'-1') AND ISNULL(c.PersonalizeValue,'-1') = ISNULL(b.PersonalizeValue,'-1'))
	 WHERE b.ParentOmsSavedCartLineItemId = a.OmsSavedCartLineItemId )
	 AND OmsSavedCartId = @OldOmsSavedCartId 


	 DELETE FROM ZnodeOmsSavedCartLineItem WHERE OmsSavedCartId = @OldOmsSavedCartId 

	 ;with CTE_UpdateOrder 
	 AS 
	 (
	   SELECT Sequence,  ROW_NUMBER()Over(order BY OmsSavedCartLineItemId ASC) RowId
	   FROM ZnodeOmsSavedCartLineItem
	   WHERE  OmsSavedCartId = @OmsSavedCartId
	 
	 ) 
	 UPDATE CTE_UpdateOrder 
	 SET Sequence =RowId
	 

	 UPDATE ZnodeOmsSavedCart
	 SET ModifiedDate = GETDATE()
	 WHERE OmsSavedCartId = @OmsSavedCartId


	 SET @Status = 1 

 END TRY 
 BEGIN CATCH 
  SELECT ERROR_MESSAGE()
   SET @Status = 0
 END CATCH 
END