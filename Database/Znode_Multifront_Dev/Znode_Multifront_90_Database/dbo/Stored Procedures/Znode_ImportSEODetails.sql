﻿  
CREATE  PROCEDURE [dbo].[Znode_ImportSEODetails](  
   @TableName nvarchar(100), 
   @Status bit OUT, @UserId int, 
   @ImportProcessLogId int, 
   @NewGUId nvarchar(200), 
   @LocaleId int= 1,
   @PortalId int ,
   @CsvColumnString nvarchar(max))  
AS  
 --------------------------------------------------------------------------------------  
 -- Summary :  Import SEO Details  
   
 -- Unit Testing :   
 --------------------------------------------------------------------------------------  
  
BEGIN  
 BEGIN TRAN A;  
 BEGIN TRY  
   
  DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);  
  DECLARE @GetDate datetime= dbo.Fn_GetDate();  
    
    
  DECLARE @CMSSEOTypeProduct INT ,@CMSSEOTypeCategory INT  
  
  SELECT @CMSSEOTypeProduct = CMSSEOTypeId FROM ZnodeCMSSEOType WHERE Name = 'Product'  
  SELECT @CMSSEOTypeCategory = CMSSEOTypeId FROM ZnodeCMSSEOType WHERE Name = 'Category'  
  
  
  -- Three type of import required three table varible for product , category and brand  
  DECLARE @InsertSEODetails TABLE  
  (   
   RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ImportType varchar(20), Code nvarchar(300),   
   IsRedirect bit ,MetaInformation nvarchar(max),PortalId int ,SEOUrl nvarchar(max),IsActive varchar(10),  
   SEOTitle nvarchar(max),SEODescription nvarchar(max),SEOKeywords nvarchar(max),   
   RedirectFrom nvarchar(max),RedirectTo nvarchar(max), EnableRedirection bit, CanonicalURL VARCHAR(200),   
   RobotTag VARCHAR(50), GUID nvarchar(400)  
   --Index Ind_ImportType (ImportType),Index Ind_Code (Code)  
  );  
  
  DECLARE @InsertSEODetailsOFProducts TABLE  
  (   
   RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ImportType varchar(20), Code nvarchar(300),   
   IsRedirect bit ,MetaInformation nvarchar(max),PortalId int ,SEOUrl nvarchar(max),IsActive varchar(10),  
   SEOTitle nvarchar(max),SEODescription nvarchar(max),SEOKeywords nvarchar(max),  
   RedirectFrom nvarchar(max),RedirectTo nvarchar(max), EnableRedirection bit, CanonicalURL VARCHAR(200),  
   RobotTag VARCHAR(50),GUID nvarchar(400)  
   --Index Ind_ImportType (ImportType),Index Ind_Code (Code)  
  );  
  
  DECLARE @InsertSEODetailsOFCategory TABLE  
  (   
   RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ImportType varchar(20), Code nvarchar(300),   
   IsRedirect bit ,MetaInformation nvarchar(max),PortalId int ,SEOUrl nvarchar(max),IsActive varchar(10),  
   SEOTitle nvarchar(max),SEODescription nvarchar(max),SEOKeywords nvarchar(max),  
   RedirectFrom nvarchar(max),RedirectTo nvarchar(max), EnableRedirection bit, CanonicalURL VARCHAR(200), 
   RobotTag VARCHAR(50),GUID nvarchar(400)  
   --Index Ind_ImportType (ImportType),Index Ind_Code (Code)  
  );  
  
  DECLARE @InsertSEODetailsOFBrand TABLE  
  (   
   RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ImportType varchar(20), Code nvarchar(300),   
   IsRedirect bit ,MetaInformation nvarchar(max),PortalId int ,SEOUrl nvarchar(max),IsActive varchar(10),  
   SEOTitle nvarchar(max),SEODescription nvarchar(max),SEOKeywords nvarchar(max),   
   RedirectFrom nvarchar(max),RedirectTo nvarchar(max), EnableRedirection bit, CanonicalURL VARCHAR(200),  
   RobotTag VARCHAR(50),GUID nvarchar(400)  
   --Index Ind_ImportType (ImportType),Index Ind_Code (Code)  
  );  
  
    
  DECLARE @InsertedZnodeCMSSEODetail TABLE  
  (   
   CMSSEODetailId int , SEOCode Varchar(4000), CMSSEOTypeId int  
  );  
    
  --SET @SSQL = 'Select RowNumber,ImportType,Code,IsRedirect,MetaInformation,SEOUrl,IsActive,SEOTitle,SEODescription,SEOKeywords,GUID  FROM '+@TableName;  
  SET @SSQL = 'Select RowNumber,'+@CsvColumnString+',GUID  FROM '+@TableName;  
  
  INSERT INTO @InsertSEODetails(RowNumber,ImportType,Code,IsRedirect,MetaInformation,SEOUrl,IsActive,SEOTitle,SEODescription,SEOKeywords,RedirectFrom,RedirectTo,EnableRedirection,CanonicalURL,RobotTag,GUID )  
  EXEC sys.sp_sqlexec @SSQL;  
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '30', 'SEOUrl', SEOUrl, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii   
      where ii.SEOURL in (Select ISD.SEOURL from @InsertSEODetails ISD Group by ISD.SEOUrl having count(*) > 1 ) 
	    
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '10', 'SEOUrl', SEOUrl, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii   
      where EXISTS (Select TOP 1 1 from ZnodeCMSSEODetail ZCSD WHERE ZCSD.SEOUrl = ii.SEOUrl AND ZCSD.PortalId = @PortalId
	  AND ZCSD.SEOCode <> ii.Code  AND EXISTS  
     (SELECT TOP 1 1 FROM ZnodeCMSSEODetailLocale dl WHERE dl.CMSSEODetailId = ZCSD.CMSSEODetailId AND dl.LocaleId = @LocaleId  
           AND dl.SEODescription = ii.SEODescription AND dl.SEOTitle = ii.SEOTitle AND dl.SEOKeywords = ii.SEOKeywords))   
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '53', 'RedirectFrom', RedirectFrom, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii   
      where ii.RedirectFrom in (Select ISD.RedirectFrom from @InsertSEODetails ISD Group by ISD.RedirectFrom having count(*) > 1 )   
  AND (ii.RedirectFrom <> '' )

  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '35', 'RedirectFrom\RedirectTo', RedirectFrom + '  ' + RedirectTo  , @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii   
      where ii.RedirectFrom = ii.RedirectTo  
	  AND (ii.RedirectFrom <> '' AND ii.RedirectTo <> '' )
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '35', 'SEOUrl', SEOUrl, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii  
      WHERE ltrim(rtrim(isnull(ii.SEOUrl,''))) like '% %' -----space not allowed  
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '19', 'ImportType', ImportType, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii  
      WHERE ii.ImportType NOT in   
      (  
       Select NAME from ZnodeCMSSEOType where NAME NOT IN ('Content Page','BlogNews','Brand')  
      );  

  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '35', 'IsActive', IsActive, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetails AS ii  
      WHERE ii.IsActive not in ('True','1','Yes','FALSE','0','No')
  
  --INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
  --    SELECT '30', 'CanonicalURL', CanonicalURL, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
  --    FROM @InsertSEODetails AS ii   
  --    where ii.CanonicalURL in (Select ISD.CanonicalURL from @InsertSEODetails ISD Group by ISD.CanonicalURL having count(*) > 1 )

  --INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
  --    SELECT '10', 'CanonicalURL', CanonicalURL, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
  --    FROM @InsertSEODetails AS ii   
  --    where EXISTS (Select TOP 1 1 from ZnodeCMSSEODetail ZCSD WHERE ZCSD.PortalId = @PortalId
	 -- AND ZCSD.SEOCode <> ii.Code  AND EXISTS  
  --   (SELECT TOP 1 1 FROM ZnodeCMSSEODetailLocale dl WHERE dl.CMSSEODetailId = ZCSD.CMSSEODetailId AND dl.LocaleId = @LocaleId  
  --         AND dl.CanonicalURL = ii.CanonicalURL AND dl.SEODescription = ii.SEODescription AND dl.SEOTitle = ii.SEOTitle AND dl.SEOKeywords = ii.SEOKeywords)) 

  --INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
  --    SELECT '35', 'CanonicalURL', CanonicalURL, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId  
  --    FROM @InsertSEODetails AS ii  
  --    WHERE ltrim(rtrim(isnull(ii.CanonicalURL,''))) like '% %'

  UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ SEOCode - ' + ISNULL(Code,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN @InsertSEODetails IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


  
  DELETE FROM @InsertSEODetails  
  WHERE RowNumber IN  
  (  
   SELECT DISTINCT   
       RowNumber  
   FROM ZnodeImportLog  
   WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null   
   --AND GUID = @NewGUID  
  );  
    
   
  
-------------------------------------------------------------------------------------------------------------------------------  
  
  INSERT INTO @InsertSEODetailsOFProducts(  RowNumber , ImportType , Code ,   
   IsRedirect ,MetaInformation ,SEOUrl ,IsActive ,  
   SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag, GUID )  
   SELECT RowNumber , ImportType , Code , IsRedirect ,MetaInformation ,SEOUrl , 
      CASE WHEN IsActive in ('True','1','Yes') 
	       Then 1 
           ELSE 0
      END as IsActive, SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag, GUID  
   FROM @InsertSEODetails WHERE ImportType = 'Product'  
  
  
  INSERT INTO @InsertSEODetailsOFCategory( RowNumber , ImportType , Code ,   
   IsRedirect ,MetaInformation,SEOUrl ,IsActive ,  
   SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag , GUID )  
   SELECT RowNumber , ImportType , Code , IsRedirect ,MetaInformation ,SEOUrl , 
	   CASE WHEN IsActive in ('True','1','Yes') 
			Then 1 
			ELSE 0
	   END as IsActive, SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag, GUID  
   FROM @InsertSEODetails WHERE ImportType = 'Category'  
  
  INSERT INTO @InsertSEODetailsOFBrand( RowNumber , ImportType , Code ,   
   IsRedirect ,MetaInformation ,SEOUrl ,IsActive ,  
   SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag , GUID )  
   SELECT RowNumber , ImportType , Code , IsRedirect ,MetaInformation ,SEOUrl ,
		CASE WHEN IsActive in ('True','1','Yes') 
			Then 1 
			ELSE 0
	    END as IsActive, SEOTitle ,SEODescription ,SEOKeywords, RedirectFrom, RedirectTo,EnableRedirection, CanonicalURL, RobotTag, GUID  
   FROM @InsertSEODetails WHERE ImportType = 'Brand'  
  
  
     -- start Functional Validation   
  --1. Product  
  --2. Category  
  --3. Content Page  
  --4. Brand  
  -----------------------------------------------  
  
    
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '19', 'SKU', CODE, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetailsOFProducts AS ii  
      WHERE ii.CODE NOT in   
      (  
     SELECT ZPAVL.AttributeValue  
     FROM ZnodePimAttributeValue ZPAV   
     inner join ZnodePimAttributeValueLocale ZPAVL ON ZPAV.PimAttributeValueId = ZPAVL.PimAttributeValueId  
     inner join ZnodePimAttribute ZPA on ZPAV.PimAttributeId = ZPA.PimAttributeId  
     Where ZPA.AttributeCode = 'SKU' AND ZPAVL.AttributeValue IS NOT NULL   
      )  AND ImportType = 'Product';  
  
  
    
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '19', 'Category', CODE, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetailsOFCategory AS ii  
      WHERE ii.CODE NOT in   
      (  
     SELECT ZPCAVL.CategoryValue  
     FROM ZnodePimCategoryAttributeValue ZPCAV   
     INNER JOIN ZnodePimCategoryAttributeValueLocale ZPCAVL on ZPCAV.PimCategoryAttributeValueId = ZPCAVL.PimCategoryAttributeValueId  
     INNER JOIN ZnodePimAttribute ZPA on ZPCAV.PimAttributeId = ZPA.PimAttributeId  
     Where ZPA.AttributeCode = 'CategoryCode' AND ZPCAVL.CategoryValue IS NOT NULL  
      )  AND ImportType = 'Category';  
  
  INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
      SELECT '19', 'Brand', CODE, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
      FROM @InsertSEODetailsOFBrand AS ii  
      WHERE ii.CODE NOT in   
      (  
       Select BrandCode from ZnodeBrandDetails WHERE BrandCode IS NOT NULL  
      )  AND ImportType = 'Brand';  
    
    -- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertSEODetails
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
	

  --Note : Content page import is not required   
    
  -- End Function Validation    
  -----------------------------------------------  
  --- Delete Invalid Data after functional validatin    
  
  DELETE FROM @InsertSEODetailsOFProducts  
  WHERE RowNumber IN  
  (  
   SELECT DISTINCT   
       RowNumber  
   FROM ZnodeImportLog  
   WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null   
   --AND GUID = @NewGUID  
  );  
  
  DELETE FROM @InsertSEODetailsOFCategory  
  WHERE RowNumber IN  
  (  
   SELECT DISTINCT   
       RowNumber  
   FROM ZnodeImportLog  
   WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null   
   --AND GUID = @NewGUID  
  );  
  
  DELETE FROM @InsertSEODetailsOFBrand  
  WHERE RowNumber IN  
  (  
   SELECT DISTINCT   
       RowNumber  
   FROM ZnodeImportLog  
   WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null   
   --AND GUID = @NewGUID  
  );  
  
   
  -- Insert Product Data   
  If Exists (Select top 1 1 from @InsertSEODetailsOFProducts)  
  Begin  
   Update ZCSD SET ZCSD.IsRedirect = ISD.IsRedirect ,  
         ZCSD.MetaInformation =  ISD.MetaInformation,  
         ZCSD.SEOUrl=  ISD.SEOUrl,  
         ZCSD.IsPublish = 0  
   FROM   
   @InsertSEODetailsOFProducts ISD    
   INNER JOIN ZnodeCMSSEODetail ZCSD ON  ZCSD.CMSSEOTypeId = @CMSSEOTypeProduct AND ZCSD.SEOCode = ISD.Code  
   INNER JOIN ZnodeCMSSEODetailLocale ZCSDL ON ZCSD.CMSSEODetailId = ZCSDL.CMSSEODetailId  
   where  ZCSD.PortalId  =@PortalId  AND ZCSDL.LocaleId = @LocaleId;  
     
   Update ZCSDL SET ZCSDL.SEOTitle = ISD.SEOTitle  
       ,ZCSDL.SEODescription = ISD.SEODescription  
       ,ZCSDL.SEOKeywords= ISD.SEOKeywords
	   ,ZCSDL.CanonicalURL = ISD.CanonicalURL
	   ,ZCSDL.RobotTag = ISD.RobotTag  
    FROM   
   @InsertSEODetailsOFProducts ISD    
   INNER JOIN ZnodeCMSSEODetail ZCSD ON  ZCSD.CMSSEOTypeId = @CMSSEOTypeProduct AND ZCSD.SEOCode = ISD.Code  
   INNER JOIN ZnodeCMSSEODetailLocale ZCSDL ON ZCSD.CMSSEODetailId = ZCSDL.CMSSEODetailId  
   where  ZCSD.PortalId = @PortalId AND ZCSDL.LocaleId = @LocaleId;   
  
     
   insert into ZnodeCMSSEODetailLocale (CMSSEODetailId,LocaleId,SEOTitle,SEODescription,SEOKeywords,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,CanonicalURL,RobotTag)  
   SELECT distinct CSD.CMSSEODetailId,@LocaleId,ISD.SEOTitle,ISD.SEODescription,ISD.SEOKeywords,@USerId, @GetDate,@USerId, @GetDate, CanonicalURL,RobotTag  
   FROM ZnodeCMSSEODetail CSD  
   INNER JOIN @InsertSEODetailsOFProducts ISD ON CSD.SEOCode = ISD.Code AND CSD.CMSSEOTypeId = @CMSSEOTypeProduct AND CSD.SEOUrl = ISD.SEOUrl  
   WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSSEODetailLocale CSDL WHERE CSDL.LocaleId = @LocaleId AND CSD.CMSSEODetailId = CSDL.CMSSEODetailId )  
   AND CSD.portalId = @PortalId  
  
     
   Delete from @InsertedZnodeCMSSEODetail  
  
   IF NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSSEODetail SD INNER JOIN @InsertSEODetailsOFProducts DP ON SD.SEOCode = DP.Code AND SD.PortalId =  @PortalId  
                        AND  SD.CMSSEOTypeId = @CMSSEOTypeProduct)  
   BEGIN  
   INSERT INTO ZnodeCMSSEODetail(CMSSEOTypeId,SEOCode,IsRedirect,MetaInformation,PortalId,SEOUrl,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)    
   OUTPUT Inserted.CMSSEODetailId,Inserted.SEOCode,Inserted.CMSSEOTypeId INTO @InsertedZnodeCMSSEODetail    
   Select Distinct @CMSSEOTypeProduct,ISD.Code , ISD.IsRedirect,ISD.MetaInformation,@PortalId,ISD.SEOUrl,@USerId, @GetDate,@USerId, @GetDate from   
   @InsertSEODetailsOFProducts ISD    
   where NOT EXISTS (Select TOP 1 1 from ZnodeCMSSEODetail ZCSD where ZCSD.CMSSEOTypeId = @CMSSEOTypeProduct AND ZCSD.SEOCode = ISD.Code and  ZCSD.PortalId =@PortalId   );  
    
   insert into ZnodeCMSSEODetailLocale(CMSSEODetailId,LocaleId,SEOTitle,SEODescription,SEOKeywords,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,CanonicalURL,RobotTag)  
   Select Distinct IZCSD.CMSSEODetailId,@LocaleId,ISD.SEOTitle,ISD.SEODescription,ISD.SEOKeywords,@USerId, @GetDate,@USerId, @GetDate,CanonicalURL,RobotTag   
   from @InsertedZnodeCMSSEODetail IZCSD   
   INNER JOIN @InsertSEODetailsOFProducts ISD ON IZCSD.SEOCode = ISD.Code   
  
   END  
     
  
   -----RedirectUrlInsert  
  -- INSERT INTO ZnodeCMSUrlRedirect ( RedirectFrom,RedirectTo,IsActive,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)  
  -- select RedirectFrom,RedirectTo,EnableRedirection,@PortalId as PortalId ,@USerId as CreatedBy,@GetDate as CreatedDate,@USerId as ModifiedBy,@GetDate as ModifiedDate  
  -- from @InsertSEODetailsOFProducts  
  -- where IsRedirect = 1  
  --END  
  
  
     INSERT INTO ZnodeCMSUrlRedirect ( RedirectFrom,RedirectTo,IsActive,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)  
   select RedirectFrom,RedirectTo,EnableRedirection 
 ,@PortalId as PortalId ,@USerId as CreatedBy,@GetDate as CreatedDate,@USerId as ModifiedBy,@GetDate as ModifiedDate  
   from @InsertSEODetailsOFProducts  SDP
   where IsRedirect = 1  
   AND NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSUrlRedirect ZCR 
								  WHERE ZCR.RedirectFrom = SDP.RedirectFrom AND ZCR.RedirectTo = SDP.RedirectTo)
   AND (SDP.RedirectFrom <> '' AND SDP.RedirectTo <> '' )
  END  
  
  -- Insert Category Data   
  If Exists (Select top 1 1 from @InsertSEODetailsOFCategory)  
  Begin  
  
   Update ZCSD SET ZCSD.IsRedirect = ISD.IsRedirect ,  
         ZCSD.MetaInformation =  ISD.MetaInformation,  
         ZCSD.SEOUrl=  ISD.SEOUrl,  
         ZCSD.IsPublish = 0  
   FROM   
   @InsertSEODetailsOFCategory ISD    
   INNER JOIN ZnodeCMSSEODetail ZCSD ON  ZCSD.CMSSEOTypeId = @CMSSEOTypeCategory AND ZCSD.SEOCode = ISD.Code  
   INNER JOIN ZnodeCMSSEODetailLocale ZCSDL ON ZCSD.CMSSEODetailId = ZCSDL.CMSSEODetailId  
   where  ZCSD.PortalId  =@PortalId  AND ZCSDL.LocaleId = @LocaleId;  
     
     
   Update ZCSDL SET ZCSDL.SEOTitle = ISD.SEOTitle  
       ,ZCSDL.SEODescription = ISD.SEODescription  
       ,ZCSDL.SEOKeywords= ISD.SEOKeywords 
	   ,CanonicalURL = ISD.CanonicalURL
	   ,RobotTag = ISD.RobotTag
    FROM   
   @InsertSEODetailsOFCategory ISD    
   INNER JOIN ZnodeCMSSEODetail ZCSD ON  ZCSD.CMSSEOTypeId = @CMSSEOTypeCategory AND ZCSD.SEOCode = ISD.Code  
   INNER JOIN ZnodeCMSSEODetailLocale ZCSDL ON ZCSD.CMSSEODetailId = ZCSDL.CMSSEODetailId  
   where  ZCSD.PortalId  =@PortalId AND ZCSDL.LocaleId = @LocaleId;   
  
   insert into ZnodeCMSSEODetailLocale (CMSSEODetailId,LocaleId,SEOTitle,SEODescription,SEOKeywords,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,CanonicalURL,RobotTag)  
   SELECT distinct CSD.CMSSEODetailId,@LocaleId,ISD.SEOTitle,ISD.SEODescription,ISD.SEOKeywords,@USerId, @GetDate,@USerId, @GetDate,CanonicalURL,RobotTag  
   FROM ZnodeCMSSEODetail CSD  
   INNER JOIN @InsertSEODetailsOFProducts ISD ON CSD.SEOCode = ISD.Code AND CSD.CMSSEOTypeId = @CMSSEOTypeCategory AND CSD.SEOUrl = ISD.SEOUrl  
   WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSSEODetailLocale CSDL WHERE CSDL.LocaleId = @LocaleId AND CSD.CMSSEODetailId = CSDL.CMSSEODetailId )  
   AND CSD.portalId = @PortalId  
  
  
   Delete from @InsertedZnodeCMSSEODetail  
  
   IF NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSSEODetail SD INNER JOIN @InsertSEODetailsOFProducts DP ON SD.SEOCode = DP.Code AND SD.PortalId =  @PortalId  
                        AND  SD.CMSSEOTypeId = @CMSSEOTypeCategory)  
   BEGIN  
   INSERT INTO ZnodeCMSSEODetail(CMSSEOTypeId,SEOCode,IsRedirect,MetaInformation,PortalId,SEOUrl,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)    
   OUTPUT Inserted.CMSSEODetailId,Inserted.SEOCode,Inserted.CMSSEOTypeId INTO @InsertedZnodeCMSSEODetail    
   Select Distinct @CMSSEOTypeCategory,ISD.Code , ISD.IsRedirect,ISD.MetaInformation,@PortalId,ISD.SEOUrl,@USerId, @GetDate,@USerId, @GetDate   
   from @InsertSEODetailsOFCategory ISD    
   where NOT EXISTS (Select TOP 1 1 from ZnodeCMSSEODetail ZCSD where ZCSD.CMSSEOTypeId = @CMSSEOTypeCategory AND ZCSD.SEOCode  = ISD.Code AND ZCSD.PortalId = @PortalId );  
  
   insert into ZnodeCMSSEODetailLocale(CMSSEODetailId,LocaleId,SEOTitle,SEODescription,SEOKeywords,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,CanonicalURL,RobotTag)  
   Select Distinct IZCSD.CMSSEODetailId,@LocaleId,ISD.SEOTitle,ISD.SEODescription,ISD.SEOKeywords,@USerId, @GetDate,@USerId, @GetDate,CanonicalURL,RobotTag   
   from @InsertedZnodeCMSSEODetail IZCSD   
   INNER JOIN @InsertSEODetailsOFCategory ISD ON IZCSD.SEOCode = ISD.Code   
   WHERE IZCSD.CMSSEOTypeId =@CMSSEOTypeCategory    
   END  
  
   -----RedirectUrlInsert  
   INSERT INTO ZnodeCMSUrlRedirect ( RedirectFrom,RedirectTo,IsActive,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)  
   SELECT RedirectFrom,RedirectTo,
   EnableRedirection,@PortalId as PortalId ,2 as CreatedBy,@GetDate as CreatedDate,2 as ModifiedBy,@GetDate as ModifiedDate  
   FROM @InsertSEODetailsOFProducts SDP  
   WHERE IsRedirect = 1 
   --AND (SDP.RedirectFrom <> '' AND SDP.RedirectTo <> '' ) 
   AND NOT EXISTS (SELECT TOP 1 1 FROM ZnodeCMSUrlRedirect ZCR 
								  WHERE ZCR.RedirectFrom = SDP.RedirectFrom AND ZCR.RedirectTo = SDP.RedirectTo)
   AND (SDP.RedirectFrom <> '' AND SDP.RedirectTo <> '' )
   --AND ((SDP.RedirectFrom <> '' OR SDP.RedirectFrom IS NOT NULL )
   --OR ( SDP.RedirectTo <> '' OR SDP.RedirectTo IS NOT NULL ))
  END  
             
  --select 'End'  
  --      SET @Status = 1;  
  UPDATE ZnodeImportProcessLog  
    SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate  
  WHERE ImportProcessLogId = @ImportProcessLogId;  
  
  COMMIT TRAN A;  
 END TRY  
 BEGIN CATCH  
  
  UPDATE ZnodeImportProcessLog  
    SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate  
  WHERE ImportProcessLogId = @ImportProcessLogId;  
  
  SET @Status = 0;  
  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();  
  ROLLBACK TRAN A;  
 END CATCH;  
END;