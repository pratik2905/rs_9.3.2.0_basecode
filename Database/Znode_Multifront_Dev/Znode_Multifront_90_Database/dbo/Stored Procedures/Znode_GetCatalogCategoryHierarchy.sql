﻿-- =============================================
-- Author:		Shubham
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Znode_GetCatalogCategoryHierarchy] 
	-- Add the parameters for the stored procedure here
	@PimProductId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DEclare @PimAttributeId int

	set  @PimAttributeId = (SELECT TOP 1 PimAttributeId from ZnodePimAttribute where AttributeCode='CategoryName')

	SELECT Distinct c.PimCatalogId,c.CatalogName,locale.CategoryValue, zCatalogCategory.PimCategoryId 
	FROM ZnodePimCatalogCategory as zCatalogCategory
	INNER JOIN ZnodePimCatalog as c on c.PimCatalogId =zCatalogCategory.PimCatalogId
	INNER JOIN ZnodePimCategoryAttributeValue as categoryAttribute on categoryAttribute.PimCategoryId = zCatalogCategory.PimCategoryId
	INNER JOIN ZnodePimCategoryAttributeValueLocale as locale on locale.PimCategoryAttributeValueId = categoryAttribute.PimCategoryAttributeValueId
	WHERE zCatalogCategory.PimProductId=@PimProductId and categoryAttribute.PimAttributeId = @PimAttributeId 
END;