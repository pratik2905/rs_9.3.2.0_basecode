﻿CREATE PROCEDURE [dbo].[Znode_GetPimCatalogAssociatedCategory]
(	@WhereClause      XML,
	@Rows             INT           = 100,
	@PageNo           INT           = 1,
	@Order_BY         VARCHAR(1000) = '',
	@RowsCount        INT OUT,
	@LocaleId         INT           = 1,
	@PimCatalogId     INT           = 0,
	@IsAssociated     BIT           = 0,
	@ProfileCatalogId INT           = 0,
	@PimCategoryId    INT           = -1,
	@PimCategoryHierarchyId INT = 0 )
AS
/*
     Summary :- This procedure is used to get the attribute values as per changes 
     Unit Testing 
	 begin tran
     EXEC [dbo].[Znode_GetPimCatalogAssociatedCategory] @WhereClause = '',@RowsCount = 0 ,@PimCatalogId = 5 ,@ProfileCatalogId= 0,@IsAssociated = 1,@PimCategoryId = -1
     rollback tran
	 SELECT * FROM ZnodePimCategoryHierarchy
*/
     BEGIN
         BEGIN TRY
             SET NOCOUNT ON;
             DECLARE @TBL_PimcategoryDetails TABLE
             (PimCategoryId INT,
              CountId       INT,
              RowId         INT
             );
             DECLARE @TBL_CategoryIds TABLE
             (PimCategoryId       INT,
              ParentPimcategoryId INT
             );
             DECLARE @TBL_AttributeValue TABLE
             (PimCategoryAttributeValueId INT,
              PimCategoryId               INT,
              CategoryValue               NVARCHAR(MAX),
              AttributeCode               VARCHAR(300),
              PimAttributeId              INT
             );
             DECLARE @TBL_FamilyDetails TABLE
             (PimCategoryId        INT,
              PimAttributeFamilyId INT,
              AttributeFamilyName  NVARCHAR(MAX)
             );
             DECLARE @TBL_DefaultAttributeValue TABLE
             (PimAttributeId            INT,
              AttributeDefaultValueCode VARCHAR(600),
              IsEditable                BIT,
              AttributeDefaultValue     NVARCHAR(MAX)
			  ,DisplayOrder             INT 
             );
             DECLARE @TBL_ProfileCatalogCategory TABLE
             (ProfileCatalogId       INT,
              PimCategoryHierarchyId INT,
              PimCategoryId          INT,
              PimParentCategoryId    INT,
			  ParentPimCategoryHierarchyId INT 
             );
             DECLARE @PimCategoryIds VARCHAR(MAX), @PimAttributeIds VARCHAR(MAX);
             IF @ProfileCatalogId > 0
                 BEGIN
						INSERT INTO @TBL_ProfileCatalogCategory(ProfileCatalogId,PimCategoryHierarchyId,PimCategoryId,ParentPimCategoryHierarchyId)
						SELECT ZPCC.ProfileCatalogId,ZPCC.PimCategoryHierarchyId,ZCC.PimCategoryId ,ZCC.ParentPimCategoryHierarchyId
						FROM ZnodePimCategoryHierarchy AS ZCC
						--INNER JOIN ZnodePimCategoryHierarchy AS tr ON (tr.PimCategoryHierarchyId = ZCC.ParentPimCategoryHierarchyId)
						INNER JOIN ZnodeProfileCategoryHierarchy AS ZPCC ON(ZPCC.PimCategoryHierarchyId = ZCC.PimCategoryHierarchyId)
						WHERE ZPCC.ProfileCatalogId = @ProfileCatalogId --AND (ZCC.ParentPimCategoryHierarchyId = @PimCategoryHierarchyId OR @PimCategoryHierarchyId = -1)								
					    AND ZCC.PimCatalogId = 	@pimCatalogId
						
							;
                     IF @IsAssociated = 1
                     BEGIN
						INSERT INTO @TBL_CategoryIds(PimCategoryId,ParentPimcategoryId)
						SELECT PimCategoryId, NULL  
						FROM @TBL_ProfileCatalogCategory FNGTRCT                                      							
						--WHERE EXISTS
						--(SELECT TOP 1 1 FROM @TBL_ProfileCatalogCategory AS TBPCC WHERE TBPCC.PimCategoryId = FNGTRCT.PimCategoryId OR TBPCC.PimParentCategoryId = FNGTRCT.ParentPimcategoryId);
																																								
                     END;
                     ELSE
					 BEGIN
				
						                                                                         
						SELECT ZCC.PimCategoryId ,ZCC.ParentPimCategoryHierarchyId,ZCC.PimCategoryHierarchyId
						INTO #temp_table 
						FROM [dbo].[Fn_GetRecurciveCategoryIds_ForChild] (@PimCategoryHierarchyId,@PimCatalogId) AS ZCC
						WHERE NOT EXISTS(SELECT TOP 1 1 FROM @TBL_ProfileCatalogCategory AS TBPCC WHERE TBPCC.PimCategoryHierarchyId = ZCC.PimCategoryHierarchyId) 
						AND ZCC.ParentPimCategoryHierarchyId = @PimCategoryHierarchyId
						--AND ZCC.PimCatalogId=  @PimCatalogId
						--AND (EXISTS( SELECT TOP 1 1 FROM  WHERE RTYR.PimCategoryHierarchyId = ZCC.ParentPimCategoryHierarchyId   ) OR @PimCategoryHierarchyId = -1 )
						--AND (ISNULL(FNGTRCT.ParentPimcategoryId, 0) = @PimCategoryId
					 --    OR @PimCategoryHierarchyId = -1
					--	)
						;           
                       --   SELECT * FROM @TBL_ProfileCatalogCategory                                                                                                                                                                                                                                     
                       --SELECT * FROM #temp_table
             --   SELECT * FROM @TBL_CategoryIds                                      
					 IF @PimCategoryHierarchyId = -1 
						BEGIN
						
						 INSERT INTO @TBL_CategoryIds(PimCategoryId, ParentPimcategoryId)           
						 SELECT DISTINCT PimCategoryId ,NULL 
						 FROM ZnodePimCAtegoryHierarchy a

						 WHERE ParentPimCategoryHierarchyId IS NULL 
						 AND NOT EXISTS ( SELECT TOP 1 1 FROM @TBL_ProfileCatalogCategory RTRYR WHERE RTRYR.PimCategoryHierarchyId = a.PimCategoryHierarchyId )
						 AND a.PimCatalogId=  @PimCatalogId
						-- DELETE FROM @TBL_CategoryIds  WHERE PimCategoryId IN (SELECT PimCategoryId FROM @TBL_CategoryIds WHERE ParentPimcategoryId IS NOT NULL );                                                                                                                

                                                                                                                     
						END
						ELSE 
						BEGIN 

						 INSERT INTO @TBL_CategoryIds(PimCategoryId, ParentPimcategoryId)           
						 SELECT DISTINCT PimCategoryId ,NULL 
						 FROM #temp_table
						-- WHERE ParentPimCategoryHierarchyId IS NULL 

						END 
						
						;
					 SET @IsAssociated = 1;
					 END;
                   
                     IF NOT EXISTS (SELECT TOP 1 1 FROM @TBL_CategoryIds)                                                                                           
                        BEGIN
                        INSERT INTO @TBL_CategoryIds(PimCategoryId,ParentPimcategoryId)
                        SELECT -1,0;                                                                                                                                        
                        END;
                 END;
					 ELSE
						BEGIN
						INSERT INTO @TBL_CategoryIds(PimCategoryId,ParentPimcategoryId )
						SELECT PimCategoryId,ParentPimcategoryId 
						FROM  [dbo].[Fn_GetRecurciveCategoryIds_new](@PimCategoryHierarchyId, @PimCatalogId) AS FNGTRCT 
						UNION ALL 
					    SELECT ass.PimCategoryId,null
						FROM  ZnodePimCategoryHierarchy ass 
						WHERE (ass.ParentPimCategoryHierarchyId =  @PimCategoryHierarchyId   OR ( @PimCategoryHierarchyId = -1 AND ass.ParentPimCategoryHierarchyId IS NULL ))
						AND ass.PimCatalogId =@PimCatalogId
						 SET @IsAssociated = 0                                                                                                                               
						
						END;
						

					--	SELECT * FROM @TBL_CategoryIds
						SET @PimCategoryIds = SUBSTRING((SELECT ','+CAST(PimCategoryId AS VARCHAR(100)) 
						FROM @TBL_CategoryIds  FOR XML PATH('') ), 2, 4000);
                        
                        INSERT INTO @TBL_PimcategoryDetails(PimCategoryId, CountId,RowId  )                      
                        EXEC Znode_GetCategoryIdForPaging @WhereClause,@Rows,@PageNo,@Order_BY,@RowsCount,@LocaleId,'',@PimCategoryIds,@IsAssociated;
						
						SET @RowsCount = ISNULL((SELECT TOP 1 CountId FROM @TBL_PimcategoryDetails), 0);
																																														
						SET @PimCategoryIds = SUBSTRING((SELECT ','+CAST(PimCategoryId AS VARCHAR(100)) FROM @TBL_PimcategoryDetails FOR XML PATH('') ), 2, 4000);
																																																																				
						SET @PimAttributeIds = SUBSTRING((SELECT ','+CAST(PimAttributeId AS VARCHAR(100)) FROM [dbo].[Fn_GetCategoryGridAttributeDetails]()
											   FOR XML PATH('')	), 2, 4000);	
						DECLARE @TBL_MediaAttribute TABLE (Id INT ,PimAttributeId INT ,AttributeCode VARCHAR(600) )

						 INSERT INTO @TBL_MediaAttribute (Id,PimAttributeId,AttributeCode )
						 SELECT Id,PimAttributeId,AttributeCode 
						 FROM [dbo].[Fn_GetProductMediaAttributeId]()
																																																															
						INSERT INTO @TBL_AttributeValue(PimCategoryAttributeValueId,PimCategoryId,CategoryValue,AttributeCode,PimAttributeId)
						EXEC [dbo].[Znode_GetCategoryAttributeValue]@PimCategoryIds,@PimAttributeIds,@LocaleId;

						INSERT INTO @TBL_FamilyDetails(PimAttributeFamilyId,AttributeFamilyName,PimCategoryId)
						EXEC Znode_GetCategoryFamilyDetails @PimCategoryIds,@LocaleId;
							
							
						INSERT INTO @TBL_DefaultAttributeValue(PimAttributeId,AttributeDefaultValueCode,IsEditable,AttributeDefaultValue,DisplayOrder )
						EXEC [dbo].[Znode_GetAttributeDefaultValueLocale] @PimAttributeIds,@LocaleId;
						
						INSERT INTO @TBL_AttributeValue ( PimCategoryId , CategoryValue , AttributeCode )

						SELECT PimCategoryId,AttributeFamilyName , 'AttributeFamily'
						FROM @TBL_FamilyDetails 		

						UPDATE  TBAV
						SET CategoryValue  = SUBSTRING ((SELECT ','+[dbo].FN_GetMediaThumbnailMediaPath(zm.Path) FROM ZnodeMedia ZM  WHERE EXISTS (SELECT TOP 1 1 FROM dbo.split(TBAV.CategoryValue ,',') SP  WHERE SP.Item = CAST(ZM.MediaId AS VARCHAR(50)) ) FOR XML PATH('')



),2,4000)
						FROM @TBL_AttributeValue TBAV 
						INNER JOIN @TBL_MediaAttribute TBMA ON (TBMA.PimAttributeId = TBAV.PimAttributeId)	

						DECLARE @CategoryXML XML 

						SET @CategoryXML =  '<MainCategory>'+ STUFF( ( SELECT '<Category>'+'<PimCategoryId>'+CAST(TBAD.PimCategoryId AS VARCHAR(50))+'</PimCategoryId>'+ STUFF(    (  SELECT '<'+TBADI.AttributeCode+'>'+CAST((SELECT ''+TBADI.CategoryValue FOR XML PATH('')) AS

 NVARCHAR(max))+'</'+TBADI.AttributeCode+'>'   
						 									 FROM @TBL_AttributeValue TBADI      
															 WHERE TBADI.PimCategoryId = TBAD.PimCategoryId 
															 ORDER BY TBADI.PimCategoryId DESC
															 FOR XML PATH (''), TYPE
																).value('.', ' Nvarchar(max)'), 1, 0, '')+'</Category>'	   

			FROM @TBL_AttributeValue TBAD
			INNER JOIN @TBL_PimcategoryDetails TBPI ON (TBAD.PimCategoryId = TBPI.PimCategoryId )
			GROUP BY TBAD.PimCategoryId,TBPI.RowId 
			ORDER BY TBPI.RowId 
			FOR XML PATH (''),TYPE).value('.', ' Nvarchar(max)'), 1, 0, '')+'</MainCategory>'


			SELECT  @CategoryXML  CategoryXMl
		   
		     SELECT AttributeCode ,  ZPAL.AttributeName
			 FROM ZnodePimAttribute ZPA 
			 LEFT JOIN ZnodePiMAttributeLOcale ZPAL ON (ZPAL.PimAttributeId = ZPA.PimAttributeId )
             WHERE LocaleId = 1 
			 AND IsCategory = 1  
			 AND ZPA.IsShowOnGrid = 1  

		    SELECT ISNULL(@RowsCount,0) AS RowsCount;
						
						
						
						
						
						--WITH Cte_DefaultCategoryValue	
						--AS (SELECT PimCategoryId,PimAttributeId,
						--	SUBSTRING((SELECT ','+AttributeDefaultValue FROM @TBL_DefaultAttributeValue AS TBDAV WHERE TBDAV.PimAttributeId = TBAV.PimAttributeId
						--	AND EXISTS(SELECT TOP 1 1 FROM dbo.Split(TBAV.CategoryValue, ',') AS SP WHERE sp.Item = TBDAV.AttributeDefaultValueCode)
						--	FOR XML PATH('')), 2, 4000) AS AttributeValue FROM @TBL_AttributeValue AS TBAV
						--	WHERE EXISTS(SELECT TOP 1 1	FROM [dbo].[Fn_GetCategoryDefaultValueAttribute]() AS SP WHERE SP.PimAttributeId = TBAV.PimAttributeId))
																																																			
						--UPDATE TBAV SET TBAV.CategoryValue = CTDCV.AttributeValue FROM @TBL_AttributeValue TBAV
						--INNER JOIN Cte_DefaultCategoryValue CTDCV ON(CTDCV.PimCategoryId = TBAV.PimCategoryId AND CTDCV.PimAttributeId = TBAV.PimAttributeId);
																																										
						--SELECT TBCD.PimCategoryId,Piv.CategoryName,ZPC.IsActive AS [Status],dbo.FN_GetMediaThumbnailMediaPath(Zm.Path) AS CategoryImage,
						--ISNULL(TBFD.AttributeFamilyName, '') AS AttributeFamilyName FROM @TBL_PimcategoryDetails AS TBCD
						--INNER JOIN(SELECT PimCategoryId,CategoryValue,AttributeCode FROM @TBL_AttributeValue)
					 --   AS TBAV PIVOT(MAX(CategoryValue) FOR AttributeCode IN([CategoryName],[CategoryImage])) PIV ON(Piv.PimCategoryId = TBCD.PimCategoryId)																				
						--LEFT JOIN @TBL_FamilyDetails AS TBFD ON(TBFD.PimCategoryId = PIV.PimCategoryId)
						--LEFT JOIN ZnodeMedia AS ZM ON(CAST(ZM.MediaId AS VARCHAR(50)) = PIV.[CategoryImage])
						--LEFT JOIN ZnodePimCategory AS ZPC ON(ZPC.PimCategoryId = Piv.PimCategoryId)
						--ORDER BY RowId;
         END TRY
         BEGIN CATCH
		     SELECT ERROR_MESSAGE()
             DECLARE @Status BIT ;
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetPimCatalogAssociatedCategory @WhereClause = '+CAST(@WhereClause AS VARCHAR(max))+',@Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',@LocaleId = '+CAST(@LocaleId AS VARCHAR(50))+',@PimCatalogId='+CAST(@PimCatalogId AS VARCHAR(50))+',@IsAssociated='+CAST(@IsAssociated AS VARCHAR(50))+',@ProfileCatalogId='+CAST(@ProfileCatalogId AS VARCHAR(50))+',@PimCategoryId='+CAST(@PimCategoryId AS VARCHAR(50))+',@RowsCount='+CAST(@RowsCount AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_GetPimCatalogAssociatedCategory',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
     END;
