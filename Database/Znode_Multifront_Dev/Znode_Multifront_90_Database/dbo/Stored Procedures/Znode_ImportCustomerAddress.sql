﻿CREATE PROCEDURE [dbo].[Znode_ImportCustomerAddress](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @LocaleId int= 0,@PortalId int ,@CsvColumnString nvarchar(max), @IsAccountAddress bit = 0 )
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import SEO Details
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max),@IsAllowGlobalLevelUserCreation nvarchar(10)

		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		-- Retrive Value from global setting 
		Select @IsAllowGlobalLevelUserCreation = FeatureValues from ZnodeGlobalsetting where FeatureName = 'AllowGlobalLevelUserCreation'
		-- Three type of import required three table varible for product , category and brand

		CREATE TABLE #InsertCustomerAddress 
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int,UserName	nvarchar(512)
			,FirstName	varchar	(300),LastName	varchar	(300),DisplayName	nvarchar(1200),Address1	varchar	(300),Address2	varchar	(300)
			,CountryName	varchar	(3000),StateName	varchar	(3000),CityName	varchar	(3000),PostalCode	varchar	(50)
			,PhoneNumber	varchar	(50),
			--Mobilenumber	varchar(50),AlternateMobileNumber	varchar(50),FaxNumber	varchar(30),
			IsDefaultBilling	bit 
			,IsDefaultShipping	bit	,IsActive	bit	,ExternalId	nvarchar(2000),CompanyName nvarchar(2000), GUID NVARCHAR(400)
		);
		
		--SET @SSQL = 'SELECT RowNumber,UserName,FirstName,LastName,MiddleName,BudgetAmount,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId,GUID FROM '+ @TableName;
		--SET @SSQL = 'SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		SET @SSQL = ' INSERT INTO #InsertCustomerAddress ( RowNumber, ' + @CsvColumnString + ' ,GUID )
		SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		--INSERT INTO @InsertCustomerAddress( RowNumber,UserName,FirstName,LastName,DisplayName,Address1,Address2,CountryName,
		--									StateName,CityName,PostalCode,PhoneNumber,
		--									IsDefaultBilling,IsActive,IsDefaultShipping,ExternalId,CompanyName,GUID )
		EXEC sys.sp_sqlexec @SSQL;

	
		-- start Functional Validation 
		-----------------------------------------------
		IF (@IsAccountAddress = 0)
		BEGIN
			IF @IsAllowGlobalLevelUserCreation = 'true'
					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
						   SELECT '19', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
						   FROM #InsertCustomerAddress AS ii
						   WHERE ii.UserName NOT IN 
						   (
							   SELECT UserName FROM AspNetZnodeUser   where PortalId = @PortalId
						   );
			ELSE 
					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
						   SELECT '19', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
						   FROM #InsertCustomerAddress AS ii
						   WHERE ii.UserName NOT IN 
						   (
							   SELECT UserName FROM AspNetZnodeUser   
						   );

					INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
							SELECT '8', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
							FROM #InsertCustomerAddress AS ii
							WHERE ISnull(ltrim(rtrim(ii.UserName)), '') = ''

		 END

		 -- error log when atleast db have 
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '63', 'IsDefaultBilling/IsDefaultShipping', IsDefaultBilling, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM #InsertCustomerAddress IC where  exists (
		SELECT TOP 1 1  from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
		INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
		INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
		INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
		where ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
		AND ZA.IsDefaultShipping =IC.IsDefaultShipping )
			
		--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--SELECT '35', 'IsDefaultBilling', IsDefaultBilling, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--FROM #InsertCustomerAddress IC WHERE IsDefaultBilling = 0 AND IsDefaultShipping = 0 


		--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--	   SELECT '35', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--	   FROM @InsertCustomer AS ii
		--	   WHERE ii.UserName not like '%_@_%_.__%' 
		 
		--Note : Content page import is not required 
		
		-- End Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  

		--IF ( @IsAccountAddress = 1 )
		--BEGIN
		--	INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--		SELECT '53', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--		FROM #InsertCustomerAddress AS ii
		--		WHERE isnull(ii.UserName,'') <> ''
		--END


		UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ UserName - ' + ISNULL(UserName,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN #InsertCustomerAddress IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		DELETE FROM #InsertCustomerAddress
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber IS NOT NULL 
			--AND GUID = @NewGUID
		);

		-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM #InsertCustomerAddress
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount ,
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		----------update ZnodeAddress
		DECLARE @AddressColumnString VARCHAR(1000), @WhereConditionString VARCHAR(1000), @UpdateColumnString VARCHAR(1000)

		SELECT @AddressColumnString = COALESCE(@AddressColumnString + ',', '')+a.ColumnName --COALESCE(@CsvColumnString + ' and ', '') +'ZA.'+ColumnName+' =  IC.'+ColumnName
		FROM ZnodeImportUpdatableColumns a
		INNER JOIN INFORMATION_SCHEMA.COLUMNS b on a.ColumnName = b.COLUMN_NAME  
		INNER JOIN dbo.Split(@CsvColumnString,',')C on b.COLUMN_NAME = c.Item
		WHERE b.TABLE_NAME = 'ZnodeAddress' 
		AND EXISTS(SELECT * FROM ZnodeImportHead IH where a.ImportHeadId = IH.ImportHeadId and IH.Name = 'ShippingAddress')

		SELECT @UpdateColumnString = COALESCE(@UpdateColumnString + ' , ', '') +'ZA.'+a.COLUMN_NAME+' =  IC.'+a.COLUMN_NAME  
		FROM INFORMATION_SCHEMA.COLUMNS a
		INNER JOIN dbo.Split(@CsvColumnString,',')b on a.COLUMN_NAME = b.Item
		WHERE NOT EXISTS (SELECT * FROM dbo.Split(@AddressColumnString,',') c WHERE a.COLUMN_NAME = c.Item )
		AND a.TABLE_NAME = 'ZnodeAddress'

		SELECT @WhereConditionString = COALESCE(@WhereConditionString + ' AND ', '') +'ZA.'+item+' =  IC.'+item from dbo.split(@AddressColumnString,',')
				
			
		-- Insert Product Data 
				
				CREATE TABLE #InsertedUserAddress (AddressId  nvarchar(256), UserId nvarchar(max)) 
		-- Pending for discussion include one identity column for modify address
				
				--UPDATE ANU SET 
				--ANU.PhoneNumber	= IC.PhoneNumber
				--from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				--INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				--INNER JOIN @InsertCustomerAddress IC ON ANZU.UserName = IC.UserName 
				--INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
				--INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
				 
				--where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)			

			    IF ( @IsAccountAddress = 1 )
				BEGIN

					UPDATE ZnodeAddress SET IsDefaultBilling = 0,  IsDefaultShipping = 0
					from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
					INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
					INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
					INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
					INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
															AND ZA.IsDefaultShipping =IC.IsDefaultShipping

					SET @SSQL = '
						UPDATE ZA set ModifiedBy = '+CONVERT(VARCHAR(10), @UserId)+', ModifiedDate = getdate() '+CASE WHEN ISNULL(@UpdateColumnString,'') = '' THEN '' ELSE ','+@UpdateColumnString END+' 
						FROM ZnodeAddress ZA
						INNER JOIN #InsertCustomerAddress IC ON '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END

					EXEC (@SSQL)

					SET @SSQL = '
					Insert into ZnodeAddress (FirstName,LastName,DisplayName,Address1,Address2,Address3,CountryName,
											StateName,CityName,PostalCode,PhoneNumber,
											IsDefaultBilling,IsDefaultShipping,IsActive,ExternalId,CompanyName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
					OUTPUT INSERTED.AddressId INTO  #InsertedUserAddress (AddressId) 			 
					SELECT IC.FirstName,IC.LastName,IC.DisplayName,IC.Address1,IC.Address2,null,IC.CountryName,
					IC.StateName,IC.CityName,IC.PostalCode,IC.PhoneNumber,
					isnull(IC.IsDefaultBilling,0),isnull(IC.IsDefaultShipping,0),isnull(IC.IsActive,0),IC.ExternalId,IC.CompanyName, '+CONVERT(VARCHAR(10), @UserId)+' , getdate() , '+CONVERT(VARCHAR(10), @UserId)+' ,getdate()
					FROM  #InsertCustomerAddress IC
					WHERE NOT EXISTS(SELECT * FROM ZnodeAddress ZA WHERE '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END +')'

					EXEC (@SSQL)

					DECLARE @AccountId INT
					SELECT @AccountId = AccountId FROM ZnodeUser where UserId = @UserId
					INSERT INTO ZnodeAccountAddress ( AccountId, AddressId, CreatedBy, CreatedDate,	ModifiedBy,	ModifiedDate )
					SELECT @AccountId, Addressid ,  @UserId , @GetDate, @UserId , @GetDate FROM #InsertedUserAddress UA
					WHERE NOT EXISTS ( SELECT * FROM ZnodeAccountAddress AA WHERE AccountId = @AccountId and AA.Addressid = UA.Addressid )
				END
				ELSE
				BEGIN
					
					UPDATE ZnodeAddress SET IsDefaultBilling = 0,  IsDefaultShipping = 0
					from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
					INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
					INNER JOIN ZnodeUserAddress ZUA ON ZUA.UserId = ZU.UserId
					INNER JOIN ZnodeAddress ZA ON ZUA.AddressId = ZA.AddressId
					INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName AND ZA.IsDefaultBilling =IC.IsDefaultBilling 
															AND ZA.IsDefaultShipping =IC.IsDefaultShipping
					
					SET @SSQL = '
						UPDATE ZA set ModifiedBy = '+CONVERT(VARCHAR(10), @UserId)+', ModifiedDate = getdate() '+CASE WHEN ISNULL(@UpdateColumnString,'') = '' THEN '' ELSE ','+@UpdateColumnString END+' 
						FROM ZnodeAddress ZA
						INNER JOIN #InsertCustomerAddress IC ON '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END
						print @SSQL
					EXEC (@SSQL)

					SET @SSQL = '
					Insert into ZnodeAddress (FirstName,LastName,DisplayName,Address1,Address2,Address3,CountryName,
												StateName,CityName,PostalCode,PhoneNumber,
												IsDefaultBilling,IsDefaultShipping,IsActive,ExternalId,CompanyName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
					OUTPUT INSERTED.AddressId, INSERTED.Address3 INTO  #InsertedUserAddress (AddressId, UserId ) 			 
					SELECT IC.FirstName,IC.LastName,IC.DisplayName,IC.Address1,IC.Address2,convert(nvarchar(100),ZU.UserId),IC.CountryName,
					IC.StateName,IC.CityName,IC.PostalCode,IC.PhoneNumber,
					isnull(IC.IsDefaultBilling,0),isnull(IC.IsDefaultShipping,0),isnull(IC.IsActive,0),IC.ExternalId,IC.CompanyName, '+CONVERT(VARCHAR(10), @UserId)+' , getdate() , '+CONVERT(VARCHAR(10), @UserId)+' ,getdate()
					FROM AspNetZnodeUser ANZU 
					INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
					INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
					INNER JOIN #InsertCustomerAddress IC ON ANZU.UserName = IC.UserName 
					WHERE NOT EXISTS(SELECT * FROM ZnodeAddress ZA WHERE '+CASE WHEN ISNULL(@WhereConditionString,'') = '' THEN ' 1 = 0 ' ELSE @WhereConditionString END +')'
					--print @SSQL
					EXEC (@SSQL)

					INSERT INTO ZnodeUserAddress(UserId,AddressId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
					SELECT CAST( UserId AS INT ) , Addressid , @UserId , @GetDate, @UserId , @GetDate FROM  #InsertedUserAddress
				END
	
				UPDATE ZA SET ZA.Address3 = null 
				From ZnodeAddress ZA INNER JOIN #InsertedUserAddress IUA ON ZA.AddressId = IUA.AddressId 

		-- 'End'
		--      SET @Status = 1;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH

		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		ROLLBACK TRAN A;
	END CATCH;
END;