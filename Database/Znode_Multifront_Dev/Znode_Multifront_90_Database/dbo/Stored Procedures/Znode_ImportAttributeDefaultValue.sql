﻿
CREATE PROCEDURE [dbo].[Znode_ImportAttributeDefaultValue](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Attribute Code Name and their default input validation rule other 
	--			  flag will be inserted as default we need to modify front end
	
	-- Unit Testing: 

	--------------------------------------------------------------------------------------
BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);
		DECLARE @GetDate datetime= dbo.Fn_GetDate(), @LocaleId int  ;
		SELECT @LocaleId = DBO.Fn_GetDefaultLocaleId();
		-- Retrive RoundOff Value from global setting 
		DECLARE @InsertPimAtrribute TABLE
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY,
			-- RowNumber int, AttributeName varchar(300), AttributeCode varchar(300), AttributeType varchar(300), DisplayOrder int, GUID nvarchar(400)
			RowNumber int, AttributeCode varchar(300),AttributeDefaultValueCode varchar(300),AttributeDefaultValue varchar(1000),IsEditable varchar(10),DisplayOrder int, IsDefault varchar(10), SwatchText varchar(1000),SwatchImage varchar(500), SwatchImagePath varchar(500), GUID nvarchar(400)
		
		);
		DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,PimAttributeDefaultValueId int,AttributeDefaultValueCode nvarchar(300))
		
		SET @SSQL = 'Select RowNumber,AttributeCode,AttributeDefaultValueCode,AttributeDefaultValue,IsEditable,DisplayOrder,IsDefault,SwatchText,SwatchImage, SwatchImagePath ,GUID FROM '+@TableName;
		INSERT INTO @InsertPimAtrribute( RowNumber,AttributeCode,AttributeDefaultValueCode,AttributeDefaultValue,IsEditable,DisplayOrder,IsDefault,SwatchText,SwatchImage, SwatchImagePath ,GUID)
		EXEC sys.sp_sqlexec @SSQL;

		--@MessageDisplay will use to display validate message for input inventory value  
		DECLARE @AttributeCode TABLE
		( 
		   AttributeCode nvarchar(300)
		);
		INSERT INTO @AttributeCode
			   SELECT AttributeCode
			   FROM ZnodePimAttribute 

		-- Start Functional Validation 
		-----------------------------------------------
		--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--	   SELECT '10', 'AttributeCode', AttributeCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
		--	   FROM @InsertPimAtrribute AS ii
		--	   WHERE ii.AttributeCode in 
		--	   (
		--		   SELECT AttributeCode FROM @AttributeCode  where AttributeCode is not null 
		--	   );
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '9', 'AttributeCode', AttributeCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ii.AttributeCode not in 
			   (
				   SELECT AttributeCode FROM @AttributeCode
			   );
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '53', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ii.AttributeDefaultValueCode in 
			   (
				   select AttributeDefaultValueCode FROM @InsertPimAtrribute  Group BY AttributeCode, AttributeDefaultValueCode  having count(*) > 1 
			   );

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '50', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ltrim(rtrim(isnull(ii.AttributeDefaultValueCode,''))) like '%[^0-9A-Za-z]%'

		--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--	   SELECT '50', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
		--	   FROM @InsertPimAtrribute AS ii
		--	   WHERE Isnumeric(ltrim(rtrim(isnull(ii.AttributeDefaultValueCode,'')))) =1

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '35', 'AttributeDefaultValueCode', AttributeDefaultValueCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertPimAtrribute AS ii
			   WHERE ltrim(rtrim(isnull(ii.AttributeDefaultValueCode,''))) like '% %' -----space not allowed

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '35', 'IsEditable', IsEditable, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertPimAtrribute AS ii  
			WHERE ii.IsEditable not in ('True','1','Yes','FALSE','0','No','')

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '35', 'IsDefault', IsDefault, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertPimAtrribute AS ii  
			WHERE ii.IsDefault not in ('True','1','Yes','FALSE','0','No','')

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '17', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
			FROM @InsertPimAtrribute AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL ) AND  ii.DisplayOrder = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '64', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
			FROM @InsertPimAtrribute AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL )AND  ii.DisplayOrder > 999

		UPDATE ZIL
			SET ZIL.ColumnName =   ZIL.ColumnName + ' [ AttributeDefaultValueCode - ' + ISNULL(AttributeDefaultValueCode,'') + ' ] '
			FROM ZnodeImportLog ZIL 
			INNER JOIN @InsertPimAtrribute IPA ON (ZIL.RowNumber = IPA.RowNumber)
			WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		-- End Function Validation 	
		-----------------------------------------------
		-- Delete Invalid Data after functional validatin  
		DELETE FROM @InsertPimAtrribute
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
		);
		
		-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertPimAtrribute
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount
		,	TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0)) 
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		DECLARE @MediaId INT
		SET @MediaId = (SELECT TOP 1 MediaId from @InsertPimAtrribute IPA INNER JOIN ZnodeMedia ZM ON IPA.SwatchImage = ZM.FileName and IPA.SwatchImagePath = ZM.Path)

		if (isnull(@MediaId,0)=0)
			SET @MediaId = (SELECT max(MediaId) from @InsertPimAtrribute IPA INNER JOIN ZnodeMedia ZM ON IPA.SwatchImage = ZM.FileName)

        update ZPADV set ZPADV.IsEditable = case when IPA.IsEditable in ('True','1','Yes') then 1 else 0 end,ZPADV.DisplayOrder = Case when Isnull(IPA.DisplayOrder,0) <> 0 then  IPA.DisplayOrder else ZPADV.DisplayOrder end ,
		                 ZPADV.IsDefault = case when IPA.IsDefault in ('True','1','Yes') then 1 else 0 end ,ZPADV.SwatchText = IPA.SwatchText ,
		                 ZPADV.MediaId = case when isnull(@MediaId,0)= 0 then ZPADV.MediaId else @MediaId end, ZPADV.ModifiedBy = @UserId, ZPADV.ModifiedDate = @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode 
		inner join ZnodePimAttributeDefaultValue ZPADV on ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode

		update ZPADVL set ZPADVL.AttributeDefaultValue = IPA.AttributeDefaultValue, ZPADVL.ModifiedBy = @UserId, ZPADVL.ModifiedDate = @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode 
		inner join ZnodePimAttributeDefaultValue ZPADV on ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode
		inner join ZnodePimAttributeDefaultValueLocale ZPADVL ON ( ZPADVL.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId)

		--- Insert data into base table ZnodePimatrribute with their validation 

		INSERT INTO ZnodePimAttributeDefaultValue (PimAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,IsDefault,SwatchText,MediaId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
		OUTPUT Inserted.PimAttributeId,Inserted.PimAttributeDefaultValueId,Inserted.AttributeDefaultValueCode INTO @InsertedPimAttributeIds  		
		SELECT ZPA.PimAttributeId,IPA.AttributeDefaultValueCode, case when IPA.IsEditable in ('True','1','Yes') then 1 else 0 end , Case when Isnull(IPA.DisplayOrder,0) = 0 then  999 else IPA.DisplayOrder end  , 
		       case when IPA.IsDefault in ('True','1','Yes') then 1 else 0 end , IPA.SwatchText, @MediaId,@UserId , @GetDate ,@UserId , @GetDate 
		from @InsertPimAtrribute IPA 
		INNER JOIN ZnodePimAttribute ZPA ON IPA.AttributeCode = ZPA.AttributeCode  
		where not exists(select * from ZnodePimAttributeDefaultValue ZPADV where ZPA.PimAttributeId = ZPADV.PimAttributeId and IPA.AttributeDefaultValueCode = ZPADV.AttributeDefaultValueCode)
		
		INSERT INTO ZnodePimAttributeDefaultValueLocale(LocaleId,PimAttributeDefaultValueId,AttributeDefaultValue,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select @LocaleId ,IPAS.PimAttributeDefaultValueId, IPA.AttributeDefaultValue, '', @UserId , @GetDate ,@UserId , @GetDate   
		FROM @InsertedPimAttributeIds IPAS 
		INNER JOIN ZnodePimAttribute ZPA ON IPAS.PimAttributeId = ZPA.PimAttributeId  
		INNER JOIN @InsertPimAtrribute IPA ON ZPA.AttributeCode= IPA.AttributeCode and IPAS.AttributeDefaultValueCode = IPA.AttributeDefaultValueCode


		UPDATE ZnodeImportProcessLog
		  SET STATUS = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;
		COMMIT TRAN A;
	END TRY
	BEGIN CATCH

		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		ROLLBACK TRAN A;
	END CATCH;
END;