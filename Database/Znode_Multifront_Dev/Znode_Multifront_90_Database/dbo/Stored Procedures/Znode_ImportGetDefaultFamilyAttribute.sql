﻿CREATE  PROCEDURE [dbo].[Znode_ImportGetDefaultFamilyAttribute]
(@ImportHeadId  INT =  0 ,
 @PimAttributeFamilyId  INT = 0 )

AS 
 /*Summary : - this procedure is ued to get the default family attribute used in import/emport ( to download sample format for import) 
  Unit Testing 
  EXEC Znode_ImportGetDefaultFamilyAttribute 1,1
 */
 BEGIN 
 BEGIN TRY
 SET NOCOUNT ON
  
    DECLARE @ImportHead NVARCHAR(100) = (SELECT TOP 1 Name FROM ZnodeImportHead WHERE ImportHeadId = @ImportHeadId  )
    DECLARE @DefaultFamilyId INT 
	DECLARE @Tlb_AttributeCode TABLE(AttributeCode varchar(300), GroupDisplayOrder int,DisplayOrder int)
	Declare @Tlb_OtherTemplateData Table  (AttributeCode nvarchar(300), DisplayOrder int ) 

	IF @ImportHead in ( 'Product')
	BEGIN 
	  	SET @DefaultFamilyId = dbo.Fn_GetDefaultPimProductFamilyId();
		   Insert into @Tlb_AttributeCode (AttributeCode,GroupDisplayOrder,DisplayOrder)	
	       SELECT Distinct AttributeCode ,ZPFM.GroupDisplayOrder,ZPA.DisplayOrder 
 		   FROM ZnodePimAttribute ZPA 
		   INNER JOIN ZnodeAttributeType ZAT ON (ZAT.AttributeTypeId = ZPA.AttributeTypeId)
		   INNER JOIN ZnodePimFamilyGroupMapper ZPFM ON (ZPFM.PimAttributeId = ZPA.PimAttributeId )
		   INNER JOIN ZnodePimAttributeGroup ZPAG ON ZPFM.PimAttributeGroupId = ZPAG.PimAttributeGroupId
		   Where ZPA.IsCategory = 0  AND PimAttributeFamilyId = @PimAttributeFamilyId 
		
		   Insert into @Tlb_AttributeCode (AttributeCode,GroupDisplayOrder,DisplayOrder)	 
		   SELECT Distinct AttributeCode, ZPFM.GroupDisplayOrder,ZPA.DisplayOrder
		   FROM ZnodePimAttribute ZPA 
		   INNER JOIN ZnodeAttributeType ZAT ON (ZAT.AttributeTypeId = ZPA.AttributeTypeId)
		   INNER JOIN ZnodePimFamilyGroupMapper ZPFM ON (ZPFM.PimAttributeId = ZPA.PimAttributeId )
		   INNER JOIN ZnodePimAttributeGroup ZPAG ON ZPFM.PimAttributeGroupId = ZPAG.PimAttributeGroupId
		   Where ZPA.IsCategory = 0  AND PimAttributeFamilyId = @DefaultFamilyId
		   and AttributeCode not in (select AttributeCode from @Tlb_AttributeCode)
		
		SELECT AttributeCode TargetColumnName FROM  @Tlb_AttributeCode 
		ORDER BY 
		--ISNULL(GroupDisplayOrder,0) ,ISNULL(DisplayOrder,0)

		CASE when GroupDisplayOrder IS null then 1 else 0 end , GroupDisplayOrder ,
		CASE when DisplayOrder IS null then 1 else 0 end , DisplayOrder		
	END
	Else IF @ImportHead in ( 'Category')
	BEGIN
	SET @DefaultFamilyId = dbo.Fn_GetCategoryDefaultFamilyId(); 
	Insert into @Tlb_AttributeCode (AttributeCode,DisplayOrder,GroupDisplayOrder)	
		SELECT Distinct AttributeCode ,DisplayOrder ,ZPA.DisplayOrder
 		   FROM ZnodePimAttribute ZPA 
		   INNER JOIN ZnodeAttributeType ZAT ON (ZAT.AttributeTypeId = ZPA.AttributeTypeId)
		   INNER JOIN ZnodePimFamilyGroupMapper ZPFM ON (ZPFM.PimAttributeId = ZPA.PimAttributeId )
		   Where ZPA.IsCategory = 1  AND PimAttributeFamilyId = @PimAttributeFamilyId
		
		Insert into @Tlb_AttributeCode (AttributeCode,DisplayOrder,GroupDisplayOrder) 
		SELECT Distinct AttributeCode ,DisplayOrder ,ZPA.DisplayOrder
 		   FROM ZnodePimAttribute ZPA 
		   INNER JOIN ZnodeAttributeType ZAT ON (ZAT.AttributeTypeId = ZPA.AttributeTypeId)
		   INNER JOIN ZnodePimFamilyGroupMapper ZPFM ON (ZPFM.PimAttributeId = ZPA.PimAttributeId )
		   Where ZPA.IsCategory = 1  AND PimAttributeFamilyId = @DefaultFamilyId
		   and AttributeCode not in (select AttributeCode from @Tlb_AttributeCode)

		SELECT AttributeCode TargetColumnName FROM @Tlb_AttributeCode 
		ORDER BY 
		CASE when GroupDisplayOrder IS null then 1 else 0 end , GroupDisplayOrder ,
		CASE when DisplayOrder IS null then 1 else 0 end , DisplayOrder		
	END 
	Else
	   Begin
			INSERT INTO @Tlb_OtherTemplateData (AttributeCode,DisplayOrder)
			Select Distinct ZiA.AttributeCode,ZiA.SequenceNumber from ZnodeImportAttributeValidation ZiA where ZiA.importHeadId =@ImportHeadId
			and ZiA.AttributeCode not in (Select AttributeCode from @Tlb_OtherTemplateData )
			
			IF @ImportHead in ( 'B2BCustomer') 
			BEGIN
				INSERT INTO @Tlb_OtherTemplateData (AttributeCode,DisplayOrder)
				SELECT ZGA.AttributeCode, ZGA.DisplayOrder
				FROM ZnodeGlobalGroupEntityMapper zggem 
				INNER JOIN ZnodeGlobalEntity ZGE ON zggem.GlobalEntityId = ZGE.GlobalEntityId
				INNER JOIN ZnodeGlobalAttributeGroupMapper ZGAGM ON  zggem.GlobalAttributeGroupId = ZGAGM.GlobalAttributeGroupId
				INNER JOIN ZnodeGlobalAttribute ZGA ON ZGAGM.GlobalAttributeId = ZGA.GlobalAttributeId  
				WHERE ZGE.EntityName = 'User' and ZGA.AttributeCode not in (Select AttributeCode from @Tlb_OtherTemplateData )
			END
			 
			Select  AttributeCode TargetColumnName   from @Tlb_OtherTemplateData order by DisplayOrder
	   End
	   END TRY

	   BEGIN CATCH
			DECLARE @Status BIT ;
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportGetDefaultFamilyAttribute @ImportHeadId = '+CAST(@ImportHeadId AS VARCHAR(max))+',@PimAttributeFamilyId='+CAST(@PimAttributeFamilyId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_ImportGetDefaultFamilyAttribute',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
	   END CATCH
 END