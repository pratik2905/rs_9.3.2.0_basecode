﻿using CyberSource.Clients;
using CyberSource.Clients.SoapServiceReference;
using System;
using System.Configuration;
using Znode.Multifront.PaymentApplication.Data;
using Znode.Multifront.PaymentApplication.Helpers;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Providers
{
    /// <summary>
    /// This class will have all the methods of implementation of Cybersource payment provider 
    /// </summary>
    public class CyberSourceCustomerProviderProvider : BaseProvider, IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card through Cyber source
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
                return (paymentModel.IsCapture)
                    ? CaptureTransaction(paymentModel) : AuthorizeTransaction(paymentModel);

            return CreateCustomer(paymentModel);
        }

        /// <summary>
        /// This method will call the refund payment of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the refund response</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                //Create Request object and map parameters
                RequestMessage request = new RequestMessage();
                request.merchantReferenceCode = paymentModel.CustomerPaymentProfileId;
                request.ccCreditService = new CCCreditService();
                request.ccCreditService.run = "true";
                request.ccCreditService.captureRequestID = paymentModel.CaptureTransactionId;
                request.ccCreditService.captureRequestToken = paymentModel.CardDataToken;
                request.orderRequestToken = paymentModel.CardDataToken;
                request.purchaseTotals = new PurchaseTotals();
                request.purchaseTotals.grandTotalAmount = paymentModel.Total;

                //Submit Refund request
                ReplyMessage reply = SoapClient.RunTransaction(request);

                //Map response
                paymentGatewayResponse.ResponseCode = reply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(paymentGatewayResponse.ResponseCode);
                paymentGatewayResponse.CustomerProfileId = reply.requestID;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.requestToken;

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.REFUNDED;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the void payment of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();

            try
            {
                //Create Request object and map parameters
                RequestMessage request = new RequestMessage();
                request.merchantReferenceCode = paymentModel.CustomerPaymentProfileId;
                request.voidService = new VoidService();
                request.voidService.run = "true";
                request.voidService.voidRequestID = paymentModel.TransactionId;
                request.voidService.voidRequestToken = paymentModel.CardDataToken;
                ReplyMessage reply = SoapClient.RunTransaction(request);
                paymentGatewayResponse.ResponseCode = reply.reasonCode;

                //Submit Refund request
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.reasonCode);

                //Map response
                paymentGatewayResponse.CustomerProfileId = reply.requestID;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.requestToken;
                paymentGatewayResponse.Token = reply.merchantReferenceCode;
                paymentGatewayResponse.TransactionId = reply.requestID;

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.VOIDED;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the Subscription method of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                //Create Recurring Subscription object and map parameters
                RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                recurringSubscriptionInfo.amount = paymentModel.Subscription.InitialAmount.ToString();
                recurringSubscriptionInfo.numberOfPayments = paymentModel.Subscription.TotalCycles.ToString();
                recurringSubscriptionInfo.numberOfPaymentsToAdd = paymentModel.Subscription.Frequency.ToString();
                recurringSubscriptionInfo.subscriptionID = paymentModel.CustomerProfileId;
                recurringSubscriptionInfo.startDate = DateTime.Now.ToString("yyyyMMdd");
                recurringSubscriptionInfo.frequency = "on-demand";

                switch (paymentModel.Subscription.Period)
                {
                    case "WEEK":
                        recurringSubscriptionInfo.frequency = "weekly";
                        recurringSubscriptionInfo.endDate = DateTime.Now.AddDays((Convert.ToInt32(paymentModel.Subscription.Frequency) * 7 * paymentModel.Subscription.TotalCycles)).ToString("yyyyMMdd");
                        break;
                    case "MONTH":
                        recurringSubscriptionInfo.frequency = "monthly";
                        recurringSubscriptionInfo.endDate = DateTime.Now.AddMonths(Convert.ToInt32(paymentModel.Subscription.Frequency) * paymentModel.Subscription.TotalCycles).ToString("yyyyMMdd");
                        break;
                    case "YEAR":
                        recurringSubscriptionInfo.frequency = "annually";
                        recurringSubscriptionInfo.endDate = DateTime.Now.AddYears(Convert.ToInt32(paymentModel.Subscription.Frequency) * paymentModel.Subscription.TotalCycles).ToString("yyyyMMdd");
                        break;
                }

                PurchaseTotals purchaseTotals = new PurchaseTotals();
                purchaseTotals.currency = string.IsNullOrEmpty(paymentModel.GatewayCurrencyCode) ? Convert.ToString(ConfigurationManager.AppSettings["CurrencyCode"]) : paymentModel.GatewayCurrencyCode;
                purchaseTotals.grandTotalAmount = paymentModel.Subscription.InitialAmount.ToString();

                // Create request object and map parameters
                RequestMessage request = new RequestMessage();
                request.card = MapToCardObject(paymentModel);
                request.purchaseTotals = purchaseTotals;
                request.recurringSubscriptionInfo = recurringSubscriptionInfo;
                request.merchantReferenceCode = Guid.NewGuid().ToString();

                //Submit RecurringSubscription request
                ReplyMessage reply = SoapClient.RunTransaction(request);

                paymentGatewayResponse.ResponseCode = reply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.reasonCode);

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.TransactionId = reply.requestID;
                    paymentGatewayResponse.CardAuthCode = reply.requestToken;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }

            return paymentGatewayResponse;
        }

        public TransactionDetailsModel GetTransactionDetails(PaymentModel paymentModel)
        {
            return new TransactionDetailsModel();
        }

        #region private methods
        /// <summary>
        /// Create payment transaction based on created customer token.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel AuthorizeTransaction(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();

            try
            {
                //Create Request object and map parameters
                RequestMessage request = new RequestMessage();
                request.billTo = MapToBillToObject(paymentModel);
                request.merchantReferenceCode = Guid.NewGuid().ToString();
                request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                request.purchaseTotals = new PurchaseTotals();
                request.recurringSubscriptionInfo.subscriptionID = paymentModel.CustomerProfileId;
                request.purchaseTotals.grandTotalAmount = paymentModel.Total;
                //To Do.
                request.purchaseTotals.currency = string.IsNullOrEmpty(paymentModel.GatewayCurrencyCode) ? Convert.ToString(ConfigurationManager.AppSettings["CurrencyCode"]) : paymentModel.GatewayCurrencyCode;
                ReplyMessage reply;

                //Authorize the Transaction 
                request.ccAuthService = new CCAuthService();
                request.ccAuthService.run = "true";
                reply = SoapClient.RunTransaction(request);

                //Map response
                paymentGatewayResponse.ResponseCode = reply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.reasonCode);
                paymentGatewayResponse.CustomerProfileId = reply.requestID;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.merchantReferenceCode;
                paymentGatewayResponse.Token = reply.requestToken;

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.TransactionId = reply.ccAuthReply.reconciliationID;
                    paymentGatewayResponse.IsGatewayPreAuthorize = paymentModel.GatewayPreAuthorize;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.AUTHORIZED;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }
            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will create customer profile 
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer details response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            GatewayConnector gatewayConnector = new GatewayConnector();
            bool isSuccess = false;

            if (!paymentModel.IsAnonymousUser)
            {
                if (string.IsNullOrEmpty(paymentModel.CustomerGUID))
                {
                    if (paymentModel.IsSaveCreditCard)
                    {
                        response = CreateCustomerPayment(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.CustomerPaymentProfileId;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        return response;
                    }
                    else
                        return CreateCustomerPayment(paymentModel);
                }
                else
                {
                    if (paymentModel.IsSaveCreditCard)
                    {

                        response = CreatePaymentGatewayVault(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        return response;
                    }
                    else if (!string.IsNullOrEmpty(paymentModel.PaymentToken))
                    {
                        isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.CustomerPaymentProfileId;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        return response;
                    }
                    else
                        return CreateCustomerPayment(paymentModel);
                }
            }
            else
                return CreateCustomerPayment(paymentModel);
        }

        /// <summary>
        /// To vault using existing customer id
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns gateway response</returns>
        private GatewayResponseModel CreatePaymentGatewayVault(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            PaymentMethodsService repository = new PaymentMethodsService();
            ZnodePaymentMethod payment = repository.GetPaymentMethod(paymentModel.PaymentApplicationSettingId, paymentModel.CustomerGUID);
            if (!Equals(payment, null) && !string.IsNullOrEmpty(payment.CustomerProfileId))
            {
                paymentModel.CustomerProfileId = payment.CustomerProfileId;
                response = CreateCustomerPayment(paymentModel);
            }
            else
                response = CreateCustomerPayment(paymentModel);

            return Equals(response, null) ? new GatewayResponseModel() : response;
        }

        /// <summary>
        /// Create the Customer Payment
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer payment response</returns>
        private GatewayResponseModel CreateCustomerPayment(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                //Create Request object and map parameters
                RequestMessage request = new RequestMessage();
                request.card = MapToCardObject(paymentModel);
                request.billTo = MapToBillToObject(paymentModel);
                request.merchantReferenceCode = Guid.NewGuid().ToString();
                request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                request.recurringSubscriptionInfo.frequency = "on-demand";
                request.purchaseTotals = new PurchaseTotals();
                request.purchaseTotals.currency = string.IsNullOrEmpty(paymentModel.GatewayCurrencyCode) ? Convert.ToString(ConfigurationManager.AppSettings["CurrencyCode"]) : paymentModel.GatewayCurrencyCode;
                request.paySubscriptionCreateService = new PaySubscriptionCreateService();
                request.paySubscriptionCreateService.run = "true";

                //Submit Subscription request
                ReplyMessage reply = SoapClient.RunTransaction(request);

                //Map resopnse
                paymentGatewayResponse.ResponseCode = reply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.reasonCode);
                paymentGatewayResponse.CustomerProfileId = reply.requestID;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.merchantReferenceCode;
                paymentGatewayResponse.Token = reply.paySubscriptionCreateReply.subscriptionID;
                paymentModel.CustomerProfileId = reply.requestID;
                paymentModel.CustomerPaymentProfileId = reply.merchantReferenceCode;

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.AUTHORIZED;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the capture method of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel CaptureTransaction(PaymentModel paymentModel)
        {
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                //Create Request object and map parameters
                RequestMessage request = new RequestMessage();
                request.ccCaptureService = new CCCaptureService();
                request.merchantReferenceCode = paymentModel.CustomerPaymentProfileId;
                request.ccCaptureService.run = "true";
                request.ccCaptureService.reconciliationID = paymentModel.TransactionId;
                request.ccCaptureService.authRequestToken = paymentModel.CardDataToken;
                request.purchaseTotals = new PurchaseTotals();
                request.purchaseTotals.grandTotalAmount = paymentModel.Total;

                //Submit Capture request
                ReplyMessage reply = SoapClient.RunTransaction(request);

                //Map resopnse
                paymentGatewayResponse.ResponseCode = reply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.reasonCode);
                paymentGatewayResponse.TransactionId = reply.requestID;
                paymentGatewayResponse.CardAuthCode = reply.requestToken;

                //100 is success response code
                if (reply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.CAPTURED;
                }
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(null, ex.Message);
            }
            return paymentGatewayResponse;
        }

        /// <summary>
        /// Get the reason code description
        /// </summary>
        /// <param name="reasonCode">Reason code</param>
        /// <returns>Returns the reason description</returns>
        private string GetReasoncodeDescription(string reasonCode)
        {
            switch (reasonCode)
            {
                case "100":
                    return "Successful transaction.";
                case "203":
                    return "General decline of the card.";
                case "204":
                    return "Insufficient funds in the account.";
                case "208":
                    return "Inactive card or card not authorized for card-not-present transactions.";
                case "210":
                    return "The card has reached the credit limit.";
                case "211":
                    return "Invalid card verification number.";
                case "232":
                    return "The card type is not accepted by the payment processor.";
                case "234":
                    return "There is a problem with your CyberSource merchant configuration.";
                case "235":
                    return "The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.";
                case "237":
                    return "The authorization has already been reversed.";
                case "238":
                    return "The authorization has already been captured.";
                case "239":
                    return "The requested transaction amount must match the previous transaction amount.";
                case "240":
                    return "The card type sent is invalid or does not correlate with the credit card number.";
                case "241":
                    return "The request ID is invalid.";
                case "243":
                    return "The transaction has already been settled or reversed.";
                case "247":
                    return "You requested a credit for a capture that was previously voided.";
                default:
                    break;
            }

            return "Unable to process, please contact customer support.";
        }

        /// <summary>
        /// This method will return the Credit Card Type number
        /// </summary>
        /// <param name="cardType"></param>
        /// <returns>returns the Credit Card Type number</returns>
        private string GetCardType(string cardType)
        {
            string ccType = "000";
            switch (cardType.ToLower())
            {
                case "amex":
                case "american express":
                    ccType = "003";//"Amex";
                    break;
                case "visa":
                    ccType = "001";//"Visa";
                    break;
                case "mastercard":
                case "master card":
                    ccType = "002";//"MasterCard";
                    break;
                case "discover":
                    ccType = "004";//"Discover";
                    break;
            }
            return ccType;
        }

        //Map payment model to BillTo object
        private BillTo MapToBillToObject(PaymentModel paymentModel)
            => new BillTo
            {
                firstName = paymentModel.BillingFirstName,
                lastName = paymentModel.BillingLastName,
                email = paymentModel.BillingEmailId,
                street1 = paymentModel.BillingStreetAddress1,
                street2 = paymentModel.BillingStreetAddress2,
                city = paymentModel.BillingCity,
                state = paymentModel.BillingStateCode,
                postalCode = paymentModel.BillingPostalCode,
                country = paymentModel.BillingCountryCode
            };

        //Map payment model to Card object
        private Card MapToCardObject(PaymentModel paymentModel)
            => new Card
            {
                accountNumber = paymentModel.CardNumber,
                expirationMonth = paymentModel.CardExpirationMonth,
                expirationYear = paymentModel.CardExpirationYear,
                cvNumber = paymentModel.CardSecurityCode,
                cardType = GetCardType(paymentModel.CardType)
            };
        #endregion
    }
}
